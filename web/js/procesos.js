$(function(){
	$("#accordion").accordion({header: "h4"});
	$('#dialog').dialog({
	autoOpen: false,
	width: 170,
	buttons: {
	"Ok": function() { 
            $(this).dialog("close");
	} 
	}
        });
        // Dialog Link
        $('#dialog_link').click(function(){
                $('#dialog').dialog('open');
                return false;
        });
        $('#dialog_link, ul#icons li').hover(
                function() {$(this).addClass('ui-state-hover');},
                function() {$(this).removeClass('ui-state-hover');}
        );
        /** Acciones de Menu Principal */
        /** Buscar de Personas */
        $("#link1-0").click(function(){
                var container = "<div></div>";
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "consultarPersona"});
                $('#contenido').html(container);
                return false;

        });
        
        /** Registro de Personas */
        $("#link1-1").click(function(){
                var container = "<div></div>";
                container = $(container).load('../apps/sac/modules/persona/templates/formNew.php');
                $('#contenido').html(container);
                return false;

        });
        /** Editar personas y primero muestra el procedo de buscar persona */
        $("#link1-2").click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "buscarPersona"});
                return false;

        });
        /** Agregar Grupo Familiar y primero muestra el proceso de buscar persona */
        $("#link1-3").click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "buscarPersonaAddGrupFamiliar"});
                return false;
        });

        /** Editar Grupo Familiar y primero muestra el proceso de buscar persona */
        $("#link1-4").click(function(){
                 $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "buscarPersonaEditGrupFamiliar"});
                return false;
        });

        $("#link1-5").click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "buscarPersonaAddDiagnostico"});
                return false;
        });
         // Procesar el reporte de expediente
        $("#link1-6").click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "expedienteDiagnostico"});
                return false;
        });

     /* Gestion Padrino */
        $('#link2-1').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/padrino/templates/formNew.php');
                return false;
        });
        $('#link2-2').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/padrino/templates/searchPadrino.php');
                return false;
        });

        $('#link2-3').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/padrino/templates/searchPadrinoPersona.php',{divId: "searchPadPac"});
                return false;
        });
        $('#link2-4').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/padrino/templates/searchPadrinoPersonaDes.php');
                return false;
        });
        /* Registro Horarios */
        $('#link3-1').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/horario/templates/formNew.php', {divId: "registroHorario"});
                return false;
        });

        $('#link3-2').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "buscarPersonaHorario"});
                return false;
        });
        
        // Procesar el reporte de horario
        $("#link3-3").click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "expedienteHorario"});
                return false;
        });

        /* Gestionar las Ayudas */
        /** consultar Ayudas entregadas */
        $('#link4-0').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "searchAyuda"});
                return false;
        });
        /** Registrar Ayudas */
        $('#link4-1').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "registrarAyuda"});
                return false;
        });
        /** Registar ayuda especiales */
        $('#link4-2').click(function(){ 
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/ayuda/templates/ayudaEspecial.php');
                return false;
        });
        /** Registar de Tipo de ayuda especiales */
        $('#link4-3').click(function(){ 
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/ayuda/templates/formNewTipoAyuda');
             
        });
        /** Gesionar usuarios */
        /** Registro de usuario */
        $('#link5-1').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/usuario/templates/formNew.php');
        });
        /** Listar los usuario */
        $('#link5-2').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/usuario/templates/listado.php');
        });
        
          $('#link6-1').click(function(){
                $('#contenido').html('');
                $('#contenido').load('../apps/sac/modules/reportes/templates/searchFrecuenciaCeguera.php');
        });
        
        /** Cargar el formulario de autenticación de usuario del modulo de autenticacion en el template correspondiente a esa app */
        $('#contenidoAutenticar').load('../apps/autenticacion/modules/autenticacion/templates/formAutenticar.php');
        
        
        /** Procedimiento para renovar la contraseña de usuario */
    setTimeout(function() {
         /** procedimiento para efectuar la autenticacion de usuario */
         
        $("a#linkRenovar").click(function(){
                 $('form.#renovacionForm')[0].reset();
                 $('#resulRen').html('');
                 $('#renovacion').dialog({
                        autoOpen: false,

			buttons: {"Ok":  function(){
                           $('#grafica').remove();
                           $(this).dialog('close');
                      }},
                  modal: true,
                  title: 'Ficha de renovaci&oacute;n de clave ',
                  width: 400,
                  autoOpen: true,
		  height: 300
                });
		return false;
	});
    }, 1000 );
    
     $('form.#renovacionForm').submit(function(){
     // Capturo los valores de fecha inicio y fin
     var ced = $("#cedula").val();
     var fech  = $("#fecha_nac").val();
     var cor = $("#correo").val();
     var act = $("#action").val();

     if(ced=='' || fech=='' || cor=='' || act=='') return $("#resulRen").html('<br><b>Todos los campos som obligatorio.</b>');
            $.ajax({
                type: "POST",
                url:'../apps/autenticacion/modules/autenticacion/actions/actions.class.php',
                data: ({cedula: ced, fecha:fech, correo:cor, action:act}),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n');
                },
                 dataType: "json",
                success: function(data){
                    /* Recargar una pagina
                    location.reload();*/
                    if(data.error==1){
                        $("#resulRen").html(data.mensaje);
                    }else{
                        $("#resulRen").html(data.mensaje + 'Login: '+data.login+'<br/>Clave: '+data.clave);
                    }
                }
            });
     /*$('#resulRen').append(cedula + correo + fecha);*/
     return false;
  });

});

$(document).ready(function(){
    
    
    $('#time').jTime();
     /** Codigo de validacion de session de usuario */
    $("#ingresar").submit(function(){
        var use = $("#usuario").val();
        var cla = $("#clave").val();
        if(use=='' || cla==''){
            $("#mensaje").slideDown("slow");
            document.getElementById("mensaje").innerHTML="";
            $("#mensaje").append("<img src='images/error.png' title='Error' width='15px'/>&nbsp;Todos los campos son requeridos");
            return false
        }else{
        $("#mensaje").hide();
            document.formulario.submit();
        }
    });
});
function cambiarIdSerarch(data){
        setTimeout(function() {
            $('form').attr({id:data});
        }, 1000 );
}

(function($){
  $.fn.jTime = function(o) {
    var d = {x:'time-capa',ma:new Date(),i:0};
    var o = $.extend(d, o);
    o.ma = new Date(o.ma);
    
    var mHF = function (){
      var ma = new Date(o.ma.getTime() + o.i * 1000);
      h = ma.getHours();
      m = ma.getMinutes();
      s = ma.getSeconds(); 
      if (h<=9) h = '0'+h;
      if (m<=9) m = '0'+m;
      if (s<=9) s = '0'+s;
      hi = h + ":" + m + ":" + s;
      $('#'+o.x).html(hi); 
      o.i += 1;   
    }
    return this.each(function(){
      o.x = $(this).attr('id');
      setInterval(mHF,1000);     
    });
};
})(jQuery);

