/** Modulo de Validaciones paracampos de formulario */
$(document).ready(function(){
    $(".telfMov").attr({pattern:"(+[0]{1})(+[4]{1})[0-9]{8}}"});
    $(".telfNac").attr({pattern:"(+[0]{1})(+[2]{1})[0-9]{8}}"});
    $(".number").attr({pattern:"[0-9]"});

    $(".string").attr({pattern:"[a-zA-Z 0-9]+"});
    $(".user").attr({pattern:"^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$"});
    $(".clave").attr({pattern:"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,10})$"});

});
