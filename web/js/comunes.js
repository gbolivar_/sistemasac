$(function(){
       /** Pestaña de los formularios */
    	$("#tabs" ).tabs();
	$('#dialog').dialog({
            autoOpen: false,
            width: 170,
            buttons: {
            "Ok": function() {
                $(this).dialog("close");
            }
            }
	});	
       

    $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            showWeek: true,
            firstDay: 1,
            numberOfMonths: 1,
            showButtonPanel: true,
            yearRange:"c-60:c+1",
            /*dateFormat: 'yy-mm-dd',*/
            dateFormat: 'dd-mm-yy',
            closeText: 'Cerrar',
            prevText: '&#x3c;Ant',
            nextText: 'Sig&#x3e;',
            currentText: 'Hoy',
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
                         'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
                            'Jul','Ago','Sep','Oct','Nov','Dic'],
            dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
            dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
            weekHeader: 'Sm'
        });
        
    /**
     *Codigo para agregar los estilo a los botones de submit y reset
     */
    $('input[type="submit"]').attr({class:"ui-state-default ui-corner-all"});
    $('input[type="button"]').attr({class:"ui-state-default ui-corner-all"});
    $('input[type="reset"]').attr({class:"ui-state-default ui-corner-all"});
    
});


function mostrarMensaje(error,mensaje){
    if(error==1){
         /** Agregar unos estylo para identificar */
        $("#menInfo").attr({class:"ui-state-error ui-corner-all", style:"padding: 0 .7em;"});
        
        /** Agregar la iconografia correspondiente */
        var err = '<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Alerta:</strong>';
       
       /** Cargar mensaje que viene de json a la vista */
        $("#menInfo").html(err + ' ' + mensaje);
    }else if(error==0){
         /** Agregar unos estylo para identificar */
        $("#menInfo").attr({class:"ui-state-highlight ui-corner-all", style:" padding: 0 .7em;"});
        
        /** Agregar la iconografia correspondiente */
        var ok = '<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><strong>Bien:</strong>';
        
        /** Cargar mensaje que viene de json a la vista */
        $("#menInfo").html(ok + ' ' + mensaje);
    }else{
         /** Agregar unos estylo para identificar */
         $("#menInfo").attr({class:"", style:""});
         $("#menInfo").html('&nbsp;');
    }
    /** Mostrar mensaje en la vista */
    $("#menInfo").show(12000);
    
    /** Procedimiento para ocultar los mensajes de alertar */
    setTimeout(function() {
        $("#menInfo").removeAttr("class").fadeOut();
    }, 10000 );
}
function mostrarMensaje2(error,mensaje){
    if(error==1){
         /** Agregar unos estylo para identificar */
        $("#menInfo2").attr({class:"ui-state-error ui-corner-all", style:"padding: 0 .7em;"});
        
        /** Agregar la iconografia correspondiente */
        var err = '<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Alerta:</strong>';
       
       /** Cargar mensaje que viene de json a la vista */
        $("#menInfo2").html(err + ' ' + mensaje);
    }else if(error==0){
         /** Agregar unos estylo para identificar */
        $("#menInfo2").attr({class:"ui-state-highlight ui-corner-all", style:" padding: 0 .7em;"});
        
        /** Agregar la iconografia correspondiente */
        var ok = '<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><strong>Bien:</strong>';
        
        /** Cargar mensaje que viene de json a la vista */
        $("#menInfo2").html(ok + ' ' + mensaje);
    }else{
         /** Agregar unos estylo para identificar */
         $("#menInfo2").attr({class:"", style:""});
         $("#menInfo2").html('&nbsp;');
    }
    /** Mostrar mensaje en la vista */
    $("#menInfo2").show(12000);
    
    /** Procedimiento para ocultar los mensajes de alertar */
    setTimeout(function() {
        $("#menInfo2").removeAttr("class").fadeOut();
    }, 10000 );
}


function mostrarBottonReporte(mensaje){
    /** Cargar mensaje que viene de json a la vista */
    $("#menInfo").html(mensaje);
    /** Mostrar mensaje en la vista */
    $("#menInfo").show(900);
    $("#menInfo").addClass("ui-state-default ui-corner-all");
    /** Controlar que el mensaje de oculte en 6000 milesima de segundo */
    setTimeout(function() {
    $( "#menInfo:visible" ).removeAttr( "style" ).fadeOut();
    }, 120000 );
}


/** FUncionan encargada de llenar los selectores necesario para poner en funcionamiento
 * Recibe por parametro Tabla | identificador de la tabla | nombre del campo */
function selectores(divId,v1,v2,v3){
    var t =  "'"+ v1 +"'";
    var c1 = "'"+ v2 +"'";
    var c2 = "'"+ v3 +"'";
    $.ajax({
        type: "POST",
        url:'../apps/sac/modules/comunes/actions/actions.class.php' ,
        data: {action:'comun', bat: t, c1:c1, c2:c2},
        beforeSend: function(Obj){
        },
        error:function(Obj,err,obj){
            exceptionLog('Error(EV01), Error en selectores', 'comunes.php','selectores','111');
            alert('Error(EV01)');
        },
        dataType: "json",
        success: function(data){
            for(var i=0; i < data.length; i++){
                /** Procesamos una class que sera usado en una tabla dinamica delimitado por un caracter especial - */
                var new_text = divId.split('-');
                /** Contamos para  verificar si es un arreglo */
                var arra=new_text.length;
                /** si solo en un valor que estamos  hablando de un id procesa este punto de lo contrario hablamos de una class */
                if(arra==1){
                     var id = document.getElementById(divId);
                }else{
                     var id = '.'+divId;
                }
                 $(id).append('<option value="' + data[i].id + '">'+data[i].data+'</option>');
            }
        }
    });
}

/** Funcion que permite mostrar la ventana de confirmación para eliminar logicamente el registro */
function confElim(val, tabNam, campWher,divHide)
     {
            var mens = "<div id='confMens'><span class='ui-icon ui-icon-alert' style='float: left; margin: 1pt 7px 20px 0pt;'></span>¿Esta seguro de eliminar el registro?</div>";
            $(mens).dialog({
                height: 150,
                buttons: {
                            "Cancelar": function(){
                                $(this).dialog('destroy');
                        },
                            "Aceptar": function(){
                                 var div = document.getElementById(divHide);
                                 $(div).hide(900);
                                eliminarDatos(val, tabNam, campWher);                              
                                $(this).dialog('destroy');
                        }},
            modal: true,
            title: '!Confirmacion'
            });
    }
  /* Funcion que recibe tres parametros valor, nombre de la tabla y campo al cual sera efectuado el where */
 function eliminarDatos(val, tabNam, campWher)
 {
          if(val=='') return;
            $.ajax({
               type:"POST",
               url:'../apps/sac/modules/comunes/actions/actions.class.php',
                data: ({action:'deleted',valor: val, tabNam:tabNam, campWher:campWher}),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en eliminarDatos', 'comunes.php','eliminarDatos','163');
                    alert('Error(EV01)');
                },
                dataType: 'json',
                success: function(data){
                    /* Recargar una pagina */
                    mostrarMensaje(data.error,data.mensaje);//location.reload();
                }
            });
  }
  
function numeric(e) {
    tecla = (document.all)?e.keyCode:e.which;
    if (tecla==8) return true;
    patron = /[0-9 ]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

  function validos(e) {
        tecla = (document.all)?e.keyCode:e.which;
        if (tecla==8) return true;
        patron = /[aA-zZ]/;
        te = String.fromCharCode(tecla);
        return patron.test(te);
   }
   
   /** Function que permite validar que los campos sean campos mayuscula */
function mayuscula(e)
{
    return  e.value = e.value.toUpperCase();
}
/** Function que permite validar que los campos sean campos minuscula */
function minuscula(e)
{
    return  e.value = e.value.toLowerCase();
}
function soloText(e) {
    tecla = (document.all)?e.keyCode:e.which;
    if (tecla==8) return true;
    patron = /[aA-zZ {1}]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function validar_email(valor)
{
    // creamos nuestra regla con expresiones regulares.
    var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    // utilizamos test para comprobar si el parametro valor cumple la regla
    if(filter.test(valor))
            return true;
    else
            return false;
}
//
//
//function caracteresNombrePermitidos(evento, objTexto){
//
//  var checkOK = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
//  var checkStr = objTexto.value;
//  var allValid = true;
//  var decPoints = 0;
//  var allNum = "";
//
//  var valorValido;
//
//  for (i = 0; i < checkStr.length; i++) {
//    ch = checkStr.charAt(i);
//    for (j = 0; j < checkOK.length; j++)
//      if (ch == checkOK.charAt(j))
//        break;
//      if (j == checkOK.length) {
//         allValid = false;
//         break;
//      }
//      allNum += ch;
//    }
//  if (!allValid) {
//       objTexto.value = checkStr.substring(0, objTexto.value.length-1);
//       objTexto.focus();
//       return (false);
//  }
//  else{
//       objTexto.value = objTexto.value.toUpperCase();
//  }
//}

function nombre_horario(divId, mostrar){
    $.ajax({
        type: "POST",
        url:'../apps/sac/modules/horario/actions/actions.class.php' ,
        data: {action:'consulta_horarios', mostrar: mostrar},
        beforeSend: function(Obj){
        },
        error:function(Obj,err,obj){
            exceptionLog('Error(EV01), Error en nombre_horario', 'comunes.php','nombre_horario','220');
            alert('Error(EV01)');
        },
        dataType: "json",
        success: function(data){
            for(var i=0; i < data.length; i++){
                var id = document.getElementById(divId);
                $(id).append('<option value="' + data[i].id + '">'+data[i].data+'</option>');
            }
        }
    });
}

 function exceptionLog(msg,c,m,l)
 {
            $.ajax({
               type:"POST",
               url:'../apps/sac/modules/comunes/actions/actions.class.php',
                data: ({action:'log', clas:c, funct:m, line:l, mensj:msg+'; navegador:'+detectarNavegador()+''}),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert("Error(EV01)");
                },
                dataType: 'json',
                success: function(data){
                    /* Recargar una pagina */
                    //mostrarMensaje(data.error,data.mensaje);//location.reload();
                    return false;
                }
            });
  }
  function detectarNavegador(){
      var navegador='';
    jQuery.each(jQuery.browser, function(i, val) {
         navegador+="" + i + " : " + val + " ";
    });        
    return navegador;
}

