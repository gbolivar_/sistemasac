<?php
      /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 21/02/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: Configuration.class
       * @version: 1.0
       */
class configuracion
{
    var $template1 = '../apps/autenticacion/templates/template.php';
    var $template2 = '../apps/sac/templates/template.php';
    var $session = null;
    var $interfaz = null;
    var $autenticar = null;
    var $realIp;
    var $realName;

    public function __construct(){
        $this->template1;
        $this->template2;
        $this->session;
        $this->interfaz;
        $this->autenticar;
        $this->realIp;
        $this->realName;
    }
    public function template($valor){
        return $this->interfaz=$valor;
    }
    public function validateInicio(){
//	session_save_path('/var/www/sistemaSAC/');
        session_start();
        /** Instanciar la class de autenticacion si la variable $_POST tiene informacion y pasar los datos user y password */
        if(count($_POST) >= 2){
            /** Incluir las conexion de base de datos */
            include_once 'ConexionDataBase.class.php';
            
            /** Incluir las class del modulo de autenticar de la apps de autenticacion */
            include_once '../apps/autenticacion/modules/autenticacion/actions/actions.class.php';
            /** Incluir funciones extras al comportamiento del sistema */
            include_once '../lib/complementos/funcionesExtras.php';
            $this->autenticar = new autenticarAction();
            $this->methods_ = new funcionesExtras();
            /** Cargar dos datos que son importante la ip del usuario y el hostname */
            $this->realIp=$this->methods_->realIp();
            $this->realName=$this->methods_->realNameHost();
            
            $mensaje=$this->autenticar->executarAutenticar($this->methods_->encrypt($_POST['usuario']), MD5($this->methods_->encrypt($_POST['clave'])), $this->realIp, $this->realName);
            if(!empty ($mensaje)){
                $_SESSION['mensaje']=$mensaje;
                header('location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
            }
        }elseif(!empty ($_GET['logout'])){
            self::logout();
        }
        /** Validar si la autenticacion fue exitosa */ 
        if(@$_SESSION['inicio']==1 AND @$_SESSION['autenticado']==1 AND count($_SESSION['userAut'])>0 AND count($_SESSION['credenciales'])>0){
            /** Destruyo el mensaje */
            unset($_SESSION['mensaje']);
            /** Monto en sesion el nombre real y ip del cliente */
            $_SESSION['realIpS'] = $this->realIp;
            $_SESSION['realNameS'] = $this->realName;
            /** Template cuando esta autenticado */
            return self::template($this->template2);
        }else{
            /** Template de inicio de seccion */
            return self::template($this->template1);
        }
    }
    public function logout(){
        /** Incluir las conexion de base de datos */
            include_once '../apps/autenticacion/modules/autenticacion/actions/actions.class.php';
            $this->autenticar = new autenticarAction();
            $this->autenticar->executarDesconectar();
            header('location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
    }


    public function main(){
        self::validateInicio();
        @include_once($this->interfaz);

    }

}

?>
