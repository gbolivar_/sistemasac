--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: audiperm; Type: SCHEMA; Schema: -; Owner: sispdv
--

CREATE SCHEMA audiperm;


ALTER SCHEMA audiperm OWNER TO sispdv;

--
-- Name: SCHEMA audiperm; Type: COMMENT; Schema: -; Owner: sispdv
--

COMMENT ON SCHEMA audiperm IS 'Schema encargado de llevar la auditoria del sistema y las tablas relacionadas a las permisologia de los usuarios';


--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: sispdv
--

CREATE PROCEDURAL LANGUAGE plpgsql;


ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO sispdv;

SET search_path = public, pg_catalog;

--
-- Name: dblink_pkey_results; Type: TYPE; Schema: public; Owner: sispdv
--

CREATE TYPE dblink_pkey_results AS (
	"position" integer,
	colname text
);


ALTER TYPE public.dblink_pkey_results OWNER TO sispdv;

SET search_path = audiperm, pg_catalog;

--
-- Name: auditoria_tri(); Type: FUNCTION; Schema: audiperm; Owner: sispdv
--

CREATE FUNCTION auditoria_tri() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
     IF(TG_OP = 'INSERT')THEN
	IF(CURRENT_USER='sispdv')THEN
		insert into audiperm.tbl_auditoria(entidad,proceso, fecha, operacion_id, usuario_id,usuario_db,host,hora) 
		values (TG_TABLE_NAME, TG_OP, current_date, new.id, new.usuario_id, USER , inet_client_addr(), current_time);
	ELSE
		insert into audiperm.tbl_auditoria(entidad,proceso, fecha, operacion_id,usuario_db,host,hora) 
		values (TG_TABLE_NAME, TG_OP, current_date, new.id, USER , inet_client_addr(), current_time);
	END IF;
     END IF;

     IF(TG_OP = 'UPDATE')THEN 
	IF(CURRENT_USER='sispdv')THEN
		insert into audiperm.tbl_auditoria(entidad,proceso, fecha, operacion_id, usuario_id, usuario_db,host,hora) 
		values (TG_TABLE_NAME, TG_OP, current_date,old.id,old.usuario_id, USER , inet_client_addr(),  current_time);
	ELSE
		insert into audiperm.tbl_auditoria(entidad,proceso, fecha, operacion_id,usuario_db,host,hora) 
		values (TG_TABLE_NAME, TG_OP, current_date,old.id, USER , inet_client_addr(), current_time);
	END IF;
     END IF;

     IF(TG_OP = 'DELETE')THEN
	IF(CURRENT_USER='sispdv')THEN
		insert into audiperm.tbl_auditoria(entidad,proceso, fecha, operacion_id, usuario_id,usuario_db,host,hora) 
		values (TG_TABLE_NAME, TG_OP, current_date, old.id, old.usuario_id, USER , inet_client_addr(), current_time);
	ELSE
		insert into audiperm.tbl_auditoria(entidad,proceso,fecha, operacion_id,usuario_db,host,hora) 
		values (TG_TABLE_NAME, TG_OP, current_date,old.id, USER , inet_client_addr(), current_time);
		
	END IF;
     END IF;
     return new;
END;
$$;


ALTER FUNCTION audiperm.auditoria_tri() OWNER TO sispdv;

--
-- Name: resg_auditoria(); Type: FUNCTION; Schema: audiperm; Owner: sispdv
--

CREATE FUNCTION resg_auditoria() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
     IF(TG_OP = 'DELETE') THEN
	RAISE EXCEPTION 'No se pueden eliminar registros de auditoria';
     END IF;
     IF (TG_OP = 'UPDATE') THEN 
	RAISE EXCEPTION 'No se pueden modificar registros de auditoria';
     END IF;
END;
$$;


ALTER FUNCTION audiperm.resg_auditoria() OWNER TO sispdv;

--
-- Name: roles_user(); Type: FUNCTION; Schema: audiperm; Owner: sispdv
--

CREATE FUNCTION roles_user() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
     IF(TG_OP = 'INSERT') THEN
	insert into audiperm.tbl_usuario_rol(rol_id, sub_rol_id, usuario_id, estatus, created_at)
	values (1, 1, new.id, true, new.created_at);
	insert into audiperm.tbl_usuario_rol(rol_id, sub_rol_id, usuario_id, estatus, created_at)
	values (5, 18, new.id, true, new.created_at);
     END IF;
     return new;
END;
$$;


ALTER FUNCTION audiperm.roles_user() OWNER TO sispdv;

SET search_path = public, pg_catalog;

--
-- Name: xml_encode_special_chars(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xml_encode_special_chars(text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pgxml', 'xml_encode_special_chars';


ALTER FUNCTION public.xml_encode_special_chars(text) OWNER TO postgres;

--
-- Name: xml_is_well_formed(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xml_is_well_formed(text) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pgxml', 'xml_is_well_formed';


ALTER FUNCTION public.xml_is_well_formed(text) OWNER TO postgres;

--
-- Name: xml_valid(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xml_valid(text) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pgxml', 'xml_is_well_formed';


ALTER FUNCTION public.xml_valid(text) OWNER TO postgres;

--
-- Name: xpath_bool(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xpath_bool(text, text) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pgxml', 'xpath_bool';


ALTER FUNCTION public.xpath_bool(text, text) OWNER TO postgres;

--
-- Name: xpath_list(text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xpath_list(text, text, text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pgxml', 'xpath_list';


ALTER FUNCTION public.xpath_list(text, text, text) OWNER TO postgres;

--
-- Name: xpath_list(text, text); Type: FUNCTION; Schema: public; Owner: sispdv
--

CREATE FUNCTION xpath_list(text, text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT xpath_list($1,$2,',')$_$;


ALTER FUNCTION public.xpath_list(text, text) OWNER TO sispdv;

--
-- Name: xpath_nodeset(text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xpath_nodeset(text, text, text, text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pgxml', 'xpath_nodeset';


ALTER FUNCTION public.xpath_nodeset(text, text, text, text) OWNER TO postgres;

--
-- Name: xpath_nodeset(text, text); Type: FUNCTION; Schema: public; Owner: sispdv
--

CREATE FUNCTION xpath_nodeset(text, text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT xpath_nodeset($1,$2,'','')$_$;


ALTER FUNCTION public.xpath_nodeset(text, text) OWNER TO sispdv;

--
-- Name: xpath_nodeset(text, text, text); Type: FUNCTION; Schema: public; Owner: sispdv
--

CREATE FUNCTION xpath_nodeset(text, text, text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT xpath_nodeset($1,$2,'',$3)$_$;


ALTER FUNCTION public.xpath_nodeset(text, text, text) OWNER TO sispdv;

--
-- Name: xpath_number(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xpath_number(text, text) RETURNS real
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pgxml', 'xpath_number';


ALTER FUNCTION public.xpath_number(text, text) OWNER TO postgres;

--
-- Name: xpath_string(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xpath_string(text, text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pgxml', 'xpath_string';


ALTER FUNCTION public.xpath_string(text, text) OWNER TO postgres;

--
-- Name: xpath_table(text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xpath_table(text, text, text, text, text) RETURNS SETOF record
    LANGUAGE c STABLE STRICT
    AS '$libdir/pgxml', 'xpath_table';


ALTER FUNCTION public.xpath_table(text, text, text, text, text) OWNER TO postgres;

--
-- Name: xslt_process(text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xslt_process(text, text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/pgxml', 'xslt_process';


ALTER FUNCTION public.xslt_process(text, text, text) OWNER TO postgres;

--
-- Name: xslt_process(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION xslt_process(text, text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pgxml', 'xslt_process';


ALTER FUNCTION public.xslt_process(text, text) OWNER TO postgres;

SET search_path = audiperm, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tbl_usuario; Type: TABLE; Schema: audiperm; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_usuario (
    id integer NOT NULL,
    cedula character varying(12) NOT NULL,
    apellido character varying(50) NOT NULL,
    nombre character varying(50) NOT NULL,
    sexo character varying(1) NOT NULL,
    fech_nacimiento date NOT NULL,
    tipo_usuario_id integer NOT NULL,
    correo_elec character varying(100) NOT NULL,
    telefono character varying(11) NOT NULL,
    login character varying(50) NOT NULL,
    clave character varying(50) NOT NULL,
    estatus boolean NOT NULL,
    nacionalidad character varying(2) NOT NULL,
    usuario_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE audiperm.tbl_usuario OWNER TO sispdv;

--
-- Name: empleados_id_seq; Type: SEQUENCE; Schema: audiperm; Owner: sispdv
--

CREATE SEQUENCE empleados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE audiperm.empleados_id_seq OWNER TO sispdv;

--
-- Name: empleados_id_seq; Type: SEQUENCE OWNED BY; Schema: audiperm; Owner: sispdv
--

ALTER SEQUENCE empleados_id_seq OWNED BY tbl_usuario.id;


--
-- Name: empleados_id_seq; Type: SEQUENCE SET; Schema: audiperm; Owner: sispdv
--

SELECT pg_catalog.setval('empleados_id_seq', 35, true);


--
-- Name: tbl_rol; Type: TABLE; Schema: audiperm; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_rol (
    id integer NOT NULL,
    roles character varying(30) NOT NULL,
    modulo character varying(40) NOT NULL,
    credencial character varying(30) NOT NULL,
    activa_menu boolean NOT NULL,
    div_id character varying(30) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE audiperm.tbl_rol OWNER TO sispdv;

--
-- Name: rol_id_rol_seq; Type: SEQUENCE; Schema: audiperm; Owner: sispdv
--

CREATE SEQUENCE rol_id_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE audiperm.rol_id_rol_seq OWNER TO sispdv;

--
-- Name: rol_id_rol_seq; Type: SEQUENCE OWNED BY; Schema: audiperm; Owner: sispdv
--

ALTER SEQUENCE rol_id_rol_seq OWNED BY tbl_rol.id;


--
-- Name: rol_id_rol_seq; Type: SEQUENCE SET; Schema: audiperm; Owner: sispdv
--

SELECT pg_catalog.setval('rol_id_rol_seq', 8, true);


--
-- Name: tbl_auditoria; Type: TABLE; Schema: audiperm; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_auditoria (
    id integer NOT NULL,
    entidad character varying(50) NOT NULL,
    proceso character varying(20) NOT NULL,
    fecha date NOT NULL,
    operacion_id integer NOT NULL,
    usuario_id integer,
    usuario_db character varying(20) NOT NULL,
    host character varying(16),
    hora time with time zone NOT NULL
);


ALTER TABLE audiperm.tbl_auditoria OWNER TO sispdv;

--
-- Name: tbl_auditoria_auditoria_id_seq; Type: SEQUENCE; Schema: audiperm; Owner: sispdv
--

CREATE SEQUENCE tbl_auditoria_auditoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE audiperm.tbl_auditoria_auditoria_id_seq OWNER TO sispdv;

--
-- Name: tbl_auditoria_auditoria_id_seq; Type: SEQUENCE SET; Schema: audiperm; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_auditoria_auditoria_id_seq', 1, false);


--
-- Name: tbl_auditoria_id_seq; Type: SEQUENCE; Schema: audiperm; Owner: sispdv
--

CREATE SEQUENCE tbl_auditoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE audiperm.tbl_auditoria_id_seq OWNER TO sispdv;

--
-- Name: tbl_auditoria_id_seq; Type: SEQUENCE OWNED BY; Schema: audiperm; Owner: sispdv
--

ALTER SEQUENCE tbl_auditoria_id_seq OWNED BY tbl_auditoria.id;


--
-- Name: tbl_auditoria_id_seq; Type: SEQUENCE SET; Schema: audiperm; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_auditoria_id_seq', 28, true);


--
-- Name: tbl_tipo_usuario; Type: TABLE; Schema: audiperm; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_tipo_usuario (
    id integer NOT NULL,
    descripcion character varying(60) NOT NULL
);


ALTER TABLE audiperm.tbl_tipo_usuario OWNER TO sispdv;

--
-- Name: tbl_cargo_id_seq; Type: SEQUENCE; Schema: audiperm; Owner: sispdv
--

CREATE SEQUENCE tbl_cargo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE audiperm.tbl_cargo_id_seq OWNER TO sispdv;

--
-- Name: tbl_cargo_id_seq; Type: SEQUENCE OWNED BY; Schema: audiperm; Owner: sispdv
--

ALTER SEQUENCE tbl_cargo_id_seq OWNED BY tbl_tipo_usuario.id;


--
-- Name: tbl_cargo_id_seq; Type: SEQUENCE SET; Schema: audiperm; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_cargo_id_seq', 1, false);


--
-- Name: tbl_sub_rol; Type: TABLE; Schema: audiperm; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_sub_rol (
    id integer NOT NULL,
    rol_id integer NOT NULL,
    usuario_id integer NOT NULL,
    detalles character varying(40) NOT NULL,
    title character varying(100) NOT NULL,
    div_id character varying(30),
    activa_menu boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE audiperm.tbl_sub_rol OWNER TO sispdv;

--
-- Name: tbl_usuario_rol; Type: TABLE; Schema: audiperm; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_usuario_rol (
    id integer NOT NULL,
    rol_id integer NOT NULL,
    sub_rol_id integer NOT NULL,
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE audiperm.tbl_usuario_rol OWNER TO sispdv;

--
-- Name: tbl_usuario_rol_id_seq; Type: SEQUENCE; Schema: audiperm; Owner: sispdv
--

CREATE SEQUENCE tbl_usuario_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE audiperm.tbl_usuario_rol_id_seq OWNER TO sispdv;

--
-- Name: tbl_usuario_rol_id_seq; Type: SEQUENCE OWNED BY; Schema: audiperm; Owner: sispdv
--

ALTER SEQUENCE tbl_usuario_rol_id_seq OWNED BY tbl_usuario_rol.id;


--
-- Name: tbl_usuario_rol_id_seq; Type: SEQUENCE SET; Schema: audiperm; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_usuario_rol_id_seq', 554, true);


--
-- Name: usuario_rol_id_seq; Type: SEQUENCE; Schema: audiperm; Owner: sispdv
--

CREATE SEQUENCE usuario_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE audiperm.usuario_rol_id_seq OWNER TO sispdv;

--
-- Name: usuario_rol_id_seq; Type: SEQUENCE OWNED BY; Schema: audiperm; Owner: sispdv
--

ALTER SEQUENCE usuario_rol_id_seq OWNED BY tbl_sub_rol.id;


--
-- Name: usuario_rol_id_seq; Type: SEQUENCE SET; Schema: audiperm; Owner: sispdv
--

SELECT pg_catalog.setval('usuario_rol_id_seq', 10, true);


SET search_path = public, pg_catalog;

--
-- Name: tbl_area; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_area (
    area_id integer NOT NULL,
    area character varying(150) NOT NULL
);


ALTER TABLE public.tbl_area OWNER TO sispdv;

--
-- Name: tbl_area_area_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_area_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_area_area_id_seq OWNER TO sispdv;

--
-- Name: tbl_area_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_area_area_id_seq OWNED BY tbl_area.area_id;


--
-- Name: tbl_area_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_area_area_id_seq', 6, true);


--
-- Name: tbl_ayuda; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_ayuda (
    id integer NOT NULL,
    medicina_id integer,
    pers_disc_visual_id integer NOT NULL,
    tipo_ayuda_id integer NOT NULL,
    monto integer NOT NULL,
    observacion text,
    usuario_id integer,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.tbl_ayuda OWNER TO sispdv;

--
-- Name: COLUMN tbl_ayuda.usuario_id; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_ayuda.usuario_id IS 'Identificador del usuario que realizara el registro';


--
-- Name: COLUMN tbl_ayuda.estatus; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_ayuda.estatus IS 'Estatus del registro, TRUE registro activo, FALSE registro eliminado logicamente';


--
-- Name: tbl_ayuda_especial; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_ayuda_especial (
    id integer NOT NULL,
    nacionalidad character varying NOT NULL,
    nombres character varying NOT NULL,
    apellidos character varying NOT NULL,
    monto integer,
    fecha_entrega character varying,
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    medicina_id integer,
    cedula integer NOT NULL,
    observacion text,
    tipo_ayuda_id integer
);


ALTER TABLE public.tbl_ayuda_especial OWNER TO sispdv;

--
-- Name: tbl_ayuda_especial_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_ayuda_especial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_ayuda_especial_id_seq OWNER TO sispdv;

--
-- Name: tbl_ayuda_especial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_ayuda_especial_id_seq OWNED BY tbl_ayuda_especial.id;


--
-- Name: tbl_ayuda_especial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_ayuda_especial_id_seq', 28, true);


--
-- Name: tbl_detalle_horario; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_detalle_horario (
    id integer NOT NULL,
    hora_inicio time without time zone NOT NULL,
    hora_fin time without time zone NOT NULL,
    dia_id integer NOT NULL,
    area_id integer NOT NULL,
    horario_id integer NOT NULL
);


ALTER TABLE public.tbl_detalle_horario OWNER TO sispdv;

--
-- Name: tbl_detalle_horario_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_detalle_horario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_detalle_horario_id_seq OWNER TO sispdv;

--
-- Name: tbl_detalle_horario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_detalle_horario_id_seq OWNED BY tbl_detalle_horario.id;


--
-- Name: tbl_detalle_horario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_detalle_horario_id_seq', 151, true);


--
-- Name: tbl_dia; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_dia (
    id integer NOT NULL,
    dia character varying
);


ALTER TABLE public.tbl_dia OWNER TO sispdv;

--
-- Name: tbl_dia_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_dia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_dia_id_seq OWNER TO sispdv;

--
-- Name: tbl_dia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_dia_id_seq OWNED BY tbl_dia.id;


--
-- Name: tbl_dia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_dia_id_seq', 5, true);


--
-- Name: tbl_diag_medico; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_diag_medico (
    diag_medico_id integer NOT NULL,
    pers_disc_visual_id integer NOT NULL,
    medico_id integer NOT NULL,
    tipo_ceguera_id integer,
    esta_rehabilitado character varying(2),
    otra_enfermedad character varying(150),
    diagnostico text,
    pronostico text NOT NULL,
    campo_visual_od character varying(20),
    campo_visual_oi character varying(20),
    agudez_visual_od character varying(20),
    agudez_visual_id character varying(20),
    usa_lentes boolean NOT NULL,
    consideracion_visual text,
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.tbl_diag_medico OWNER TO sispdv;

--
-- Name: COLUMN tbl_diag_medico.usuario_id; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_diag_medico.usuario_id IS 'Identificador del usuario que realizara el registro';


--
-- Name: COLUMN tbl_diag_medico.estatus; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_diag_medico.estatus IS 'Estatus del registro, TRUE registro activo, FALSE registro eliminado logicamente';


--
-- Name: tbl_diag_medico_diag_medico_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_diag_medico_diag_medico_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_diag_medico_diag_medico_id_seq OWNER TO sispdv;

--
-- Name: tbl_diag_medico_diag_medico_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_diag_medico_diag_medico_id_seq OWNED BY tbl_diag_medico.diag_medico_id;


--
-- Name: tbl_diag_medico_diag_medico_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_diag_medico_diag_medico_id_seq', 29, true);


--
-- Name: tbl_donaciones_donaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_donaciones_donaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_donaciones_donaciones_id_seq OWNER TO sispdv;

--
-- Name: tbl_donaciones_donaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_donaciones_donaciones_id_seq', 1, false);


--
-- Name: tbl_donaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_donaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_donaciones_id_seq OWNER TO sispdv;

--
-- Name: tbl_donaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_donaciones_id_seq OWNED BY tbl_ayuda.id;


--
-- Name: tbl_donaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_donaciones_id_seq', 77, true);


--
-- Name: tbl_estado; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_estado (
    co_estado integer NOT NULL,
    nb_estado character varying,
    ca_estado character varying,
    abreviatura character varying
);


ALTER TABLE public.tbl_estado OWNER TO sispdv;

--
-- Name: tbl_estudio_persona; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_estudio_persona (
    estudio_id integer NOT NULL,
    pers_disc_visual_id integer,
    estudia character varying(2) NOT NULL,
    grado_inst_id integer,
    lugar_estudia character varying(150),
    estudio_realiza character varying(150),
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.tbl_estudio_persona OWNER TO sispdv;

--
-- Name: COLUMN tbl_estudio_persona.usuario_id; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_estudio_persona.usuario_id IS 'Identificador del usuario que realizara el registro';


--
-- Name: COLUMN tbl_estudio_persona.estatus; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_estudio_persona.estatus IS 'Estatus del registro, TRUE registro activo, FALSE registro eliminado logicamente';


--
-- Name: tbl_estudio_persona_estudio_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_estudio_persona_estudio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_estudio_persona_estudio_id_seq OWNER TO sispdv;

--
-- Name: tbl_estudio_persona_estudio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_estudio_persona_estudio_id_seq OWNED BY tbl_estudio_persona.estudio_id;


--
-- Name: tbl_estudio_persona_estudio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_estudio_persona_estudio_id_seq', 58, true);


--
-- Name: tbl_grado_instruccion; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_grado_instruccion (
    grado_inst_id integer NOT NULL,
    grado_instruccion character varying(150) NOT NULL
);


ALTER TABLE public.tbl_grado_instruccion OWNER TO sispdv;

--
-- Name: tbl_grado_instruccion_grado_inst_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_grado_instruccion_grado_inst_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_grado_instruccion_grado_inst_id_seq OWNER TO sispdv;

--
-- Name: tbl_grado_instruccion_grado_inst_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_grado_instruccion_grado_inst_id_seq OWNED BY tbl_grado_instruccion.grado_inst_id;


--
-- Name: tbl_grado_instruccion_grado_inst_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_grado_instruccion_grado_inst_id_seq', 1, false);


--
-- Name: tbl_grupo_familiar; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_grupo_familiar (
    familiar_id integer NOT NULL,
    pers_disc_visual_id integer NOT NULL,
    parentesco_id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50) NOT NULL,
    nacionalidad character varying(1) NOT NULL,
    fech_nacimiento date,
    sexo character varying(1) NOT NULL,
    direccion text,
    telefono numeric(11,0),
    estudia boolean NOT NULL,
    esp_estudia character varying(100),
    trabaja boolean NOT NULL,
    oficio_id integer,
    direcc_trabajo text,
    ingreso integer,
    sufre_ceguera boolean NOT NULL,
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    num_hijo character varying(2)
);


ALTER TABLE public.tbl_grupo_familiar OWNER TO sispdv;

--
-- Name: COLUMN tbl_grupo_familiar.usuario_id; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_grupo_familiar.usuario_id IS 'Identificador del usuario que realizara el registro';


--
-- Name: COLUMN tbl_grupo_familiar.estatus; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_grupo_familiar.estatus IS 'Estatus del registro, TRUE registro activo, FALSE registro eliminado logicamente';


--
-- Name: tbl_grupo_familiar_familiar_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_grupo_familiar_familiar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_grupo_familiar_familiar_id_seq OWNER TO sispdv;

--
-- Name: tbl_grupo_familiar_familiar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_grupo_familiar_familiar_id_seq OWNED BY tbl_grupo_familiar.familiar_id;


--
-- Name: tbl_grupo_familiar_familiar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_grupo_familiar_familiar_id_seq', 50, true);


--
-- Name: tbl_horario; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_horario (
    id integer NOT NULL,
    observacion text,
    estatus boolean NOT NULL,
    tipo_horario_id integer NOT NULL,
    usuario_id integer NOT NULL,
    created_up timestamp without time zone NOT NULL,
    updated_up timestamp without time zone
);


ALTER TABLE public.tbl_horario OWNER TO sispdv;

--
-- Name: tbl_horario_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_horario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_horario_id_seq OWNER TO sispdv;

--
-- Name: tbl_horario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_horario_id_seq OWNED BY tbl_horario.id;


--
-- Name: tbl_horario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_horario_id_seq', 9, true);


--
-- Name: tbl_laboral; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_laboral (
    laboral_id integer NOT NULL,
    pers_disc_visual_id integer NOT NULL,
    trabaja character varying(2) NOT NULL,
    empresa_trabaja character varying(100),
    direccion_trabajo text,
    telef_trabajo character varying(11),
    fecha_ingreso date,
    ingresos integer,
    oficio_id integer,
    profesion_id integer,
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.tbl_laboral OWNER TO sispdv;

--
-- Name: COLUMN tbl_laboral.estatus; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_laboral.estatus IS 'Estatus del registro, TRUE registro activo, FALSE registro eliminado logicamente';


--
-- Name: tbl_laboral_id_laboral_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_laboral_id_laboral_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_laboral_id_laboral_seq OWNER TO sispdv;

--
-- Name: tbl_laboral_id_laboral_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_laboral_id_laboral_seq OWNED BY tbl_laboral.laboral_id;


--
-- Name: tbl_laboral_id_laboral_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_laboral_id_laboral_seq', 67, true);


--
-- Name: tbl_medicina; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_medicina (
    medicina_id integer NOT NULL,
    medicina character(80),
    created_at date NOT NULL
);


ALTER TABLE public.tbl_medicina OWNER TO sispdv;

--
-- Name: tbl_medicina_medicina_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_medicina_medicina_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_medicina_medicina_id_seq OWNER TO sispdv;

--
-- Name: tbl_medicina_medicina_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_medicina_medicina_id_seq OWNED BY tbl_medicina.medicina_id;


--
-- Name: tbl_medicina_medicina_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_medicina_medicina_id_seq', 3, true);


--
-- Name: tbl_medico; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_medico (
    medico_id integer NOT NULL,
    med_nacionalidad character varying(1) NOT NULL,
    med_cedula character varying(11),
    med_nombre character varying(40) NOT NULL,
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.tbl_medico OWNER TO sispdv;

--
-- Name: COLUMN tbl_medico.usuario_id; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_medico.usuario_id IS 'Identificador del usuario que realizara el registro';


--
-- Name: COLUMN tbl_medico.estatus; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_medico.estatus IS 'Estatus del registro, TRUE registro activo, FALSE registro eliminado logicamente';


--
-- Name: tbl_medico_medico_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_medico_medico_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_medico_medico_id_seq OWNER TO sispdv;

--
-- Name: tbl_medico_medico_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_medico_medico_id_seq OWNED BY tbl_medico.medico_id;


--
-- Name: tbl_medico_medico_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_medico_medico_id_seq', 20, true);


--
-- Name: tbl_municipio; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_municipio (
    co_estado integer NOT NULL,
    co_municipio integer NOT NULL,
    nb_municipio character varying,
    ca_municipio character varying
);


ALTER TABLE public.tbl_municipio OWNER TO sispdv;

--
-- Name: tbl_oficio; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_oficio (
    oficio_id integer NOT NULL,
    oficio character varying(150) NOT NULL
);


ALTER TABLE public.tbl_oficio OWNER TO sispdv;

--
-- Name: tbl_oficio_oficio_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_oficio_oficio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_oficio_oficio_id_seq OWNER TO sispdv;

--
-- Name: tbl_oficio_oficio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_oficio_oficio_id_seq OWNED BY tbl_oficio.oficio_id;


--
-- Name: tbl_oficio_oficio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_oficio_oficio_id_seq', 15, true);


--
-- Name: tbl_padrino; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_padrino (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50) NOT NULL,
    nacionalidad character varying(1) NOT NULL,
    cedula integer NOT NULL,
    telefono numeric(11,0),
    correo character varying(150),
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.tbl_padrino OWNER TO sispdv;

--
-- Name: COLUMN tbl_padrino.usuario_id; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_padrino.usuario_id IS 'Identificador del usuario que realizara el registro';


--
-- Name: COLUMN tbl_padrino.estatus; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_padrino.estatus IS 'Estatus del registro, TRUE registro activo, FALSE registro eliminado logicamente';


--
-- Name: tbl_padrino_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_padrino_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_padrino_id_seq OWNER TO sispdv;

--
-- Name: tbl_padrino_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_padrino_id_seq OWNED BY tbl_padrino.id;


--
-- Name: tbl_padrino_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_padrino_id_seq', 1, false);


--
-- Name: tbl_padrino_padrino_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_padrino_padrino_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_padrino_padrino_id_seq OWNER TO sispdv;

--
-- Name: tbl_padrino_padrino_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_padrino_padrino_id_seq OWNED BY tbl_padrino.id;


--
-- Name: tbl_padrino_padrino_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_padrino_padrino_id_seq', 62, true);


--
-- Name: tbl_padrino_tbl_grupo_familiar; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_padrino_tbl_grupo_familiar (
    id integer NOT NULL,
    familiar_id integer NOT NULL,
    padrino_id integer NOT NULL,
    cant_donacion_bs integer NOT NULL,
    fecha_inicio timestamp without time zone NOT NULL,
    estatus boolean NOT NULL,
    usuario_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.tbl_padrino_tbl_grupo_familiar OWNER TO sispdv;

--
-- Name: tbl_padrino_tbl_grupo_familiar_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_padrino_tbl_grupo_familiar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_padrino_tbl_grupo_familiar_id_seq OWNER TO sispdv;

--
-- Name: tbl_padrino_tbl_grupo_familiar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_padrino_tbl_grupo_familiar_id_seq OWNED BY tbl_padrino_tbl_grupo_familiar.id;


--
-- Name: tbl_padrino_tbl_grupo_familiar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_padrino_tbl_grupo_familiar_id_seq', 7, true);


--
-- Name: tbl_padrino_tbl_pers_disc_visual; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_padrino_tbl_pers_disc_visual (
    id integer NOT NULL,
    pers_disc_visual_id integer NOT NULL,
    padrino_id integer NOT NULL,
    cant_donacion_bs integer NOT NULL,
    fecha_inicio date NOT NULL,
    estatus boolean NOT NULL,
    usuario_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.tbl_padrino_tbl_pers_disc_visual OWNER TO sispdv;

--
-- Name: tbl_padrino_tbl_pers_disc_visual_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_padrino_tbl_pers_disc_visual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_padrino_tbl_pers_disc_visual_id_seq OWNER TO sispdv;

--
-- Name: tbl_padrino_tbl_pers_disc_visual_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_padrino_tbl_pers_disc_visual_id_seq OWNED BY tbl_padrino_tbl_pers_disc_visual.id;


--
-- Name: tbl_padrino_tbl_pers_disc_visual_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_padrino_tbl_pers_disc_visual_id_seq', 55, true);


--
-- Name: tbl_parentesco; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_parentesco (
    parentesco_id integer NOT NULL,
    parentesco character varying(150) NOT NULL
);


ALTER TABLE public.tbl_parentesco OWNER TO sispdv;

--
-- Name: tbl_parentesco_parentesco_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_parentesco_parentesco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_parentesco_parentesco_id_seq OWNER TO sispdv;

--
-- Name: tbl_parentesco_parentesco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_parentesco_parentesco_id_seq OWNED BY tbl_parentesco.parentesco_id;


--
-- Name: tbl_parentesco_parentesco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_parentesco_parentesco_id_seq', 1, false);


--
-- Name: tbl_parroquia; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_parroquia (
    co_estado integer NOT NULL,
    co_municipio integer NOT NULL,
    co_parroquia integer NOT NULL,
    nb_parroquia character varying,
    ca_parroquia character varying
);


ALTER TABLE public.tbl_parroquia OWNER TO sispdv;

--
-- Name: tbl_pers_disc_visual; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_pers_disc_visual (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50) NOT NULL,
    nacionalidad character varying(1) NOT NULL,
    cedula integer NOT NULL,
    sexo character varying(1) NOT NULL,
    co_estado integer NOT NULL,
    co_municipio integer NOT NULL,
    co_parroquia integer NOT NULL,
    lugar_nacimiento character varying(100),
    fech_nacimiento date NOT NULL,
    telf_habitacion numeric(11,0),
    telf_celular numeric(11,0),
    correo character varying(150),
    direccion text,
    tenencia_vivienda_id integer,
    tipo_vivienda_id integer NOT NULL,
    nom_ape_pers_contacto character varying(150),
    telef_pers_contacto numeric,
    direccion_pers_contacto text,
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    horario_id integer
);


ALTER TABLE public.tbl_pers_disc_visual OWNER TO sispdv;

--
-- Name: COLUMN tbl_pers_disc_visual.usuario_id; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_pers_disc_visual.usuario_id IS 'Identificador del usuario que realizara el registro';


--
-- Name: COLUMN tbl_pers_disc_visual.estatus; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_pers_disc_visual.estatus IS 'Estatus del registro, TRUE registro activo, FALSE registro eliminado logicamente';


--
-- Name: COLUMN tbl_pers_disc_visual.created_at; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_pers_disc_visual.created_at IS 'Fecha que se creo el registro
';


--
-- Name: COLUMN tbl_pers_disc_visual.updated_at; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_pers_disc_visual.updated_at IS 'Fecha que se actualizo el registro';


--
-- Name: tbl_pers_disc_visual_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_pers_disc_visual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_pers_disc_visual_id_seq OWNER TO sispdv;

--
-- Name: tbl_pers_disc_visual_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_pers_disc_visual_id_seq OWNED BY tbl_pers_disc_visual.id;


--
-- Name: tbl_pers_disc_visual_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_pers_disc_visual_id_seq', 1, false);


--
-- Name: tbl_pers_disc_visual_pers_disc_visual_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_pers_disc_visual_pers_disc_visual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_pers_disc_visual_pers_disc_visual_id_seq OWNER TO sispdv;

--
-- Name: tbl_pers_disc_visual_pers_disc_visual_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_pers_disc_visual_pers_disc_visual_id_seq', 357, true);


--
-- Name: tbl_profesion; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_profesion (
    profesion_id integer NOT NULL,
    profesion character varying(150) NOT NULL
);


ALTER TABLE public.tbl_profesion OWNER TO sispdv;

--
-- Name: tbl_profesion_profesion_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_profesion_profesion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_profesion_profesion_id_seq OWNER TO sispdv;

--
-- Name: tbl_profesion_profesion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_profesion_profesion_id_seq OWNED BY tbl_profesion.profesion_id;


--
-- Name: tbl_profesion_profesion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_profesion_profesion_id_seq', 239, true);


--
-- Name: tbl_tenencia_vivienda; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_tenencia_vivienda (
    tenencia_vivienda_id integer NOT NULL,
    tenencia_vivienda character varying(100)
);


ALTER TABLE public.tbl_tenencia_vivienda OWNER TO sispdv;

--
-- Name: tbl_tenencia_vivienda_tenencia_vivienda_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_tenencia_vivienda_tenencia_vivienda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_tenencia_vivienda_tenencia_vivienda_id_seq OWNER TO sispdv;

--
-- Name: tbl_tenencia_vivienda_tenencia_vivienda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_tenencia_vivienda_tenencia_vivienda_id_seq OWNED BY tbl_tenencia_vivienda.tenencia_vivienda_id;


--
-- Name: tbl_tenencia_vivienda_tenencia_vivienda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_tenencia_vivienda_tenencia_vivienda_id_seq', 5, true);


--
-- Name: tbl_tipo_ayuda; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_tipo_ayuda (
    tipo_ayuda_id integer NOT NULL,
    tipo_ayuda character varying(150) NOT NULL,
    total_monto integer,
    anio_autorizado integer,
    usuario_id integer NOT NULL,
    estatus boolean NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.tbl_tipo_ayuda OWNER TO sispdv;

--
-- Name: COLUMN tbl_tipo_ayuda.usuario_id; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_tipo_ayuda.usuario_id IS 'Identificador del usuario que realizara el registro';


--
-- Name: COLUMN tbl_tipo_ayuda.estatus; Type: COMMENT; Schema: public; Owner: sispdv
--

COMMENT ON COLUMN tbl_tipo_ayuda.estatus IS 'Estatus del registro, TRUE registro activo, FALSE registro eliminado logicamente';


--
-- Name: tbl_tipo_ayuda_tipo_ayuda_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_tipo_ayuda_tipo_ayuda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_tipo_ayuda_tipo_ayuda_id_seq OWNER TO sispdv;

--
-- Name: tbl_tipo_ayuda_tipo_ayuda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_tipo_ayuda_tipo_ayuda_id_seq OWNED BY tbl_tipo_ayuda.tipo_ayuda_id;


--
-- Name: tbl_tipo_ayuda_tipo_ayuda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_tipo_ayuda_tipo_ayuda_id_seq', 9, true);


--
-- Name: tbl_tipo_cegera; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_tipo_cegera (
    tipo_ceguera_id integer NOT NULL,
    tipo_ceguera character varying(60) NOT NULL
);


ALTER TABLE public.tbl_tipo_cegera OWNER TO sispdv;

--
-- Name: tbl_tipo_cegera_tipo_ceguera_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_tipo_cegera_tipo_ceguera_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_tipo_cegera_tipo_ceguera_id_seq OWNER TO sispdv;

--
-- Name: tbl_tipo_cegera_tipo_ceguera_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_tipo_cegera_tipo_ceguera_id_seq OWNED BY tbl_tipo_cegera.tipo_ceguera_id;


--
-- Name: tbl_tipo_cegera_tipo_ceguera_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_tipo_cegera_tipo_ceguera_id_seq', 7, true);


--
-- Name: tbl_tipo_horario; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_tipo_horario (
    id integer NOT NULL,
    tipo_horario character(1)
);


ALTER TABLE public.tbl_tipo_horario OWNER TO sispdv;

--
-- Name: tbl_tipo_horario_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_tipo_horario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_tipo_horario_id_seq OWNER TO sispdv;

--
-- Name: tbl_tipo_horario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_tipo_horario_id_seq OWNED BY tbl_tipo_horario.id;


--
-- Name: tbl_tipo_horario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_tipo_horario_id_seq', 11, true);


--
-- Name: tbl_tipo_vivienda; Type: TABLE; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE TABLE tbl_tipo_vivienda (
    tipo_vivienda_id integer NOT NULL,
    tipo_vivienda character varying(50)
);


ALTER TABLE public.tbl_tipo_vivienda OWNER TO sispdv;

--
-- Name: tbl_tipo_vivienda_tipo_vivienda_id_seq; Type: SEQUENCE; Schema: public; Owner: sispdv
--

CREATE SEQUENCE tbl_tipo_vivienda_tipo_vivienda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tbl_tipo_vivienda_tipo_vivienda_id_seq OWNER TO sispdv;

--
-- Name: tbl_tipo_vivienda_tipo_vivienda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sispdv
--

ALTER SEQUENCE tbl_tipo_vivienda_tipo_vivienda_id_seq OWNED BY tbl_tipo_vivienda.tipo_vivienda_id;


--
-- Name: tbl_tipo_vivienda_tipo_vivienda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sispdv
--

SELECT pg_catalog.setval('tbl_tipo_vivienda_tipo_vivienda_id_seq', 1, false);


SET search_path = audiperm, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_auditoria ALTER COLUMN id SET DEFAULT nextval('tbl_auditoria_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_rol ALTER COLUMN id SET DEFAULT nextval('rol_id_rol_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_sub_rol ALTER COLUMN id SET DEFAULT nextval('usuario_rol_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_tipo_usuario ALTER COLUMN id SET DEFAULT nextval('tbl_cargo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_usuario ALTER COLUMN id SET DEFAULT nextval('empleados_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_usuario_rol ALTER COLUMN id SET DEFAULT nextval('tbl_usuario_rol_id_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- Name: area_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_area ALTER COLUMN area_id SET DEFAULT nextval('tbl_area_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_ayuda ALTER COLUMN id SET DEFAULT nextval('tbl_donaciones_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_ayuda_especial ALTER COLUMN id SET DEFAULT nextval('tbl_ayuda_especial_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_detalle_horario ALTER COLUMN id SET DEFAULT nextval('tbl_detalle_horario_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_dia ALTER COLUMN id SET DEFAULT nextval('tbl_dia_id_seq'::regclass);


--
-- Name: diag_medico_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_diag_medico ALTER COLUMN diag_medico_id SET DEFAULT nextval('tbl_diag_medico_diag_medico_id_seq'::regclass);


--
-- Name: estudio_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_estudio_persona ALTER COLUMN estudio_id SET DEFAULT nextval('tbl_estudio_persona_estudio_id_seq'::regclass);


--
-- Name: grado_inst_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_grado_instruccion ALTER COLUMN grado_inst_id SET DEFAULT nextval('tbl_grado_instruccion_grado_inst_id_seq'::regclass);


--
-- Name: familiar_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_grupo_familiar ALTER COLUMN familiar_id SET DEFAULT nextval('tbl_grupo_familiar_familiar_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_horario ALTER COLUMN id SET DEFAULT nextval('tbl_horario_id_seq'::regclass);


--
-- Name: laboral_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_laboral ALTER COLUMN laboral_id SET DEFAULT nextval('tbl_laboral_id_laboral_seq'::regclass);


--
-- Name: medicina_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_medicina ALTER COLUMN medicina_id SET DEFAULT nextval('tbl_medicina_medicina_id_seq'::regclass);


--
-- Name: medico_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_medico ALTER COLUMN medico_id SET DEFAULT nextval('tbl_medico_medico_id_seq'::regclass);


--
-- Name: oficio_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_oficio ALTER COLUMN oficio_id SET DEFAULT nextval('tbl_oficio_oficio_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_padrino ALTER COLUMN id SET DEFAULT nextval('tbl_padrino_padrino_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_padrino_tbl_grupo_familiar ALTER COLUMN id SET DEFAULT nextval('tbl_padrino_tbl_grupo_familiar_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_padrino_tbl_pers_disc_visual ALTER COLUMN id SET DEFAULT nextval('tbl_padrino_tbl_pers_disc_visual_id_seq'::regclass);


--
-- Name: parentesco_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_parentesco ALTER COLUMN parentesco_id SET DEFAULT nextval('tbl_parentesco_parentesco_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_pers_disc_visual ALTER COLUMN id SET DEFAULT nextval('tbl_pers_disc_visual_id_seq'::regclass);


--
-- Name: profesion_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_profesion ALTER COLUMN profesion_id SET DEFAULT nextval('tbl_profesion_profesion_id_seq'::regclass);


--
-- Name: tenencia_vivienda_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_tenencia_vivienda ALTER COLUMN tenencia_vivienda_id SET DEFAULT nextval('tbl_tenencia_vivienda_tenencia_vivienda_id_seq'::regclass);


--
-- Name: tipo_ayuda_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_tipo_ayuda ALTER COLUMN tipo_ayuda_id SET DEFAULT nextval('tbl_tipo_ayuda_tipo_ayuda_id_seq'::regclass);


--
-- Name: tipo_ceguera_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_tipo_cegera ALTER COLUMN tipo_ceguera_id SET DEFAULT nextval('tbl_tipo_cegera_tipo_ceguera_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_tipo_horario ALTER COLUMN id SET DEFAULT nextval('tbl_tipo_horario_id_seq'::regclass);


--
-- Name: tipo_vivienda_id; Type: DEFAULT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_tipo_vivienda ALTER COLUMN tipo_vivienda_id SET DEFAULT nextval('tbl_tipo_vivienda_tipo_vivienda_id_seq'::regclass);


SET search_path = audiperm, pg_catalog;

--
-- Data for Name: tbl_auditoria; Type: TABLE DATA; Schema: audiperm; Owner: sispdv
--

COPY tbl_auditoria (id, entidad, proceso, fecha, operacion_id, usuario_id, usuario_db, host, hora) FROM stdin;
1	tbl_pers_disc_visual	UPDATE	2012-10-09	317	\N	postgres	localhost	23:38:09.688968-04:30
2	tbl_pers_disc_visual	UPDATE	2012-10-09	321	\N	postgres	localhost	23:38:11.873649-04:30
3	tbl_pers_disc_visual	UPDATE	2012-10-09	304	\N	postgres	localhost	23:40:56.247632-04:30
4	tbl_pers_disc_visual	UPDATE	2012-10-09	304	1	sispdv	localhost	23:42:53.533123-04:30
5	tbl_pers_disc_visual	UPDATE	2012-10-09	304	1	sispdv	localhost	23:43:44.83027-04:30
6	tbl_pers_disc_visual	UPDATE	2012-10-09	322	\N	postgres	localhost	23:44:02.284268-04:30
7	tbl_pers_disc_visual	UPDATE	2012-10-10	322	\N	postgres	127.0.0.1/32	00:17:58.706541-04:30
8	tbl_pers_disc_visual	UPDATE	2012-10-10	321	\N	postgres	127.0.0.1/32	00:34:45.413343-04:30
9	tbl_pers_disc_visual	UPDATE	2012-10-10	304	1	sispdv	127.0.0.1/32	00:35:27.028768-04:30
10	tbl_pers_disc_visual	UPDATE	2012-10-10	304	1	sispdv	127.0.0.1/32	00:44:19.419258-04:30
11	tbl_pers_disc_visual	UPDATE	2012-10-12	310	\N	postgres	127.0.0.1/32	12:53:49.999584-04:30
12	tbl_pers_disc_visual	UPDATE	2012-10-16	304	1	sispdv	127.0.0.1/32	16:26:26.145075-04:30
13	tbl_pers_disc_visual	UPDATE	2012-10-16	341	\N	postgres	127.0.0.1/32	16:27:07.784037-04:30
14	tbl_pers_disc_visual	UPDATE	2012-10-16	304	1	sispdv	127.0.0.1/32	18:44:44.946156-04:30
15	tbl_pers_disc_visual	UPDATE	2012-10-16	341	\N	postgres	127.0.0.1/32	18:45:23.782142-04:30
16	tbl_pers_disc_visual	INSERT	2012-10-23	352	1	sispdv	127.0.0.1/32	16:47:28.039676-04:30
17	tbl_pers_disc_visual	INSERT	2012-10-23	353	1	sispdv	127.0.0.1/32	17:06:02.283262-04:30
18	tbl_pers_disc_visual	INSERT	2012-10-23	354	1	sispdv	127.0.0.1/32	17:08:35.765466-04:30
19	tbl_pers_disc_visual	INSERT	2012-10-23	356	1	sispdv	127.0.0.1/32	17:41:36.513143-04:30
20	tbl_ayuda	INSERT	2012-10-23	72	1	sispdv	127.0.0.1/32	18:28:05.646729-04:30
21	tbl_ayuda	INSERT	2012-10-23	73	1	sispdv	127.0.0.1/32	18:30:43.593842-04:30
22	tbl_ayuda	INSERT	2012-10-23	74	1	sispdv	127.0.0.1/32	18:32:50.703488-04:30
23	tbl_ayuda	INSERT	2012-10-23	75	1	sispdv	127.0.0.1/32	18:35:03.584581-04:30
24	tbl_ayuda	INSERT	2012-10-23	76	1	sispdv	127.0.0.1/32	18:36:29.405818-04:30
25	tbl_ayuda	INSERT	2012-10-23	77	1	sispdv	127.0.0.1/32	18:38:04.6969-04:30
26	tbl_pers_disc_visual	UPDATE	2012-10-23	304	1	sispdv	127.0.0.1/32	19:14:03.880798-04:30
27	tbl_pers_disc_visual	UPDATE	2012-10-23	310	\N	postgres	127.0.0.1/32	19:14:40.530393-04:30
28	tbl_pers_disc_visual	INSERT	2012-10-23	357	1	sispdv	127.0.0.1/32	19:21:10.111125-04:30
\.


--
-- Data for Name: tbl_rol; Type: TABLE DATA; Schema: audiperm; Owner: sispdv
--

COPY tbl_rol (id, roles, modulo, credencial, activa_menu, div_id, created_at, updated_at) FROM stdin;
2	Gest. Padrino	padrino	men_padrino	t	link2	2012-04-06 00:00:00	\N
1	Gest. Personas	personas	men_personas	t	link1	2012-04-06 00:00:00	\N
5	Gest. Usuarios	usuarios	men_usuarios	t	link5	2012-04-07 00:00:00	\N
4	Gest. Ayudas	ayudas	men_ayudas	t	link4	2012-04-06 00:00:00	\N
3	Gest. Horario	horario	men_horario	t	link3	2012-04-06 00:00:00	\N
6	Gest. Reportes	reportes	men_reportes	t	link6	2012-06-18 00:00:00	\N
\.


--
-- Data for Name: tbl_sub_rol; Type: TABLE DATA; Schema: audiperm; Owner: sispdv
--

COPY tbl_sub_rol (id, rol_id, usuario_id, detalles, title, div_id, activa_menu, created_at, updated_at) FROM stdin;
7	2	1	Registrar padrino	Registrar padrino	link2-1	t	2012-04-06 00:00:00	\N
9	2	1	Edit. padrino	Editar padrino	link2-2	t	2012-04-06 00:00:00	\N
10	2	1	Asoc. padrino	Asociar padrino	link2-3	t	2012-04-06 00:00:00	\N
11	2	1	Des. padrino	Desasociar padrino	link2-4	t	2012-04-06 00:00:00	\N
1	1	1	Registrar Personas	Registrar Personas	link1-1	t	2012-04-06 00:00:00	\N
2	1	1	Editar Personas	Editar Personas	link1-2	t	2012-04-06 00:00:00	\N
3	1	1	Agre. grup. Familiar	Agregar grupo Familiar	link1-3	t	2012-04-06 00:00:00	\N
4	1	1	Edit. grup. familiar	Editar grupo familiar	link1-4	t	2012-04-06 00:00:00	\N
5	1	1	Registrar diagn&oacute;stico	Registrar diagn&oacute;stico	link1-5	t	2012-04-06 00:00:00	\N
6	1	1	Emitir Expediente	Emitir Expediente	link1-6	t	2012-04-06 00:00:00	\N
14	4	1	Registrar Ayuda	Registrar Ayuda	link4-1	t	2012-04-06 00:00:00	\N
13	3	1	Asociar  horario	Asociar Horario	link3-2	t	2012-04-06 00:00:00	\N
12	3	1	Registrar Horario	Registrar los horarios del las personas que estan en tratamiento	link3-1	t	2012-04-06 00:00:00	\N
19	3	1	Exportar Horario	Exportar Horario	link3-3	t	2012-04-07 00:00:00	\N
17	5	1	Registro de usuario	Registrar usuario de sistema	link5-1	t	2012-04-07 00:00:00	\N
18	5	1	Listar usuarios	Mostrar un listado con los usuarios	link5-2	t	2012-04-07 00:00:00	\N
15	4	1	Reg. Ayu. Especiales	Registrar Ayudas Especiales	link4-2	t	2012-04-06 00:00:00	\N
8	6	1	Causa de ceguera	Exportar un reporte de causa de ceguera mas frecuente	link6-1	t	2012-06-18 00:00:00	\N
21	4	1	Consultar ayudas	Consultar todas las ayudas entregadas	link4-0	t	2012-04-06 00:00:00	\N
20	1	1	Consultar Personas	Consultar personas	link1-0	f	2012-04-06 00:00:00	\N
16	4	1	Tipo Ayuda	Registrar Tipo Ayuda	link4-3	t	2012-04-06 00:00:00	\N
\.


--
-- Data for Name: tbl_tipo_usuario; Type: TABLE DATA; Schema: audiperm; Owner: sispdv
--

COPY tbl_tipo_usuario (id, descripcion) FROM stdin;
3	Analista
2	Jefa de servicio
1	Administrador
\.


--
-- Data for Name: tbl_usuario; Type: TABLE DATA; Schema: audiperm; Owner: sispdv
--

COPY tbl_usuario (id, cedula, apellido, nombre, sexo, fech_nacimiento, tipo_usuario_id, correo_elec, telefono, login, clave, estatus, nacionalidad, usuario_id, created_at, updated_at) FROM stdin;
1	1744293	GREGORIO	BOLIVAR	M	1982-08-12	1	elalconxvii@gmail.com	04125596029	18mi2pzowt4=	e3bdbdcc532dd25e336cb16c976c5f60	t	V	1	2012-04-07 00:00:00	\N
5	17444475	Quiaro	Yarelis	F	1987-04-16	1	yarelis.quiaro@hotmail.com	412455566	6dio15Tk0A==	c0ebf2eb7c6dcb757f2dc7c19b031b6a	t	V	1	2012-04-15 04:09:24	2012-07-04 08:49:50
7	18000111	pedro	camejo	M	2012-04-10	3	ro@gmail.com	09090909090	2sqU25jc0A==	c0ebf2eb7c6dcb757f2dc7c19b031b6a	t	V	1	2012-04-23 11:04:58	2012-07-06 05:32:18
4	17167137	Santander	Juan	M	1985-08-18	1	santander.juancho@gmail.com	04264044700	2tqU3KfTz9CY5Q==	df6a08059ea5392c3f66a2156da77a25	t	V	1	2012-04-15 04:04:48	2012-07-07 08:07:40
8	234084	Prueba	sistema	F	2012-07-04	3	hols@hotmail	123456	oZlm	9deadf9ec03eefd0f93fab5559a04496	t	V	1	2012-07-05 02:25:38	2012-07-05 06:24:02
18	1234567	ttt	fff	F	2012-07-04	1	ya@hotmail.com	999999	6cil05/b1A==	9deadf9ec03eefd0f93fab5559a04496	t	V	5	2012-07-11 11:12:17	\N
15	76765654	pedro	perez	M	1994-04-21	3	r2o@gmail.com	76757654545	19mY1aLkytuV4s/bp9HZa6Bz2tDgoNTM3l/T1qA=	a6b4daec93eba9775de7fcc47fd22084	t	V	1	2012-07-08 09:59:48	2012-07-10 10:43:20
16	87665543	simon jose	antonio	M	1987-02-04	3	r3o@gmail.com	87867656546	4tar15Lly6Fz29LmntHQn5yW4c4=	a6b4daec93eba9775de7fcc47fd22084	t	V	1	2012-07-08 10:04:46	2012-07-11 09:19:44
19	131313	nuevo	uno	M	2012-07-12	1	elmatatan@gmail.com	412455566	3tyY5KI=	9819b788ee8aa2ef19693f508cffdfbc	t	V	1	2012-07-12 09:10:22	\N
20	89999999	perez	juan	F	1994-02-17	1	santander.juancarlos@gmail.com	41245556645	4NGoz6E=	3cd8a530cba54d057bf4d7dd8f6c8dda	f	V	1	2012-07-19 07:50:03	\N
22	39999999	diaz	felicia	F	1993-05-12	1	yy.kkkkkk@hotmail.com	04124555664	1M2Y2pzVys0=	9deadf9ec03eefd0f93fab5559a04496	f	V	5	2012-07-19 07:55:39	2012-07-19 09:33:06
3	18804762	ROXENIS	SANOJA	F	1987-10-03	1	roxenis.sanoja50@gmail.com	04120154916	4tqU3KLcwg==	c0ebf2eb7c6dcb757f2dc7c19b031b6a	t	V	3	2012-04-11 04:00:11	2012-10-09 06:18:44
\.


--
-- Data for Name: tbl_usuario_rol; Type: TABLE DATA; Schema: audiperm; Owner: sispdv
--

COPY tbl_usuario_rol (id, rol_id, sub_rol_id, usuario_id, estatus, created_at, updated_at) FROM stdin;
393	1	4	7	t	2012-07-08 09:41:49	\N
2	1	2	1	t	2012-04-08 00:00:00	\N
3	1	3	1	t	2012-04-08 00:00:00	\N
4	1	4	1	t	2012-04-08 00:00:00	\N
5	1	5	1	t	2012-04-08 00:00:00	\N
6	1	6	1	t	2012-04-08 00:00:00	\N
8	2	7	1	t	2012-04-08 00:00:00	\N
9	2	9	1	t	2012-04-08 00:00:00	\N
10	2	10	1	t	2012-04-08 00:00:00	\N
11	2	11	1	t	2012-04-08 00:00:00	\N
12	3	12	1	t	2012-04-08 00:00:00	\N
395	1	2	7	t	2012-07-08 09:41:49	\N
397	1	20	7	t	2012-07-08 09:41:49	\N
399	1	5	7	t	2012-07-08 09:41:49	\N
401	2	10	7	t	2012-07-08 09:41:49	\N
403	2	7	7	t	2012-07-08 09:41:49	\N
405	3	13	7	t	2012-07-08 09:41:49	\N
407	4	15	7	t	2012-07-08 09:41:49	\N
409	4	14	7	t	2012-07-08 09:41:49	\N
56	1	6	3	t	2012-04-11 04:01:26	\N
411	5	17	7	t	2012-07-08 09:41:49	\N
59	1	6	3	t	2012-04-11 08:53:39	\N
61	1	6	3	t	2012-04-14 01:36:39	\N
13	3	13	1	t	2012-04-08 00:00:00	\N
14	4	14	1	t	2012-04-08 00:00:00	\N
15	4	15	1	t	2012-04-08 00:00:00	\N
16	4	16	1	t	2012-04-08 00:00:00	\N
17	5	17	1	t	2012-04-08 00:00:00	\N
183	1	18	1	t	2012-04-23 11:03:27	\N
19	3	19	1	t	2012-04-07 00:00:00	\N
65	1	1	3	t	2012-04-15 11:04:58	\N
66	1	6	3	t	2012-04-15 11:04:58	\N
67	1	5	3	t	2012-04-15 11:04:58	\N
68	2	11	3	t	2012-04-15 11:04:58	\N
69	2	10	3	t	2012-04-15 11:04:58	\N
70	2	9	3	t	2012-04-15 11:04:58	\N
71	2	7	3	t	2012-04-15 11:04:58	\N
72	3	19	3	t	2012-04-15 11:04:58	\N
73	3	12	3	t	2012-04-15 11:04:58	\N
74	3	13	3	t	2012-04-15 11:04:58	\N
75	4	16	3	t	2012-04-15 11:04:58	\N
76	4	15	3	t	2012-04-15 11:04:58	\N
77	4	14	3	t	2012-04-15 11:04:58	\N
422	1	1	15	t	2012-07-08 09:59:48	\N
423	1	1	15	t	2012-07-08 09:59:48	\N
81	1	3	4	t	2012-04-15 04:11:09	\N
426	1	4	1	t	2012-07-09 12:07:31	\N
83	1	1	4	t	2012-04-15 04:11:09	\N
84	1	6	4	t	2012-04-15 04:11:09	\N
85	1	5	4	t	2012-04-15 04:11:09	\N
86	2	11	4	t	2012-04-15 04:11:09	\N
428	1	2	1	t	2012-07-09 12:07:31	\N
87	2	10	4	t	2012-04-15 04:11:09	\N
430	1	6	1	t	2012-07-09 12:07:31	\N
89	2	7	4	t	2012-04-15 04:11:09	\N
90	3	19	4	t	2012-04-15 04:11:09	\N
91	3	12	4	t	2012-04-15 04:11:09	\N
432	2	11	1	t	2012-07-09 12:07:31	\N
92	3	13	4	t	2012-04-15 04:11:09	\N
93	4	16	4	t	2012-04-15 04:11:09	\N
94	4	15	4	t	2012-04-15 04:11:09	\N
95	4	14	4	t	2012-04-15 04:11:09	\N
96	5	18	4	t	2012-04-15 04:11:09	\N
434	2	9	1	t	2012-07-09 12:07:31	\N
436	3	13	1	t	2012-07-09 12:07:31	\N
438	3	19	1	t	2012-07-09 12:07:31	\N
440	4	15	1	t	2012-07-09 12:07:31	\N
442	4	16	1	t	2012-07-09 12:07:31	\N
444	5	17	1	t	2012-07-09 12:07:31	\N
446	1	4	1	t	2012-07-09 04:02:41	\N
448	1	2	1	t	2012-07-09 04:02:41	\N
450	1	20	1	t	2012-07-09 04:02:41	\N
452	1	5	1	t	2012-07-09 04:02:41	\N
454	2	10	1	t	2012-07-09 04:02:41	\N
456	2	7	1	t	2012-07-09 04:02:41	\N
458	3	12	1	t	2012-07-09 04:02:41	\N
460	4	14	1	t	2012-07-09 04:02:41	\N
462	4	21	1	t	2012-07-09 04:02:41	\N
464	5	18	1	t	2012-07-09 04:02:41	\N
466	6	8	1	t	2012-07-09 04:02:41	\N
467	1	1	5	t	2012-07-11 09:20:25	\N
469	1	4	5	t	2012-07-11 09:20:25	\N
471	1	2	5	t	2012-07-11 09:20:25	\N
473	2	10	5	t	2012-07-11 09:20:25	\N
475	2	7	5	t	2012-07-11 09:20:25	\N
477	3	19	5	t	2012-07-11 09:20:25	\N
479	3	12	5	t	2012-07-11 09:20:25	\N
481	4	15	5	t	2012-07-11 09:20:25	\N
483	4	16	5	t	2012-07-11 09:20:25	\N
485	5	17	5	t	2012-07-11 09:20:25	\N
487	1	1	18	t	2012-07-11 11:12:17	\N
488	1	1	18	t	2012-07-11 11:12:17	\N
489	1	1	19	t	2012-07-12 09:10:22	\N
119	1	1	3	t	2012-04-17 10:57:39	\N
120	1	6	3	t	2012-04-17 10:57:39	\N
121	1	5	3	t	2012-04-17 10:57:39	\N
122	2	11	3	t	2012-04-17 10:57:39	\N
123	2	10	3	t	2012-04-17 10:57:39	\N
124	2	9	3	t	2012-04-17 10:57:39	\N
125	2	7	3	t	2012-04-17 10:57:39	\N
126	3	13	3	t	2012-04-17 10:57:39	\N
127	3	12	3	t	2012-04-17 10:57:39	\N
128	3	19	3	t	2012-04-17 10:57:39	\N
129	4	14	3	t	2012-04-17 10:57:39	\N
130	4	16	3	t	2012-04-17 10:57:39	\N
131	4	15	3	t	2012-04-17 10:57:39	\N
490	1	1	19	t	2012-07-12 09:10:22	\N
491	1	1	20	t	2012-07-19 07:50:03	\N
492	1	1	20	t	2012-07-19 07:50:03	\N
495	1	1	5	t	2012-07-19 07:59:23	\N
497	1	4	5	t	2012-07-19 07:59:23	\N
499	1	2	5	t	2012-07-19 07:59:23	\N
501	2	10	5	t	2012-07-19 07:59:23	\N
503	2	7	5	t	2012-07-19 07:59:23	\N
505	3	19	5	t	2012-07-19 07:59:23	\N
184	1	5	3	t	2012-04-23 11:03:27	\N
507	3	12	5	t	2012-07-19 07:59:23	\N
185	2	11	3	t	2012-04-23 11:03:27	\N
186	2	10	3	t	2012-04-23 11:03:27	\N
187	2	9	3	t	2012-04-23 11:03:27	\N
509	4	15	5	t	2012-07-19 07:59:23	\N
511	4	16	5	t	2012-07-19 07:59:23	\N
513	5	17	5	t	2012-07-19 07:59:23	\N
515	1	1	8	t	2012-09-04 09:43:27	\N
517	1	4	8	t	2012-09-04 09:43:27	\N
519	1	2	8	t	2012-09-04 09:43:27	\N
521	2	10	8	t	2012-09-04 09:43:27	\N
523	2	7	8	t	2012-09-04 09:43:27	\N
525	3	19	8	t	2012-09-04 09:43:27	\N
188	2	7	3	t	2012-04-23 11:03:27	\N
189	3	13	3	t	2012-04-23 11:03:27	\N
190	3	12	3	t	2012-04-23 11:03:27	\N
191	3	19	3	t	2012-04-23 11:03:27	\N
192	4	14	3	t	2012-04-23 11:03:27	\N
193	4	15	3	t	2012-04-23 11:03:27	\N
194	4	16	3	t	2012-04-23 11:03:27	\N
195	5	18	3	t	2012-04-23 11:03:27	\N
182	1	1	1	t	2012-04-23 11:03:27	\N
196	5	17	3	t	2012-04-23 11:03:27	\N
197	1	3	4	t	2012-04-25 11:34:26	\N
198	1	1	4	t	2012-04-25 11:34:26	\N
199	1	6	4	t	2012-04-25 11:34:26	\N
200	1	5	4	t	2012-04-25 11:34:26	\N
201	2	11	4	t	2012-04-25 11:34:26	\N
202	2	10	4	t	2012-04-25 11:34:26	\N
203	2	7	4	t	2012-04-25 11:34:26	\N
204	3	13	4	t	2012-04-25 11:34:26	\N
205	3	12	4	t	2012-04-25 11:34:26	\N
206	3	19	4	t	2012-04-25 11:34:26	\N
207	4	14	4	t	2012-04-25 11:34:26	\N
208	4	15	4	t	2012-04-25 11:34:26	\N
209	4	16	4	t	2012-04-25 11:34:26	\N
210	5	18	4	t	2012-04-25 11:34:26	\N
229	1	4	4	t	2012-04-26 05:14:38	\N
230	1	3	4	t	2012-04-26 05:14:38	\N
231	1	2	4	t	2012-04-26 05:14:38	\N
232	1	1	4	t	2012-04-26 05:14:38	\N
233	1	6	4	t	2012-04-26 05:14:38	\N
234	1	5	4	t	2012-04-26 05:14:38	\N
235	2	11	4	t	2012-04-26 05:14:38	\N
236	2	10	4	t	2012-04-26 05:14:38	\N
237	2	9	4	t	2012-04-26 05:14:38	\N
238	2	7	4	t	2012-04-26 05:14:38	\N
239	3	13	4	t	2012-04-26 05:14:38	\N
240	3	12	4	t	2012-04-26 05:14:38	\N
241	3	19	4	t	2012-04-26 05:14:38	\N
242	4	14	4	t	2012-04-26 05:14:38	\N
243	4	15	4	t	2012-04-26 05:14:38	\N
244	4	16	4	t	2012-04-26 05:14:38	\N
245	5	18	4	t	2012-04-26 05:14:38	\N
246	5	17	4	t	2012-04-26 05:14:38	\N
247	1	3	7	t	2012-04-26 05:36:47	\N
248	1	1	3	t	2012-05-18 03:49:54	\N
249	1	6	3	t	2012-05-18 03:49:54	\N
250	1	5	3	t	2012-05-18 03:49:54	\N
251	2	11	3	t	2012-05-18 03:49:54	\N
252	2	10	3	t	2012-05-18 03:49:54	\N
253	2	9	3	t	2012-05-18 03:49:54	\N
254	2	7	3	t	2012-05-18 03:49:54	\N
255	3	13	3	t	2012-05-18 03:49:54	\N
256	3	12	3	t	2012-05-18 03:49:54	\N
257	3	19	3	t	2012-05-18 03:49:54	\N
258	4	14	3	t	2012-05-18 03:49:54	\N
259	4	15	3	t	2012-05-18 03:49:54	\N
260	4	16	3	t	2012-05-18 03:49:54	\N
261	5	18	3	t	2012-05-18 03:49:54	\N
262	5	17	3	t	2012-05-18 03:49:54	\N
263	1	4	3	t	2012-06-03 02:11:49	\N
264	1	3	3	t	2012-06-03 02:11:49	\N
265	1	2	3	t	2012-06-03 02:11:49	\N
266	1	1	3	t	2012-06-03 02:11:49	\N
267	1	6	3	t	2012-06-03 02:11:49	\N
268	1	5	3	t	2012-06-03 02:11:49	\N
269	2	11	3	t	2012-06-03 02:11:49	\N
270	2	10	3	t	2012-06-03 02:11:49	\N
271	2	9	3	t	2012-06-03 02:11:49	\N
272	2	7	3	t	2012-06-03 02:11:49	\N
273	3	13	3	t	2012-06-03 02:11:49	\N
274	3	12	3	t	2012-06-03 02:11:49	\N
275	3	19	3	t	2012-06-03 02:11:49	\N
276	4	14	3	t	2012-06-03 02:11:49	\N
277	4	15	3	t	2012-06-03 02:11:49	\N
278	4	16	3	t	2012-06-03 02:11:49	\N
279	5	18	3	t	2012-06-03 02:11:49	\N
280	5	17	3	t	2012-06-03 02:11:49	\N
299	1	4	1	t	2012-06-18 10:36:01	\N
300	1	3	1	t	2012-06-18 10:36:01	\N
301	1	2	1	t	2012-06-18 10:36:01	\N
302	1	6	1	t	2012-06-18 10:36:01	\N
303	1	5	1	t	2012-06-18 10:36:01	\N
304	2	11	1	t	2012-06-18 10:36:01	\N
305	2	10	1	t	2012-06-18 10:36:01	\N
306	2	9	1	t	2012-06-18 10:36:01	\N
307	2	7	1	t	2012-06-18 10:36:01	\N
308	3	12	1	t	2012-06-18 10:36:01	\N
309	3	13	1	t	2012-06-18 10:36:01	\N
310	3	19	1	t	2012-06-18 10:36:01	\N
311	4	15	1	t	2012-06-18 10:36:01	\N
312	4	16	1	t	2012-06-18 10:36:01	\N
313	4	14	1	t	2012-06-18 10:36:01	\N
314	5	17	1	t	2012-06-18 10:36:01	\N
315	6	8	1	t	2012-06-18 10:36:01	\N
316	1	4	7	t	2012-06-25 12:51:14	\N
317	1	3	7	t	2012-06-25 12:51:14	\N
318	1	2	7	t	2012-06-25 12:51:14	\N
319	1	20	7	t	2012-06-25 12:51:14	\N
320	1	6	7	t	2012-06-25 12:51:14	\N
321	1	5	7	t	2012-06-25 12:51:14	\N
322	2	11	7	t	2012-06-25 12:51:14	\N
323	2	10	7	t	2012-06-25 12:51:14	\N
324	2	9	7	t	2012-06-25 12:51:14	\N
325	2	7	7	t	2012-06-25 12:51:14	\N
326	3	12	7	t	2012-06-25 12:51:14	\N
327	3	13	7	t	2012-06-25 12:51:14	\N
328	3	19	7	t	2012-06-25 12:51:14	\N
329	1	4	5	t	2012-07-04 08:48:55	\N
330	1	3	5	t	2012-07-04 08:48:55	\N
331	1	2	5	t	2012-07-04 08:48:55	\N
332	1	20	5	t	2012-07-04 08:48:55	\N
333	1	6	5	t	2012-07-04 08:48:55	\N
334	1	5	5	t	2012-07-04 08:48:55	\N
335	2	11	5	t	2012-07-04 08:48:55	\N
336	2	10	5	t	2012-07-04 08:48:55	\N
337	2	9	5	t	2012-07-04 08:48:55	\N
338	2	7	5	t	2012-07-04 08:48:55	\N
339	3	12	5	t	2012-07-04 08:48:55	\N
340	3	13	5	t	2012-07-04 08:48:55	\N
341	3	19	5	t	2012-07-04 08:48:55	\N
342	4	15	5	t	2012-07-04 08:48:55	\N
343	4	16	5	t	2012-07-04 08:48:55	\N
344	5	18	5	t	2012-07-04 08:48:55	\N
345	5	17	5	t	2012-07-04 08:48:55	\N
346	6	8	5	t	2012-07-04 08:48:55	\N
347	1	4	8	t	2012-07-05 06:24:29	\N
348	1	3	8	t	2012-07-05 06:24:29	\N
349	1	2	8	t	2012-07-05 06:24:29	\N
350	1	20	8	t	2012-07-05 06:24:29	\N
351	1	6	8	t	2012-07-05 06:24:29	\N
352	1	5	8	t	2012-07-05 06:24:29	\N
353	2	11	8	t	2012-07-05 06:24:29	\N
354	2	10	8	t	2012-07-05 06:24:29	\N
355	1	4	8	t	2012-07-05 06:25:52	\N
356	1	3	8	t	2012-07-05 06:25:52	\N
357	1	2	8	t	2012-07-05 06:25:52	\N
358	1	20	8	t	2012-07-05 06:25:52	\N
359	1	6	8	t	2012-07-05 06:25:52	\N
360	1	5	8	t	2012-07-05 06:25:52	\N
361	2	11	8	t	2012-07-05 06:25:52	\N
362	2	10	8	t	2012-07-05 06:25:52	\N
363	2	9	8	t	2012-07-05 06:25:52	\N
364	2	7	8	t	2012-07-05 06:25:52	\N
365	3	12	8	t	2012-07-05 06:25:52	\N
366	3	13	8	t	2012-07-05 06:25:52	\N
367	3	19	8	t	2012-07-05 06:25:52	\N
368	4	15	8	t	2012-07-05 06:25:52	\N
369	4	16	8	t	2012-07-05 06:25:52	\N
370	5	18	8	t	2012-07-05 06:25:52	\N
371	5	17	8	t	2012-07-05 06:25:52	\N
372	6	8	8	t	2012-07-05 06:25:52	\N
373	1	4	5	t	2012-07-06 06:17:10	\N
374	1	3	5	t	2012-07-06 06:17:10	\N
375	1	2	5	t	2012-07-06 06:17:10	\N
376	1	1	5	t	2012-07-06 06:17:10	\N
377	1	20	5	t	2012-07-06 06:17:10	\N
378	1	6	5	t	2012-07-06 06:17:10	\N
379	1	5	5	t	2012-07-06 06:17:10	\N
380	2	11	5	t	2012-07-06 06:17:10	\N
381	2	10	5	t	2012-07-06 06:17:10	\N
382	2	9	5	t	2012-07-06 06:17:10	\N
383	2	7	5	t	2012-07-06 06:17:10	\N
384	3	12	5	t	2012-07-06 06:17:10	\N
385	3	13	5	t	2012-07-06 06:17:10	\N
386	3	19	5	t	2012-07-06 06:17:10	\N
387	4	15	5	t	2012-07-06 06:17:10	\N
388	4	16	5	t	2012-07-06 06:17:10	\N
389	4	14	5	t	2012-07-06 06:17:10	\N
390	5	18	5	t	2012-07-06 06:17:10	\N
391	5	17	5	t	2012-07-06 06:17:10	\N
392	6	8	5	t	2012-07-06 06:17:10	\N
394	1	3	7	t	2012-07-08 09:41:49	\N
396	1	1	7	t	2012-07-08 09:41:49	\N
398	1	6	7	t	2012-07-08 09:41:49	\N
400	2	11	7	t	2012-07-08 09:41:49	\N
402	2	9	7	t	2012-07-08 09:41:49	\N
404	3	12	7	t	2012-07-08 09:41:49	\N
406	3	19	7	t	2012-07-08 09:41:49	\N
408	4	16	7	t	2012-07-08 09:41:49	\N
410	5	18	7	t	2012-07-08 09:41:49	\N
412	6	8	7	t	2012-07-08 09:41:49	\N
424	1	1	16	t	2012-07-08 10:04:46	\N
425	1	1	16	t	2012-07-08 10:04:46	\N
427	1	3	1	t	2012-07-09 12:07:31	\N
429	1	1	1	t	2012-07-09 12:07:31	\N
431	1	5	1	t	2012-07-09 12:07:31	\N
433	2	10	1	t	2012-07-09 12:07:31	\N
435	2	7	1	t	2012-07-09 12:07:31	\N
437	3	12	1	t	2012-07-09 12:07:31	\N
439	4	14	1	t	2012-07-09 12:07:31	\N
441	4	21	1	t	2012-07-09 12:07:31	\N
443	5	18	1	t	2012-07-09 12:07:31	\N
445	6	8	1	t	2012-07-09 12:07:31	\N
447	1	3	1	t	2012-07-09 04:02:41	\N
449	1	1	1	t	2012-07-09 04:02:41	\N
451	1	6	1	t	2012-07-09 04:02:41	\N
453	2	11	1	t	2012-07-09 04:02:41	\N
455	2	9	1	t	2012-07-09 04:02:41	\N
457	3	13	1	t	2012-07-09 04:02:41	\N
459	3	19	1	t	2012-07-09 04:02:41	\N
461	4	15	1	t	2012-07-09 04:02:41	\N
463	4	16	1	t	2012-07-09 04:02:41	\N
465	5	17	1	t	2012-07-09 04:02:41	\N
468	1	5	5	t	2012-07-11 09:20:25	\N
470	1	3	5	t	2012-07-11 09:20:25	\N
472	1	6	5	t	2012-07-11 09:20:25	\N
474	2	9	5	t	2012-07-11 09:20:25	\N
476	2	11	5	t	2012-07-11 09:20:25	\N
478	3	13	5	t	2012-07-11 09:20:25	\N
480	4	21	5	t	2012-07-11 09:20:25	\N
482	4	14	5	t	2012-07-11 09:20:25	\N
484	5	18	5	t	2012-07-11 09:20:25	\N
486	6	8	5	t	2012-07-11 09:20:25	\N
493	1	1	22	t	2012-07-19 07:55:39	\N
494	5	18	22	t	2012-07-19 07:55:39	\N
496	1	5	5	t	2012-07-19 07:59:23	\N
498	1	3	5	t	2012-07-19 07:59:23	\N
500	1	6	5	t	2012-07-19 07:59:23	\N
502	2	9	5	t	2012-07-19 07:59:23	\N
504	2	11	5	t	2012-07-19 07:59:23	\N
506	3	13	5	t	2012-07-19 07:59:23	\N
508	4	21	5	t	2012-07-19 07:59:23	\N
510	4	14	5	t	2012-07-19 07:59:23	\N
512	5	18	5	t	2012-07-19 07:59:23	\N
514	6	8	5	t	2012-07-19 07:59:23	\N
516	1	5	8	t	2012-09-04 09:43:27	\N
518	1	3	8	t	2012-09-04 09:43:27	\N
520	1	6	8	t	2012-09-04 09:43:27	\N
522	2	9	8	t	2012-09-04 09:43:27	\N
524	2	11	8	t	2012-09-04 09:43:27	\N
526	3	13	8	t	2012-09-04 09:43:27	\N
527	3	12	8	t	2012-09-04 09:43:27	\N
528	4	21	8	t	2012-09-04 09:43:27	\N
529	4	15	8	t	2012-09-04 09:43:27	\N
530	4	14	8	t	2012-09-04 09:43:27	\N
531	4	16	8	t	2012-09-04 09:43:27	\N
532	5	18	8	t	2012-09-04 09:43:27	\N
533	5	17	8	t	2012-09-04 09:43:27	\N
534	6	8	8	t	2012-09-04 09:43:27	\N
535	1	1	3	t	2012-10-09 06:19:18	\N
536	1	5	3	t	2012-10-09 06:19:18	\N
537	1	4	3	t	2012-10-09 06:19:18	\N
538	1	3	3	t	2012-10-09 06:19:18	\N
539	1	2	3	t	2012-10-09 06:19:18	\N
540	1	6	3	t	2012-10-09 06:19:18	\N
541	2	10	3	t	2012-10-09 06:19:18	\N
542	2	9	3	t	2012-10-09 06:19:18	\N
543	2	7	3	t	2012-10-09 06:19:18	\N
544	2	11	3	t	2012-10-09 06:19:18	\N
545	3	19	3	t	2012-10-09 06:19:18	\N
546	3	13	3	t	2012-10-09 06:19:18	\N
547	3	12	3	t	2012-10-09 06:19:18	\N
548	4	21	3	t	2012-10-09 06:19:18	\N
549	4	15	3	t	2012-10-09 06:19:18	\N
550	4	14	3	t	2012-10-09 06:19:18	\N
551	4	16	3	t	2012-10-09 06:19:18	\N
552	5	18	3	t	2012-10-09 06:19:18	\N
553	5	17	3	t	2012-10-09 06:19:18	\N
554	6	8	3	t	2012-10-09 06:19:18	\N
\.


SET search_path = public, pg_catalog;

--
-- Data for Name: tbl_area; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_area (area_id, area) FROM stdin;
0	ninguna
3	Computación
4	Destrezas básicas
5	Psicología
6	Act. de la vida diaria
1	Técn de la comn
2	Orient y movilidad
\.


--
-- Data for Name: tbl_ayuda; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_ayuda (id, medicina_id, pers_disc_visual_id, tipo_ayuda_id, monto, observacion, usuario_id, estatus, created_at, updated_at) FROM stdin;
1	\N	300	2	333	Prueba de sistema	1	t	2012-04-11 03:48:05	\N
2	\N	300	2	333	Prueba de sistema	1	t	2012-04-11 03:52:56	\N
3	3	300	2	222	ddddddddddddddd	1	t	2012-04-11 04:55:28	\N
4	\N	300	2	200	ninguna	3	t	2012-04-23 11:07:05	\N
5	2	300	2	1234	yyyyyyyy	1	t	2012-04-24 10:14:21	\N
6	\N	300	2	120	kjkjkjkj	1	t	2012-04-26 05:25:54	\N
7	\N	300	2	1918	bnbnb	3	t	2012-06-03 08:23:01	\N
8	\N	300	2	200	dc	3	t	2012-06-03 09:02:14	\N
9	\N	300	2	200	dc	3	t	2012-06-03 09:02:23	\N
10	\N	300	2	1918	cddsa	3	t	2012-06-03 09:05:37	\N
11	\N	300	2	1918	cddsa	3	t	2012-06-03 09:06:34	\N
12	\N	300	2	1918	drf	3	t	2012-06-03 09:09:22	\N
13	\N	300	2	1918	drf	3	t	2012-06-03 09:09:36	\N
14	\N	300	2	1918	ed	3	t	2012-06-03 09:16:03	\N
15	2	300	2	1918	edffwfwe	3	t	2012-06-03 09:20:08	\N
16	\N	300	2	200	wdqd	3	t	2012-06-03 09:25:53	\N
17	2	300	2	1918	dcfsf	3	t	2012-06-03 09:29:18	\N
18	2	300	2	1918	dcfsf	3	t	2012-06-03 09:31:13	\N
19	2	300	2	1918	dcfsf	3	t	2012-06-03 09:31:23	\N
20	2	300	2	1918	dcfsf	3	t	2012-06-03 09:37:04	\N
21	\N	304	2	200	Prueba	3	t	2012-06-03 09:40:26	\N
22	\N	304	2	200	Prueba	3	t	2012-06-03 09:41:25	\N
23	\N	304	2	200	Prueba	3	t	2012-06-03 09:42:57	\N
24	\N	304	2	200	Prueba	3	t	2012-06-03 09:43:36	\N
25	\N	304	2	200	Prueba	3	t	2012-06-03 09:44:31	\N
26	\N	304	2	200	Prueba	3	t	2012-06-03 09:46:13	\N
27	\N	304	2	200	Prueba	3	t	2012-06-03 09:46:13	\N
28	\N	304	2	200	Prueba	3	t	2012-06-03 09:48:06	\N
29	\N	304	2	200	Prueba	3	t	2012-06-03 09:48:58	\N
30	\N	304	2	200	Prueba	3	t	2012-06-03 09:49:49	\N
31	\N	304	2	200	Prueba	3	t	2012-06-03 09:50:12	\N
32	\N	304	2	200	Prueba	3	t	2012-06-03 09:52:09	\N
33	\N	304	2	200	Prueba	3	t	2012-06-03 09:53:00	\N
34	\N	304	2	200	Prueba	3	t	2012-06-03 09:53:19	\N
35	\N	304	2	200	Prueba	3	t	2012-06-03 09:53:57	\N
36	\N	304	2	200	Prueba	3	t	2012-06-03 09:54:31	\N
37	\N	304	2	200	Prueba	3	t	2012-06-03 09:54:53	\N
38	\N	304	2	200	Prueba	3	t	2012-06-03 09:55:19	\N
39	\N	304	2	200	Prueba	3	t	2012-06-03 09:56:12	\N
40	\N	304	2	200	Prueba	3	t	2012-06-03 09:57:53	\N
41	\N	304	2	200	Prueba	3	t	2012-06-03 09:58:31	\N
42	\N	304	2	200	Prueba	3	t	2012-06-03 09:59:44	\N
43	\N	304	2	200	Prueba	3	t	2012-06-03 10:02:07	\N
44	\N	304	2	200	Prueba	3	t	2012-06-03 10:04:02	\N
45	\N	304	2	200	Prueba	3	t	2012-06-03 10:04:40	\N
46	\N	304	2	200	Prueba	3	t	2012-06-03 10:05:49	\N
47	\N	300	2	234	edwfwf	3	t	2012-06-03 10:17:39	\N
48	\N	300	2	200	sdcs	3	t	2012-06-03 10:18:38	\N
49	\N	300	2	200	fre	3	t	2012-06-03 10:19:57	\N
50	2	300	1	1918	cvsd	3	t	2012-06-03 10:51:50	\N
51	2	300	1	123	xvf	3	t	2012-06-03 10:54:22	\N
52	3	300	1	123	cdasedef	3	t	2012-06-03 10:55:20	\N
53	3	300	1	124	ewfwf	3	t	2012-06-03 10:58:13	\N
54	3	300	1	156	eew	3	t	2012-06-03 11:00:36	\N
55	\N	300	2	123	ergetgertgerg	3	t	2012-06-03 11:01:52	\N
56	2	300	1	324	wfefwef	3	t	2012-06-03 11:03:48	\N
57	2	300	1	324	wfefwef	3	t	2012-06-03 11:04:07	\N
58	\N	304	4	1234	ddddddddddddddddddddddddddddddd	1	t	2012-06-07 01:02:10	\N
59	\N	304	4	1200	ddddd	1	t	2012-06-07 04:05:45	\N
60	\N	304	2	333	dfs	1	t	2012-06-07 04:08:20	\N
61	\N	300	2	1234	adwdedwedf	1	t	2012-06-07 08:48:16	\N
62	3	300	1	333	ddfsfdsfsff	1	t	2012-06-07 08:50:01	\N
63	\N	300	2	1234	edwd	3	t	2012-06-07 09:54:18	\N
64	\N	300	4	5		1	t	2012-07-05 06:22:40	\N
65	\N	304	4	120		1	t	2012-07-08 11:59:40	\N
66	\N	304	2	1200	dddddd	1	t	2012-07-09 12:00:59	\N
67	\N	304	2	1234	wswww	1	t	2012-07-09 12:14:42	\N
68	2	340	1	1234	alguna	1	t	2012-07-19 07:46:42	\N
69	3	341	1	1234	qeweqw	3	t	2012-10-09 06:18:13	\N
72	3	356	1	1234	sdfsdfsdf	1	t	2012-10-23 06:28:05	\N
73	\N	356	4	1200	sfdsfsfd	1	t	2012-10-23 06:30:43	\N
74	2	356	1	450	asdasdas	1	t	2012-10-23 06:32:50	\N
75	\N	356	4	1234	dfgdfgdfgdfgdfg	1	t	2012-10-23 06:35:03	\N
76	\N	356	4	450	sdfsfdsf	1	t	2012-10-23 06:36:29	\N
77	2	356	1	450	dsfsdfsdfsdf	1	t	2012-10-23 06:38:04	\N
\.


--
-- Data for Name: tbl_ayuda_especial; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_ayuda_especial (id, nacionalidad, nombres, apellidos, monto, fecha_entrega, usuario_id, estatus, created_at, medicina_id, cedula, observacion, tipo_ayuda_id) FROM stdin;
3	V	Prueba	Prueba de sistem	1234	2012-04-18	1	t	2012-04-24 12:40:13	2	1111		\N
4	V	roxenis	Quiaro	1234	2012-04-18	1	t	2012-04-24 10:14:45	\N	456667	yyyyyy	\N
5	V	juju	prueba	100	2012-04-01	5	t	2012-04-26 12:34:50	\N	1234	m	\N
16	V	yutuyu	rrrrr	666	2012-06-06	3	t	2012-06-04 09:59:41	\N	123444	gvgv	2
17	V	roxenis	gregorio	200	2012-06-05	3	t	2012-06-04 10:03:43	\N	65656	edewf	4
18	V	Prueba	Bolivar	1234	2012-06-04	1	t	2012-06-04 10:35:42	\N	12344	cccccc	4
19	V	roxenis	gregorio	200	2012-06-11	3	t	2012-06-04 10:05:37	\N	111	ewfw	4
20	V	roxenis	sanoja	123	2012-06-05	3	t	2012-06-04 10:08:59	\N	2211	dfdf	4
21	V	ffffff	ffffff	22	2012-06-05	3	t	2012-06-04 10:10:54	\N	17999999	2222222	4
22	V	ffffff	ffffff	22	2012-06-05	3	t	2012-06-04 10:11:21	\N	17999999	2222222	4
23	V	ffffff	ffffff	22	2012-06-05	3	t	2012-06-04 10:12:30	\N	17999999	2222222	4
24	V	Gregorio Bolivar	Prueba de sistem	123	2012-06-04	3	t	2012-06-04 10:41:44	\N	67565	mhvhj	4
25	V	Gregorio Bolivar	gregorio	123	2012-06-20	3	t	2012-06-04 10:44:17	2	2211	edeqf	1
26	V	roxenis	prueba	200	2012-06-12	3	t	2012-06-04 10:45:12	2	111	ee	2
27	V	Prueba	prueba	1234	2012-04-18	1	t	2012-06-07 04:06:15	\N	17442933	lll	4
28	V	vicente	PEREZ	100	05-07-2012	5	t	2012-07-05 02:18:54	\N	222222	ayuda de prueba	4
\.


--
-- Data for Name: tbl_detalle_horario; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_detalle_horario (id, hora_inicio, hora_fin, dia_id, area_id, horario_id) FROM stdin;
2	08:00:00	09:00:00	1	0	2
3	09:00:00	10:00:00	1	0	2
4	10:00:00	11:00:00	1	0	2
5	11:00:00	11:45:00	1	0	2
6	12:15:00	01:00:00	1	0	2
7	01:00:00	02:00:00	1	0	2
8	08:00:00	09:00:00	2	3	2
9	09:00:00	10:00:00	2	0	2
10	10:00:00	11:00:00	2	2	2
11	11:00:00	11:45:00	2	1	2
12	12:15:00	01:00:00	2	5	2
13	01:00:00	02:00:00	2	0	2
14	08:00:00	09:00:00	3	0	2
15	09:00:00	10:00:00	3	6	2
16	10:00:00	11:00:00	3	4	2
17	11:00:00	11:45:00	3	3	2
18	12:15:00	01:00:00	3	2	2
19	01:00:00	02:00:00	3	0	2
20	08:00:00	09:00:00	4	5	2
21	09:00:00	10:00:00	4	1	2
22	10:00:00	11:00:00	4	0	2
23	11:00:00	11:45:00	4	4	2
24	12:15:00	01:00:00	4	6	2
25	01:00:00	02:00:00	4	0	2
26	08:00:00	09:00:00	5	0	2
27	09:00:00	10:00:00	5	0	2
28	10:00:00	11:00:00	5	0	2
29	11:00:00	11:45:00	5	0	2
30	12:15:00	01:00:00	5	0	2
31	01:00:00	02:00:00	5	0	2
32	08:00:00	09:00:00	1	6	3
33	09:00:00	10:00:00	1	2	3
34	10:00:00	11:00:00	1	6	3
35	11:00:00	11:45:00	1	4	3
36	12:15:00	01:00:00	1	5	3
37	01:00:00	02:00:00	1	6	3
38	08:00:00	09:00:00	2	6	3
39	09:00:00	10:00:00	2	4	3
40	10:00:00	11:00:00	2	4	3
41	11:00:00	11:45:00	2	6	3
42	12:15:00	01:00:00	2	3	3
43	01:00:00	02:00:00	2	3	3
44	08:00:00	09:00:00	3	3	3
45	09:00:00	10:00:00	3	5	3
46	10:00:00	11:00:00	3	2	3
47	11:00:00	11:45:00	3	4	3
48	12:15:00	01:00:00	3	5	3
49	01:00:00	02:00:00	3	6	3
50	08:00:00	09:00:00	4	5	3
51	09:00:00	10:00:00	4	1	3
52	10:00:00	11:00:00	4	2	3
53	11:00:00	11:45:00	4	3	3
54	12:15:00	01:00:00	4	6	3
55	01:00:00	02:00:00	4	6	3
56	08:00:00	09:00:00	5	5	3
57	09:00:00	10:00:00	5	3	3
58	10:00:00	11:00:00	5	3	3
59	11:00:00	11:45:00	5	2	3
60	12:15:00	01:00:00	5	3	3
61	01:00:00	02:00:00	5	5	3
62	08:00:00	09:00:00	1	6	4
63	09:00:00	10:00:00	1	6	4
64	10:00:00	11:00:00	1	3	4
65	11:00:00	11:45:00	1	3	4
66	12:15:00	01:00:00	1	3	4
67	01:00:00	02:00:00	1	6	4
68	08:00:00	09:00:00	2	3	4
69	09:00:00	10:00:00	2	6	4
70	10:00:00	11:00:00	2	3	4
71	11:00:00	11:45:00	2	6	4
72	12:15:00	01:00:00	2	3	4
73	01:00:00	02:00:00	2	5	4
74	08:00:00	09:00:00	3	6	4
75	09:00:00	10:00:00	3	3	4
76	10:00:00	11:00:00	3	6	4
77	11:00:00	11:45:00	3	4	4
78	12:15:00	01:00:00	3	3	4
79	01:00:00	02:00:00	3	3	4
80	08:00:00	09:00:00	4	3	4
81	09:00:00	10:00:00	4	0	4
82	10:00:00	11:00:00	4	4	4
83	11:00:00	11:45:00	4	5	4
84	12:15:00	01:00:00	4	5	4
85	01:00:00	02:00:00	4	4	4
86	08:00:00	09:00:00	5	3	4
87	09:00:00	10:00:00	5	6	4
88	10:00:00	11:00:00	5	3	4
89	11:00:00	11:45:00	5	1	4
90	12:15:00	01:00:00	5	1	4
91	01:00:00	02:00:00	5	4	4
92	08:00:00	09:00:00	1	0	7
93	09:00:00	10:00:00	1	0	7
94	10:00:00	11:00:00	1	0	7
95	11:00:00	11:45:00	1	0	7
96	12:15:00	01:00:00	1	0	7
97	01:00:00	02:00:00	1	0	7
98	08:00:00	09:00:00	2	0	7
99	09:00:00	10:00:00	2	0	7
100	10:00:00	11:00:00	2	0	7
101	11:00:00	11:45:00	2	0	7
102	12:15:00	01:00:00	2	0	7
103	01:00:00	02:00:00	2	0	7
104	08:00:00	09:00:00	3	0	7
105	09:00:00	10:00:00	3	0	7
106	10:00:00	11:00:00	3	0	7
107	11:00:00	11:45:00	3	0	7
108	12:15:00	01:00:00	3	0	7
109	01:00:00	02:00:00	3	0	7
110	08:00:00	09:00:00	4	0	7
111	09:00:00	10:00:00	4	0	7
112	10:00:00	11:00:00	4	0	7
113	11:00:00	11:45:00	4	0	7
114	12:15:00	01:00:00	4	0	7
115	01:00:00	02:00:00	4	0	7
116	08:00:00	09:00:00	5	0	7
117	09:00:00	10:00:00	5	0	7
118	10:00:00	11:00:00	5	0	7
119	11:00:00	11:45:00	5	0	7
120	12:15:00	01:00:00	5	0	7
121	01:00:00	02:00:00	5	0	7
122	08:00:00	09:00:00	1	6	9
123	09:00:00	10:00:00	1	4	9
124	10:00:00	11:00:00	1	0	9
125	11:00:00	11:45:00	1	0	9
126	12:15:00	01:00:00	1	0	9
127	01:00:00	02:00:00	1	0	9
128	08:00:00	09:00:00	2	0	9
129	09:00:00	10:00:00	2	0	9
130	10:00:00	11:00:00	2	2	9
131	11:00:00	11:45:00	2	0	9
132	12:15:00	01:00:00	2	0	9
133	01:00:00	02:00:00	2	2	9
134	08:00:00	09:00:00	3	0	9
135	09:00:00	10:00:00	3	5	9
136	10:00:00	11:00:00	3	0	9
137	11:00:00	11:45:00	3	4	9
138	12:15:00	01:00:00	3	0	9
139	01:00:00	02:00:00	3	0	9
140	08:00:00	09:00:00	4	3	9
141	09:00:00	10:00:00	4	0	9
142	10:00:00	11:00:00	4	4	9
143	11:00:00	11:45:00	4	0	9
144	12:15:00	01:00:00	4	4	9
145	01:00:00	02:00:00	4	0	9
146	08:00:00	09:00:00	5	6	9
147	09:00:00	10:00:00	5	0	9
148	10:00:00	11:00:00	5	0	9
149	11:00:00	11:45:00	5	4	9
150	12:15:00	01:00:00	5	0	9
151	01:00:00	02:00:00	5	2	9
\.


--
-- Data for Name: tbl_dia; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_dia (id, dia) FROM stdin;
1	Lunes
2	Martes
3	Miércoles
4	Jueves
5	Viernes
\.


--
-- Data for Name: tbl_diag_medico; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_diag_medico (diag_medico_id, pers_disc_visual_id, medico_id, tipo_ceguera_id, esta_rehabilitado, otra_enfermedad, diagnostico, pronostico, campo_visual_od, campo_visual_oi, agudez_visual_od, agudez_visual_id, usa_lentes, consideracion_visual, usuario_id, estatus, created_at) FROM stdin;
22	307	1	2	\N	\N	ceguera		20	20	01	01	f	CIEGO	5	t	2012-04-26 06:30:08
18	300	1	4	\N	\N	sdfsdf		sdfsdf	sdfsdf	sdfsdf	sdfsdfsdf	t	CIEGO	1	t	2012-04-19 04:24:56
23	304	1	2	\N	\N	ALGUNO		bjbj	klkl	b	lkjlkjlk	t	DEFICIENTE VISUAL	3	t	2012-06-17 08:39:32
20	306	1	3	\N	\N	algun diagnostico		kkk	lll	kk	kkk	t	DEFICIENTE VISUAL	3	t	2012-04-22 04:49:57
25	313	18	1	\N	\N	DSFSDFSDFSDFDSF		20	20	20	20	t	CIEGO	1	t	2012-07-12 08:41:10
26	337	18	1	\N	\N	HGJ		7	7	7	7	f	CIEGO	1	t	2012-07-12 08:50:36
27	338	18	5	\N	\N	jkhjkh		7	7	7	7	f	CIEGO	1	t	2012-07-12 09:02:58
28	340	20	1	\N	\N	algun dignostico		0.10	0.10	0.10	0.10	f	CIEGO	1	t	2012-07-19 06:58:52
29	341	19	2	\N	\N	algun diagnostico		2	2	0.10	7	t	DEFICIENTE VISUAL	3	t	2012-10-09 06:14:48
\.


--
-- Data for Name: tbl_estado; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_estado (co_estado, nb_estado, ca_estado, abreviatura) FROM stdin;
1	DISTRITO CAPITAL	CARACAS	DC
2	AMAZONAS	PUERTO AYACUCHO	AM
3	ANZOATEGUI	BARCELONA	AN
6	BARINAS	BARINAS	BA
9	COJEDES	SAN CARLOS	CO
12	GUARICO	SAN JUAN DE LOS MORROS	GU
15	MIRANDA	LOS TEQUES	MI
24	VARGAS	LA GUAIRA	VA
18	PORTUGUESA	GUANARE	PO
4	APURE	SAN FERNANDO	AP
8	CARABOBO	VALENCIA	CA
11	FALCON	SANTA ANA DE CORO	FA
5	ARAGUA	MARACAY	AR
7	BOLIVAR	CIUDAD BOLIVAR	BO
10	DELTA AMACURO	TUCUPITA	DA
13	LARA	BARQUISIMETO	LA
14	MERIDA	MERIDA	ME
16	MONAGAS	MATURIN	MO
17	NUEVA ESPARTA	LA ASUNCION	NE
19	SUCRE	CUMANA	SU
20	TACHIRA	SAN CRISTOBAL	TA
21	TRUJILLO	TRUJILLO	TR
22	YARACUY	SAN FELIPE	YA
23	ZULIA	MARACAIBO	ZU
\.


--
-- Data for Name: tbl_estudio_persona; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_estudio_persona (estudio_id, pers_disc_visual_id, estudia, grado_inst_id, lugar_estudia, estudio_realiza, usuario_id, estatus, created_at, updated_at) FROM stdin;
31	300	NO	0			1	t	2012-04-08 01:37:58	\N
33	305	NO	0			1	t	2012-04-11 07:06:42	\N
34	306	NO	0			5	t	2012-04-22 04:43:18	\N
35	307	SI	0			5	t	2012-04-26 06:27:16	\N
36	308	NO	0			1	t	2012-05-18 03:45:32	\N
37	309	NO	0			1	t	2012-07-05 05:31:35	\N
38	310	NO	0			1	t	2012-07-09 02:16:23	\N
39	311	NO	0			1	t	2012-07-10 07:34:58	\N
40	312	SI	6	CARACAS	TECNICO MEDIO	4	t	2012-07-10 11:25:40	\N
42	316	SI	0			5	t	2012-07-11 10:19:04	\N
43	317	SI	0			5	t	2012-07-11 10:35:44	\N
44	321	SI	0			5	t	2012-07-11 10:47:11	\N
45	322	NO	0			1	t	2012-07-11 11:47:20	\N
46	323	NO	0			1	t	2012-07-11 11:48:36	\N
47	324	SI	0			5	t	2012-07-12 12:04:09	\N
41	313	SI	11	CARACAS	TUTYU	1	t	2012-07-10 11:33:18	\N
48	329	SI	0			5	t	2012-07-12 12:19:16	\N
49	337	NO	0			1	t	2012-07-12 08:20:28	\N
50	338	NO	0			1	t	2012-07-12 08:58:51	\N
51	339	NO	0			1	t	2012-07-18 11:01:08	\N
52	340	NO	0			5	t	2012-07-19 06:36:48	\N
53	341	NO	0			3	t	2012-10-08 07:47:14	\N
54	352	NO	0			1	t	2012-10-23 04:47:28	\N
55	353	NO	0			1	t	2012-10-23 05:06:02	\N
56	354	NO	0			1	t	2012-10-23 05:08:35	\N
57	356	NO	0			1	t	2012-10-23 05:41:36	\N
32	304	NO	0			1	t	2012-04-08 01:47:27	\N
58	357	NO	0			1	t	2012-10-23 07:21:10	\N
\.


--
-- Data for Name: tbl_grado_instruccion; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_grado_instruccion (grado_inst_id, grado_instruccion) FROM stdin;
1	SIN INSTRUCCION
2	PRIMARIA INCOMPLETA
3	PRIMARIA COMPLETA
4	SECUNDARIA INCOMPLETA
5	SECUNDARIA COMPLETA
6	TECNICO MEDIO
7	TECNICO SUPERIOR
8	UNIVERSITARIO
10	PREESCOLAR
11	NO APLICA
12	SIN RESPUESTA
0	Ninguno
\.


--
-- Data for Name: tbl_grupo_familiar; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_grupo_familiar (familiar_id, pers_disc_visual_id, parentesco_id, nombre, apellido, nacionalidad, fech_nacimiento, sexo, direccion, telefono, estudia, esp_estudia, trabaja, oficio_id, direcc_trabajo, ingreso, sufre_ceguera, usuario_id, estatus, created_at, updated_at, num_hijo) FROM stdin;
13	287	4	Sistema	Prueba de sistem	E	1993-03-19	M	Prueb	4123345678	t	kjkjkjkkjkjkjjk	t	12	ddddddddd	12222	t	1	f	2012-03-11 06:17:07	\N	\N
14	287	14	Pry	cccccccc	V	1993-03-17	M	Prueb	4125678909	t	fff	t	6	ddddddddd	12222	t	1	f	2012-03-11 06:17:40	\N	\N
15	287	5	oooooooooo	aaaaaaaaaa	V	1993-03-19	M	Prueb	66666	t	fff	t	4	ddddddddd	12222	t	1	f	2012-03-12 08:14:26	\N	\N
4	287	12	Prueba Drupo	Familar	V	1993-03-19	M	Prueb	9090090	t	fff	t	6	ddddddddd	12222	t	1	f	2012-03-10 11:16:39	\N	\N
5	287	12	Prueba Drupo	Familar	V	1993-03-19	M	Prueb	9090090	t	fff	t	6	ddddddddd	12222	t	1	f	2012-03-10 11:16:51	\N	\N
12	287	12	Prueba Drupo	Familar	V	1993-03-19	M	Prueb	9090090	t	fff	t	6	ddddddddd	12222	t	1	f	2012-03-11 06:11:57	\N	\N
17	288	12	Prueba Drupo	Familar	V	1993-03-19	M	Prueb	9090090	t	fff	t	6	ddddddddd	12222	t	1	t	2012-03-13 10:56:17	\N	\N
19	287	8	prueba2	Prueba2	V	1993-03-12	M	ddddd	98989	t		t	6		989	t	1	t	2012-03-14 03:39:06	\N	\N
18	288	12	Prueba Drupo	Familar	V	1993-03-19	M	Prueb	9090090	t	fff	t	6	ddddddddd	12222	t	1	t	2012-03-13 10:57:14	\N	\N
20	287	13	fffffff	prueba	V	2012-03-16	F	dkjdkjdkdj	98989	t		t	13		0	t	1	f	2012-03-14 06:23:42	\N	\N
16	287	9	Prueba Drupo	Familar	V	1993-03-19	M	Prueb	9090090	t	fff	t	6	ddddddddd	12222	t	1	t	2012-03-12 08:17:39	\N	\N
46	304	9	Yarelis	Bolivar	V	2000-06-03	F		0	f		f	15		0	f	1	t	2012-06-13 09:49:30	\N	03
44	304	1	roxenis	Prueba de sistem	V	2000-06-03	M	sddd	0	f		f	0		0	f	1	t	2012-06-07 09:25:16	\N	01
45	304	10	GRELIMAR 	BOLIVAR G	V	2007-03-15	F		0	t	1 Grado	f	11		0	f	1	t	2012-06-13 09:47:57	\N	02
49	340	8	JUANA MARIA	MENDOZA	V	1976-03-17	M	av v	41245556600	t	cualquiera	t	4	alguna direccion	1222	f	1	t	2012-07-19 06:51:38	\N	\N
47	310	13	grelimar	kjkjkjkjk	V	2008-07-17	M		412455566	f		f	10		658	t	1	t	2012-07-09 02:19:36	\N	\N
48	313	17	ARIDANES	ROBERTO	V	1976-01-14	M		412556789	f		f	8		1200	f	1	f	2012-07-11 12:26:08	\N	\N
50	341	2	PEDROPE	PPPP	V	1994-03-15	M	efewnnn	0	f		t	15	ee	444	f	3	t	2012-10-09 06:13:50	\N	\N
\.


--
-- Data for Name: tbl_horario; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_horario (id, observacion, estatus, tipo_horario_id, usuario_id, created_up, updated_up) FROM stdin;
2	Horario de Rehabilitación Funcional	t	1	1	2012-06-03 07:02:15	\N
3	horario de prueba	t	2	5	2012-07-05 02:47:33	\N
4		t	4	5	2012-07-11 10:58:13	\N
7		t	7	5	2012-07-11 10:58:20	\N
9	alguna	t	3	1	2012-07-19 07:45:07	\N
\.


--
-- Data for Name: tbl_laboral; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_laboral (laboral_id, pers_disc_visual_id, trabaja, empresa_trabaja, direccion_trabajo, telef_trabajo, fecha_ingreso, ingresos, oficio_id, profesion_id, usuario_id, estatus, created_at) FROM stdin;
7	226	NO			0	\N	0	0	0	1	t	2012-02-24 01:00:05
8	228	NO			0	\N	0	0	0	1	t	2012-02-24 01:01:11
9	229	NO			0	\N	0	0	0	1	t	2012-02-24 01:01:52
10	230	NO			0	\N	0	0	0	1	t	2012-02-24 01:02:17
11	231	NO			0	\N	0	0	0	1	t	2012-02-24 01:02:40
12	232	NO			0	\N	0	0	0	1	t	2012-02-24 01:03:14
13	234	NO			0	\N	0	0	0	1	t	2012-02-24 01:04:34
14	235	NO			0	\N	0	0	0	1	t	2012-02-24 01:05:02
15	236	NO			0	\N	0	0	0	1	t	2012-02-24 01:07:55
16	237	NO			0	\N	0	0	0	1	t	2012-02-24 01:12:53
17	238	NO			0	\N	0	0	0	1	t	2012-02-24 01:15:59
18	239	NO			0	\N	0	0	0	1	t	2012-02-24 01:17:00
19	241	NO			0	\N	0	0	0	1	t	2012-02-24 01:19:32
20	242	NO			0	\N	0	0	0	1	t	2012-02-24 01:24:51
21	244	NO			0	\N	0	0	0	1	t	2012-02-24 01:26:08
22	245	NO			0	\N	0	0	0	1	t	2012-02-24 01:27:35
24	247	NO			0	\N	0	0	0	1	t	2012-02-24 01:34:59
25	254	NO			0	\N	0	0	0	1	t	2012-02-24 03:44:15
26	257	NO			0	\N	0	0	0	1	t	2012-02-24 03:53:44
27	269	NO			0	\N	0	0	0	1	t	2012-02-24 05:06:33
28	272	NO			0	\N	0	0	0	1	t	2012-02-24 08:15:11
29	273	NO			0	\N	0	0	0	1	t	2012-02-26 01:44:17
23	246	NO	FNC	Prueba del sistema 	02123456789	2012-02-20	6000	0	0	1	t	2012-02-24 01:28:25
30	274	NO			0	\N	0	0	0	1	t	2012-02-26 11:04:40
31	275	NO			0	\N	0	0	0	1	t	2012-02-27 12:15:34
32	276	NO			0	\N	0	0	0	1	t	2012-02-29 07:44:14
33	286	NO			0	\N	0	0	0	1	t	2012-03-10 10:38:14
34	287	NO			0	\N	0	0	0	1	t	2012-03-10 10:57:01
35	288	NO			0	\N	0	0	0	1	t	2012-03-13 10:54:12
36	292	SI	dayco	las mercedes	9999000	\N	5000	0	238	1	t	2012-03-14 06:42:27
37	293	NO			0	\N	0	0	0	1	t	2012-03-14 06:45:14
38	294	SI	caradura	caracas	04127890989	1993-01-01	1233	15	239	1	t	2012-03-25 02:11:43
39	299	SI	daycohost	las mercedes caracas	02129999000	\N	0	15	0	1	t	2012-04-06 03:43:25
40	300	NO			0	\N	0	0	0	1	t	2012-04-08 01:37:58
42	305	NO			0	\N	0	0	0	1	t	2012-04-11 07:06:42
43	306	NO			0	\N	0	0	0	5	t	2012-04-22 04:43:18
44	307	NO			0	\N	0	0	0	5	t	2012-04-26 06:27:16
45	308	NO			0	\N	0	0	0	1	t	2012-05-18 03:45:32
46	309	NO			0	\N	0	0	0	1	t	2012-07-05 05:31:35
47	310	NO			0	\N	0	0	0	1	t	2012-07-09 02:16:23
48	311	NO			0	\N	0	0	0	1	t	2012-07-10 07:34:58
49	312	SI	MIA	AVENIDA UNIVERSIDAD	02121231212	\N	5014	1	2	4	t	2012-07-10 11:25:40
51	316	SI			0	\N	0	0	0	5	t	2012-07-11 10:19:04
52	317	SI			0	\N	0	0	0	5	t	2012-07-11 10:35:44
53	321	SI			0	\N	0	0	0	5	t	2012-07-11 10:47:11
54	322	NO			0	\N	0	0	0	1	t	2012-07-11 11:47:20
55	323	NO			0	\N	0	0	0	1	t	2012-07-11 11:48:36
56	324	SI			0	\N	0	0	0	5	t	2012-07-12 12:04:09
50	313	SI	MIA	TYUTY	02121231212	\N	5014	8	2	1	t	2012-07-10 11:33:18
57	329	SI			0	\N	0	0	0	5	t	2012-07-12 12:19:16
58	337	NO			0	\N	0	0	0	1	t	2012-07-12 08:20:28
59	338	NO			0	\N	0	0	0	1	t	2012-07-12 08:58:51
60	339	NO			0	\N	0	0	0	1	t	2012-07-18 11:01:08
61	340	NO			0	\N	0	0	0	5	t	2012-07-19 06:36:48
62	341	NO			0	\N	0	0	0	3	t	2012-10-08 07:47:14
63	352	NO			0	\N	0	0	0	1	t	2012-10-23 04:47:28
64	353	NO			0	\N	0	0	0	1	t	2012-10-23 05:06:02
65	354	NO			0	\N	0	0	0	1	t	2012-10-23 05:08:35
66	356	NO			0	\N	0	0	0	1	t	2012-10-23 05:41:36
41	304	SI	Che Guevara		0	2012-07-02	0	0	0	1	t	2012-04-08 01:47:27
67	357	NO			0	\N	0	0	0	1	t	2012-10-23 07:21:10
\.


--
-- Data for Name: tbl_medicina; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_medicina (medicina_id, medicina, created_at) FROM stdin;
2	Celestamine                                                                     	2012-04-11
3	Tantun                                                                          	2012-04-11
\.


--
-- Data for Name: tbl_medico; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_medico (medico_id, med_nacionalidad, med_cedula, med_nombre, usuario_id, estatus, created_at) FROM stdin;
1	V	123456	Prueba	1	t	2012-12-12 00:00:00
14	V	1234567	DFGDFGDF	1	t	2012-07-11 10:52:19
16	V	44444	FFFFF	1	t	2012-07-11 10:52:39
17	V	657567	PRURBA DE SISTEMA	1	t	2012-07-12 12:47:52
18	V	7123123	YINNY	1	t	2012-07-12 08:28:12
19	V	234567	MATA FIGUEROA	1	t	2012-07-19 12:24:47
20	V	13000001	VALLADARES MEBIL	1	t	2012-07-19 06:53:18
\.


--
-- Data for Name: tbl_municipio; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_municipio (co_estado, co_municipio, nb_municipio, ca_municipio) FROM stdin;
9	92	Falcon	Tinaquillo
1	1	Libertador	Caracas
9	93	Girardot	El Baul
9	94	Lima Blanco	Macapo
9	95	Pao de San Juan Bautista	El Pao
9	96	Ricaurte	Libertad
9	97	Romulo Gallegos	Las Vegas
9	98	San Carlos	San Carlos
9	99	Tinaco	Tinaco
10	100	Antonio Diaz	Curiapo
10	101	Casacoima	Sierra Imataca
10	102	Pedernales	Pedernales
10	103	Tucupita	Tucupita
11	104	Acosta	San Juan  de los Cayos
11	106	Buchivacoa	Capatárida
11	107	Cacique Manaure	Yaracal
11	108	Carirubana	Punto Fijo
11	109	Colina	La Vela de Coro
11	110	Dabajuro	Dabajuro
11	111	Democracia	Pedregal
11	112	Falcon	Pueblo Nuevo
11	115	Los Taques	Santa Cruz de Los Taques
11	116	Mauroa	Mene de Mauroa
11	117	Miranda	Santa Ana de Coro
11	118	Monseñor Iturriza	Chichiriviche
11	119	Palmasola	Palmasola
11	120	Petit	Cabure
11	122	San Francisco	Mirimire
11	123	Silva	Tucacas
11	124	Sucre	La Cruz de Taratara
11	127	Urumaco	Urumaco
11	128	Zamora	Puerto Cumarebo
12	130	Chaguaramas	Chaguaramas
12	131	El Socorro	El Socorro
12	133	Leonardo Infante	Valle de La Pascua
12	134	Las Mercedes	Las Mercedes
12	136	Francisco de Miranda	Calabozo
12	138	Ortiz	Ortiz
12	143	Pedro Zaraza	Zaraza
13	145	Crespo	Duaca
13	146	Iribarren	Barquisimeto
13	149	Palavecino	Cabudare
13	151	Torres	Carora
13	152	Urdaneta	Siquisique
24	333	Municipio Vargas	La Guaira
2	6	Autonomo Maroa	Maroa
2	5	Autonomo Autana	Isla Raton
2	4	Autonomo Atures	Puerto Ayacucho
3	28	Sir Arthur Mc Gregor	El Chaparro
3	27	Simon Rodriguez	El Tigre
3	26	Simon Bolivar	Barcelona
3	25	Santa Ana	Santa Ana
3	24	San Juan de Capistrano	Boca de Uchire
3	23	San Jose de Guanipa	San Jose de Guanipa (El Tigrito)
3	22	Piritu	Piritu
3	21	Pedro Maria Freites	Cantaura
3	20	Manuel Ezequiel Bruzual	Clarines
3	19	Libertad	San Mateo
3	18	Jose Gregorio Monagas	Mapire
3	17	Juan Manuel Cajigal	Onoto
3	16	Juan Antonio Sotillo	Puerto La Cruz
3	15	Independencia	Soledad
3	14	Guanta	Guanta
3	13	Francisco de Miranda	Pariaguan
3	11	Fernando de Peñalver	Puerto Píritu
3	10	Aragua	Aragua de Barcelona
3	9	Anaco	Anaco
4	30	Achaguas	Achaguas
4	31	Biruaca	Biruaca
4	32	Muñoz	Bruzual
4	33	Paez	Guasdualito
4	34	Pedro Camejo	San Juan de Payara
4	35	Romulo Gallegos	Elorza
4	36	San Fernando	San Fernando de Apure
5	37	Bolivar	San Mateo
5	38	Camatagua	Camatagua
5	39	Girardot	Maracay
5	40	Jose Angel Lamas	Santa Cruz
5	41	Jose Felix Ribas	La Victoria
5	42	Jose Rafael Revenga	El Consejo
5	43	Libertador	Palo Negro
5	44	Mario Briceño Iragorry	El Limon
5	45	San Casimiro	San Casimiro
5	46	San Sebastian	San Sebastian
5	47	Santiago Mariño	Turmero
5	49	Sucre	Cagua
5	50	Tovar	La Colonia Tovar
5	51	Urdaneta	Barbacoas
5	52	Zamora	Villa de Cura
5	53	Francisco Linares Alcantara	Santa Rita
5	54	Ocumare de La Costa de Oro	Ocumare de la Costa
6	55	Alberto Arvelo Torrealba	Sabaneta
6	56	Antonio Jose de Sucre	Socopo
6	57	Arismendi	Arismendi
6	58	Barinas	Barinas
6	59	Bolivar	Barinitas
6	60	Cruz Paredes	Barrancas
6	61	Ezequiel Zamora	Santa Barbara
6	62	Obispos	Obispos
6	63	Pedraza	Ciudad Bolivia
6	64	Rojas	Libertad
6	65	Sosa	Ciudad de Nutrias
7	67	Caroni	Ciudad Guayana
7	68	Cedeño	Caicara del Orinoco
7	69	El Callao	El Callao
7	70	Gran Sabana	Santa Elena de Uairen
7	71	Heres	Ciudad Bolivar
7	72	Piar	Upata
7	73	Roscio	Guasipati
7	74	Sifontes	Tumeremo
7	75	Sucre	Maripa
7	76	Padre Pedro Chien	El Palmar
7	335	Raul Leoni	Ciudad Piar
8	77	Bejuma	Bejuma
8	78	Carlos Arvelo	Güigüe
8	79	Diego Ibarra	Mariara
8	80	Guacara	Guacara
8	82	Libertador	Tocuyito
8	83	Los Guayos	Los Guayos
8	84	Miranda	Miranda
8	86	Naguanagua	Naguanagua
8	87	Puerto Cabello	Puerto Cabello
8	88	San Diego	San Diego
8	89	San Joaquin	San Joaquin
8	90	Valencia	Valencia
9	91	Anzoategui	Cojedes
17	210	Antolin del Campo	La Plaza de Paraguachi
17	211	Arismendi	La Asunción
17	212	Diaz	San Juan Bautista
17	213	Garcia	El Valle del Espiritu Santo
17	214	Gomez	Santa Ana
17	215	Maneiro	Pampatar
17	216	Marcano	Juangriego
17	217	Mariño	Porlamar
17	218	Peninsula de Macanao	Boca del Rio
17	219	Tubores	Punta de Piedras
17	220	Villalba	San Pedro de Coche
18	221	Agua Blanca	Agua Blanca
18	222	Araure	Araure
18	223	Esteller	Píritu
18	224	Guanare	Guanare
18	225	Guanarito	Guanarito
18	227	Ospino	Ospino
18	228	Paez	Acarigua
18	229	Papelon	Papelon
18	230	San Genaro de Boconoito	Boconoito
18	231	San Rafael de Onoto	San Rafael de Onoto
18	232	Santa Rosalia	El Playon
18	233	Sucre	Biscucuy
18	234	Turen	Villa Bruzual
19	237	Arismendi	Río Caribe
19	241	Cajigal	Yaguaraparo
19	243	Libertador	Tunapuy
19	244	Mariño	Irapa
19	246	Montes	Cumanacoa
19	247	Ribero	Cariaco
19	249	Valdez	Güiria
20	251	Antonio Rómulo Costa	Las Mesas
20	252	Ayacucho	Colón
20	253	Bolívar	San Antonio del Táchira
20	260	Independencia	Capacho Nuevo
20	264	Libertad	Capacho Viejo
20	265	Libertador	Abejales
20	266	Lobatera	Lobatera
20	267	Michelena	Michelena
20	268	Panamericano	Coloncito
20	270	Rafael Urdaneta	Delicias
20	273	Seboruco	Seboruco
20	275	Sucre	Queniquea
20	276	Torbes	San Josecito
20	277	Uribante	Pregonero
20	278	San Judas Tadeo	Umuquena
21	283	Carache	Carache
21	284	Escuque	Escuque
21	287	La Ceiba	Santa Apolonia
21	288	Miranda	El Dividive
21	289	Monte Carmelo	Monte Carmelo
21	292	Pampanito	Pampanito
21	293	Rafael Rangel	Betijoque
21	294	San Rafael de Carvajal	Carvajal
21	295	Sucre	Sabana de Mendoza
21	296	Trujillo	Trujillo
21	297	Urdaneta	La Quebrada
21	298	Valera	Valera
22	301	Bruzual	Chivacoa
22	302	Cocorote	Cocorote
22	303	Independencia	Independencia
22	305	La Trinidad	Boraure
22	306	Manuel Monge	Yumare
11	113	Federacion	Churuguara
11	121	Piritu	Piritu
11	125	Tocopero	Tocopero
11	126	Union	Santa Cruz de Bucaral
12	129	Camaguan	Camaguán
12	132	San Geronimo de Guayabal	Guayabal
12	135	Julian Mellado	El Sombrero
12	139	Jose Felix Ribas	Tucupido
12	141	San Jose de Guaribe	San José de Guaribe
12	142	Santa Maria de Ipire	Santa María de Ipire
13	144	Andres Eloy Blanco	Sanare
13	147	Jimenez	Quíbor
13	148	Moran	El Tocuyo
13	150	Simon Planas	Sarare
14	153	Alberto Adriani	El Vigía
14	154	Andres Bello	La Azulita
14	155	Antonio Pinto Salinas	Santa Cruz de Mora
14	156	Aricagua	Aricagua
14	157	Arzobispo Chacon	Canagua
14	158	Campo Elias	Ejido
14	159	Caracciolo Parra Olmedo	Tucani
14	160	Cardenal Quintero	Santo Domingo
14	161	Guaraque	Guaraque
14	162	Julio Cesar Salas	Arapuey
14	163	Justo Briceño	Torondoy
14	164	Libertador	Mérida
14	165	Miranda	Timotes
14	167	Padre Noguera	Santa Maria de Caparo
14	168	Pueblo Llano	Pueblo Llano
14	169	Rangel	Mucuchíes
14	170	Rivas Davila	Bailadores
14	171	Santos Marquina	Tabay
14	172	Sucre	Lagunillas
14	173	Tovar	Tovar
14	174	Tulio Febres Cordero	Nueva Bolivia
14	175	Zea	Zea
15	176	Acevedo	Caucagua
15	177	Andres Bello	San Jose de Barlovento
15	178	Baruta	Nuestra Señora del Rosario de Baruta
15	179	Brion	Higuerote
15	180	Buroz	Mamporal
15	181	Carrizal	Carrizal
15	182	Chacao	Chacao
15	183	Cristobal Rojas	Charallave
15	184	El Hatillo	El Hatillo
15	185	Guaicaipuro	Los Teques
15	187	Lander	Ocumare del Tuy
15	188	Los Salias	San Antonio de Los Altos
15	189	Paez	Rio Chico
15	190	Paz Castillo	Santa Lucia
15	191	Pedro Gual	Cupira
15	192	Plaza	Guarenas
15	193	Simon Bolivar	San Francisco de Yare
15	194	Sucre	Petare
15	195	Urdaneta	Cúa
15	196	Zamora	Guatire
16	197	Acosta	San Antonio
16	198	Aguasay	Aguasay
16	199	Bolivar	Caripito
16	200	Caripe	Caripe
16	201	Cedeño	Caicara
16	202	Ezequiel Zamora	Punta de Mata
16	203	Libertador	Temblador
16	204	Maturin	Maturin
16	205	Piar	Aragua
16	206	Punceres	Quiriquire
16	208	Sotillo	Barrancas
16	209	Uracoa	Uracoa
23	317	Francisco Javier Pulgar	Pueblo Nuevo El Chivo
23	318	Jesus Enrique Lossada	La Concepcion
23	319	Jesus Maria Semprun	Casigua El Cubo
23	320	La Cañada de Urdaneta	Concepcion
23	321	Lagunillas	Ciudad Ojeda
23	322	Machiques de Perija	Machiques
23	323	Mara	San Rafael de El Mojan
23	324	Maracaibo	Maracaibo
23	325	Miranda	Los Puertos de Altagracia
11	105	Bolivar	San Luis
23	326	Paez	Sinamaica
11	114	Jacura	Jacura
12	137	Jose Tadeo Monagas	Altagracia de Orituco
18	226	Monseñor Jose Vicente de Unda	Paraiso de Chabasquen
19	235	Andres Eloy Blanco	Casanay
23	327	Rosario de Perija	La Villa del Rosario
23	328	San Francisco	San Francisco
23	329	Santa Rita	Santa Rita
23	330	Simon Bolivar	Tia Juana
23	331	Sucre	Bobures
23	332	Valmore Rodriguez	Bachaquero
12	140	Juan German Roscio	San Juan de los Morros
14	166	Obispo Ramos de Lora	Santa Elena de Arenales
15	186	Independencia	Santa Teresa del Tuy
16	207	Santa Bárbara	Santa Bárbara
19	236	Andres Mata	San Jose de Aerocuar
19	238	Benitez	El Pilar
19	239	Bermudez	Carupano
19	240	Bolivar	Marigüitar
2	8	Autonomo Rio Negro	San Carlos de Rio Negro
2	7	Autonomo Manapiare	San Juan de Manapiare
2	3	Autonomo Atabapo	San Fernando de Atabapo
2	2	Autonomo Alto Orinoco	La Esmeralda
3	29	Diego Bautista Urbaneja	Lecherias
3	12	Francisco del Carmen Carvajal	Valle de Guanape
5	48	Santos Michelena	Las Tejerias
6	66	Andres Eloy Blanco	El Canton
8	81	Juan Jose Mora	Moron
8	85	Montalban	Montalban
19	242	Cruz Salmeron Acosta	Araya
19	245	Mejia	San Antonio del Golfo
19	248	Sucre	Cumana
20	250	Andres Bello	Cordero
20	254	Cardenas	Tariba
20	255	Cordoba	Santa Ana
20	256	Fernandez Feo	San Rafael del Piñal
20	257	Francisco de Miranda	San Jose de Bolivar
20	258	Garcia de Hevia	La Fria
20	259	Guasimos	Palmira
20	261	Jauregui	La Grita
20	262	Jose Maria Vargas	EL Cobre
20	263	Junin	Rubio
20	269	Pedro Maria Ureña	Ureña
20	271	Samuel Dario Maldonado	La Tendida
20	272	San Cristobal	San Cristobal
20	274	Simon Rodriguez	San Simon
21	279	Andres Bello	Santa Isabel
21	280	Bocono	Bocono
21	281	Bolivar	Sabana Grande
21	282	Candelaria	Chejende
21	285	Jose Felipe Marquez Cañizales	El Paradero
21	286	Juan Vicente Campo Elias	Campo Elias
21	290	Motatan	Motatan
21	291	Pampan	Pampan
22	299	Aristides Bastidas	San Pablo
22	300	Bolivar	Aroa
22	304	Jose Antonio Paez	Sabana de Parra
22	307	Nirgua	Nirgua
22	308	San Felipe	San Felipe
22	309	Sucre	Guama
22	310	Urachiche	Urachiche
22	311	Veroes	Farriar
23	312	Almirante Padilla	El Toro
23	313	Baralt	San Timoteo
23	314	Cabimas	Cabimas
23	315	Catatumbo	Encontrados
23	316	Colon	San Carlos del Zulia
22	334	Peña	Yaritagua
\.


--
-- Data for Name: tbl_oficio; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_oficio (oficio_id, oficio) FROM stdin;
1	Arquitectura
2	Artista
3	Administrador de Empresas
4	Profesor
5	Médico
6	Ecónomo
7	Psicología
8	Abogado
9	Biólogo
10	Sociólogo
11	Estudiante
12	Ingeniero
13	Militar
14	Empleado
15	Otro
0	Ninguno
\.


--
-- Data for Name: tbl_padrino; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_padrino (id, nombre, apellido, nacionalidad, cedula, telefono, correo, usuario_id, estatus, created_at, updated_at) FROM stdin;
11	maria	sifontes maldonado	V	12345678	12354354534	nuevo@hotmail.com	1	t	2012-03-14 07:36:03	2012-03-14 07:36:03
59	SANTANDER B	TOMAS E	E	17212121	4261231212	tomastomas@hotmail.com	4	t	2012-07-10 11:49:30	2012-07-10 11:49:30
9	roxenis	La amo	V	9900	0	3	1	t	2012-03-14 06:28:53	2012-03-14 06:28:53
16	roxenis	Prueba de sistem	V	1111	412455566	ELALCONXVII@GMAIL.COM	1	t	2012-04-08 03:01:32	2012-04-08 03:01:32
60	EWDEF	DS	V	1234	0	jdjdjdjjd@hotmail.com	5	t	2012-07-11 10:54:19	2012-07-11 10:54:19
23	roxenis	Roxenio	V	17442931	412455566	dqhh@hotmail.com	1	t	2012-06-06 10:25:36	2012-06-06 10:25:36
61	MIGUEL	FRANCISCO	V	14888888	41245556609	taaooas@hotmail.com	1	t	2012-07-19 07:07:01	2012-07-19 07:07:01
62	POOPOOOOO	PPOPOPOP	V	5556666	0	wdd@ddddd.com	3	t	2012-10-09 06:16:09	2012-10-09 06:16:09
35	roxenis	Roxenio	V	5555555	412123	elalconxvii@hotmail	1	t	2012-07-08 12:25:20	2012-07-08 12:25:20
47	PRUEBA	ROXENIO	V	17442938	4121232333	elalconxvii@gmail.com2	1	t	2012-07-08 10:09:08	2012-07-08 10:09:08
48	ROXENIS 	PRUEBA 	V	17442933	657766676	elalconxvii@gmail.com3	1	t	2012-07-08 10:09:54	2012-07-08 10:09:54
12	catanclaro	el pollo	V	121212	412455566	elmatatan@tumeta.1com	1	t	2012-03-25 07:26:07	2012-03-25 07:26:07
1	roxenis	Prueba de sistem	V	2211	412455566	2	1	t	2012-03-14 02:15:40	2012-03-14 02:15:40
18	roxenis	Prueba de sistem	V	1112	0	4	1	t	2012-04-08 03:05:08	2012-04-08 03:05:08
13	quiaro	yarelis	V	17123456	657766676	5	1	t	2012-03-28 07:58:03	2012-03-28 07:58:03
14	sanoja	roxenis	V	17999999	8667867	6	1	t	2012-03-28 08:09:05	2012-03-28 08:09:05
15	millan	gustavo	V	7654321	999999	8	1	f	2012-03-28 08:12:29	2012-03-28 08:12:29
10	yarelis	blanco	V	17444475	999999	9	5	t	2012-03-14 06:50:02	2012-03-14 06:50:02
21	Gregorio	Prueba de sistema	V	12345677	412455566	10	1	t	2012-06-06 12:40:03	2012-06-06 12:40:03
26	Quiaro	Yarelis 	V	9876543	2129999047	11	5	t	2012-07-05 02:04:36	2012-07-05 02:04:36
31	sksks	yarelis	V	123456	9999999	12	1	t	2012-07-05 05:53:45	2012-07-05 05:53:45
32	hd	hd	V	98765432	999	13	1	t	2012-07-05 06:21:39	2012-07-05 06:21:39
36	Prueba	Roxenio	V	1744293	412455566	14	1	t	2012-07-08 12:56:09	2012-07-08 12:56:09
58	ROXENIS	ROXENIO	V	23123456	412455566	elmatatan@tumeta.com	1	t	2012-07-08 11:03:18	2012-07-08 11:03:18
\.


--
-- Data for Name: tbl_padrino_tbl_grupo_familiar; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_padrino_tbl_grupo_familiar (id, familiar_id, padrino_id, cant_donacion_bs, fecha_inicio, estatus, usuario_id, created_at, updated_at) FROM stdin;
4	44	23	100	2012-06-13 00:00:00	t	1	2012-06-13 11:29:30	\N
5	45	23	100	2012-06-14 00:00:00	t	1	2012-06-13 11:32:24	\N
6	44	23	100	2012-06-14 00:00:00	t	1	2012-06-14 07:57:36	\N
7	44	16	100	2012-06-28 00:00:00	t	1	2012-06-28 10:58:16	\N
\.


--
-- Data for Name: tbl_padrino_tbl_pers_disc_visual; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_padrino_tbl_pers_disc_visual (id, pers_disc_visual_id, padrino_id, cant_donacion_bs, fecha_inicio, estatus, usuario_id, created_at, updated_at) FROM stdin;
48	304	16	1000	2012-06-04	f	1	2012-06-07 08:47:05	2012-06-07 08:47:42
39	304	16	100	2012-06-14	f	1	2012-06-06 11:27:07	2012-06-07 08:47:42
40	304	16	1000	2012-06-21	f	1	2012-06-07 12:26:30	2012-06-07 08:47:42
41	304	16	100	2012-06-06	f	1	2012-06-07 12:32:08	2012-06-07 08:47:42
42	304	16	100	2012-06-07	f	1	2012-06-07 12:35:31	2012-06-07 08:47:42
43	304	16	100	2012-06-01	f	1	2012-06-07 12:39:00	2012-06-07 08:47:42
44	304	16	1000	2012-06-06	f	1	2012-06-07 08:40:56	2012-06-07 08:47:42
45	304	16	100	2012-06-21	f	1	2012-06-07 08:42:25	2012-06-07 08:47:42
46	304	16	100	2012-06-05	f	1	2012-06-07 08:44:45	2012-06-07 08:47:42
47	304	16	100	2012-06-14	f	1	2012-06-07 08:46:22	2012-06-07 08:47:42
50	321	60	123	2012-07-01	t	5	2012-07-11 10:56:25	\N
52	304	61	100	1995-04-11	t	1	2012-07-19 07:18:29	\N
54	340	61	100	2012-07-12	f	1	2012-07-19 07:31:23	2012-07-19 07:43:49
55	341	62	1000	2012-10-01	t	3	2012-10-09 06:16:59	\N
49	300	16	100	2012-06-13	f	3	2012-06-07 09:52:01	2012-10-23 06:11:35
\.


--
-- Data for Name: tbl_parentesco; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_parentesco (parentesco_id, parentesco) FROM stdin;
5	Yernos, nueras
6	Padre, Madres
14	Otro pariente
15	No pariente
16	Servicio doméstico
3	Jefe o jefa de hogar
1	Hijos(as)
2	Esposo(a), compañero(a)
4	Nietos(as)
7	Suegro(a)
8	Hermano(a)
9	Cuñado(a)
10	Sobrino(a)
11	Tio(a)
12	Primo(a)
13	Abuelo(a)
17	Familiares del servicio doméstico
\.


--
-- Data for Name: tbl_parroquia; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_parroquia (co_estado, co_municipio, co_parroquia, nb_parroquia, ca_parroquia) FROM stdin;
2	2	1118	Sierra Parima	Parimabé
2	4	1123	Fernando Giron Tovar	Puerto Ayacucho
1	1	6	Coche	Caracas
2	4	1125	Luis Alberto Gomez	Puerto Ayacucho
3	9	32	San Joaquin	San Joaquin
3	12	39	Santa Barbara	Santa Barbara
3	14	45	Chorreron	Pertigalete
3	19	60	Santa Ines	Santa Inés
3	21	64	Capital Pedro Maria Freites	Cantaura
3	22	68	Capital Piritu	Píritu
3	23	70	Capital San Jose de Guanipa	San José de Guanipa (El Tigrito)
3	24	72	Boca de Chavez	Boca de Chávez
3	28	83	Tomas Alfaro Calatrava	José Gregorio Monagas
3	29	84	Lecherias	Lecherías
4	32	96	Rincon Hondo	La Estacada
11	106	343	Borojo	Borojó
11	106	346	Zazarida	Zazárida
11	108	350	Punta Cardon	Punta Cardón
1	1	14	San Agustin	Caracas
1	1	16	San Jose	Caracas
1	1	19	Santa Rosalia	Caracas
3	27	1	Edmundo Barrios	El Tigre
3	27	2	Miguel Otero Silva	El Tigre
5	39	115	No Urbana Choroni	Choroní
5	39	117	Urbana Madre Maria de San Jose	Maracay
5	40	123	Capital Jose Angel Lamas	Santa Cruz
5	41	124	Capital Jose Felix Ribas	La Victoria
5	41	125	Castor Nieves Rios	Las Mercedes
5	41	127	No Urbana Pao de Zarate	Pao de Zárate
5	42	129	Capital Jose Rafael Revenga	El Consejo
5	43	131	No Urbana San Martin de Porres	La Pica
5	44	133	Caña de Azucar	Caña de Azúcar
5	45	137	No Urbana Valle Morin	Valle Morín
5	47	140	No Urbana Arevalo Aponte	Rosario de Paya
5	47	142	No Urbana Saman de Güere	19 de Abril
5	52	155	No Urbana San Francisco de Asis	San Francisco de Asís
5	53	158	Capital Francisco Linares Alcantara	Santa Rita
5	53	160	No Urbana Monseñor Feliciano Gonzalez	Paraparal
6	55	163	Rodriguez Dominguez	Veguitas
6	56	165	Andres Bello	Bum-Bum
6	56	166	Nicolas Pulido	Chameta
6	57	169	La Union	La Unión
6	61	191	Santa Barbara	Santa Bárbara
6	61	192	Jose Ignacio del Pumar	Pedraza La Vieja
6	61	193	Pedro Briceño Mendez	Capitanejo
6	61	194	Ramon Ignacio Mendez	Punta de Piedra
6	63	201	Jose Felix Ribas	Curbatí
6	63	202	Paez	San Rafael de Canaguá
6	66	211	El Canton	El Cantón
7	68	224	Capital Cedeño	Caicara del Orinoco
7	70	231	Capital Gran Sabana	Santa Elena de Uairén
7	70	232	Ikabaru	Ikabarú
7	72	242	Capital Piar	Upata
7	72	243	Andres Eloy Blanco	El Pao de El Hierro
7	73	249	Capital Roscio	Guasipati
7	74	251	Capital Sifontes	Tumeremo
7	75	254	Capital Sucre	Maripa
7	335	245	Capital Raul Leoni	Ciudad Piar
7	335	248	Santa Barbara	Santa Bárbara de Centurión
8	77	262	No Urbana Simon Bolivar	Chirgua
8	78	264	No Urbana Belen	Belén
8	81	271	Urbana Moron	Morón
8	85	277	Urbana Montalban	Montalbán
8	87	279	Urbana Bartolome Salom	Puerto Cabello
8	89	288	Urbana San Joaquin	San Joaquín
8	90	295	Urbana San Jose	Valencia
9	91	299	Juan de Mata Suarez	Apartaderos
9	93	301	El Baul	El Baúl
9	97	308	Romulo Gallegos	Las Vegas
10	100	314	Almirante Luis Brion	Manoa
10	101	323	Romulo Gallegos	Santa Catalina
10	102	325	Luis Beltran Prieto Figueroa	Capure
10	103	326	San Jose	Tucupita
10	103	327	Jose Vidal Marcano	Hacienda del Medio
10	103	328	Juan Millan	Carapal de Guara
10	103	329	Leonardo Ruiz Pineda	Urbanización Leonardo Ruíz Pineda
10	103	330	Mariscal Antonio Jose de Sucre	Paloma
10	103	331	Monseñor Argimiro Garcia	Urbanización Delfín Mendoza
15	185	621	Los Teques	Los Teques
11	112	364	Adicora	Adícora
11	113	374	El Pauji	El Paují
11	113	376	Maparari	Mapararí
11	111	360	Avaria	Tupure
13	148	481	Guarico	Guarico
15	176	599	Caucagua	Caucagua
15	176	600	Aragüita	Aragüita
23	313	1005	Pueblo Nuevo	Pueblo Nuevo
23	314	1011	Rómulo Betancourt	Cabimas
17	219	716	Capital Tubores	Punta de Piedras
20	263	847	La Petrólea	Río Chiquito
21	279	884	Santa Isabel	Santa Isabel
21	279	885	Araguaney	Araguaney
21	279	886	El Jagüito	El Jagüito
21	295	954	Sabana de Mendoza	Sabana de Mendoza
21	296	958	Andrés Linares	San Lázaro
21	296	959	Chiquinquirá	Chiquinquirá
21	296	960	Cristóbal Mendoza	Santa Rosa
21	296	961	Cruz Carrillo	La Plazuela
21	297	965	La Quebrada	La Quebrada
21	297	966	Cabimbú	Cabimbú
21	297	967	Jajó	Jajó
21	297	968	La Mesa	La Mesa de Esnujaque
21	297	969	Santiago	Santiago
21	297	970	Tuñame	Tuñame
21	298	971	Juan Ignacio Montilla	Juan Ignacio Montilla
21	298	972	La Beatriz	La Beatriz
21	298	973	Mercedes Díaz	Mercedes Díaz
21	298	976	Mendoza	Mendoza
2	8	30	Capital Rio Negro	San Carlos de Rio Negro
3	18	52	Capital Jose Gregorio Monagas	Mapire
3	26	76	San Cristobal	Barcelona
3	26	77	Bergantin	Bergantín
5	37	112	Capital Bolivar	San Mateo
5	39	118	Urbana Joaquin Crespo	Maracay
5	39	119	Urbana Pedro Jose Ovalles	Maracay
5	39	120	Urbana Jose Casanova Godoy	Maracay
5	39	121	Urbana Andres Eloy Blanco	Maracay
5	46	138	Capital San Sebastian	San Sebastián
6	58	174	Santa Ines	Santa Inés
6	58	175	Santa Lucia	Santa Lucía
6	58	178	Romulo Betancourt	Barinas
6	58	179	Corazon de Jesus	Barinas
6	58	180	Ramon Ignacio Mendez	Barinas
6	58	183	Juan Antonio Rodriguez Dominguez	El Corozo
6	58	184	Dominga Ortiz de Paez	La Mula
7	67	218	Simon Bolivar	Ciudad Guayana
7	68	226	Ascension Farreras	Santa Rosalía
7	71	235	Jose Antonio Paez	Ciudad Bolívar
8	87	283	Urbana Juan Jose Flores	Puerto Cabello
8	87	284	Urbana Union	Puerto Cabello
9	99	312	General en Jefe Jose Laurencio Silva	Tinaco
11	106	341	Capatarida	Capatárida
1	1	2	Antimano	Caracas
1	1	8	EL Paraiso	Caracas
3	13	5	Mucura	Múcura
13	151	15	Heriberto Arroyo	El Paradero
13	151	500	Espinoza de los Monteros	Arenales
13	151	501	Lara	San Pedro
15	176	604	Marizapa	Marizapa
15	176	605	Panaquire	Panaquire
2	8	1132	Solano	Solano
2	8	1133	Casiquiare	Curimacare
2	8	1134	Cocuy	Santa Lucía
2	5	1128	Guayapo	San Pedro del Orinoco
2	4	25	Parhueña	Limon de Parhueña
3	18	57	Zuata	Zuata
24	333	1114	Raúl Leoni	Catia La Mar
23	324	1060	Francisco Eugenio Bustamante	Maracaibo
23	324	1061	Idelfonso Vásquez	Maracaibo
23	324	1062	Juana de Avila	Maracaibo
23	324	1063	Luis Hurtado Higuera	Maracaibo
23	324	1064	Manuel Dagnino	Maracaibo
23	324	1065	Olegario Villalobos	Maracaibo
23	324	1066	Raúl Leoni	Maracaibo
23	324	1067	Santa Lucía	Maracaibo
23	324	1068	Venancio Pulgar	Maracaibo
23	324	1069	San Isidro	San Isidro
23	325	1073	San Antonio	El Consejo de Ciruma
23	325	1074	San José	Sabaneta de Palmas
23	326	1077	Elías Sánchez Rubio	El Molinete
23	326	1078	Guajira	Paraguaipoa
23	328	1086	Los Cortijos	Los Cortijos
23	328	1087	Marcial Hernández	Sur América
23	331	1100	Rómulo Gallegos	Caja Seca
24	333	1110	Macuto	Macuto
24	333	1111	Maiquetía	Maiquetía
24	333	1112	Naiguatá	Naiguatá
24	333	1113	Carlos Soublette	Maiquetía
21	280	899	San José	Tostós
1	1	1	Altagracia	Caracas
1	1	3	Candelaria	Caracas
1	1	4	Caricuao	Caracas
1	1	5	Catedral	Caracas
1	1	7	El Junquito	Caracas
1	1	9	El Recreo	Caracas
1	1	10	El Valle	Caracas
1	1	11	La Pastora	Caracas
1	1	12	La Vega	Caracas
1	1	13	Macarao	Caracas
1	1	15	San Bernardino	Caracas
1	1	17	San Juan	Caracas
1	1	18	San Pedro	Caracas
1	1	20	Santa Teresa	Caracas
1	1	21	Sucre	Caracas
1	1	22	23 de Enero	Caracas
10	103	333	Virgen del Valle	La Horqueta
12	135	430	Sosa	Sosa
2	7	29	Alto Ventuari	Cacurí
2	7	1129	Medio Ventuari	Camani
2	7	1130	Bajo Ventuari	Marueta
2	6	28	Victorino	Victorino
2	6	1131	Comunidad	Comunidad
2	5	27	Samariapo	Samariapo
2	5	1126	Sipapo	Pendare
2	5	1127	Munduapo	Munduapo
2	4	26	Platanillal	Platanillal
2	3	24	Ucata	Laja Lisa
2	3	1124	Yapacana	Macuruco
2	3	1120	Caname	Guarinuma
2	2	23	Capital Alto Orinoco	La Esmeralda
2	2	1122	Huachamacare	Acanaña
2	2	1116	Marawaka	Toky-Shamanaña
2	2	1117	Mavaca	Mavaca
3	29	85	El Morro	Lecherías
3	28	82	El Chaparro	El Chaparro
3	26	75	El Carmen	Barcelona
3	26	78	Caigua	Caigua
3	26	79	El Pilar	El Pilar
3	26	80	Naricual	Naricual
3	25	73	Capital Santa Ana	Santa Ana
3	25	74	Pueblo Nuevo	Pueblo Nuevo
3	24	71	Capital Boca de Uchire	Boca de Uchire
3	22	69	San Francisco	San Francisco
3	21	65	Libertador	Mundo Nuevo
3	21	66	Santa Rosa	Santa Rosa
3	21	67	Urica	Urica
3	20	61	Capital Clarines	Clarines
3	20	62	Guanape	Guanape
3	20	63	Sabana de Uchire	Sabana de Uchire
3	19	58	Capital Libertad	San Mateo
3	19	59	El Carito	El Carito
3	18	53	Piar	Santa Cruz de Orinoco
3	18	54	San Diego de Cabrutica	San Diego de Cabrutica
3	18	55	Santa Clara	Santa Clara
3	18	56	Uverito	Uverito
3	17	50	Capital Onoto	Onoto
3	17	51	San Pablo	San Pablo
3	16	48	Capital Puerto La Cruz	Puerto La Cruz
3	16	49	Pozuelos	Pozuelos
3	15	46	Capital Independencia	Soledad
3	15	47	Mamo	Carapa
3	14	44	Guanta	Guanta
3	13	41	Atapirire	Atapirire
3	13	42	Boca del Pao	Boca del Pao
3	13	43	El Pao	El Pao de Barcelona
3	13	40	Capital Francisco de Miranda	Pariaguan
3	12	38	Valle de Guanape	Valle de Guanape
3	11	36	San Miguel	San Miguel
3	11	37	Sucre	El Hatillo
11	123	402	Tucacas	Tucacas
3	11	35	Capital Fernando de Peñalver	Puerto Piritu
3	10	33	Capital Aragua	Aragua de Barcelona
3	10	34	Cachipo	Cachipo
3	9	31	Capital Anaco	Anaco
4	30	86	Urbana Achaguas	Achaguas
4	30	87	Apurito	Apurito
4	30	88	El Yagual	El Yagual
4	30	89	Guachara	Guachara
4	30	90	Mucuritas	El Samán de Apure
4	31	92	Urbana Biruaca	Biruaca
4	32	93	Urbana Bruzual	Bruzual
4	32	94	Mantecal	Mantecal
4	32	95	Quintero	Quintero
4	32	97	San Vicente	San Vicente
4	33	98	Urbana Guasdualito	Guasdualito
4	33	99	Aramendi	Palmarito
4	33	100	El Amparo	El Amparo
4	33	101	San Camilo	El Nula
4	33	102	Urdaneta	La Victoria
4	34	103	Urbana San Juan de Payara	San Juan de Payara
4	34	104	Codazzi	Puerto Páez
4	34	105	Cunaviche	San Miguel de Cunaviche
4	35	106	Urbana Elorza	Elorza
4	35	107	La Trinidad	La Trinidad de Orichuna
11	108	351	Santa Ana	Santa Ana
11	109	355	Las Calderas	Las Calderas
11	109	356	Macoruca	El Moyepo
11	112	367	Jadacaquiva	Jadacaquiva
11	112	369	Adaure	Adaure
11	112	370	El Hato	El Hato
11	112	371	El Vínculo	El Vínculo
12	137	438	Paso Real de Macaira	Paso Real de Macaira
12	137	439	San Francisco de Macaira	San Francisco de Macaira
12	137	440	San Rafael de Orituco	San Rafael de Orituco
12	137	441	Soublette	Sabana Grande de Orituco
5	38	113	Capital Camatagua	Camatagua
5	38	114	No Urbana Carmen de Cura	Carmen de Cura
5	39	116	Urbana Las Delicias	Maracay
5	39	122	Urbana Los Tacariguas	Maracay
5	41	126	No Urbana Las Guacamayas	Las Guacamayas
5	41	128	No Urbana Zuata	Zuata
5	43	130	Capital Libertador	Palo Negro
5	44	132	Capital Mario Briceño Iragorry	El Limón
5	45	134	Capital San Casimiro	San Casimiro
5	45	135	No Urbana Güiripa	Güiripa
5	45	136	No Urbana Ollas de Caramacate	Ollas de Caramacate
5	47	139	Capital Santiago Mariño	Turmero
5	47	141	No Urbana Chuao	Chuao
5	47	143	No Urbana Alfredo Pacheco Miranda	San Joaquín
5	48	144	Capital Santos Michelena	Las Tejerías
5	48	145	No Urbana Tiara	Tiara
5	49	146	Capital Sucre	Cagua
5	49	147	No Urbana Bella Vista	Bella Vista
5	50	148	Capital Tovar	La Colonia Tovar
5	51	149	Capital Urdaneta	Barbacoas
5	51	150	No Urbana Las Peñitas	Las Peñitas
5	51	151	No Urbana San Francisco de Cara	San Francisco de Cara
5	51	152	No Urbana Taguay	Taguay
5	52	153	Capital Zamora	Villa de Cura
5	52	154	No Urbana Magdaleno	Magdaleno
5	52	156	No Urbana Valles de Tucutunemo	Los Bagres
5	52	157	No Urbana Augusto Mijares	Tocorón
5	53	159	No Urbana Francisco de Miranda	Francisco de Miranda
5	54	161	Capital Ocumare de La Costa de Oro	Ocumare de la Costa
6	55	162	Sabaneta	Sabaneta
6	56	164	Ticoporo	Socopó
6	57	167	Arismendi	Arismendi
6	57	168	Guadarrama	Guadarrama
6	57	170	San Antonio	San Antonio
6	58	171	Barinas	Barinas
6	58	172	Alfredo Arvelo Larriva	Quebrada Seca
6	58	173	San Silvestre	San Silvestre
6	58	176	Torunos	Torunos
6	58	177	El Carmen	Barinas
6	58	181	Alto Barinas	Barinas
6	59	185	Barinitas	Barinitas
6	59	186	Altamira	Altamira
6	59	187	Calderas	Calderas
6	60	188	Barrancas	Barrancas
6	60	189	El Socorro	La Yuca
6	60	190	Masparrito	Masparrito
6	62	195	Obispos	Obispos
6	62	196	El Real	El Real
6	62	197	La Luz	La Luz
6	62	198	Los Guasimitos	Los Guasimitos
6	63	199	Ciudad Bolivia	Ciudad Bolivia
6	63	200	Ignacio Briceño	Maporal
6	64	203	Libertad	Libertad
6	64	204	Dolores	Dolores
6	64	205	Palacios Fajardo	Mijagual
6	64	206	Santa Rosa	Santa Rosa
6	65	207	Ciudad de Nutrias	Ciudad de Nutrias
6	65	208	El Regalo	El Regalo
6	65	209	Puerto de Nutrias	Puerto de Nutrias
6	65	210	Santa Catalina	Santa Catalina
6	66	212	Santa Cruz de Guacas	Santa Cruz de Guacas
6	66	213	Puerto Vivas	Puerto Vivas
7	67	214	Cachamay	Ciudad Guayana
7	67	215	Chirica	Ciudad Guayana
7	67	216	Dalla Costa	Ciudad Guayana
7	67	217	Once de Abril	Ciudad Guayana
7	67	219	Unare	Ciudad Guayana
7	67	220	Universidad	Ciudad Guayana
7	67	221	Vista al Sol	Ciudad Guayana
7	67	222	Pozo Verde	Pozo Verde
7	67	223	Yocoima	El Rosario
9	92	300	Tinaquillo	Tinaquillo
9	93	302	Sucre	Sucre
9	94	303	Macapo	Macapo
9	94	304	La Aguadita	La Aguadita
9	95	305	El Pao	El Pao
9	96	306	Libertad de Cojedes	Libertad
9	96	307	El Amparo	El Amparo
9	98	309	San Carlos de Austria	San Carlos
9	98	310	Juan Angel Bravo	La Sierra
9	98	311	Manuel Manrique	Manrique
10	100	313	Curiapo	Curiapo
10	100	315	Francisco Aniceto Lugo	Boca de Cuyubini
10	100	316	Manuel Renaud	Araguabisi
10	100	317	Padre Barral	San Francisco de Guayo
10	100	318	Santos de Abelgas	Araguaimujo
10	101	319	Imataca	Sierra Imataca
10	101	320	Cinco de Julio	Moruca
10	101	321	Juan Bautista Arismendi	Piacoa
10	101	322	Manuel Piar	El Triunfo
10	102	324	Pedernales	Pedernales
10	103	332	San Rafael	San Rafael
11	104	334	San Juan de los Cayos	San Juan de los Cayos
11	104	335	Capadare	Capadare
11	104	336	La Pastora	La Pastora
11	104	337	Libertador	El Mene de San Lorenzo
11	105	338	San Luis	San Luis
11	105	339	Aracua	Aracua
11	105	340	La Peña	La Peña
11	106	342	Bariro	Bariro
11	106	344	Guajiro	Guajiro
11	106	345	Seque	San José de Seque
11	107	347	Capital Cacique Manaure	Yaracal
11	108	348	Carirubana	Punto Fijo
13	146	468	Aguedo Felipe Alvarado	Bobare
13	146	469	Buena Vista	Buena Vista
13	146	470	Juárez	Río Claro
13	147	473	Diego de Lozada	Cubiro
13	147	474	Paraíso de San José	Agua Negra
13	147	475	San Miguel	San Miguel
13	147	476	Tintorero	Tintorero
13	147	477	José Bernardo Dorante	El Hato
13	147	478	Coronel Mariano Peraza	La Ceiba
7	69	230	Capital El Callao	El Callao
7	71	233	Agua Salada	Ciudad Bolívar
7	71	234	Catedral	Ciudad Bolívar
7	71	236	La Sabanita	Ciudad Bolívar
7	71	237	Marhuanta	Ciudad Bolívar
7	72	244	Pedro Cova	El Manteco
7	73	250	Salom	El Miamo
7	74	252	Dalla Costa	El Dorado
7	74	253	San Isidro	Las Claritas
7	75	255	Aripao	Aripao
7	75	256	Guarataro	Guarataro
7	75	257	Las Majadas	Las Majadas
7	75	258	Moitaco	Moitaco
7	76	259	Capital Padre Pedro Chien	El Palmar
7	335	246	Barceloneta	La Paragua
7	335	247	San Francisco	San Francisco
8	77	260	Urbana Bejuma	Bejuma
8	77	261	No Urbana Canoabo	Canoabo
8	78	263	Urbana Güigüe	Güigüe
8	78	265	No Urbana Tacarigua	Central Tacarigua
8	79	266	Urbana Aguas Calientes	Mariara
8	79	267	Urbana Mariara	Mariara
8	80	268	Urbana Ciudad Alianza	Guacara
8	80	269	Urbana Guacara	Guacara
8	80	270	No Urbana Yagua	Yagua
8	81	272	No Urbana Urama	Urama
8	82	273	Urbana Tocuyito	Tocuyito
8	82	274	Urbana Independencia	Tocuyito
8	83	275	Urbana Los Guayos	Los Guayos
8	84	276	Urbana Miranda	Miranda
8	86	278	Urbana Naguanagua	Naguanagua
8	87	280	Urbana Democracia	Puerto Cabello
8	87	281	Urbana Fraternidad	Puerto Cabello
8	87	282	Urbana Goaigoaza	Puerto Cabello
8	87	285	No Urbana Borburata	Borburata
8	87	286	No Urbana Patanemo	Patanemo
8	88	287	Urbana San Diego	San Diego
8	90	289	Urbana Candelaria	Valencia
8	90	290	Urbana Catedral	Valencia
8	90	291	Urbana El Socorro	Valencia
8	90	292	Urbana Miguel Peña	Valencia
8	90	293	Urbana Rafael Urdaneta	Valencia
8	90	294	Urbana San Blas	Valencia
8	90	296	Urbana Santa Rosa	Valencia
9	91	298	Cojedes	Cojedes
18	224	728	San Juan de Guanaguanare	Mesa de Cavacas
18	224	729	Virgen de la Coromoto	Quebrada de la Virgen
18	233	755	San José de Saguaz	San José de Saguaz
18	233	756	Villa Rosa	Villa Rosa
19	238	775	Unión	Guariquén
19	246	801	San Lorenzo	San Lorenzo
19	248	812	Raúl Leoni	Los Puertos de Santa Fe
11	108	349	Norte	Punto Fijo
11	109	352	La Vela de Coro	La Vela de Coro
11	109	353	Acurigua	Acurigua
11	109	354	Guaibacoa	Guaibacoa
11	110	357	Capital Dabajuro	Dabajuro
11	111	358	Pedregal	Pedregal
11	111	359	Agua Clara	El Manantial (Agua Clara)
11	111	361	Piedra Grande	Piedra Grande
11	111	362	Purureche	Purureche
11	112	363	Pueblo Nuevo	Pueblo Nuevo
11	112	365	Baraived	Baraived
11	112	366	Buena Vista	Buena Vista
11	112	368	Moruy	Moruy
11	113	372	Churuguara	Churuguara
11	113	373	Agua Larga	Agua Larga
11	113	375	Independencia	El Tupí
11	114	377	Jacura	Jacura
11	114	378	Agua Linda	Agua Linda
11	114	379	Araurima	Araurima
11	115	380	Los Taques	Santa Cruz de Los Taques
11	115	381	Judibana	Judibana
11	116	382	Mene de Mauroa	Mene de Mauroa
11	116	383	Casigua	Casigua
11	116	384	San Félix	San Félix
11	117	385	San Antonio	Santa Ana de Coro
11	117	386	San Gabriel	Santa Ana de Coro
11	117	387	Santa Ana	Santa Ana de Coro
11	117	388	Guzmán Guillermo	La Negrita
11	117	389	Mitare	Mitare
11	117	390	Río Seco	Río Seco
11	117	391	Sabaneta	Sabaneta
11	118	392	Chichiriviche	Chichiriviche
11	118	393	Boca de Tocuyo	Boca de Tocuyo
11	118	394	Tocuyo de la Costa	Tocuyo de La Costa
11	119	395	Capital Palmasola	Palmasola
11	120	396	Cabure	Cabure
11	120	397	Colina	Pueblo Nuevo de la Sierra
11	120	398	Curimagua	Curimagua
11	121	399	Píritu	Píritu
11	121	400	San José de la Costa	San José de la Costa
11	122	401	Capital San Francisco	Mirimire
11	123	403	Boca de Aroa	Boca de Aroa
11	124	404	Sucre	La Cruz de Taratara
11	124	405	Pecaya	Pecaya
11	125	406	Capital Tocópero	Tocópero
11	126	407	Santa Cruz de Bucaral	Santa Cruz de Bucaral
11	126	408	El Charal	El Charal
11	126	409	Las Vegas del Tuy	Las Vegas del Tuy
11	127	410	Urumaco	Urumaco
11	127	411	Bruzual	San José de Bruzual
11	128	412	Puerto Cumarebo	Puerto Cumarebo
11	128	413	La Ciénaga	La Ciénaga
11	128	414	La Soledad	La Soledad
11	128	415	Pueblo Cumarebo	Pueblo Cumarebo
11	128	416	Zazárida	Zazárida
12	129	417	Capital Camaguán	Camaguán
12	129	418	Puerto Miranda	Puerto Miranda
12	129	419	Uverito	Uverito
12	130	420	Chaguaramas	Chaguaramas
12	131	421	El Socorro	El Socorro
12	132	422	Capital San Gerónimo de Guayabal	Guayabal
12	132	423	Cazorla	Cazorla
12	133	424	Capital Valle de La Pascua	Valle de La Pascua
12	133	425	Espino	Espino
12	134	426	Capital Las Mercedes	Las Mercedes
12	134	427	Cabruta	Cabruta
12	134	428	Santa Rita de Manapire	Santa Rita
12	135	429	Capital El Sombrero	El Sombrero
12	136	431	Capital Calabozo	Calabozo
12	136	432	El Calvario	El Calvario
12	136	433	El Rastro	El Rastro
12	136	434	Guardatinajas	Guardatinajas
12	137	435	Capital Altagracia de Orituco	Altagracia de Orituco
12	137	436	Lezama	Lezama
12	137	437	Libertad de Orituco	Libertad de Orituco
12	138	442	Capital Ortiz	Ortiz
12	138	443	San Francisco de Tiznados	San Francisco de Tiznados
12	138	444	San José de Tiznados	San José de Tiznados
12	138	445	San Lorenzo de Tiznados	La Unión de Canuto
12	139	446	Capital Tucupido	Tucupido
12	139	447	San Rafael de Laya	San Rafael de Laya
12	140	448	Capital San Juan de Los Morros	San Juan de los Morros
12	140	449	Cantagallo	Cantagallo
12	140	450	Parapara	Parapara
12	141	451	San José de Guaribe	San José de Guaribe
12	142	452	Capital Santa María de Ipire	Santa María de Ipire
12	142	453	Altamira	Altamira
12	143	454	Capital Zaraza	Zaraza
12	143	455	San José de Unare	San José de Unare
13	144	456	Pío Tamayo	Sanare
13	144	457	Quebrada Honda de Guache	La Bucarita
13	144	458	Yacambú	La Escalera
13	145	459	Fréitez	Duaca
13	145	460	José María Blanco	El Eneal
13	146	461	Catedral	Barquisimeto
13	146	462	Concepción	Barquisimeto
13	146	463	El Cují	Barquisimeto
13	146	464	Juan de Villegas	Barquisimeto
13	146	465	Santa Rosa	Barquisimeto
13	146	466	Tamaca	Barquisimeto
13	146	467	Unión	Barquisimeto
20	256	832	Alberto Adriani	Puerto Teteo
20	256	833	Santo Domingo	San Lorenzo
20	258	836	Boca de Grita	Boca de Grita
20	258	837	José Antonio Páez	Orope
20	263	849	Bramón	Bramón
20	265	856	San Joaquín de Navay	San Joaquín de Navay
20	272	871	San Sebastián	San Cristóbal
20	272	872	Dr. Francisco Romero Lobo	Macanillo
21	280	894	Guaramacal	Guaramacal
21	280	895	Vega de Guaramacal	Vega de Guaramacal
21	280	896	Monseñor Jáuregui	Niquitao
21	280	897	Rafael Rangel	San Rafael
21	280	898	San Miguel	San Miguel
13	147	471	Juan Bautista Rodríguez	Quíbor
13	147	472	Cuara	Cuara
13	148	480	Anzoátegui	Anzoátegui
13	148	482	Hilario Luna y Luna	Villanueva
13	148	483	Humocaro Alto	Humocaro Alto
13	148	484	Humocaro Bajo	Humocaro Bajo
13	148	485	La Candelaria	Guaitó
13	148	486	Morán	Barbacoas
13	149	487	Cabudare	Cabudare
13	149	488	José Gregorio Bastidas	Los Rastrojos
13	149	489	Agua Viva	Agua Viva
13	150	490	Sarare	Sarare
13	150	491	Buría	Manzanita
13	150	492	Gustavo Vegas León	La Miel
13	151	493	Trinidad Samuel	Carora
13	151	494	Antonio Díaz	Curarigua
13	151	495	Camacaro	Río Tocuyo
13	151	496	Castañeda	Atarigua
13	151	497	Cecilio Zubillaga	La Pastora
13	151	498	Chiquinquirá	Aregue
13	151	499	El Blanco	Quebrada Arriba
13	152	509	Siquisique	Siquisique
13	152	510	Moroturo	Santa Inés
13	152	511	San Miguel	Aguada Grande
13	152	512	Xaguas	Baragua
14	153	513	Presidente Betancourt	El Vigía
14	153	514	Presidente Páez	El Vigía
14	153	515	Presidente Rómulo Gallegos	El Vigía
19	238	770	El Pilar	El Pilar
14	153	516	Gabriel Picón González	La Palmita
14	153	517	Héctor Amable Mora	Mucujepe
14	153	518	José Nucete Sardi	Los Naranjos
14	153	519	Pulido Méndez	La Blanca (12 de Octubre)
14	154	520	Capital Andrés Bello	La Azulita
14	155	521	Capital Antonio Pinto Salinas	Santa Cruz de Mora
14	155	522	Mesa Bolívar	Mesa Bolívar
14	155	523	Mesa de Las Palmas	Mesa de las Palmas
14	156	524	Capital Aricagua	Aricagua
14	156	525	San Antonio	Campo Elías
14	157	526	Capital Arzobispo Chacón	Canaguá
14	157	527	Capurí	Capurí
14	157	528	Chacantá	Chacantá
14	157	529	El Molino	El Molino
14	157	530	Guaimaral	El Viento
14	157	531	Mucutuy	Mucutuy
14	157	532	Mucuchachí	Mucuchachí
14	158	533	Fernández Peña	Ejido
14	158	534	Matriz	Ejido
14	158	535	Montalbán	Ejido
14	158	536	Acequias	Acequias
14	158	537	Jají	Jají
14	158	538	La Mesa	La Mesa
14	158	539	San José del Sur	San José
14	159	540	Capital Caracciolo Parra Olmedo	Tucaní
14	159	541	Florencio Ramírez	El Pinar
14	160	542	Capital Cardenal Quintero	Santo Domingo
14	160	543	Las Piedras	Las Piedras
14	161	544	Capital Guaraque	Guaraque
14	161	545	Mesa de Quintero	Mesa de Quintero
14	161	546	Río Negro	Río Negro
14	162	547	Capital Julio César Salas	Arapuey
14	162	548	Palmira	San José de Palmira
14	163	549	Capital Justo Briceño	Torondoy
14	163	550	San Cristóbal de Torondoy	San Cristóbal de Torondoy
14	164	551	Antonio Spinetti Dini	Mérida
14	164	552	Arias	Mérida
14	164	553	Caracciolo Parra Pérez	Mérida
14	164	554	Domingo Peña	Mérida
14	164	555	El Llano	Mérida
14	164	556	Gonzalo Picón Febres	Mérida
14	164	557	Jacinto Plaza	Mérida
14	164	558	Juan Rodríguez Suárez	Mérida
14	164	559	Lasso de la Vega	Mérida
14	164	560	Mariano Picón Salas	Mérida
14	164	561	Milla	Mérida
14	164	562	Osuna Rodríguez	Mérida
14	164	563	Sagrario	Mérida
14	165	566	Capital Miranda	Timotes
14	165	567	Andrés Eloy Blanco	Chachopo
14	165	568	La Venta	La Venta
14	165	569	Piñango	Piñango
14	166	570	Capital Obispo Ramos de Lora	Santa Elena de Arenales
14	166	571	Eloy Paredes	Guayabones
14	166	572	San Rafael de Alcázar	San Rafael de Alcázar
14	167	573	Capital Padre Noguera	Santa María de Caparo
14	168	574	Capital Pueblo Llano	Pueblo Llano
14	169	575	Capital Rangel	Mucuchíes
14	169	576	Cacute	Cacute
14	169	577	La Toma	La Toma
14	169	578	Mucurubá	Mucurubá
14	169	579	San Rafael	San Rafael
14	170	580	Capital Rivas Dávila	Bailadores
14	170	581	Gerónimo Maldonado	La Playa
14	171	582	Capital Santos Marquina	Tabay
14	172	583	Capital Sucre	Lagunillas
14	172	584	Chiguará	Chiguará
14	172	585	Estánquez	Estánquez
14	172	586	La Trampa	La Trampa
14	172	587	Pueblo Nuevo del Sur	Pueblo Nuevo del Sur
14	172	588	San Juan	San Juan
14	173	589	El Amparo	Tovar
15	176	601	Arévalo González	El Clavo
15	176	602	Capaya	Capaya
15	176	603	El Café	El Café
15	177	607	San José de Barlovento	San José de Barlovento
15	177	608	Cumbo	Cumbo
15	178	609	Baruta	Nuestra Señora del Rosario de Baruta
15	178	610	El Cafetal	El Cafetal
21	294	953	José Leonardo Suárez	Las Mesetas
21	295	955	El Paraíso	El Paraíso
21	295	956	Junín	Junín
21	295	957	Valmore Rodríguez	Valmore Rodríguez
21	296	962	Matriz	Matriz
21	296	963	Monseñor Carrillo	San Jacinto
21	296	964	Tres Esquinas	Tres Esquinas
14	174	593	Capital Tulio Febres Cordero	Nueva Bolivia
14	174	594	Independencia	Palmarito
14	175	597	Capital Zea	Zea
14	175	598	Caño El Tigre	Caño Tigre
15	178	611	Las Minas de Baruta	Las Minas de Baruta
15	179	612	Higuerote	Higuerote
15	179	613	Curiepe	Curiepe
15	179	614	Tacarigua	Tacarigua de Mamporal
15	180	615	Mamporal	Mamporal
15	181	616	Carrizal	Carrizal
15	182	617	Chacao	Chacao
15	183	618	Charallave	Charallave
15	183	619	Las Brisas	Las Brisas
15	184	620	El Hatillo	El Hatillo
15	185	622	Altagracia de La Montaña	Altagracia de la Montaña
15	185	623	Cecilio Acosta	San Diego
15	185	624	El Jarillo	El Jarillo
15	185	625	Paracotos	Paracotos
15	185	626	San Pedro	San Pedro
15	185	627	Tácata	Tácata
15	186	628	Santa Teresa del Tuy	Santa Teresa del Tuy
15	186	629	El Cartanal	El Cartanal
15	187	630	Ocumare del Tuy	Ocumare del Tuy
15	187	631	La Democracia	La Democracia
15	187	632	Santa Bárbara	Santa Bárbara
15	188	633	San Antonio de Los Altos	San Antonio de Los Altos
15	189	634	Río Chico	Río Chico
15	189	635	El Guapo	El Guapo
15	189	636	Tacarigua de La Laguna	Tacarigua de La Laguna
15	189	637	Paparo	Paparo
15	189	638	San Fernando del Guapo	San Fernando
15	190	639	Santa Lucía	Santa Lucía
15	191	640	Cúpira	Cúpira
15	191	641	Machurucuto	Machurucuto
15	192	642	Guarenas	Guarenas
15	193	643	San Francisco de Yare	San Francisco de Yare
15	193	644	San Antonio de Yare	San Antonio de Yare
15	194	645	Petare	Petare
15	194	646	Caucagüita	Caucagüita
15	194	647	Fila de Mariches	Fila de Mariches
15	194	648	La Dolorita	La Dolorita
15	194	649	Leoncio Martínez	Los Dos Caminos
15	195	650	Cúa	Cúa
15	195	651	Nueva Cúa	Nueva Cúa
15	196	652	Guatire	Guatire
15	196	653	Bolívar	Araira
16	197	654	Capital Acosta	San Antonio
16	197	655	San Francisco	San Francisco
16	198	656	Capital Aguasay	Aguasay
16	199	657	Capital Bolívar	Caripito
16	200	658	Capital Caripe	Caripe
16	200	659	El Guácharo	El Guácharo
16	200	660	La Guanota	La Guanota
16	200	661	Sabana de Piedra	Sabana de Piedra
16	201	664	Capital Cedeño	Caicara
16	201	665	Areo	Areo
16	201	666	San Félix	San Félix
16	201	667	Viento Fresco	Viento Fresco
16	202	668	Capital Ezequiel Zamora	Punta de Mata
16	202	669	El Tejero	El Tejero
16	203	670	Capital Libertador	Temblador
16	203	671	Chaguaramas	Chaguaramas
16	203	672	Las Alhuacas	Las Alhuacas
16	203	673	Tabasca	Tabasca
16	204	674	Capital Maturín	Maturín
16	204	675	Alto de los Godos	Maturín
16	204	676	Boquerón	Maturín
16	204	677	Las Cocuizas	Maturín
16	204	678	San Simón	Maturín
16	205	685	Capital Piar	Aragua
16	205	686	Aparicio	Aparicio
16	205	687	Chaguaramal	Chaguaramal
16	205	688	El Pinto	El Pinto
16	205	689	Guanaguana	Guanaguana
16	205	690	La Toscana	La Toscana
16	205	691	Taguaya	Taguaya
16	206	692	Capital Punceres	Quiriquire
16	206	693	Cachipo	Cachipo
16	207	694	Capital Santa Bárbara	Santa Bárbara
16	208	695	Capital Sotillo	Barrancas
16	208	696	Los Barrancos de Fajardo	Los Barrancos de Fajardo
16	209	697	Capital Uracoa	Uracoa
17	210	698	Capital Antolín del Campo	La Plaza de Paraguachí
17	211	699	Capital Arismendi	La Asunción
17	212	700	Capital Díaz	San Juan Bautista
17	212	701	Zabala	La Guardia
17	213	702	Capital García	El Valle del Espíritu Santo
17	213	703	Francisco Fajardo	Villa Rosa
17	214	704	Capital Gómez	Santa Ana
17	214	705	Bolívar	El Maco
17	214	706	Guevara	Tacarigua
17	214	707	Matasiete	Pedro González
17	214	708	Sucre	Altagracia
17	215	709	Capital Maneiro	Pampatar
17	215	710	Aguirre	El Pilar (Los Robles)
17	216	711	Capital Marcano	Juangriego
17	216	712	Adrián	Los Millanes
17	217	713	Capital Mariño	Porlamar
17	219	717	Los Barales	El Guamache
17	220	718	Capital Villalba	San Pedro de Coche
17	220	719	Vicente Fuentes	Güinima
18	221	720	Capital Agua Blanca	Agua Blanca
18	222	721	Capital Araure	Araure
18	222	722	Río Acarigua	Río Acarigua
18	223	723	Capital Esteller	Píritu
18	223	724	Uveral	Uveral
18	224	725	Capital Guanare	Guanare
18	224	726	Córdoba	Córdoba
18	225	730	Capital Guanarito	Guanarito
18	225	731	Trinidad de la Capilla	Trinidad de la Capilla
18	225	732	Divina Pastora	Morrones
18	226	733	Capital Monseñor José Vicente de Unda	Paraíso de Chabasquén
18	226	734	Peña Blanca	Peña Blanca
18	227	735	Capital Ospino	Ospino
18	227	736	Aparición	La Aparición
18	227	737	La Estación	La Estación
18	228	738	Capital Páez	Acarigua
18	228	739	Payara	Payara
18	228	740	Pimpinela	Pimpinela
18	228	741	Ramón Peraza	Mijagüito
18	229	742	Capital Papelón	Papelón
18	229	743	Caño Delgadito	Caño Delgadito
18	230	744	Capital San Genaro de Boconoito	Boconoito
18	230	745	Antolín Tovar	San Nicolás
18	231	746	Capital San Rafael de Onoto	San Rafael de Onoto
18	231	747	Santa Fe	Santa Fe
18	231	748	Thermo Morles	El Algarrobito
18	232	749	Capital Santa Rosalía	El Playón
18	232	750	Florida	Nueva Florida
18	233	751	Capital Sucre	Biscucuy
18	233	752	Concepción	La Concepción
18	233	753	San Rafael de Palo Alzado	San Rafael de Palo Alzado
18	233	754	Uvencio Antonio Velásquez	Las Cruces
18	234	757	Capital Turén	Villa Bruzual
18	234	758	Canelones	La Misión
18	234	759	Santa Cruz	Santa Cruz
18	234	760	San Isidro Labrador	Colonia Turén
19	235	761	Mariño	Casanay
19	235	762	Rómulo Gallegos	San Vicente
19	236	763	San José de Aerocuar	San José de Aerocuar
19	236	764	Tavera Acosta	Río Casanay
19	237	765	Río Caribe	Río Caribe
19	237	766	Antonio José de Sucre	San Juan de Unare
19	237	767	El Morro de Puerto Santo	El Morro de Puerto Santo
19	237	768	Puerto Santo	Puerto Santo
19	237	769	San Juan de Las Galdonas	San Juan de Las Galdonas
19	238	771	El Rincón	El Rincón
19	238	772	General Francisco Antonio Vásquez	Los Arroyos
19	238	773	Guaraúnos	Guaraúnos
19	238	774	Tunapuicito	Tunapuicito
19	239	776	Bolívar	Playa Grande
19	239	777	Macarapana	Carúpano
19	239	778	Santa Catalina	Carúpano
19	239	779	Santa Rosa	Carúpano
19	239	780	Santa Teresa	Carúpano
19	240	781	Capital Bolívar	Marigüitar
19	241	782	Yaguaraparo	Yaguaraparo
19	241	783	El Paujil	El Paujil
19	241	784	Libertad	Río Seco
19	242	785	Araya	Araya
19	242	786	Chacopata	Chacopata
19	242	787	Manicuare	Manicuare
19	243	788	Tunapuy	Tunapuy
19	243	789	Campo Elías	Guayana
19	244	790	Irapa	Irapa
19	244	791	Campo Claro	Campo Claro
19	244	792	Marabal	Marabal
19	244	793	San Antonio de Irapa	San Antonio de Irapa
19	244	794	Soro	Soro
19	245	795	Capital Mejía	San Antonio del Golfo
19	246	796	Cumanacoa	Cumanacoa
19	246	797	Arenas	Arenas
19	246	798	Aricagua	Aricagua
19	246	799	Cocollar	Las Piedras
19	246	800	San Fernando	Villarroel (Quebrada Seca)
19	247	802	Villa Frontado (Muelle de Cariaco)	Cariaco
19	247	803	Catuaro	Catuaro
19	247	804	Rendón	(Muelle de Cariaco)
19	247	805	Santa Cruz	Santa Cruz
19	247	806	Santa María	Santa María
19	248	807	Altagracia	Cumaná
19	248	808	Ayacucho	Cumaná
19	248	809	Santa Inés	Cumaná
19	248	810	Valentín Valiente	Caigüire
19	248	811	San Juan	San Juan
19	248	813	Santa Fe	Los Altos
19	249	814	Güiria	Güiria
19	249	815	Bideau	Rio Salado
19	249	816	Cristóbal Colón	Macuro
19	249	817	Punta de Piedras	Yoco
20	250	818	Capital Andrés Bello	Cordero
20	251	819	Capital Antonio Rómulo Costa	Las Mesas
20	252	820	Ayacucho	Colón
20	252	821	Rivas Berti	San Félix
20	252	822	San Pedro del Río	San Pedro del Río
20	253	823	Bolívar	San Antonio del Táchira
20	253	824	Palotal	Palotal
20	253	825	Juan Vicente Gómez	El Recreo
20	253	826	Isaías Medina Angarita	Las Dantas
20	254	827	Cárdenas	Táriba
20	254	828	Amenodoro Rangel Lamús	Palo Gordo
20	254	829	La Florida	La Florida
20	255	830	Capital Córdoba	Santa Ana
20	256	831	Fernández Feo	San Rafael del Piñal
21	279	887	La Esperanza	El Gallo
21	280	888	Boconó	Boconó
21	280	889	El Carmen	El Carmen
21	280	890	Mosquey	Mosquey
21	280	891	Ayacucho	Batatal
21	280	892	Burbusay	Burbusay
21	280	893	General Rivas	Las Mesitas
21	281	900	Sabana Grande	Sabana Grande
21	281	901	Cheregüé	Altamira de Caús
21	281	902	Granados	Granados
21	282	903	Chejendé	Chejendé
21	282	904	Arnoldo Gabaldón	Minas
21	282	905	Bolivia	Bolivia
21	282	906	Carrillo	Torococo
21	282	907	Cegarra	Mitón
21	282	908	Manuel Salvador Ulloa	Sabana Grande
21	282	909	San José	Las Llanadas
21	283	910	Carache	Carache
21	283	911	Cuicas	Cuicas
21	283	912	La Concepción	La Concepción
21	283	913	Panamericana	El Zapatero
21	283	914	Santa Cruz	La Cuchilla
21	284	915	Escuque	Escuque
21	284	916	La Unión	El Alto
21	284	917	Sabana Libre	Sabana Libre
21	284	918	Santa Rita	La Mata
21	285	919	El Socorro	El Paradero
21	285	920	Antonio José de Sucre	La Placita
21	285	921	Los Caprichos	Los Caprichos
21	286	922	Campo Elías	Campo Elías
21	286	923	Arnoldo Gabaldón	Las Quebradas
21	287	924	Santa Apolonia	Santa Apolonia
21	287	925	El Progreso	Zona Rica
21	287	926	La Ceiba	La Ceiba
21	287	927	Tres de Febrero	Tres de Febrero
21	288	928	El Dividive	El Dividive
21	288	929	Agua Santa	Agua Santa
21	288	930	Agua Caliente	Agua Caliente
21	288	931	El Cenizo	El Cenizo
21	288	932	Valerita	Valerita
21	289	933	Monte Carmelo	Monte Carmelo
21	289	934	Buena Vista	Buena Vista
21	289	935	Santa María del Horcón	Casa de Tabla
21	290	936	Motatán	Motatán
21	290	937	El Baño	El Baño
21	290	938	Jalisco	Jalisco
21	291	939	Pampán	Pampán
21	291	940	Flor de Patria	Flor de Patria
21	291	941	La Paz	Monay
13	148	479	Bolívar	El Tocuyo
20	257	834	Capital Francisco de Miranda	San José de Bolívar
20	258	835	Capital García de Hevia	La Fría
20	259	838	Capital Guásimos	Palmira
20	260	839	Independencia	Capacho Nuevo
20	260	840	Juan Germán Roscio	El Valle
20	260	841	Román Cárdenas	Peribeca
20	261	842	Jáuregui	La Grita
20	261	843	Emilio Constantino Guerrero	Pueblo Hondo
20	261	844	Monseñor Miguel Antonio Salas	Sabana Grande
20	262	845	Capital José María Vargas	EL Cobre
20	263	846	Junín	Rubio
20	263	848	Quinimarí	San Vicente de la Revancha
20	264	850	Libertad	Capacho Viejo
20	264	851	Cipriano Castro	Hato de la Virgen
20	264	852	Manuel Felipe Rugeles	El Pueblito
20	265	853	Libertador	Abejales
20	265	854	Emeterio Ochoa	Puerto Nuevo
20	265	855	Doradas	El Milagro
20	266	857	Lobatera	Lobatera
20	266	858	Constitución	Borotá
20	267	859	Capital Michelena	Michelena
20	268	860	Panamericano	Coloncito
20	268	861	La Palmita	La Palmita
20	269	862	Pedro María Ureña	Ureña
20	269	863	Nueva Arcadia	Aguas Calientes
20	270	864	Capital Rafael Urdaneta	Delicias
20	271	865	Samuel Darío Maldonado	La Tendida
20	271	866	Boconó	Boconó
20	271	867	Hernández	Hernández
20	272	868	La Concordia	San Cristóbal
20	272	869	Pedro María Morantes	San Cristóbal
20	272	870	San Juan Bautista	San Cristóbal
20	273	873	Capital Seboruco	Seboruco
20	274	874	Capital Simón Rodríguez	San Simón
20	275	875	Sucre	Queniquea
20	275	876	Eleazar López Contreras	Mesa del Tigre
20	275	877	San Pablo	San Pablo
20	276	878	Capital Torbes	San Josecito
20	277	879	Capital Uribante	Pregonero
20	277	880	Cárdenas	La Fundación
20	277	881	Juan Pablo Peñaloza	Laguna de García
20	277	882	Potosí	Patio Redondo
20	278	883	Capital San Judas Tadeo	Umuquena
21	291	942	Santa Ana	Santa Ana
21	292	943	Pampanito	Pampanito
21	292	944	La Concepción	La Concepción
21	292	945	Pampanito II	Pampanito II
21	293	946	Betijoque	Betijoque
21	293	947	La Pueblita	Las Rurales
21	293	948	Los Cedros	Los Cedros
21	293	949	José Gregorio Hernández	Isnotú
21	294	950	Carvajal	Carvajal
21	294	951	Antonio Nicolás Briceño	La Cejita
21	294	952	Campo Alegre	Campo Alegre
23	314	1013	Arístides Calvani	Palito Blanco
23	314	1014	Punta Gorda	Punta Gorda
23	320	1035	Potreritos	Potreritos
23	323	1049	Monseñor Marcos Sergio Godoy	Cachirí
23	323	1051	Tamare	Tamare
23	324	1058	Coquivacoa	Maracaibo
23	324	1059	Chiquinquirá	Maracaibo
21	298	974	San Luis	San Luis
21	298	975	La Puerta	La Puerta
22	299	977	Capital Arístides Bastidas	San Pablo
22	300	978	Capital Bolívar	Aroa
22	301	979	Capital Bruzual	Chivacoa
22	301	980	Campo Elías	Campo Elías
22	302	981	Capital Cocorote	Cocorote
22	303	982	Capital Independencia	Independencia
22	304	983	Capital José Antonio Páez	Sabana de Parra
22	305	984	Capital La Trinidad	Boraure
22	306	985	Capital Manuel Monge	Yumare
22	307	986	Capital Nirgua	Nirgua
22	307	987	Salom	Salom
22	307	988	Temerla	Temerla
22	308	991	Capital San Felipe	San Felipe
22	308	992	Albarico	Albarico
22	308	993	San Javier	Marín
22	309	994	Capital Sucre	Guama
22	310	995	Capital Urachiche	Urachiche
22	311	996	Capital Veroes	Farriar
22	311	997	El Guayabo	Casimiro Vásquez
23	312	998	Isla de Toas	El Toro
23	312	999	Monagas	San Carlos
23	313	1000	San Timoteo	San Timoteo
23	313	1001	General Urdaneta	Ceuta
23	313	1002	Libertador	Mene Grande
23	313	1003	Manuel Guanipa Matos	El Venado
23	313	1004	Marcelino Briceño	El Tigre
23	314	1006	Ambrosio	Cabimas
23	314	1007	Carmen Herrera	Cabimas
23	314	1008	Germán Ríos Linares	Cabimas
23	314	1009	La Rosa	Cabimas
23	314	1010	Jorge Hernández	Cabimas
23	314	1012	San Benito	Cabimas
23	315	1015	Encontrados	Encontrados
23	315	1016	Udón Pérez	El Guayabo
23	316	1017	San Carlos del Zulia	San Carlos del Zulia
23	316	1018	Moralito	El Moralito
23	316	1019	Santa Bárbara	Santa Bárbara
23	316	1020	Santa Cruz del Zulia	Santa Cruz del Zulia
23	316	1021	Urribarri	Concha
22	334	989	Capital Peña	Yaritagua
22	334	990	San Andrés	Cambural
23	317	1022	Simón Rodríguez	Pueblo Nuevo El Chivo
23	317	1023	Carlos Quevedo	Cuatro Esquinas
23	317	1024	Francisco Javier Pulgar	Los Naranjos
23	318	1025	La Concepción	La Concepción
23	318	1026	José Ramón Yepes	La Paz
23	318	1027	Mariano Parra León	Jobo Alto (Kilómetro 25)
23	318	1028	San José	San José
23	319	1029	Jesús María Semprún	Casigua El Cubo
23	319	1030	Barí	El Cruce
23	320	1031	Concepción	Concepción
23	320	1032	Andrés Bello	Kilómetro 48 (Santo Domingo)
23	320	1033	Chiquinquirá	La Ensenada
23	320	1034	El Carmelo	El Carmelo
23	321	1036	Alonso de Ojeda	Ciudad  Ojeda
23	321	1037	Libertad	Ciudad  Ojeda
23	321	1038	Campo Lara	Campo Lara
23	321	1039	Eleazar López Contreras	Picapica
23	321	1040	Venezuela	Lagunillas
23	322	1041	Libertad	Machiques
23	322	1042	Bartolomé de las Casas	Las Piedras
23	322	1043	Río Negro	Río Negro
23	322	1044	San José de Perijá	San José
23	323	1045	San Rafael	San Rafael de El Moján
23	323	1046	La Sierrita	La Sierrita
23	323	1047	Las Parcelas	Las Parcelas
23	323	1048	Luis de Vicente	Carrasquero
23	323	1050	Ricaurte	Santa Cruz de Mara
23	324	1052	Antonio Borjas Romero	Maracaibo
23	324	1053	Bolívar	Maracaibo
23	324	1054	Cacique Mara	Maracaibo
23	324	1055	Caracciolo Parra Pérez	Maracaibo
23	324	1056	Cecilio Acosta	Maracaibo
23	324	1057	Cristo de Aranza	Maracaibo
23	325	1070	Altagracia	Los Puertos de Altagracia
23	325	1071	Ana María Campos	El Mecocal
23	325	1072	Faría	Quisiro
4	30	91	Queseras del Medio	Guasimal
4	36	108	Urbana San Fernando	San Fernando de Apure
4	36	109	El Recreo	El Recreo
4	36	110	Peñalver	Arichuna
4	36	111	San Rafael de Atamaica	San Rafael de Atamaica
6	58	182	Manuel Palacio Fajardo	La Caramuca
7	68	225	Altagracia	Las Bonitas
7	68	227	Guaniamo	El Milagro
7	68	228	La Urbana	La Urbana
7	68	229	Pijiguaos	Morichalito
7	71	238	Vista Hermosa	Ciudad Bolívar
7	71	239	Orinoco	Almacén
7	71	240	Panapana	San José de Bongo
7	71	241	Zea	La Carolina
8	90	297	No Urbana Negro Primero	Los Naranjos
13	151	502	Las Mercedes	Burere
13	151	503	Manuel Morillo	El Empedrado
13	151	504	Montaña Verde	Palmarito
13	151	505	Montes de Oca	San Francisco
13	151	506	Torres	El Jabón
13	151	507	Reyes Vargas	Parapara
13	151	508	Altagracia	Altagracia
14	164	564	El Morro	El Morro
14	164	565	Los Nevados	Los Nevados
14	173	590	El Llano	Tovar
14	173	591	San Francisco	Tovar
14	173	592	Tovar	Tovar
14	174	595	María de la Concepción Palacios Blanco	Las Virtudes
14	174	596	Santa Apolonia	Santa Apolonia
15	176	606	Ribas	Tapipa
16	200	662	San Agustín	San Agustín
16	200	663	Teresén	Teresén
16	204	679	Santa Cruz	Maturín
16	204	680	El Corozo	El Corozo
16	204	681	El Furrial	El Furrial
16	204	682	Jusepín	Jusepín
16	204	683	La Pica	La Pica
16	204	684	San Vicente	San Vicente
17	218	714	Capital Península de Macanao	Boca del Río
17	218	715	San Francisco	Boca del Pozo
18	224	727	San José de la Montaña	San José de la Montaña
23	326	1075	Sinamaica	Sinamaica
23	326	1076	Alta Guajira	Cojoro
23	327	1079	El Rosario	La Villa del Rosario
23	327	1080	Donaldo García	Barranquitas
23	327	1081	Sixto Zambrano	San Ignacio
23	328	1082	San Francisco	San Francisco
23	328	1083	El Bajo	El Bajo
23	328	1084	Domitila Flores	El Silencio
23	328	1085	Francisco Ochoa	Sierra Maestra
23	329	1088	Santa Rita	Santa Rita
23	329	1089	El Mene	El Mene
23	329	1090	José Cenovio Urribarri	Palmarejo
23	329	1091	Pedro Lucas Urribarri	El Guanábano
23	330	1092	Manuel Manrique	Tía Juana
23	330	1093	Rafael María Baralt	San Isidro
23	330	1094	Rafael Urdaneta	Sabana de La Plata
23	331	1095	Bobures	Bobures
23	331	1096	El Batey	El Batey
23	331	1097	Gibraltar	Gibraltar
23	331	1098	Heras	San Antonio
23	331	1099	Monseñor Arturo Celestino Alvarez	Santa María
23	332	1101	La Victoria	Bachaquero
23	332	1102	Rafael Urdaneta	Bachaquero
23	332	1103	Raúl Cuenca	El Corozo
24	333	1104	Caraballeda	Caraballeda
24	333	1105	Carayaca	Carayaca
24	333	1106	Caruao	La Sabana
24	333	1107	Catia La Mar	Catia La Mar
24	333	1108	El Junko	El Junko
24	333	1109	La Guaira	La Guaira
\.


--
-- Data for Name: tbl_pers_disc_visual; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_pers_disc_visual (id, nombre, apellido, nacionalidad, cedula, sexo, co_estado, co_municipio, co_parroquia, lugar_nacimiento, fech_nacimiento, telf_habitacion, telf_celular, correo, direccion, tenencia_vivienda_id, tipo_vivienda_id, nom_ape_pers_contacto, telef_pers_contacto, direccion_pers_contacto, usuario_id, estatus, created_at, updated_at, horario_id) FROM stdin;
300	pruebasistema	prueba	V	123456	F	2	2	1122	caracas	1993-04-08	0	4265176468		jjjjjj	2	2		0		1	t	2012-04-08 01:37:58	2012-04-26 05:25:09	\N
311	ROXENIS	SANOJA	V	18804762	F	1	1	19	el sombrero	1987-10-03	2120000000	4123456787	mireina@gmail.com	Esquina la cotufa	3	2		0		1	t	2012-07-10 07:34:58	\N	\N
340	MARIA ALEJANDRA	LEON 	V	12099999	F	3	10	33	caracas	1994-03-09	2120000009	4123456787	elalconxvii@gmail	el sombrero guarico	2	1	ROSA	98808989878	sooooo mm	5	t	2012-07-19 06:36:48	2012-07-19 07:45:23	2
312	TOMAS	SANTANDER	V	17111111	M	1	1	4	CARACAS	2012-07-10	2121231212	2121231212	tomas@hotmail.com	UD-2 CARICUAO	2	2	ROSA	2121231212	AVENIDA CASANOVA	4	t	2012-07-10 11:25:40	\N	\N
306	yinny	moraless	V	1234567	M	1	1	10	caracas	2012-04-01	2123456789	416888888		calle 1, apto 16 piso 6	1	2	luisa	4265176468	la misma	3	t	2012-04-22 04:43:18	2012-04-22 04:51:16	\N
337	SANTANDER	ELIAS DAVID	V	17121212	M	1	1	4	caracas	1981-07-09	4263232323	2121231212	elias@hotmail.com	lkhkjhkhkj	4	2	FULANITO	4264044700	jhhhjg	1	t	2012-07-12 08:20:28	\N	\N
338	ELIAS	SANTANDER	V	17131313	M	15	178	609	caracas	1981-07-02	2120000000	4123456787	elalconxvii@gmail.com	hjghjghg	2	2	JKHKJHJK	2121231212	hghjghg	1	t	2012-07-12 08:58:51	\N	\N
339	ALFA	ALFA	V	17123123	F	2	2	1122	pruebas	1993-07-01	2122345432	4127892345	elalconxvii@gmail.com	Prueba de 	2	1	PRUEBA	2123456543	zxczcxzxczxczxczxc	1	t	2012-07-18 11:01:08	\N	\N
316	PP	PRUEBA H	V	12	F	5	42	129		2012-07-01	0	0			4	2		0		5	t	2012-07-11 10:19:04	\N	\N
305	Roxenis s	Sanoja	V	98745600	F	5	42	129	guarissco	1996-04-16	65656646545	87876767756		gbhjg	5	1	mama	9999099	ghfhfgh	1	t	2012-04-11 07:06:42	2012-04-15 11:06:41	\N
307	juan	santander	V	17167137	M	1	1	4	vargas	2012-04-19	2123455545	2121234567	yarelis@hotmail.com	calle bolivar	4	1	yinny morales|	9999000	calle londrew	5	t	2012-04-26 06:27:16	\N	\N
308	Aridanes	Marrero	V	12345678	M	2	3	24	Vargas	1994-05-03	0	0	elmatatan@tumeta.com	Prueba	2	2	Prueba	612222		1	t	2012-05-18 03:45:32	\N	\N
309	prueba2	prueba	V	15662264	F	2	2	1122	caracas	2012-06-06	9999047	9999047	yq@hotmail	jrsj	2	2		0		1	t	2012-07-05 05:31:35	\N	\N
323	ROXENIS	ROXENIO	V	11119	M	2	3	24	CARACAS	2000-07-04	2120000000	4123456787	elalconxvii@gmail.com		2	2		0		1	t	2012-07-11 11:48:36	\N	\N
324	DFV	SDVS	V	1234	F	5	39	118	caracas	1982-07-02	0	0			2	2		0		5	t	2012-07-12 12:04:09	\N	\N
313	TOMAS	SANTANDER	V	17242424	M	3	29	85	CARACAS	2012-07-10	2121231212	2121231212	ts@hotmail.com	TJRUU	4	2	ROSA	2121231212	TRUTYU	1	t	2012-07-10 11:33:18	2012-07-11 02:38:30	2
329	GRWG	GR	V	5678	F	7	71	237	rrr	1993-07-10	0	0		gr	4	3		0	gwr	5	t	2012-07-12 12:19:16	\N	\N
341	ARIDANES	JJJJJJ	V	12233333	F	4	31	92	ewe	1993-10-01	2120000009	2121231212	elalconxvii@gmail	efewf hh	4	2	DFE	2121231212	ef	3	t	2012-10-08 07:47:14	2012-10-09 06:17:23	9
352	PEDRO	BLANCO	V	85000009	F	3	29	85	CARACAS	1970-10-20	2120000000	4120154915	wdd@ddddd.com		2	2	ROXENIS	98808989878		1	t	2012-10-23 04:47:28	\N	\N
353	roxenis	ROXENIO	V	89098765	F	3	29	85	CARACAS	1963-10-16	2121231212	4120154915	ts@hotmail.com		2	2	ROXENIS	4124496029		1	t	2012-10-23 05:06:02	\N	\N
354	roxenis	Prueba de sistem	V	89123456	F	4	32	95	CARACAS	1995-10-25	2120000000	4123456787	elmatatan@tumeta.com	sdfsfsdf	4	2	GHGFGH	8888888		1	t	2012-10-23 05:08:35	\N	\N
356	roxenis	PRUEBA DE SISTEM	V	111222	F	2	4	1123	123213213	1978-10-20	2120000000	4120154915	elmatatan@tumeta.com		2	1		0		1	t	2012-10-23 05:41:36	\N	\N
317	GREGORIO	PRUEBA 	V	12345	F	2	3	1120	caracas	2012-07-04	0	0			3	2		0		5	t	2012-07-11 10:35:44	\N	\N
304	Prueba	Prueba	V	17442931	F	2	8	1133	vargas	1950-10-20	2120000000	766757656	elmatatan@tumeta.com	eeeee	2	2	gmm	8888888		1	t	2012-04-08 01:47:27	2012-07-11 02:35:48	2
310	BOLIVARjj	GREGORIOsss	V	17000001	F	3	10	33	Caracas	1983-07-01	2125612114	4265176468	elalconxvii@gmail.com	sss	4	1	Alfonzo Ruiz	9798797		1	t	2012-07-09 02:16:23	\N	\N
357	ALBORNO	BONILLA	E	82033333	F	2	2	23	CARACAS	1987-10-01	2121231212	4120154915	elalconxvii@gmail.com	dddd	2	2	ROXENIS	3453453453		1	t	2012-10-23 07:21:10	\N	\N
322	ROXENIS	ROXENIO	V	1111	M	2	3	24	CARACAS	2000-07-04	2120000000	4123456787	elalconxvii@gmail.com		2	2		0		1	t	2012-07-11 11:47:20	\N	\N
321	PRUe	PRUEBA	V	123	F	2	3	24	caracas	2012-07-01	0	0			4	1		0		1	t	2012-07-11 10:47:11	2012-07-11 11:02:51	2
\.


--
-- Data for Name: tbl_profesion; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_profesion (profesion_id, profesion) FROM stdin;
1	SIN PROFESION
2	ARQUITECTO                    
3	ABOGADO                       
4	ECONOMISTA                    
5	ESP. EN DERECHO INTERNACIONAL 
6	ESP. EN FINANAZAS PARA EMPRESA
7	ESP. EN CCS. ADMINISTRATIVAS  
8	ESP. EN NEGOCIACIONES ECONOMIC
9	ESP. EN SIST. DE INFORMACION  
10	ESP. EN GERENCIA DE RR.HH.    
11	ESP. EN MERCADEO Y VENTAS     
12	ESP. EN ADMON. DE EMPRESAS    
13	ESP. EN CONTADURIA PUBLICA    
14	ESP. EN DESARROLLO ORGANIZACIONAL
15	ESP. EN RELACIONES INDUSTRIALE
16	ESP. EN RELACIONES PUBLICAS   
17	ESP. EN COMUNICACION ORGANIZACIONAL
18	ESP. EN DERECHO TRIBUTARIO    
19	ESP. EN ADMON. DE RR.HH.      
20	ESP. EN PUBLICIDAD Y MERCADEO 
21	ESP. EN INGENIERIA GENERAL    
22	INGENIERO AERONAUTICO         
23	INGENIERO CIVIL               
24	INGENIERO ADMON. DE OBRAS     
25	INGENIERO DISEÑO INDUSTRIAL   
26	INGENIERO DE MANTENIMIENTO    
27	INGENIERO MTTO. MECANICO      
28	INGENIERO DE MINAS            
29	INGENIERO DE MATERIALES       
30	INGENIERO DE SISTEMAS         
31	INGENIERO ELECTRICO           
32	INGENIERO EN ELECTRONICA      
33	INGENIERO EN COMPUTACION      
34	INGENIERO EN INFORMATICA      
35	INGENIERO EN GEOFISICA        
36	INGENIERO EN GEOLOGIA         
37	INGENIERO HIDROMETEOROLOGIA   
38	INGENIERO INDUSTRIAL          
39	INGENIERO MECANICO            
40	INGENIERO METALURGICO         
41	INGENIERO QUIMICO             
42	LIC. ADMINISTRACION ADUANERA  
43	LIC. ADMINISTRACION BANCARIA  
44	LIC. ADMINISTRACION COMERCIAL 
45	LIC. ADMON. CONTABILIDAD COSTO
46	LIC. ADMINISTRACION DE COMPRAS
47	LIC. ADMINISTRACION DE EMPRESA
48	LIC. ADMON. EMPRESAS DE DISEÑO
49	LIC. ADMON. EMPRESAS TURISTICA
50	LIC. ADMON. DE PERSONAL       
51	LIC. ADMINISTRACION DE VENTAS 
52	LIC. ADMON. DE HIDROCARBUROS  
53	LIC. ADMINISTRACION INDUSTRIAL
54	LIC. ADMON. INFORMATICA       
55	LIC. ADMON. DE MERCADEO       
56	LIC. ADMINISTRACION MUNICIPAL 
57	LIC. ADMON. ORG. Y METODOS    
58	LIC. ADMON. DE PRESUPUESTOS   
59	LIC. ADMINISTRACION PUBLICA   
60	LIC. ADMON. REC. FINANCIEROS  
61	LIC. ADMON. DE RIESGOS Y SEGUR
62	LIC. ANALISIS DE SISTEMAS     
63	LIC. BIBLIOTECOLOGIA Y ARCHIVO
64	LIC. EN CIENCIAS ESTADISTICAS 
65	LIC. EN CIENCIAS FISCALES RENT
66	LIC. EN COMERCIO EXTERIOR     
67	LIC. EN COMPUTACION           
68	LIC. EN COMUNICACION SOCIAL   
69	LIC. CONTABILIDAD COMPUTARIZ. 
70	LIC. EN CONTADURIA PUBLICA    
71	LIC. CONTADURIA - ADMINISTRAC.
72	LIC. EN CONTROL DE CALIDAD    
73	LIC. EN DISEÑO GRAFICO        
74	LIC. EN DISEÑO INDUSTRIAL     
75	LIC. EN DISEÑO DE OBRAS       
76	LIC. EN EDUCACION             
77	LIC. EN ESTADISTICAS          
78	LIC. ELECTRICIDAD Y TEC ELECTR
79	LIC. ELECTRONICA INDUSTRIAL   
80	LIC. ELECTRONICA/COMUNICAC.   
81	LIC. EN ENFERMERIA            
82	LIC. EN ESTUDIOS AMBIENTALES  
83	LIC. EN ESTUDIOS INTERNACIONAL
84	LIC. ESTUDIOS POLITICOS Y ADMO
85	LIC. EN CIENCIAS POLITICAS    
86	LIC. EN FARMACIA              
87	LIC. EN FISICA                
88	LIC. GERENCIOA DE RR.HH.      
89	LIC. EN HIDROCARBUROS         
90	LIC. EN IDIOMAS MODERNOS      
91	LIC. EN INFORMATICA           
92	LIC. EN LETRAS                
93	LIC. MTTO. EQUIPOS ELECTRICOS 
94	LIC. MANTENIMIENTO INDUSTRIAL 
95	LIC. MANTENIMIENTO MECANICO   
96	LIC. EN MATEMATICAS           
97	LIC. MERCADOTECNIA-ADMINISTRAC
98	LIC. EN MERCADEO              
99	LIC. METALURGIA Y CCIAS MATER 
100	LIC. METALURGIA Y SIDERURGIA  
101	LIC. EN MINERIA               
102	LIC. ORGANIZACION EMPRESARIAL 
103	LIC. EN PETROLEO              
104	LIC. EN PLANIFICACION         
105	LIC. PROCESOS QUIMICOS        
106	LIC. PRODUCCION INDUSTRIAL    
107	LIC. EN SICOLOGIA             
108	LIC. PUBLICIDAD Y MERCADEO    
109	LIC. EN QUIMICA               
110	LIC. QUIMICA INDUSTRIAL       
111	LIC. RELACIONES INDUSTRIALES  
112	LIC. RELACIONES PUBLICAS      
113	LIC. EN RIEGOS Y SEGURO       
114	LIC. SEGURIDAD INDUSTRIAL     
115	LIC. SIST. ADMON. MTTO.       
116	LIC. SIST. ADMON. SERVCIOOS   
117	LIC. EN SOCIOLOGIA            
118	LIC. TECNOLOGIA AUTOMOTRIZ    
119	LIC. TECNOLOGIA DE INCENDIOS  
120	LIC. TECNOLOGIA DE MATERIALES 
121	LIC. TECN. COSTRUCC. CIVIL    
122	LIC. TECNOLGIA EN ESTADISTICAS
123	LIC. TECNOLOGIA QUIMICA       
124	LIC. TOPOGRAFIA               
125	LIC. ADMINISTRACION DE RR.HH. 
126	LIC. ADMINISTRACION GERENCIA  
127	LIC. CIENCIAS ADMINISTRATIVAS 
128	LIC. EN RECURSOS HUMANOS      
129	MASTER EN GERENCIA EMPRESARIAL
130	MASTER EN DESARROLLO ORGANIZAC
131	MASTER EN ADMON. EMP. FINANZAS
132	MASTER ADMON. EMPR. MERCADEO  
133	MASTER CCIAS. PENALES Y CRIMIN
134	MASTER INSTITUCIONES FINANC.  
135	MASTER EN GERENCIA DE RR.HH.  
136	MASTER EN DERECHO LABORAL     
137	MASTER EN ADMON. DE NEGOCIOS  
138	MASTER EN ECONOMIA INTERNACIONAL  
139	MASTER EN INGENIERIA MECANICA 
140	MASTER EN INGENIERIA GERENCIAL
141	MASTER EN CIENCIAS            
142	MASTER EN CCS DE LA INGENIERIA
143	MASTER EN COMUNICACION SOCIAL 
144	MASTER EN INGENIERIA INDUSTRIAL
145	MEDICO                        
146	TSU ADMON. BANCA Y FINANZAS   
147	TSU ADMINISTRACION DE EMPRESAS
148	TSU ADMINISTRACION INDUSTRIAL 
149	TSU EN ADMINISTRACION         
150	TSU EN ADMON. PRESUPUESTARIA  
151	TSU EN ADMON. TRIBUTARIA      
152	TSU EN ANALISIS DE SISTEMAS   
153	TSU EN COMPUTACION            
154	TSU EN CONTROL DE CALIDAD     
155	TSU EN GEOLOGIA               
156	TSU EN INFORMATICA            
157	TSU EN RELACIONES INDUSTRIALES
158	TSU EN TRABAJO SOCIAL         
159	TSU EN QUIMICA                
160	TSU EN QUIMICA INDUSTRIAL     
161	TSU EN ORGANIZ. EMPRESARIAL   
162	TSU EN ORGANIZACION Y SISTEMAS
163	TSU EN INGENIERIA MECANICA    
164	TSU EN DISEÑO INDUSTRIAL      
165	TSU EN HIGIENE Y SEG. INDUSTRI
166	TSU EN PRODUCCION INDUSTRIAL  
167	TSU EN PUBLICIDAD Y MERCADEO  
168	TSU EN RIESGOS Y SEGUROS      
169	TSU EN SEGURIDAD INDUSTRIAL   
170	TSU EN SISTEMAS DE INFORMACION
171	TSU ADMINISTRACION DE PERSONAL
172	TSU EN COMERCIO EXTERIOR      
173	TSU EN DISEÑO GRAFICO         
174	DIBUJANTE ARQUITECTONICO      
175	TSU EN RECURSOS HUMANOS       
176	TSU EN CONTADURIA Y FINANZAS  
177	TSU EN MERCADEO               
178	TSU EN ELECTRICIDAD           
179	TSU EN ELECTRONICA            
180	TSU EN CONTABILIDAD DE COSTOS 
181	TSU EN MINAS                  
182	TSU EN CONSTRUCCION CIVIL     
183	TSU EN ENSAMBLAJE INDUSTRIALES
184	TSU EN MATERIALES INDUSTRIALES
185	TSU EN SECRETARIA             
186	TSU EN MACANICO INDUSTRIAL    
187	TSU EN TECNOLOGIA AUTOMOTRIZ  
188	ESP. EN CONTROL AMBIENTAL     
189	TSU EN SEG. E HIG. AMBIENTAL  
190	TSU EN PRODUCCION INDUSTRIAL  
191	ESP. EN GERENCIA DE CALIDAD   
192	ESP. EN DERECHO MERCANTIL     
193	ESP. GERENCIA EMPRESARIAL     
194	TSU EN ALIMENTOS              
195	INGENIERO AEROESPACIAL        
196	TSU  MANTENIMIENTO INDUSTRIAL 
197	ING. AMBIENTAL                
198	TSU EN TECNOLOGIA PETROLERA   
199	DOCTORADO ADMON. DE EMPRESAS  
200	DOCTORADO EN DERECHO          
201	ESP. EN FILOSOFIA E HISTARIA  
202	LIC. HISTORIA                 
203	LIC. PSICOLOGIA ORGANIZACIONAL
204	LIC. EDUCACION                
205	LIC. CIENCIAS Y ARTES MILITARE
206	TSU. ANALISI Y DISEÑOS        
207	LIC. TURISMO                  
208	LIC. GERENCIA EMPRESARIAL     
209	TSU ADUANA                    
210	TSU ORGANIZACION EMPRESARIAL  
211	LIC. ESTADISTICA              
212	LIC. ADMINISTRACION           
213	LIC. EN FINANZAS              
214	LIC. CIENCIAS GERENCIALES     
215	ING. PETROLEO                 
216	TSU CONTABILIDAD Y FINANZAS   
217	ING. AUTOMOTRIZ               
218	LIC. TRABAJO SOCIAL COMUNITARI
219	DOCTORADO GOB Y PODER CIUDADAN
220	LIC. TECNOLOGO EN FABRICACION 
221	LIC. GEOGRAFIA                
222	ESP. GERENCIA LOGISTICA       
223	ING. TELECOMUNICACIONES       
224	LIC. ADMON .RECURSOS MATERIALE
225	ESP. PLANIF. Y EVALUACION     
226	TSU CIENCIAS POLICIALES       
227	DIPLOMADO GEOLOGO             
228	TSU EN PERITO FISCAL          
229	ESP. EN PLANIF. GLOBAL        
230	MASTERL DE IMPLANTOLOGIA      
231	MAESTRIA GESTION PUBLICA INTEG
232	PEDIATRA                      
233	LIC.MECANICA INDUSTRIAL       
234	TSU INSTRUMENTACION           
235	ING. GEOLOGO                  
236	MAESTRIA PROF. GESTION CALIDAD
237	LIC. EN ARTES                 
238	TSU  ADMINISTRACION INFORMATICA
239	OTRO
0	Ninguna
\.


--
-- Data for Name: tbl_tenencia_vivienda; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_tenencia_vivienda (tenencia_vivienda_id, tenencia_vivienda) FROM stdin;
1	Propia
2	Alquilada
3	Prestada
4	De un familiar
5	Herencia
\.


--
-- Data for Name: tbl_tipo_ayuda; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_tipo_ayuda (tipo_ayuda_id, tipo_ayuda, total_monto, anio_autorizado, usuario_id, estatus, created_at) FROM stdin;
1	Medicinas	2500	2012	1	t	2012-04-11 00:00:00
2	Lentes	2500	2012	1	t	2012-04-11 00:00:00
3	Utiles Escolares	2500	2012	1	t	2012-04-11 00:00:00
4	Bast&oacute;n	2500	2012	1	t	2012-04-11 00:00:00
5	Regleta	2500	2012	1	t	2012-04-11 00:00:00
8	lentes ultravioleta	1000	2012	5	t	2012-07-05 02:21:52
9	prueba	122	2012	5	t	2012-07-11 11:30:58
\.


--
-- Data for Name: tbl_tipo_cegera; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_tipo_cegera (tipo_ceguera_id, tipo_ceguera) FROM stdin;
1	ACCIDENTE
2	CATARATAS
3	GLAUCOMA
4	UVEITIS
5	TRACOMA
6	MATA DE COCO
7	PROSIGUE
\.


--
-- Data for Name: tbl_tipo_horario; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_tipo_horario (id, tipo_horario) FROM stdin;
1	A
2	B
3	C
4	D
5	E
6	F
7	G
8	H
9	I
10	J
11	K
\.


--
-- Data for Name: tbl_tipo_vivienda; Type: TABLE DATA; Schema: public; Owner: sispdv
--

COPY tbl_tipo_vivienda (tipo_vivienda_id, tipo_vivienda) FROM stdin;
1	Casa
2	Apartamento
3	Rancho
\.


SET search_path = audiperm, pg_catalog;

--
-- Name: Id_pk; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_auditoria
    ADD CONSTRAINT "Id_pk" PRIMARY KEY (id);


--
-- Name: cedula_unica; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_usuario
    ADD CONSTRAINT cedula_unica UNIQUE (cedula);


--
-- Name: login_clave_unico; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_usuario
    ADD CONSTRAINT login_clave_unico UNIQUE (login, clave);


--
-- Name: rol_pkey; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id);


--
-- Name: tbl_cargo_pkey; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_tipo_usuario
    ADD CONSTRAINT tbl_cargo_pkey PRIMARY KEY (id);


--
-- Name: unique_cnp; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_usuario
    ADD CONSTRAINT unique_cnp UNIQUE (apellido, nombre, cedula);


--
-- Name: unique_correo; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_usuario
    ADD CONSTRAINT unique_correo UNIQUE (correo_elec);


--
-- Name: usuario_pk; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (id);


--
-- Name: usuario_rol_id_pkey; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_usuario_rol
    ADD CONSTRAINT usuario_rol_id_pkey PRIMARY KEY (id);


--
-- Name: usuario_rol_pkey; Type: CONSTRAINT; Schema: audiperm; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_sub_rol
    ADD CONSTRAINT usuario_rol_pkey PRIMARY KEY (id);


SET search_path = public, pg_catalog;

--
-- Name: ayuda_pk; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_ayuda
    ADD CONSTRAINT ayuda_pk PRIMARY KEY (id);


--
-- Name: cedula_unica_medico; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_medico
    ADD CONSTRAINT cedula_unica_medico UNIQUE (med_cedula);


--
-- Name: co_municipio; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_municipio
    ADD CONSTRAINT co_municipio PRIMARY KEY (co_estado, co_municipio);


--
-- Name: co_parroquia; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_parroquia
    ADD CONSTRAINT co_parroquia PRIMARY KEY (co_estado, co_municipio, co_parroquia);


--
-- Name: correo_unico; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_padrino
    ADD CONSTRAINT correo_unico UNIQUE (correo);


--
-- Name: datos_unico; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_padrino
    ADD CONSTRAINT datos_unico UNIQUE (nombre, apellido, nacionalidad, cedula, correo);


--
-- Name: id; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_horario
    ADD CONSTRAINT id PRIMARY KEY (id);


--
-- Name: id_estado; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_estado
    ADD CONSTRAINT id_estado PRIMARY KEY (co_estado);


--
-- Name: id_pk; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_detalle_horario
    ADD CONSTRAINT id_pk PRIMARY KEY (id, hora_inicio, hora_fin, dia_id, area_id, horario_id);


--
-- Name: identificador_pk; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_ayuda_especial
    ADD CONSTRAINT identificador_pk PRIMARY KEY (id);


--
-- Name: pers_disc_visual_id_unica; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_diag_medico
    ADD CONSTRAINT pers_disc_visual_id_unica UNIQUE (pers_disc_visual_id);


--
-- Name: relacion_2_pk; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_padrino_tbl_grupo_familiar
    ADD CONSTRAINT relacion_2_pk PRIMARY KEY (id);


--
-- Name: relacion_id_pk; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_padrino_tbl_pers_disc_visual
    ADD CONSTRAINT relacion_id_pk PRIMARY KEY (id);


--
-- Name: tbl_area_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_area
    ADD CONSTRAINT tbl_area_pkey PRIMARY KEY (area_id);


--
-- Name: tbl_dia_dia_key; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_dia
    ADD CONSTRAINT tbl_dia_dia_key UNIQUE (dia);


--
-- Name: tbl_dia_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_dia
    ADD CONSTRAINT tbl_dia_pkey PRIMARY KEY (id);


--
-- Name: tbl_diag_medico_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_diag_medico
    ADD CONSTRAINT tbl_diag_medico_pkey PRIMARY KEY (diag_medico_id);


--
-- Name: tbl_estudio_persona_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_estudio_persona
    ADD CONSTRAINT tbl_estudio_persona_pkey PRIMARY KEY (estudio_id);


--
-- Name: tbl_grado_instruccion_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_grado_instruccion
    ADD CONSTRAINT tbl_grado_instruccion_pkey PRIMARY KEY (grado_inst_id);


--
-- Name: tbl_grupo_familiar_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_grupo_familiar
    ADD CONSTRAINT tbl_grupo_familiar_pkey PRIMARY KEY (familiar_id);


--
-- Name: tbl_laboral_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_laboral
    ADD CONSTRAINT tbl_laboral_pkey PRIMARY KEY (laboral_id);


--
-- Name: tbl_medicina_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_medicina
    ADD CONSTRAINT tbl_medicina_pkey PRIMARY KEY (medicina_id);


--
-- Name: tbl_medico_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_medico
    ADD CONSTRAINT tbl_medico_pkey PRIMARY KEY (medico_id);


--
-- Name: tbl_oficio_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_oficio
    ADD CONSTRAINT tbl_oficio_pkey PRIMARY KEY (oficio_id);


--
-- Name: tbl_padrino_cedula_key; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_padrino
    ADD CONSTRAINT tbl_padrino_cedula_key UNIQUE (cedula);


--
-- Name: tbl_padrino_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_padrino
    ADD CONSTRAINT tbl_padrino_pkey PRIMARY KEY (id);


--
-- Name: tbl_parentesco_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_parentesco
    ADD CONSTRAINT tbl_parentesco_pkey PRIMARY KEY (parentesco_id);


--
-- Name: tbl_pers_disc_visual_cedula_key; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_pers_disc_visual
    ADD CONSTRAINT tbl_pers_disc_visual_cedula_key UNIQUE (cedula);


--
-- Name: tbl_pers_disc_visual_id_PK; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_pers_disc_visual
    ADD CONSTRAINT "tbl_pers_disc_visual_id_PK" PRIMARY KEY (id);


--
-- Name: tbl_profesion_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_profesion
    ADD CONSTRAINT tbl_profesion_pkey PRIMARY KEY (profesion_id);


--
-- Name: tbl_tenencia_vivienda_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_tenencia_vivienda
    ADD CONSTRAINT tbl_tenencia_vivienda_pkey PRIMARY KEY (tenencia_vivienda_id);


--
-- Name: tbl_tipo_ayuda_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_tipo_ayuda
    ADD CONSTRAINT tbl_tipo_ayuda_pkey PRIMARY KEY (tipo_ayuda_id);


--
-- Name: tbl_tipo_cegera_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_tipo_cegera
    ADD CONSTRAINT tbl_tipo_cegera_pkey PRIMARY KEY (tipo_ceguera_id);


--
-- Name: tbl_tipo_horario_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_tipo_horario
    ADD CONSTRAINT tbl_tipo_horario_pkey PRIMARY KEY (id);


--
-- Name: tbl_tipo_horario_tipo_horario_key; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_tipo_horario
    ADD CONSTRAINT tbl_tipo_horario_tipo_horario_key UNIQUE (tipo_horario);


--
-- Name: tbl_tipo_vivienda_pkey; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_tipo_vivienda
    ADD CONSTRAINT tbl_tipo_vivienda_pkey PRIMARY KEY (tipo_vivienda_id);


--
-- Name: tipo_horario_unico; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_horario
    ADD CONSTRAINT tipo_horario_unico UNIQUE (tipo_horario_id);


--
-- Name: unique_municipio; Type: CONSTRAINT; Schema: public; Owner: sispdv; Tablespace: 
--

ALTER TABLE ONLY tbl_municipio
    ADD CONSTRAINT unique_municipio UNIQUE (co_municipio);


--
-- Name: IX_; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_" ON tbl_padrino_tbl_pers_disc_visual USING btree (pers_disc_visual_id);


--
-- Name: IX_diagnostico; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_diagnostico" ON tbl_diag_medico USING btree (medico_id);


--
-- Name: IX_es; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_es" ON tbl_grupo_familiar USING btree (parentesco_id);


--
-- Name: IX_grado; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_grado" ON tbl_estudio_persona USING btree (grado_inst_id);


--
-- Name: IX_labora; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_labora" ON tbl_laboral USING btree (pers_disc_visual_id);


--
-- Name: IX_personas; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_personas" ON tbl_grupo_familiar USING btree (pers_disc_visual_id);


--
-- Name: IX_posee; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_posee" ON tbl_estudio_persona USING btree (pers_disc_visual_id);


--
-- Name: IX_presenta; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_presenta" ON tbl_ayuda USING btree (tipo_ayuda_id);


--
-- Name: IX_propiedad; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_propiedad" ON tbl_pers_disc_visual USING btree (tenencia_vivienda_id);


--
-- Name: IX_realiza; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_realiza" ON tbl_laboral USING btree (oficio_id);


--
-- Name: IX_tiene; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_tiene" ON tbl_diag_medico USING btree (pers_disc_visual_id);


--
-- Name: IX_tiene_como; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_tiene_como" ON tbl_laboral USING btree (profesion_id);


--
-- Name: IX_tipo; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX "IX_tipo" ON tbl_pers_disc_visual USING btree (tipo_vivienda_id);


--
-- Name: estado_id_index; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX estado_id_index ON tbl_municipio USING btree (co_estado);


--
-- Name: municipio_id_index; Type: INDEX; Schema: public; Owner: sispdv; Tablespace: 
--

CREATE INDEX municipio_id_index ON tbl_parroquia USING btree (co_municipio);


SET search_path = audiperm, pg_catalog;

--
-- Name: resguardo_auditoria; Type: TRIGGER; Schema: audiperm; Owner: sispdv
--

CREATE TRIGGER resguardo_auditoria
    BEFORE DELETE OR UPDATE ON tbl_auditoria
    FOR EACH ROW
    EXECUTE PROCEDURE resg_auditoria();


--
-- Name: roles_user; Type: TRIGGER; Schema: audiperm; Owner: sispdv
--

CREATE TRIGGER roles_user
    AFTER INSERT ON tbl_usuario
    FOR EACH ROW
    EXECUTE PROCEDURE roles_user();


SET search_path = public, pg_catalog;

--
-- Name: auditoria_padrino; Type: TRIGGER; Schema: public; Owner: sispdv
--

CREATE TRIGGER auditoria_padrino
    AFTER INSERT OR DELETE OR UPDATE ON tbl_padrino
    FOR EACH ROW
    EXECUTE PROCEDURE audiperm.auditoria_tri();


--
-- Name: auditoria_padrino; Type: TRIGGER; Schema: public; Owner: sispdv
--

CREATE TRIGGER auditoria_padrino
    AFTER INSERT OR DELETE OR UPDATE ON tbl_padrino_tbl_grupo_familiar
    FOR EACH ROW
    EXECUTE PROCEDURE audiperm.auditoria_tri();


--
-- Name: auditoria_padrino; Type: TRIGGER; Schema: public; Owner: sispdv
--

CREATE TRIGGER auditoria_padrino
    AFTER INSERT OR DELETE OR UPDATE ON tbl_ayuda
    FOR EACH ROW
    EXECUTE PROCEDURE audiperm.auditoria_tri();


--
-- Name: auditoria_pers_disc_visual; Type: TRIGGER; Schema: public; Owner: sispdv
--

CREATE TRIGGER auditoria_pers_disc_visual
    AFTER INSERT OR DELETE OR UPDATE ON tbl_pers_disc_visual
    FOR EACH ROW
    EXECUTE PROCEDURE audiperm.auditoria_tri();


SET search_path = audiperm, pg_catalog;

--
-- Name: rol_id_fk; Type: FK CONSTRAINT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_usuario_rol
    ADD CONSTRAINT rol_id_fk FOREIGN KEY (rol_id) REFERENCES tbl_rol(id);


--
-- Name: sub_rol_id_fk; Type: FK CONSTRAINT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_usuario_rol
    ADD CONSTRAINT sub_rol_id_fk FOREIGN KEY (sub_rol_id) REFERENCES tbl_sub_rol(id);


--
-- Name: tbl_cargo_fk; Type: FK CONSTRAINT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_usuario
    ADD CONSTRAINT tbl_cargo_fk FOREIGN KEY (tipo_usuario_id) REFERENCES tbl_tipo_usuario(id);


--
-- Name: tbl_rol_fk; Type: FK CONSTRAINT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_sub_rol
    ADD CONSTRAINT tbl_rol_fk FOREIGN KEY (rol_id) REFERENCES tbl_rol(id) DEFERRABLE;


--
-- Name: usuario_id; Type: FK CONSTRAINT; Schema: audiperm; Owner: sispdv
--

ALTER TABLE ONLY tbl_usuario_rol
    ADD CONSTRAINT usuario_id FOREIGN KEY (usuario_id) REFERENCES tbl_usuario(id);


SET search_path = public, pg_catalog;

--
-- Name: area_id; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_detalle_horario
    ADD CONSTRAINT area_id FOREIGN KEY (area_id) REFERENCES tbl_area(area_id);


--
-- Name: co_estado; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_municipio
    ADD CONSTRAINT co_estado FOREIGN KEY (co_estado) REFERENCES tbl_estado(co_estado) ON UPDATE CASCADE;


--
-- Name: co_estado; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_parroquia
    ADD CONSTRAINT co_estado FOREIGN KEY (co_estado) REFERENCES tbl_estado(co_estado) ON UPDATE CASCADE;


--
-- Name: co_municipio; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_parroquia
    ADD CONSTRAINT co_municipio FOREIGN KEY (co_estado, co_municipio) REFERENCES tbl_municipio(co_estado, co_municipio) ON UPDATE CASCADE;


--
-- Name: dia_id; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_detalle_horario
    ADD CONSTRAINT dia_id FOREIGN KEY (dia_id) REFERENCES tbl_dia(id);


--
-- Name: horario_id; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_detalle_horario
    ADD CONSTRAINT horario_id FOREIGN KEY (horario_id) REFERENCES tbl_horario(id);


--
-- Name: medicina_fk; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_ayuda
    ADD CONSTRAINT medicina_fk FOREIGN KEY (medicina_id) REFERENCES tbl_medicina(medicina_id);


--
-- Name: pers_disc_visual_fk; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_ayuda
    ADD CONSTRAINT pers_disc_visual_fk FOREIGN KEY (pers_disc_visual_id) REFERENCES tbl_pers_disc_visual(id);


--
-- Name: pers_disc_visual_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_diag_medico
    ADD CONSTRAINT pers_disc_visual_fk_2 FOREIGN KEY (pers_disc_visual_id) REFERENCES tbl_pers_disc_visual(id);


--
-- Name: pers_disc_visual_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_padrino_tbl_pers_disc_visual
    ADD CONSTRAINT pers_disc_visual_fk_3 FOREIGN KEY (pers_disc_visual_id) REFERENCES tbl_pers_disc_visual(id);


--
-- Name: pers_disc_visual_id; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_estudio_persona
    ADD CONSTRAINT pers_disc_visual_id FOREIGN KEY (pers_disc_visual_id) REFERENCES tbl_pers_disc_visual(id);


--
-- Name: profesion_fk; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_laboral
    ADD CONSTRAINT profesion_fk FOREIGN KEY (profesion_id) REFERENCES tbl_profesion(profesion_id);


--
-- Name: tbl_diag_medico_tipo_ceguera_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_diag_medico
    ADD CONSTRAINT tbl_diag_medico_tipo_ceguera_id_fkey FOREIGN KEY (tipo_ceguera_id) REFERENCES tbl_tipo_cegera(tipo_ceguera_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_donaciones_tipo_ayuda_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_ayuda
    ADD CONSTRAINT tbl_donaciones_tipo_ayuda_id_fkey FOREIGN KEY (tipo_ayuda_id) REFERENCES tbl_tipo_ayuda(tipo_ayuda_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_estado; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_pers_disc_visual
    ADD CONSTRAINT tbl_estado FOREIGN KEY (co_estado) REFERENCES tbl_estado(co_estado);


--
-- Name: tbl_estudio_persona_grado_inst_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_estudio_persona
    ADD CONSTRAINT tbl_estudio_persona_grado_inst_id_fkey FOREIGN KEY (grado_inst_id) REFERENCES tbl_grado_instruccion(grado_inst_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_grupo_familiar_oficio_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_grupo_familiar
    ADD CONSTRAINT tbl_grupo_familiar_oficio_id_fkey FOREIGN KEY (oficio_id) REFERENCES tbl_oficio(oficio_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_grupo_familiar_parentesco_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_grupo_familiar
    ADD CONSTRAINT tbl_grupo_familiar_parentesco_id_fkey FOREIGN KEY (parentesco_id) REFERENCES tbl_parentesco(parentesco_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_horario_FK; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_pers_disc_visual
    ADD CONSTRAINT "tbl_horario_FK" FOREIGN KEY (horario_id) REFERENCES tbl_horario(id);


--
-- Name: tbl_laboral_oficio_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_laboral
    ADD CONSTRAINT tbl_laboral_oficio_id_fkey FOREIGN KEY (oficio_id) REFERENCES tbl_oficio(oficio_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_medico_tbl_diag_medico; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_diag_medico
    ADD CONSTRAINT tbl_medico_tbl_diag_medico FOREIGN KEY (medico_id) REFERENCES tbl_medico(medico_id);


--
-- Name: tbl_municipio; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_pers_disc_visual
    ADD CONSTRAINT tbl_municipio FOREIGN KEY (co_estado, co_municipio) REFERENCES tbl_municipio(co_estado, co_municipio);


--
-- Name: tbl_padrino_tbl_grupo_familiar_familiar_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_padrino_tbl_grupo_familiar
    ADD CONSTRAINT tbl_padrino_tbl_grupo_familiar_familiar_id_fkey FOREIGN KEY (familiar_id) REFERENCES tbl_grupo_familiar(familiar_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_padrino_tbl_grupo_familiar_padrino_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_padrino_tbl_grupo_familiar
    ADD CONSTRAINT tbl_padrino_tbl_grupo_familiar_padrino_id_fkey FOREIGN KEY (padrino_id) REFERENCES tbl_padrino(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_padrino_tbl_pers_disc_visual_padrino_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_padrino_tbl_pers_disc_visual
    ADD CONSTRAINT tbl_padrino_tbl_pers_disc_visual_padrino_id_fkey FOREIGN KEY (padrino_id) REFERENCES tbl_padrino(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_parroquia_FK; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_pers_disc_visual
    ADD CONSTRAINT "tbl_parroquia_FK" FOREIGN KEY (co_estado, co_municipio, co_parroquia) REFERENCES tbl_parroquia(co_estado, co_municipio, co_parroquia);


--
-- Name: tbl_pers_disc_visual_tenencia_vivienda_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_pers_disc_visual
    ADD CONSTRAINT tbl_pers_disc_visual_tenencia_vivienda_id_fkey FOREIGN KEY (tenencia_vivienda_id) REFERENCES tbl_tenencia_vivienda(tenencia_vivienda_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tbl_pers_disc_visual_tipo_vivienda_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_pers_disc_visual
    ADD CONSTRAINT tbl_pers_disc_visual_tipo_vivienda_id_fkey FOREIGN KEY (tipo_vivienda_id) REFERENCES tbl_tipo_vivienda(tipo_vivienda_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: tipo_ayuda_id; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_ayuda_especial
    ADD CONSTRAINT tipo_ayuda_id FOREIGN KEY (tipo_ayuda_id) REFERENCES tbl_tipo_ayuda(tipo_ayuda_id);


--
-- Name: tipo_horario_id; Type: FK CONSTRAINT; Schema: public; Owner: sispdv
--

ALTER TABLE ONLY tbl_horario
    ADD CONSTRAINT tipo_horario_id FOREIGN KEY (tipo_horario_id) REFERENCES tbl_tipo_horario(id);


--
-- Name: audiperm; Type: ACL; Schema: -; Owner: sispdv
--

REVOKE ALL ON SCHEMA audiperm FROM PUBLIC;
REVOKE ALL ON SCHEMA audiperm FROM sispdv;
GRANT ALL ON SCHEMA audiperm TO sispdv;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO sispdv;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

