<?php
 /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 07/04/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Class generales para diferentes tipos de acciones
       * @package: datosClass
       * @version: 2.0
       */ 
class funcionesExtras
{
    public function __construct() {
    }
    
   /** Method que permite encriptar con una semilla */
    public function  encrypt($string, $key='g3n3ral3scr1p') {
       $result = '';
       for($i=0; $i<strlen($string); $i++) {
          $char = substr($string, $i, 1);
          $keychar = substr($key, ($i % strlen($key))-1, 1);
          $char = chr(ord($char)+ord($keychar));
          $result.=$char;
       }
       return base64_encode($result);
}
    /** Method que permite desencriptar con una semilla */
    public function decrypt($string, $key='g3n3ral3scr1p') {
       $result = '';
       $string = base64_decode($string);
       for($i=0; $i<strlen($string); $i++) {
          $char = substr($string, $i, 1);
          $keychar = substr($key, ($i % strlen($key))-1, 1);
          $char = chr(ord($char)-ord($keychar));
          $result.=$char;
       }
       return $result;
    }
    public function calculaEdad($fecha)
    {
        /** Formato de feccha AA-MM-YYYY */
        $fec = explode("-",$fecha);
        $info=array($fec[2],$fec[1],$fec[0]);
        list($Y,$m,$d) = $info;
        return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
    }
    
    /** Method encargada de controlar los log del controlador */
    public static function executeLog($msg,$c,$m,$l)
      {
        $fo = fopen(__DIR__."/../../logs/cont_".date('Y-m-d').'.log','a');
        fwrite($fo,"[".date("r")."] $c::$m($l): $msg \r\n");
        fclose($fo);
      }
     /** Method encargada de controlar los log del controlador */
    public static function executeLogDeng($msg,$c,$m,$l)
      {
        $fo = fopen(__DIR__."/../../logs/deng_".date('Y-m-d').'.log','a');
        fwrite($fo,"[".date("r")."] $c::$m($l): $msg \r\n");
        fclose($fo);
      }
    /** Method que permite conocer el nombre del host del cliente */
    public static function realNameHost()
    {
        if(@$_SESSION['realNameS']==''){
            return gethostbyaddr($_SERVER['SERVER_ADDR']);
        }
    }
    
    /** Method que permite saber la ip real del cliente*/
    public function realIp()
    {
        if(@$_SESSION['realIpS']==''){
            if( @$_SERVER['HTTP_X_FORWARDED_FOR'] != '' ){
            $client_ip = 
                ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
                    $_SERVER['REMOTE_ADDR'] 
                    : 
                    ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
                    $_ENV['REMOTE_ADDR'] 
                    : 
                    "unknown" );

            // los proxys van añadiendo al final de esta cabecera
            // las direcciones ip que van "ocultando". Para localizar la ip real
            // del usuario se comienza a mirar por el principio hasta encontrar 
            // una dirección ip que no sea del rango privado. En caso de no 
            // encontrarse ninguna se toma como valor el REMOTE_ADDR

            @$entries = split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']);

            @reset($entries);
            while (list(, $entry) = each($entries)) 
            {
                @$entry = trim($entry);
                if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) )
                {
                    // http://www.faqs.org/rfcs/rfc1918.html
                    $private_ip = array(
                        '/^0\./', 
                        '/^127\.0\.0\.1/', 
                        '/^192\.168\..*/', 
                        '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/', 
                        '/^10\..*/');

                    @$found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);

                    if ($client_ip != $found_ip)
                    {
                    $client_ip = $found_ip;
                    break;
                    }
                }
            }
            }
            else
            {
            @$client_ip =
            ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
            $_SERVER['REMOTE_ADDR'] 
            : 
            ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
                $_ENV['REMOTE_ADDR'] 
                : 
                "unknown" );
            }

            return $client_ip;
        }
    }
    
    /** Funciones para tomar funciones no permitidas en los actions */
    public function validarMethodGet()
    {
      if($_GET){
        $v='';
        foreach ($_GET as $id=>$valor):
            $v.=$id.'=>'.$valor;
        endforeach;
        self::executeLogDeng('Error(001), Error, Acceso no autorizado, valores ingresados indebido: '.($v),__CLASS__, __FUNCTION__, __LINE__);
        $html='<html>
                <head>
                <title>Sistema de Gestion de Personas con Discapacidad Visual en la Sociedad Amigos de los Ciegos (SAC). </title>
                <link rel="shortcut icon" href="images/icon.png" />
                <link type="text/css" href="http://'.$_SERVER['HTTP_HOST'].'/sistemaSAC/web/css/style2.css" rel="stylesheet">
                 <script>window.location=\'http://localhost/sistemaSAC/web/layout.php\';</script>
                </head>
                <body>
                <!-- Contenedor general del template --->
                <div id="general2">
                        <!-- Contenedor Cabezera --->
                        <div id="cabezera">
                                <div id="logo"><br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema Per. Gest. Disc. Vis. </div>
                                <div id="infoSession">&nbsp;&nbsp;</div>
                        </div>

                        <!--  Contenedor Principal donde se mostrara la informacion --->
                        <div id="principal">
                        <!-- Contenedor mensaje ayuda --->
                        <div id="mensaje"></div>
                                <div id="contenido2">
                                    <center><br/>Error, Acceso no autorizado. <br/>Todos los proceso estan siendo monitoriado.</center>  
                                </div>
                        </div>

                        <!-- Contenedor pie de pagina -->
                        <div id="pie">
                                <div id="conPie">Sistema desarrollado por el <a href="#" id="dialog_link" class="ui-state-default ui-corner-all">Grupo 1</a> de Ingenieria en Informatica del CUFM</div>

                        </div>
                </div>
                </body>
                </html>';
        
        die($html);  
      }
    }
    public function camFormFech($contenido){
        if($contenido!=null){
                $fech=explode('-', $contenido,3);
                $fech2=explode(' ', $fech[2],3);
                return $fech2[0]."-".$fech[1]."-".$fech[0];

        }
    }

    public function __destruct() 
    {
    }
}
/*
$proceso= new funcionesExtras();
echo $proceso->realNameHost();
echo $proceso->realIp();

echo '<br/>'.$cadena_encriptada = $proceso->encrypt("123456");
echo '<br/>'.$cadena_desencriptada = $proceso->decrypt("$cadena_encriptada");
 */
 
?>
