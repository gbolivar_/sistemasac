<?php
class HighChart
{
	/**
	 * 
	 * @param $div elemento donde sera descargada la grafica
	 * @param $title
	 * @param $subtitle
	 * @param $xAxis
	 * @param $yAxis
	 * @param $yTitle
	 * @return unknown_type
	 */
	static public function BasicLine($div='', $titulo='',$categorias, $series='',$titulo, $subtitulo, $height = '', $width = '', $unidadDeMedida = 'Unidades')
	{
            return
          "
            <script type='text/javascript'>
$(function () {
                    var chart;
                    $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                    chart: {
                                            renderTo: '$div',
                                            defaultSeriesType: 'line',
                                            marginRight: 100,
                                            marginBottom: 25,
                                                width: 1100
                                    },
                                    title: {
                                            text: '$titulo',
                                            x: -20 //center
                                    },
                                    subtitle: {
                                            text: '',
                                            x: -20
                                    },
                                    xAxis: {
                                            categories: [$categorias],
					 	max: 4,
                                                width: 900,
                                                labels: {
							style: {
								 font: 'bold 11px Verdana, sans-serif',
								 color: 'black'
								
							}}
                                    },
                                    yAxis: {
                                            title: {
                                                    text: '$subtitulo',
                                                    margin:10
                                            },
                                            plotLines: [{
                                                    value: 0,
                                                    width: 1,
                                                    color: '#808080'
                                            }],
                                            min: 0
                                    },
                                    tooltip: {
                                            formatter: function() {
                                            return '<b>'+ this.series.name + '</b><br/>'+
                                                            this.x +': '+ this.y +' $unidadDeMedida';
                                            }
                                    },
                                plotOptions: {
                                            series: {
                                                    dataLabels: {
                                                                enabled: true,
                                                                rotation: 1,
                                                                color: '#000000',
                                                                align: 'center',
                                                                formatter: function() {
                                                                    return this.y;
                                                                },
                                                                style: {
                                                                    fontSize: '13px',
                                                                    fontFamily: 'Verdana, sans-serif'
                                                                }
                                                    }
                                            }},
            
                                    legend: {
                                        layout: 'vertical',
                			align: 'right',
			                verticalAlign: 'top',
			                x: -50,
			                y: 20,
			                borderWidth: 1
                                    },
                                    series: [$series]
                                    
                            });


                    });
});
            </script>

            <div id='$div' style='min-width: $width; height: $height; margin: 0 auto; padding-left:10px'></div>
    ";
	}
        
        
        
       static public function ColumnRotated($div='', $title='', $subtitle='', $legend='', $additional='', $xAxis='', $yAxis='', $series = array(), $unidadDeMedida='Cantidad',  $height = '600px',$width = '800px')
	{

           return
           "<script type='text/javascript'>
            var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: '$div',
						defaultSeriesType: 'column',
						margin: $additional
					},
					title: {
						text: '$title'
                                                
					},
					xAxis: {
						categories: [$xAxis],
						labels: {
							rotation: -18,
							align: 'right',
							style: {
								 font: 'normal 13px Verdana, sans-serif',
								 color: 'black'
								
							}
						}
					},
					yAxis: {
						min: 0,
						title: {
							text: '$yAxis'
						}
					},
					legend: {
						enabled: $legend
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.x +'</b><br/>'+
								 '$unidadDeMedida: '+ Highcharts.numberFormat(this.y, 0);
						}
					},
				        series: [{
						$series,
						dataLabels: {
							enabled: true,
							rotation: -10,
							color: '#000000',
							align: 'center',
							x: 2,
							y: 3,
							formatter: function() {
								return this.y;
							},
							style: {
								font: 'bold 11px Verdana, sans-serif'
							}
						}			
					}]
				});
				
				
			});
				
		</script>
		
            <div id='$div' style='min-width: $width; height: $height; margin: 0 auto'></div><br/><br/>
    ";
	}
        
        
         static public function PieLegend($div='', $title='', $legend='', $series = array(), $unidadDeMedida = '%',  $height = '400px',$width = '800px')
	{
           return
           "<script type='text/javascript'>
		$(function () {
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: '$div',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: '$title'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.y +' $unidadDeMedida';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y +' $unidadDeMedida';
								}
							},
                                                        showInLegend: $legend
						}
					},
				    series: [{
						type: 'pie',
						$series
                                            
					}]
				});
			});
                });				
             </script>
             
		
            <div id='$div' style='min-width: $width; height: $height; margin: 0 auto'></div><br/><br/>
    ";
	}
        public static function StackedBar($div, $titulo, $categorias, $texto, $series, $ancho, $largo){
        return
        "<script type='text/javascript'>
(function($){ // encapsulate jQuery

var chart;
$(document).ready(function() {
	chart = new Highcharts.Chart({
		chart: {
			renderTo: '$div',
			type: 'bar'
		},
		title: {
			text: '$titulo'
		},
		xAxis: {
			categories: [$categorias]
		},
		yAxis: {
			min: 0,
			title: {
				text: '$texto'
			}
		},
		legend: {
			backgroundColor: '#FFFFFF',
			reversed: true
		},
		tooltip: {
			formatter: function() {
				return ''+
					this.series.name +': '+ this.y +'';
			}
		},
		plotOptions: {
			series: {
				stacking: 'normal',
                                dataLabels: {
                                            enabled: true,
                                            rotation: 1,
                                            color: '#FFF',
                                            align: 'center',
                                            formatter: function() {
                                                return this.y;
                                            },
                                            style: {
                                                fontSize: '13px',
                                                fontFamily: 'Verdana, sans-serif'
                                            }
				}
			}
		},
			series: [".$series."]
	});
});

})(jQuery); </script>
<div id='$div' style='min-width: $ancho; height: $largo; margin: 0 auto'></div><br/>";
        }
}
