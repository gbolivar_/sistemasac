<?php
/**
    * @propiedad: FABRICA NACIONAL DE CEMENTO
    * @Autor: Gregorio Bol?var
    * @email: elalconxvii@gmail.com
    * @Fecha de Creacion: 30/09/2011
    * @Auditado por: Gregorio J Bol?var B
    * @Fecha de Modificacion: 06/10/2011
    * @Descripci?n: Control de la ficha del censocc
    * @package: funcionesExtras.php
    * @version: 1.0
    */
class Complements
{
    static public function getTitle($matriz = array())
    {
        return key($matriz);
    }
/* Complemento que permite exponer el subTitulos*/
    static public function getSubtitleGrafico($cadena = '')
    {
        return 'Cantidad de Guias ' . $cadena;
    }


/* Complemento para especificar las leyandas de las graficas*/
    static public function getLegend($obj = null)
    {

        switch($obj)
        {

            case 'off':
                return "false";
		break;
            case 'on':
                return "true";
		break;

            default:
                return "layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        x: 0,
                        y: 50,
                        borderWidth: 1,
                        backgroundColor: '#EEE'";
        }
    }
 /* Complemento adicional para los margenes de la grafica*/
    static public function getAdditional($obj = null)
    {
        switch($obj)
        {
            case 1:
                return ", marginTop:400, marginLeft:100";
            break;
            case 2:
                return ", marginTop:500, marginLeft:100";
           break;
            default:
                return "[ 50, 50, 100, 80]";
        }
    }



    /* Especifica el ancho del area de la grafica */
    static public function getWidthGraphic($obj = null)
    {
        switch($obj)
        {
          case 1: return '600px'; break;
          case 2: return '700px'; break;
          case 3: return '800px'; break;
	  case 4: return '200px'; break;
          default: return  '400px';
        }
    }
    
    /* Especifica el alto del area de la grafica */
    static public function getHeightGraphic($obj = null)
    {
        switch($obj)
        {
            case 1: return '700px'; break;
            case 2: return '600px'; break;
            case 3: return '500px'; break;
	    case 4: return '200px'; break;
            default: return  '400px';
        }
    }

/* Elementos que muestras todas las categorias perteneciente a un elemento dado*/
    static public function getCategories($xAxis=array(),$name='nombre')
    {
        $categorias = '';

        foreach($xAxis as $valor)
        {
            $categorias.= "'".$valor['nombre']."',";
        }

        if(!empty($xAxis))
        {
            $categorias = substr($categorias, 0, strlen($categorias)-1);
        }
        
        return $categorias;
    }
    
    static public function getSeriesData($matriz = array(),$name='cantidad')
    { 
        $cantidad = '';
        $series = '';
        foreach($matriz as $valor)
        {         
            $cantidad.= $valor['cantidad'] . ',';
        }
            $series.= "name: '', data: [$cantidad],";
        $series = substr($series, 0, strlen($series)-1);
        return $series;
    }
    static public function getSeriesDataPorc($matriz = array(),$name='cantidad')
    {
        $porc = 100;    // Declarar el porcentaje maximo que tendra las gr?ficas
        $cantidad = ''; // Declarar los datos que tendra cada iteraci?n
        $series = '';   // Declarar donde estara el return con los datos
        $sumaVe = 0;    // Declarar el acumulador
        
        // Suma la cantidad de totales de los Item 
         foreach($matriz as $valor)
        {   
            $sumaVe = $sumaVe + $valor['cantidad'];            
        }
        
        // Para saber el porcentaje que le toca a los index o Item
        $porInd=$porc/$sumaVe;
        
        foreach($matriz as $valor)
        {  
            $result=$valor['cantidad']*$porInd;
            $cantidad.= round($result,2) . ',';             
        }
        $series.= "name: '', data: [$cantidad],";
        $series = substr($series, 0, strlen($series)-1);
        return $series; 
        
    }
    // Funcion cuando necesitamos que la grafica arroje los resultados en cantidades
     static public function getSeriesDataPie($matriz = array(),$name='cantidad')
    {
        $cantidad = '';
        $series = '';
        foreach($matriz  as $a => $valor)
        {
                     if($a==0){
                        $cantidad.= "{ name: '". $valor['nombre'] . "', 
                                        y: ".$valor['cantidad'].", 
                                        sliced: true, 
                                        selected: true},";
                     }else{
                        $cantidad.= "['". $valor['nombre'] . "',".$valor['cantidad']."]"; 
                     }
              
        }
        
            $series.= "name: 'Browser share', data: [$cantidad],";          
        $series = substr($series, 0, strlen($series)-1);
       
        return $series;      
    }
    // Method cuando necesitamos que en la grafica me arroje los resultados en porcentaje
    static public function getSeriesDataPiePorc($matriz = array(),$name='cantidad')
    {
        $porc = 100;
        $cantidad = '';
        $series = '';
        $sumCant = 0;
        
        // Sumara las cantidad totales de los resultados obtenido
        foreach ($matriz as $valor){
            $sumCant = $sumCant + $valor['cantidad'];
        }
        
        // El divido el porcentaje total entre el resultado de la suma para sacar la porci?n de porcentaje  
        $porcPorc=$porc / $sumCant;
        foreach($matriz  as $a => $valor)
        {
                     if($a==0){
                        $cantidad.= "{ name: '". $valor['nombre'] . "', 
                                        y: ".round($valor['cantidad']*$porcPorc,2).", 
                                        sliced: true, 
                                        selected: true},";
                     }else{
                        $cantidad.= "['". $valor['nombre'] . "',".round($valor['cantidad']*$porcPorc,2)."],"; 
                     }
              
        }
            $series.= "name: 'Browser share', data: [$cantidad],";          
        $series = substr($series, 0, strlen($series)-1);
       
        return $series;
        
        
    }
    
    
    
    //funcion que muestra los temarios de una grafica
        static public function getEjeY($xAxis=array(),$name='nombre')
    {
        $categorias = '';

        foreach($xAxis as $valor)
        {
            $categorias.= "'".$valor['nombre']."',";
        }

        if(!empty($xAxis))
        {
            $categorias = substr($categorias, 0, strlen($categorias)-1);
        }
        
        return $categorias;
    }

    
    
        static public function getContenido($matrizc,$name='nombre'){
            
         $series1="";
           
            $resultado="";
                foreach ($matrizc as $valor):
                     $series2="";
                  //$color=rand('000000', '999999');
                    foreach ($valor AS $item=>$cantidad):
                   
                        if($item==='empresa'){
                           $series1="name: '$cantidad',";
                        }else{
                            if($cantidad==''){
                            
                        }else{
                            $series2.=$cantidad.',';
                        }
                        }
                        
                    endforeach;
                    
                    $resultado.='{'.$series1." data: [$series2]},";
                endforeach;
                return substr($resultado,0,-1);
        }
        
        static public function getContenidoFrecuencia($datos=array(),$name='nombre'){
            
       $series="";
	
        foreach($datos as $valor):
            $empresa="{ name: ".$valor['nombre'].", data: ";
            $resul='';
           
            foreach ($valor as $datos2):
            $resul.=$datos2['cantidad'];
            endforeach;
        
             $cierre="]},";
        return $resul;
        endforeach;



        }

        static public function getCantidadEmpresa($matriz=array(),$name='nombre'){
            
$series="";
$series.= "name: 'Browser share', data:[";
		foreach($matriz as $valor){
                //$color=rand('000000', '999999');
		$series.="['$valor[nombre]', $valor[cantidad]],";

		}
		return $series."]";
        }
        
        static public function getCantidadActor($matriz=array(),$name='nombre'){
            
$series="";
$series.= "name: 'Browser share', data:[";
		foreach($matriz as $valor){
                   // $color=rand('000000', '999999');
		$series.="['$valor[nombre]', $valor[cantidad]],";

		}
		return $series."]";
		

        }
        
    
}
