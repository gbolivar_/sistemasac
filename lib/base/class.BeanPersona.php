<?php
 class BeanPersona
{
     /** Variables usada para la clases BeanPersona*/
     private $pers_disc_visual_id;
     private $nombre;
     private $apellido;
     private $nacionalidad;
     private $cedula;
     private $sexo;
     private $co_estado;
     private $co_municipio;
     private $co_parroquia;
     private $lugar_nacimiento;
     private $fech_nacimiento;
     private $telf_habitacion;
     private $telf_celular;
     private $correo;
     private $direccion;
     private $cod_postal;
     private $tenencia_vivienda_id;
     private $tipo_vivienda_id;
     private $nom_ape_pers_contacto;
     private $telef_pers_contacto;
     private $direccion_pers_contacto;
     private $usuario_id;
     private $estatus;
     private $created_at;
     private $updated_at;


     /** Constructor de la Class */
    function BeanPersona(){

    }

    /** SET y GET de los campos relacionados a la persona con discapacidad visual */
     /** asignar a pers_disc_visual_id */
    public function set_pers_disc_visual_id($pers_disc_visual_id){
	$this->pers_disc_visual_id = $pers_disc_visual_id;
    }
    /** return de pers_disc_visual_id */
    public function get_pers_disc_visual_id(){
	return $this->pers_disc_visual_id;
    }

    /** asignar a nombre */
    public function set_nombre($nombre){
	$this->nombre = $nombre;
    }
    /** return de nombre */
    public function get_nombre(){
	return $this->nombre;
    }

    /** asignar a apellido */
    public function set_apellido($apellido){
	$this->apellido = $apellido;
    }
    /** return de apellido */
    public function get_apellido(){
	return $this->apellido;
    }

     /** asignar a nacionalidad */
    public function set_nacionalidad($nacionalidad){
	$this->nacionalidad = $nacionalidad;
    }
    /** return de nacionalidad */
    public function get_nacionalidad(){
	return $this->nacionalidad;
    }

     /** asignar a cedula */
    public function set_cedula($cedula){
	$this->cedula = $cedula;
    }
    /** return de cedula */
    public function get_cedula(){
	return $this->cedula;
    }

     /** asignar a sexo */
    public function set_sexo($sexo){
	$this->sexo = $sexo;
    }
    /** return de sexo */
    public function get_sexo(){
	return $this->sexo;
    }

     /** asignar a co_estado */
    public function set_co_estado($co_estado){
	$this->co_estado = $co_estado;
    }
    /** return de co_estado */
    public function get_co_estado(){
	return $this->co_estado;
    }

     /** asignar a co_municipio */
    public function set_co_municipio($co_municipio){
	$this->co_municipio = $co_municipio;
    }
    /** return de co_municipio */
    public function get_co_municipio(){
	return $this->co_municipio;
    }

     /** asignar a marroquia_id */
    public function set_co_parroquia($co_parroquia){
	$this->co_parroquia = $co_parroquia;
    }
    /** return de marroquia_id */
    public function get_co_parroquia(){
	return $this->co_parroquia;
    }

     /** asignar a lugar_nacimiento */
    public function set_lugar_nacimiento($lugar_nacimiento){
	$this->lugar_nacimiento = $lugar_nacimiento;
    }
    /** return de lugar_nacimiento */
    public function get_lugar_nacimiento(){
	return $this->lugar_nacimiento;
    }

     /** asignar a fech_nacimiento */
    public function set_fech_nacimiento($fech_nacimiento){
	$this->fech_nacimiento = $fech_nacimiento;
    }
    /** return de fech_nacimiento */
    public function get_fech_nacimiento(){
	return $this->fech_nacimiento;
    }


     /** asignar a telf_habitacion */
    public function set_telf_habitacion($telf_habitacion){
        if(!empty($telf_habitacion)){$valor=$telf_habitacion; }else{ $valor=0; }
	$this->telf_habitacion = $valor;
    }
    /** return de telf_habitacion */
    public function get_telf_habitacion(){
	return $this->telf_habitacion;
    }

      /** asignar a telf_celular */
    public function set_telf_celular($telf_celular){
        if(!empty($telf_celular)){$valor=$telf_celular; }else{ $valor=0; }
	$this->telf_celular = $valor;
    }
    /** return de telf_celular */
    public function get_telf_celular(){
	return $this->telf_celular;
    }

     /** asignar a correo */
    public function set_correo($correo){
	$this->correo = $correo;
    }
    /** return de correo */
    public function get_correo(){
	return $this->correo;
    }

     /** asignar a direccion */
    public function set_direccion($direccion){
        if(!empty($direccion)){$valor=$direccion; }else{ $valor=NULL; }
	$this->direccion = $valor;
    }
    /** return de direccion */
    public function get_direccion(){
	return $this->direccion;
    }

     /** asignar a cod_postal */
    public function set_cod_postal($cod_postal){
        if(!empty($cod_postal)){$valor=$cod_postal; }else{ $valor=0; }
	$this->cod_postal = $valor;
    }
    /** return de cod_postal */
    public function get_cod_postal(){
	return $this->cod_postal;
    }

     /** asignar a tenencia_vivienda_id */
    public function set_tenencia_vivienda_id($tenencia_vivienda_id){
        if(!empty($tenencia_vivienda_id)){$valor=$tenencia_vivienda_id; }else{ $valor=null; }
	$this->tenencia_vivienda_id = $valor;
    }
    /** return de tenencia_vivienda_id */
    public function get_tenencia_vivienda_id(){
	return $this->tenencia_vivienda_id;
    }

     /** asignar a tipo_vivienda_id */
    public function set_tipo_vivienda_id($tipo_vivienda_id){
	$this->tipo_vivienda_id = $tipo_vivienda_id;
    }
    /** return de tipo_vivienda_id */
    public function get_tipo_vivienda_id(){
	return $this->tipo_vivienda_id;
    }

     /** asignar a nom_ape_pers_contacto */
    public function set_nom_ape_pers_contacto($nom_ape_pers_contacto){
        if(!empty($nom_ape_pers_contacto)){$valor=$nom_ape_pers_contacto; }else{ $valor=NULL; }
	$this->nom_ape_pers_contacto = $valor;
    }
    /** return de nom_ape_pers_contacto */
    public function get_nom_ape_pers_contacto(){
	return $this->nom_ape_pers_contacto;
    }

     /** asignar a telef_pers_contacto */
    public function set_telef_pers_contacto($telef_pers_contacto){
        if(!empty($telef_pers_contacto)){$valor=$telef_pers_contacto; }else{ $valor=0; }
	$this->telef_pers_contacto = $valor;
    }
    /** return de telef_pers_contacto */
    public function get_telef_pers_contacto(){
	return $this->telef_pers_contacto;
    }

     /** asignar a direccion_pers_contacto */
    public function set_direccion_pers_contacto($direccion_pers_contacto){
       if(!empty($direccion_pers_contacto)){$valor=$direccion_pers_contacto; }else{ $valor=NULL; }
	$this->direccion_pers_contacto = $valor;
    }
    /** return de direccion_pers_contacto */
    public function get_direccion_pers_contacto(){
	return $this->direccion_pers_contacto;
    }

    /** SET y GET de los campos de su estado laboral */
    /** asignar a laboral_id */
    public function set_laboral_id($laboral_id){
       if(!empty($laboral_id)){$valor=$laboral_id; }else{ $valor=NULL; }
       $this->laboral_id = $valor;
    }
    /** return de laboral_id */
    public function get_laboral_id(){
       return $this->laboral_id;
    }

    /** asignar a ocupacion_id */
    public function set_ocupacion_id($ocupacion_id){
       if(!empty($ocupacion_id)){$valor=$ocupacion_id; }else{ $valor=NULL; }
       $this->ocupacion_id = $valor;
    }
    /** return de ocupacion_id */
    public function get_ocupacion_id(){
       return $this->ocupacion_id;
    }
    /** asignar a trabaja */
    public function set_trabaja($trabaja){
       if(!empty($trabaja)){$valor=$trabaja; }else{ $valor=NULL; }
       $this->trabaja = $valor;
    }
    /** return de trabaja */
    public function get_trabaja(){
       return $this->trabaja;
    }
    /** asignar a empresa_trabaja */
    public function set_empresa_trabaja($empresa_trabaja){
       if(!empty($empresa_trabaja)){$valor=$empresa_trabaja; }else{ $valor=NULL; }
       $this->empresa_trabaja = $valor;
    }
    /** return de empresa_trabaja */
    public function get_empresa_trabaja(){
       return $this->empresa_trabaja;
    }
    /** asignar a direccion_trabajo */
    public function set_direccion_trabajo($direccion_trabajo){
       if(!empty($direccion_trabajo)){$valor=$direccion_trabajo; }else{ $valor=NULL; }
       $this->direccion_trabajo = $valor;
    }
    /** return de direccion_trabajo */
    public function get_direccion_trabajo(){
       return $this->direccion_trabajo;
    }
    /** asignar a telef_trabajo */
    public function set_telef_trabajo($telef_trabajo){
       if(!empty($telef_trabajo)){$valor=$telef_trabajo; }else{ $valor=0; }
       $this->telef_trabajo = $valor;
    }
    /** return de telef_trabajo */
    public function get_telef_trabajo(){
       return $this->telef_trabajo;
    }
    /** asignar a fecha_ingreso */
    public function set_fecha_ingreso($fecha_ingreso){
       if(!empty($fecha_ingreso)){$valor=$fecha_ingreso; }else{ $valor=NULL; }
       $this->fecha_ingreso = $valor;
    }
    /** return de fecha_ingreso */
    public function get_fecha_ingreso(){
       return $this->fecha_ingreso;
    }
    /** asignar a ingresos */
    public function set_ingresos($ingresos){
       if(!empty ($ingresos)){$valor=$ingresos; }else{ $valor=NULL; }
       $this->ingresos = $valor;
    }
    /** return de ingresos */
    public function get_ingresos(){
       return $this->ingresos;
    }
    /** asignar a oficio_id */
    public function set_oficio_id($oficio_id){
       if(!empty($oficio_id)){$valor=$oficio_id; }else{ $valor=NULL; }
       $this->oficio_id = $oficio_id;
    }
    /** return de oficio_id */
    public function get_oficio_id(){
       return $this->oficio_id;
    }
    /** asignar a profesion_id */
    public function set_profesion_id($profesion_id){
       if(!empty ($profesion_id)){$valor=$profesion_id; }else{ $valor=NULL; }
       $this->profesion_id = $valor;
    }
    /** return de profesion_id */
    public function get_profesion_id(){
       return $this->profesion_id;
    }

   /** SET y GET de los campos de su condicion de estudio */
    /** asignar a estudio_id */
    public function set_estudio_id($estudio_id){
       if(!empty ($estudio_id)){$valor=$estudio_id; }else{ $valor=NULL; }
       $this->estudio_id = $valor;
    }
    /** return de estudio_id */
    public function get_estudio_id(){
       return $this->estudio_id;
    }
   
    /** asignar a estudia */
    public function set_estudia($estudia){
       if(!empty($estudia)){echo $valor=$estudia; }else{ $valor=NULL; }
       $this->estudia = '$valor';
    }
    /** return de estudia */
    public function get_estudia(){
       return $this->estudia;
    }
    /** asignar a grado_inst_id */
    public function set_grado_inst_id($grado_inst_id){
       if(!empty ($grado_inst_id)){$valor=$grado_inst_id; }else{ $valor=NULL; }
       $this->grado_inst_id = $valor;
    }
    /** return de grado_inst_id */
    public function get_grado_inst_id(){
       return $this->grado_inst_id;
    }
    /** asignar a lugar_estudia */
    public function set_lugar_estudia($lugar_estudia){
       if(!empty ($lugar_estudia)){$valor=$lugar_estudia; }else{ $valor=NULL; }
       $this->lugar_estudia = $valor;
    }
    /** return de lugar_estudia */
    public function get_lugar_estudia(){
       return $this->lugar_estudia;
    }
    /** asignar a estudio_realiza */
    public function set_estudio_realiza($estudio_realiza){
       if(!empty ($estudio_realiza)){$valor=$estudio_realiza; }else{ $valor=NULL; }
       $this->estudio_realiza = $valor;
    }
    /** return de estudio_realiza */
    public function get_estudio_realiza(){
       return $this->estudio_realiza;
    }


     /** asignar a usuario_id */
    public function set_usuario_id(){
        session_start();
        /** Asignar el id del usuario autenticado */
	$this->usuario_id = $_SESSION['userAut'][0];
    }
    /** return de usuario_id */
    public function get_usuario_id(){
	return $this->usuario_id;
    }

     /** asignar a estatus */
    public function set_estatus(){
	$this->estatus = 'true';
    }
    /** return de estatus */
    public function get_estatus(){
	return $this->estatus;
    }

     /** asignar a created_at */
    public function set_created_at(){
	$this->created_at = date('Y-m-d h:i:s');
    }
    /** return de created_at */
    public function get_created_at(){
	return $this->created_at;
    }

      /** asignar a updated_at */
    public function set_updated_at(){
	$this->updated_at = date('Y-m-d h:i:s');
    }
    /** return de created_at */
    public function get_updated_at(){
	return $this->updated_at;
    }
}
?>