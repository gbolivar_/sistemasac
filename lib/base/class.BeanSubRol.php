
<?php
/**
* @propiedad: PROPIETARIO DEL CODIGO
* @Autor: Gregorio Bolivar
* @email: elalconxvii@gmail.com
* @Fecha de Creacion: 08/04/2012
* @Auditado por: Gregorio J Bolívar B
* @Descripción: Generado por el generador de set y get del autor
* @package: datosClass
* @version: 1.0
*/ 

class BeanSubRol 
{ 
 
private $id;

private $rol_id;

private $usuario_id;

private $detalles;

private $title;

private $div_id;

private $activa_menu;

private $created_at;

private $updated_at;

/** Constructor de la Class */
 function BeanSubRol()
{
}
/** asignar a id */
public function set_id($id){
   $this->id = $id;
}
/** return de id */
public function get_id(){
   return $this->id;
}
/** asignar a rol_id */
public function set_rol_id($rol_id){
   $this->rol_id = $rol_id;
}
/** return de rol_id */
public function get_rol_id(){
   return $this->rol_id;
}
/** asignar a usuario_id */
public function set_usuario_id(){
    session_start();
    /** Asignar el id del usuario autenticado */
    $this->usuario_id=$_SESSION['userAut'][0];
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a detalles */
public function set_detalles($detalles){
   $this->detalles = $detalles;
}
/** return de detalles */
public function get_detalles(){
   return $this->detalles;
}
/** asignar a title */
public function set_title($title){
   $this->title = $title;
}
/** return de title */
public function get_title(){
   return $this->title;
}
/** asignar a div_id */
public function set_div_id($div_id){
   $this->div_id = $div_id;
}
/** return de div_id */
public function get_div_id(){
   return $this->div_id;
}
/** asignar a activa_menu */
public function set_activa_menu($activa_menu){
   $this->activa_menu = $activa_menu;
}
/** return de activa_menu */
public function get_activa_menu(){
   return $this->activa_menu;
}
/** asignar a created_at */
public function set_created_at(){
   $this->created_at = date('Y-m-d h:i:s'); ;
}
/** return de created_at */
public function get_created_at(){
   return $this->created_at;
}
/** asignar a updated_at */
public function set_updated_at(){
   $this->updated_at = date('Y-m-d h:i:s'); 
}
/** return de updated_at */
public function get_updated_at(){
   return $this->updated_at;
}
} 
?>