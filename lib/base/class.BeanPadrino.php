
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 11/03/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */

class BeanPadrino
{

private $padrino_id;

private $nombre;

private $apellido;

private $nacionalidad;

private $cedula;

private $telefono;

private $correo;

private $usuario_id;

private $estatus;

private $created_at;

private $updated_at;

private $cant_donacion_bs;

/** Constructor de la Class */
 function BeanPadrino()
{
}
/** asignar a padrino_id */
public function set_padrino_id($padrino_id){
   $this->padrino_id = $padrino_id;
}
/** return de padrino_id */
public function get_padrino_id(){
   return $this->padrino_id;
}
/** asignar a nombre */
public function set_nombre($nombre){
   $this->nombre = $nombre;
}
/** return de nombre */
public function get_nombre(){
   return $this->nombre;
}
/** asignar a apellido */
public function set_apellido($apellido){
   $this->apellido = $apellido;
}
/** return de apellido */
public function get_apellido(){
   return $this->apellido;
}
/** asignar a nacionalidad */
public function set_nacionalidad($nacionalidad){
   $this->nacionalidad = $nacionalidad;
}
/** return de nacionalidad */
public function get_nacionalidad(){
   return $this->nacionalidad;
}
/** asignar a cedula */
public function set_cedula($cedula){
   $this->cedula = $cedula;
}
/** return de cedula */
public function get_cedula(){
   return $this->cedula;
}
/** asignar a telefono */
public function set_telefono($telefono){
if(!empty($telefono)){$valor=$telefono; }else{ $valor=0; }
    $this->telefono = $valor;
}
/** return de telefono */
public function get_telefono(){
   return $this->telefono;
}
/** asignar a correo */
public function set_correo($correo){
   $this->correo = $correo;
}
/** return de correo */
public function get_correo(){
   return $this->correo;
}
/** asignar a usuario_id */
public function set_usuario_id(){
        session_start();
    /** Asignar el id del usuario autenticado */
    $this->usuario_id=$_SESSION['userAut'][0];
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a estatus */
public function set_estatus(){
   $this->estatus = 'true';
}
/** return de estatus */
public function get_estatus(){
   return $this->estatus;
}
/** asignar a created_at */
public function set_created_at(){
   $this->created_at = date('Y-m-d h:i:s');
}
/** return de created_at */
public function get_created_at(){
   return $this->created_at;
}
/** asignar a updated_at */
public function set_updated_at(){
   $this->updated_at = date('Y-m-d h:i:s');
}
/** return de updated_at */
public function get_updated_at(){
   return $this->updated_at;
}

/** asignar a fecha_inicio */
public function set_fecha_inicio($fecha_inicio){
   $this->fecha_inicio = $fecha_inicio;
}
/** return de cantidad donacion */
public function get_fecha_inicio(){
   return $this->fecha_inicio;
}

/** asignar a cantidad donacion */
public function set_cant_donacion_bs($cant_donacion_bs){
   $this->cant_donacion_bs = $cant_donacion_bs;
}
/** return de cantidad donacion */
public function get_cant_donacion_bs(){
   return $this->cant_donacion_bs;
}
}
?>