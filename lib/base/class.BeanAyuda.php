<?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 11/04/2012
       * @Auditado por: Gregorio J BolÃ­var B
       * @DescripciÃ³n: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */ 

class BeanAyuda 
{ 
 
private $id;

private $medicina_id;

private $pers_disc_visual_id;

private $tipo_ayuda_id;

private $monto;

private $observacion;

private $usuario_id;

private $estatus;

private $created_at;

private $updated_at;

/** Constructor de la Class */
 function BeanAyuda()
{
}
/** asignar a id */
public function set_id($id){
   $this->id = $id;
}
/** return de id */
public function get_id(){
   return $this->id;
}
/** asignar a id */
public function set_fecha_entrega($fecha_entrega){
   $this->fecha_entrega = $fecha_entrega;
}
/** return de id */
public function get_fecha_entrega(){
   return $this->fecha_entrega;
}
/** asignar a medicina_id */
public function set_medicina_id($medicina_id){
   if(empty($medicina_id)){
       $this->medicina_id = 'null';
   }else{
       $this->medicina_id = $medicina_id;
   }
   
}
/** return de medicina_id */
public function get_medicina_id(){
   return $this->medicina_id;
}
/** asignar a pers_disc_visual_id */
public function set_pers_disc_visual_id($pers_disc_visual_id){
   $this->pers_disc_visual_id = $pers_disc_visual_id;
}
/** return de pers_disc_visual_id */
public function get_pers_disc_visual_id(){
   return $this->pers_disc_visual_id;
}
/** asignar a tipo_ayuda_id */
public function set_tipo_ayuda_id($tipo_ayuda_id){
   $this->tipo_ayuda_id = $tipo_ayuda_id;
}
/** return de tipo_ayuda_id */
public function get_tipo_ayuda_id(){
   return $this->tipo_ayuda_id;
}
/** asignar a monto */
public function set_monto($monto){
   $this->monto = $monto;
}
/** return de monto */
public function get_monto(){
   return $this->monto;
}
/** asignar a observacion */
public function set_observacion($observacion){
   $this->observacion = $observacion;
}
/** return de observacion */
public function get_observacion(){
   return $this->observacion;
}
/** asignar a usuario_id */
public function set_usuario_id(){
   session_start();
   /** Asignar el id del usuario autenticado */
   $this->usuario_id=$_SESSION['userAut'][0];
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a estatus */
public function set_estatus(){
   $this->estatus = 'true';
}
/** return de estatus */
public function get_estatus(){
   return $this->estatus;
}
/** asignar a created_at */
public function set_created_at(){
   $this->created_at = date('Y-m-d h:i:s');
}
/** return de created_at */
public function get_created_at(){
   return $this->created_at;
}
/** asignar a updated_at */
public function set_updated_at(){
   $this->updated_at = date('Y-m-d h:i:s');;
}
/** return de updated_at */
public function get_updated_at(){
   return $this->updated_at;
}
} 
?>