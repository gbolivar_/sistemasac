
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 23/04/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */ 

class BeanAyudaEspecial 
{ 
 
private $id;

private $nacionalidad;

private $cedula;

private $nombres;

private $apellidos;

private $monto;

private $observacion;

private $tipo_ayuda_id;

private $fecha_entrega;

private $usuario_id;

private $estatus;

private $created_at;

private $medicina_id;

/** Constructor de la Class */
 function BeanAyudaEspecial()
{
}
/** asignar a id */
public function set_id($id){
   $this->id = $id;
}
/** return de id */
public function get_id(){
   return $this->id;
}
/** asignar a nacionalidad */
public function set_nacionalidad($nacionalidad){
   $this->nacionalidad = $nacionalidad;
}
/** return de nacionalidad */
public function get_nacionalidad(){
   return $this->nacionalidad;
}
/** asignar a nacionalidad */
public function set_cedula($cedula){
   $this->cedula = $cedula;
}
/** return de nacionalidad */
public function get_cedula(){
   return $this->cedula;
}
/** asignar a nombres */
public function set_nombres($nombres){
   $this->nombres = $nombres;
}
/** return de nombres */
public function get_nombres(){
   return $this->nombres;
}
/** asignar a apellidos */
public function set_apellidos($apellidos){
   $this->apellidos = $apellidos;
}
/** return de apellidos */
public function get_apellidos(){
   return $this->apellidos;
}
/** asignar a monto */
public function set_monto($monto){
   $this->monto = $monto;
}
/** return de monto */
public function get_monto(){
   return $this->monto;
}
/** asignar a tipo_ayuda_id */
public function set_tipo_ayuda_id($tipo_ayuda_id){
   $this->tipo_ayuda_id = $tipo_ayuda_id;
}
/** return de tipo_ayuda_id */
public function get_tipo_ayuda_id(){
   return $this->tipo_ayuda_id;
}
/** asignar a fecha_entrega */
public function set_fecha_entrega($fecha_entrega){
   $this->fecha_entrega = $fecha_entrega;
}
/** return de fecha_entrega */
public function get_fecha_entrega(){
   return $this->fecha_entrega;
}
/** asignar a usuario_id */
public function set_usuario_id(){
   session_start();
   /** Asignar el id del usuario autenticado */
   $this->usuario_id=$_SESSION['userAut'][0];
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a estatus */
public function set_estatus(){
   $this->estatus = 'true';
}
/** return de estatus */
public function get_estatus(){
   return $this->estatus;
}
/** asignar a created_at */
public function set_created_at(){
   $this->created_at = date('Y-m-d h:i:s');
}
/** return de created_at */
public function get_created_at(){
   return $this->created_at;
}
/** asignar a medicina_id */
public function set_medicina_id($medicina_id){
   if(empty($medicina_id)){
       $this->medicina_id = 'null';
   }else{
       $this->medicina_id = $medicina_id;
   }
}
/** return de medicina_id */
public function get_medicina_id(){
   return $this->medicina_id;
}

/** asignar a observacion */
public function set_observacion($observacion){
   $this->observacion = $observacion;
}
/** return de observacion */
public function get_observacion(){
   return $this->observacion;
}

} 
?>