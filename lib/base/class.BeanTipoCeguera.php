
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 11/07/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */ 

class BeanTipoCeguera 
{ 
 
private $tipo_ceguera_id;

private $tipo_ceguera;

/** Constructor de la Class */
 function BeanTipoCeguera()
{
}
/** asignar a tipo_ceguera_id */
public function set_tipo_ceguera_id($tipo_ceguera_id){
   $this->tipo_ceguera_id = $tipo_ceguera_id;
}
/** return de tipo_ceguera_id */
public function get_tipo_ceguera_id(){
   return $this->tipo_ceguera_id;
}
/** asignar a tipo_ceguera */
public function set_tipo_ceguera($tipo_ceguera){
   $this->tipo_ceguera = $tipo_ceguera;
}
/** return de tipo_ceguera */
public function get_tipo_ceguera(){
   return $this->tipo_ceguera;
}
} 
?>