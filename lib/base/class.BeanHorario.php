
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 07/04/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */ 

class BeanHorario 
{ 
 
private $id;

private $observacion;

private $estatus;

private $tipo_horario_id;

private $usuario_id;

private $created_up;

private $updated_up;

/** Constructor de la Class */
 function BeanHorario()
{
}
/** asignar a id */
public function set_id($id){
   $this->id = $id;
}
/** return de id */
public function get_id(){
   return $this->id;
}
/** asignar a observacion */
public function set_observacion($observacion){
   $this->observacion = $observacion;
}
/** return de observacion */
public function get_observacion(){
   return $this->observacion;
}
/** asignar a estatus */
public function set_estatus(){
   $this->estatus = 'true';
}
/** return de estatus */
public function get_estatus(){
   return $this->estatus;
}
/** asignar a tipo_horario_id */
public function set_tipo_horario_id($tipo_horario_id){
   $this->tipo_horario_id = $tipo_horario_id;
}
/** return de tipo_horario_id */
public function get_tipo_horario_id(){
   return $this->tipo_horario_id;
}
/** asignar a usuario_id */
public function set_usuario_id(){
    session_start();
   $this->usuario_id = $_SESSION['userAut'][0];
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a created_up */
public function set_created_up(){
   $this->created_up = date('Y-m-d h:i:s');
}
/** return de created_up */
public function get_created_up(){
   return $this->created_up;
}
/** asignar a updated_up */
public function set_updated_up(){
   $this->updated_up = date('Y-m-d h:i:s');
}
/** return de updated_up */
public function get_updated_up(){
   return $this->updated_up;
}
} 
?>