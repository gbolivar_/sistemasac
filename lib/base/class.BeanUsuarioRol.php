
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 08/04/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */ 

class BeanUsuarioRol 
{ 
 
private $id;

private $desasociar;

private $rol_id;

private $sub_rol_id;

private $usuario_id;

private $estatus;

private $created_at;

private $updated_at;

/** Constructor de la Class */
 function BeanUsuarioRol()
{
}
/** asignar a id */
public function set_id($id){
   $this->id = $id;
}
/** return de id */
public function get_id(){
   return $this->id;
}
/** asignar a id */
public function set_desasociar($desasociar){
   $this->desasociar = $desasociar;
}
/** return de id */
public function get_desasociar(){
   return $this->desasociar;
}
/** asignar a rol_id */
public function set_rol_id($rol_id){
   $this->rol_id = $rol_id;
}
/** return de rol_id */
public function get_rol_id(){
   return $this->rol_id;
}
/** asignar a sub_rol_id */
public function set_sub_rol_id($sub_rol_id){
   $this->sub_rol_id = $sub_rol_id;
}
/** return de sub_rol_id */
public function get_sub_rol_id(){
   return $this->sub_rol_id;
}
/** asignar a usuario_id */
public function set_usuario_id($usuario_id){
    $this->usuario_id=$usuario_id;
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a estatus */
public function set_estatus(){
   $this->estatus = 'true';
}
/** return de estatus */
public function get_estatus(){
   return $this->estatus;
}
/** asignar a created_at */
public function set_created_at(){
   $this->created_at = date('Y-m-d h:i:s');
}
/** return de created_at */
public function get_created_at(){
   return $this->created_at;
}
/** asignar a updated_at */
public function set_updated_at(){
   $this->updated_at = date('Y-m-d h:i:s');
}
/** return de updated_at */
public function get_updated_at(){
   return $this->updated_at;
}
} 
?>