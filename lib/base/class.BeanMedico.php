
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 11/07/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */ 

class BeanMedico 
{ 
 
private $medico_id;

private $med_cedula;

private $med_nombre;

private $med_apellido;

private $usuario_id;

private $estatus;

private $created_at;

/** Constructor de la Class */
 function BeanMedico()
{
}
/** asignar a medico_id */
public function set_medico_id($medico_id){
   $this->medico_id = $medico_id;
}
/** return de medico_id */
public function get_medico_id(){
   return $this->medico_id;
}
/** asignar a med_nacionalidad */
public function set_med_nacionalidad($med_nacionalidad){
   $this->med_nacionalidad = $med_nacionalidad;
}
/** return de med_nacionalidad */
public function get_med_nacionalidad(){
   return $this->med_nacionalidad;
}
/** asignar a med_cedula */
public function set_med_cedula($med_cedula){
   $this->med_cedula = $med_cedula;
}
/** return de med_cedula */
public function get_med_cedula(){
   return $this->med_cedula;
}
/** asignar a med_nombre */
public function set_med_nombre($med_nombre){
   $this->med_nombre = $med_nombre;
}
/** return de med_nombre */
public function get_med_nombre(){
   return $this->med_nombre;
}

/** asignar a usuario_id */
public function set_usuario_id(){
   session_start();
    /** Asignar el id del usuario autenticado */
   $this->usuario_id=$_SESSION['userAut'][0];
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a estatus */
public function set_estatus(){
    $this->estatus = 'TRUE';
}
/** return de estatus */
public function get_estatus(){
   return $this->estatus;
}
/** asignar a created_at */
public function set_created_at(){
   $this->created_at = date('Y-m-d h:i:s');
}
/** return de created_at */
public function get_created_at(){
   return $this->created_at;
}
} 
?>