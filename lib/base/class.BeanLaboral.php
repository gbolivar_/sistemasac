<?php
class BeanLaboral
{ 
 
    private $laboral_id;
    private $pers_disc_visual_id;
    private $ocupacion_id;
    private $trabaja;
    private $empresa_trabaja;
    private $direccion_trabajo;
    private $telef_trabajo;
    private $fecha_ingreso;
    private $ingresos;
    private $oficio_id;
    private $profesion_id;
    private $usuario_id;
    private $estatus;
    private $created_at;

    /** Constructor de la Class */
    function beanLaboral(){

    }
    /** asignar a laboral_id */
    public function set_laboral_id($laboral_id){
       $this->laboral_id = $laboral_id;
    }
    /** return de laboral_id */
    public function get_laboral_id(){
       return $this->laboral_id;
    }
    /** asignar a pers_disc_visual_id */
    public function set_pers_disc_visual_id($pers_disc_visual_id){
       $this->pers_disc_visual_id = $pers_disc_visual_id;
    }
    /** return de pers_disc_visual_id */
    public function get_pers_disc_visual_id(){
       return $this->pers_disc_visual_id;
    }
    /** asignar a ocupacion_id */
    public function set_ocupacion_id($ocupacion_id){
       $this->ocupacion_id = $ocupacion_id;
    }
    /** return de ocupacion_id */
    public function get_ocupacion_id(){
       return $this->ocupacion_id;
    }
    /** asignar a trabaja */
    public function set_trabaja($trabaja){
       $this->trabaja = $trabaja;
    }
    /** return de trabaja */
    public function get_trabaja(){
       return $this->trabaja;
    }
    /** asignar a empresa_trabaja */
    public function set_empresa_trabaja($empresa_trabaja){
       $this->empresa_trabaja = $empresa_trabaja;
    }
    /** return de empresa_trabaja */
    public function get_empresa_trabaja(){
       return $this->empresa_trabaja;
    }
    /** asignar a direccion_trabajo */
    public function set_direccion_trabajo($direccion_trabajo){
       $this->direccion_trabajo = $direccion_trabajo;
    }
    /** return de direccion_trabajo */
    public function get_direccion_trabajo(){
       return $this->direccion_trabajo;
    }
    /** asignar a telef_trabajo */
    public function set_telef_trabajo($telef_trabajo){
       $this->telef_trabajo = $telef_trabajo;
    }
    /** return de telef_trabajo */
    public function get_telef_trabajo(){
       return $this->telef_trabajo;
    }
    /** asignar a fecha_ingreso */
    public function set_fecha_ingreso($fecha_ingreso){
       $this->fecha_ingreso = $fecha_ingreso;
    }
    /** return de fecha_ingreso */
    public function get_fecha_ingreso(){
       return $this->fecha_ingreso;
    }
    /** asignar a ingresos */
    public function set_ingresos($ingresos){
       $this->ingresos = $ingresos;
    }
    /** return de ingresos */
    public function get_ingresos(){
       return $this->ingresos;
    }
    /** asignar a oficio_id */
    public function set_oficio_id($oficio_id){
       $this->oficio_id = $oficio_id;
    }
    /** return de oficio_id */
    public function get_oficio_id(){
       return $this->oficio_id;
    }
    /** asignar a profesion_id */
    public function set_profesion_id($profesion_id){
       $this->profesion_id = $profesion_id;
    }
    /** return de profesion_id */
    public function get_profesion_id(){
       return $this->profesion_id;
    }
    /** asignar a usuario_id */
    public function set_usuario_id($usuario_id){
       $this->usuario_id = $usuario_id;
    }
    /** return de usuario_id */
    public function get_usuario_id(){
       return $this->usuario_id;
    }
    /** asignar a estatus */
    public function set_estatus(){
       $this->estatus = 'TRUE';
    }
    /** return de estatus */
    public function get_estatus(){
       return $this->estatus;
    }
    /** asignar a created_at */
    public function set_created_at(){
       $this->created_at = date('Y-m-d h:i:s');
    }
    /** return de created_at */
    public function get_created_at(){
       return $this->created_at;
    }
}
?>