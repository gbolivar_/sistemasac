
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 29/02/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */

class BeanGrupoFamiliar
{

private $familiar_id;

private $pers_disc_visual_id;

private $parentesco_id;

private $nombre;

private $apellido;

private $nacionalidad;

private $fech_nacimiento;

private $sexo;

private $direccion;

private $telefono;

private $estudia;

private $esp_estudia;

private $trabaja;

private $oficio_id;

private $direcc_trabajo;

private $ingreso;

private $sufre_ceguera;

private $usuario_id;

private $estatus;

private $created_at;

private $updated_at;

/** Constructor de la Class */
 function BeanGrupoFamiliar()
{
}
/** asignar a familiar_id */
public function set_familiar_id($familiar_id){
   $this->familiar_id = $familiar_id;
}
/** return de familiar_id */
public function get_familiar_id(){
   return $this->familiar_id;
}
/** asignar a pers_disc_visual_id */
public function set_pers_disc_visual_id($pers_disc_visual_id){
   $this->pers_disc_visual_id = $pers_disc_visual_id;
}
/** return de pers_disc_visual_id */
public function get_pers_disc_visual_id(){
   return $this->pers_disc_visual_id;
}
/** asignar a parentesco_id */
public function set_parentesco_id($parentesco_id){
   $this->parentesco_id = $parentesco_id;
}
/** return de parentesco_id */
public function get_parentesco_id(){
   return $this->parentesco_id;
}
/** asignar a nombre */
public function set_nombre($nombre){
   $this->nombre = $nombre;
}
/** return de nombre */
public function get_nombre(){
   return $this->nombre;
}
/** asignar a apellido */
public function set_apellido($apellido){
   $this->apellido = $apellido;
}
/** return de apellido */
public function get_apellido(){
   return $this->apellido;
}
/** asignar a nacionalidad */
public function set_nacionalidad($nacionalidad){
   $this->nacionalidad = $nacionalidad;
}
/** return de nacionalidad */
public function get_nacionalidad(){
   return $this->nacionalidad;
}
/** asignar a fech_nacimiento */
public function set_fech_nacimiento($fech_nacimiento){
   $this->fech_nacimiento = $fech_nacimiento;
}
/** return de fech_nacimiento */
public function get_fech_nacimiento(){
   return $this->fech_nacimiento;
}
/** asignar a sexo */
public function set_sexo($sexo){
   $this->sexo = $sexo;
}
/** return de sexo */
public function get_sexo(){
   return $this->sexo;
}
/** asignar a direccion */
public function set_direccion($direccion){
   $this->direccion = $direccion;
}
/** return de direccion */
public function get_direccion(){
   return $this->direccion;
}
/** asignar a telefono */
public function set_telefono($telefono){
   if(!empty($telefono)){$valor=$telefono; }else{ $valor=0; }
    $this->telefono = $valor;
}
/** return de telefono */
public function get_telefono(){
   return $this->telefono;
}
/** asignar a estudia */
public function set_estudia($estudia){
   $this->estudia = $estudia;
}
/** return de estudia */
public function get_estudia(){
   return $this->estudia;
}
/** asignar a esp_estudia */
public function set_esp_estudia($esp_estudia){
   $this->esp_estudia = $esp_estudia;
}
/** return de esp_estudia */
public function get_esp_estudia(){
   return $this->esp_estudia;
}
/** asignar a trabaja */
public function set_trabaja($trabaja){
   $this->trabaja = $trabaja;
}
/** return de trabaja */
public function get_trabaja(){
   return $this->trabaja;
}
/** asignar a oficio_id */
public function set_oficio_id($oficio_id){
   $this->oficio_id = $oficio_id;
}
/** return de oficio_id */
public function get_oficio_id(){
   return $this->oficio_id;
}
/** asignar a direcc_trabajo */
public function set_direcc_trabajo($direcc_trabajo){
   $this->direcc_trabajo = $direcc_trabajo;
}
/** return de direcc_trabajo */
public function get_direcc_trabajo(){
   return $this->direcc_trabajo;
}
/** asignar a ingreso */
public function set_ingreso($ingreso){
   if(!empty($ingreso)){$valor=$ingreso; }else{ $valor=0; }
    $this->ingreso = $valor;
}
/** return de ingreso */
public function get_ingreso(){
   return $this->ingreso;
}
/** asignar a sufre_ceguera */
public function set_sufre_ceguera($sufre_ceguera){
   $this->sufre_ceguera = $sufre_ceguera;
}
/** return de sufre_ceguera */
public function get_sufre_ceguera(){
   return $this->sufre_ceguera;
}
/** asignar a usuario_id */
public function set_usuario_id(){
   session_start();
   /** Asignar el id del usuario autenticado */
   $this->usuario_id=$_SESSION['userAut'][0];
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a estatus */
public function set_estatus(){
   $this->estatus = 'true';
}
/** return de estatus */
public function get_estatus(){
   return $this->estatus;
}
/** asignar a created_at */
public function set_created_at(){
   $this->created_at = date('Y-m-d h:i:s');
}
/** return de created_at */
public function get_created_at(){
   return $this->created_at;
}
/** asignar a updated_at */
public function set_updated_at(){
   $this->updated_at = date('Y-m-d h:i:s');
}
/** return de updated_at */
public function get_updated_at(){
   return $this->updated_at;
}
}
?>