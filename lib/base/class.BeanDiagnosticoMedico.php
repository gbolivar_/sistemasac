
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 29/02/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */

class BeanDiagnosticoMedico
{

private $diag_medico_id;

private $pers_disc_visual_id;

private $medico_id;

private $tipo_ceguera_id;

private $esta_rehabilitado;

private $otra_enfermedad;

private $diagnostico;

private $pronostico;

private $campo_visual_od;

private $campo_visual_oi;

private $agudez_visual_od;

private $agudez_visual_id;

private $usa_lentes;

private $consideracion_visual;

private $usuario_id;

private $estatus;

private $created_at;

/** Constructor de la Class */
 function BeanDiagnosticoMedico()
{
}
/** asignar a diag_medico_id */
public function set_diag_medico_id($diag_medico_id){
   $this->diag_medico_id = $diag_medico_id;
}
/** return de diag_medico_id */
public function get_diag_medico_id(){
   return $this->diag_medico_id;
}
/** asignar a pers_disc_visual_id */
public function set_pers_disc_visual_id($pers_disc_visual_id){
   $this->pers_disc_visual_id = $pers_disc_visual_id;
}
/** return de pers_disc_visual_id */
public function get_pers_disc_visual_id(){
   return $this->pers_disc_visual_id;
}
/** asignar a medico_id */
public function set_medico_id($medico_id){
   $this->medico_id = $medico_id;
}
/** return de medico_id */
public function get_medico_id(){
   return $this->medico_id;
}
/** asignar a tipo_ceguera_id */
public function set_tipo_ceguera_id($tipo_ceguera_id){
   $this->tipo_ceguera_id = $tipo_ceguera_id;
}
/** return de tipo_ceguera_id */
public function get_tipo_ceguera_id(){
   return $this->tipo_ceguera_id;
}
/** asignar a esta_rehabilitado */
public function set_esta_rehabilitado($esta_rehabilitado){
   $this->esta_rehabilitado = $esta_rehabilitado;
}
/** return de esta_rehabilitado */
public function get_esta_rehabilitado(){
   return $this->esta_rehabilitado;
}
/** asignar a otra_enfermedad */
public function set_otra_enfermedad($otra_enfermedad){
   $this->otra_enfermedad = $otra_enfermedad;
}
/** return de otra_enfermedad */
public function get_otra_enfermedad(){
   return $this->otra_enfermedad;
}
/** asignar a diagnostico */
public function set_diagnostico($diagnostico){
   $this->diagnostico = $diagnostico;
}
/** return de diagnostico */
public function get_diagnostico(){
   return $this->diagnostico;
}
/** asignar a pronostico */
public function set_pronostico($pronostico){
   $this->pronostico = $pronostico;
}
/** return de pronostico */
public function get_pronostico(){
   return $this->pronostico;
}
/** asignar a campo_visual_od */
public function set_campo_visual_od($campo_visual_od){
   $this->campo_visual_od = $campo_visual_od;
}
/** return de campo_visual_od */
public function get_campo_visual_od(){
   return $this->campo_visual_od;
}
/** asignar a campo_visual_oi */
public function set_campo_visual_oi($campo_visual_oi){
   $this->campo_visual_oi = $campo_visual_oi;
}
/** return de campo_visual_oi */
public function get_campo_visual_oi(){
   return $this->campo_visual_oi;
}
/** asignar a agudez_visual_od */
public function set_agudez_visual_od($agudez_visual_od){
   $this->agudez_visual_od = $agudez_visual_od;
}
/** return de agudez_visual_od */
public function get_agudez_visual_od(){
   return $this->agudez_visual_od;
}
/** asignar a agudez_visual_id */
public function set_agudez_visual_id($agudez_visual_id){
   $this->agudez_visual_id = $agudez_visual_id;
}
/** return de agudez_visual_id */
public function get_agudez_visual_id(){
   return $this->agudez_visual_id;
}
/** asignar a usa_lentes */
public function set_usa_lentes($usa_lentes){
   $this->usa_lentes = $usa_lentes;
}
/** return de usa_lentes */
public function get_usa_lentes(){
   return $this->usa_lentes;
}
/** asignar a consideracion_visual */
public function set_consideracion_visual($consideracion_visual){
   $this->consideracion_visual = $consideracion_visual;
}
/** return de consideracion_visual */
public function get_consideracion_visual(){
   return $this->consideracion_visual;
}
/** asignar a usuario_id */
public function set_usuario_id(){
   session_start();
    /** Asignar el id del usuario autenticado */
    $this->usuario_id=$_SESSION['userAut'][0];
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a estatus */
public function set_estatus(){
   $this->estatus = 'true';
}
/** return de estatus */
public function get_estatus(){
   return $this->estatus;
}
/** asignar a created_at */
public function set_created_at(){
   $this->created_at = date('Y-m-d h:i:s'); //date('Y-m-d h:i:s');
}
/** return de created_at */
public function get_created_at(){
   return $this->created_at;
}
}
?>