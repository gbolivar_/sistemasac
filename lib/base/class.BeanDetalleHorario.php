
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 07/04/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */ 

class BeanDetalleHorario 
{ 
 
private $id;

private $hora_inicio;

private $hora_fin;

private $dia_id;

private $area_id;

private $horario_id;

/** Constructor de la Class */
 function BeanDetalleHorario()
{
}
/** asignar a id */
public function set_id($id){
   $this->id = $id;
}
/** return de id */
public function get_id(){
   return $this->id;
}
/** asignar a hora_inicio */
public function set_hora_inicio($hora_inicio){
   $this->hora_inicio = $hora_inicio;
}
/** return de hora_inicio */
public function get_hora_inicio(){
   return $this->hora_inicio;
}
/** asignar a hora_fin */
public function set_hora_fin($hora_fin){
   $this->hora_fin = $hora_fin;
}
/** return de hora_fin */
public function get_hora_fin(){
   return $this->hora_fin;
}
/** asignar a dia_id */
public function set_dia_id($dia_id){
   $this->dia_id = $dia_id;
}
/** return de dia_id */
public function get_dia_id(){
   return $this->dia_id;
}
/** asignar a area_id */
public function set_area_id($area_id){
   $this->area_id = $area_id;
}
/** return de area_id */
public function get_area_id(){
   return $this->area_id;
}
/** asignar a horario_id */
public function set_horario_id($horario_id){
   $this->horario_id = $horario_id;
}
/** return de horario_id */
public function get_horario_id(){
   return $this->horario_id;
}
} 
?>