
     <?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 24/04/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */ 

class BeanTipoAyuda 
{ 
 
private $tipo_ayuda_id;

private $tipo_ayuda;

private $total_monto;

private $anio_autorizado;

private $usuario_id;

private $estatus;

private $created_at;

/** Constructor de la Class */
 function BeanTipoAyuda()
{
}
/** asignar a tipo_ayuda_id */
public function set_tipo_ayuda_id($tipo_ayuda_id){
   $this->tipo_ayuda_id = $tipo_ayuda_id;
}
/** return de tipo_ayuda_id */
public function get_tipo_ayuda_id(){
   return $this->tipo_ayuda_id;
}
/** asignar a tipo_ayuda */
public function set_tipo_ayuda($tipo_ayuda){
   $this->tipo_ayuda = $tipo_ayuda;
}
/** return de tipo_ayuda */
public function get_tipo_ayuda(){
   return $this->tipo_ayuda;
}
/** asignar a total_monto */
public function set_total_monto($total_monto){
   $this->total_monto = $total_monto;
}
/** return de total_monto */
public function get_total_monto(){
   return $this->total_monto;
}
/** asignar a anio_autorizado */
public function set_anio_autorizado(){
   $this->anio_autorizado = date('Y');
}
/** return de anio_autorizado */
public function get_anio_autorizado(){
   return $this->anio_autorizado;
}
/** asignar a usuario_id */
public function set_usuario_id(){
   session_start();
   /** Asignar el id del usuario autenticado */
   $this->usuario_id=$_SESSION['userAut'][0];
}
/** return de usuario_id */
public function get_usuario_id(){
   return $this->usuario_id;
}
/** asignar a estatus */
public function set_estatus(){
   $this->estatus = 'true';
}
/** return de estatus */
public function get_estatus(){
   return $this->estatus;
}
/** asignar a created_at */
public function set_created_at(){
   $this->created_at = date('Y-m-d h:i:s');
}
/** return de created_at */
public function get_created_at(){
   return $this->created_at;
}
} 
?>