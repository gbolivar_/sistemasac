<?php
     /**
       * @propiedad: PROPIETARIO DEL CODIGO
       * @Autor: Gregorio Bolivar
       * @email: elalconxvii@gmail.com
       * @Fecha de Creacion: 21/02/2012
       * @Auditado por: Gregorio J Bolívar B
       * @Descripción: Generado por el generador de set y get del autor
       * @package: datosClass
       * @version: 1.0
       */

    class BeanEstudio 
    {

    private $estudio_id;

    private $pers_disc_visual_id;

    private $estudia;

    private $grado_inst_id;

    private $lugar_estudia;

    private $estudio_realiza;

    private $usuario_id;

    private $estatus;

    private $created_at;

    private $updated_at;

    /** Constructor de la Class */
     function beanEstudio()
    {
    }
    /** asignar a estudio_id */
    public function set_estudio_id($estudio_id){
       $this->estudio_id = $estudio_id;
    }
    /** return de estudio_id */
    public function get_estudio_id(){
       return $this->estudio_id;
    }
    /** asignar a pers_disc_visual_id */
    public function set_pers_disc_visual_id($pers_disc_visual_id){
       $this->pers_disc_visual_id = $pers_disc_visual_id;
    }
    /** return de pers_disc_visual_id */
    public function get_pers_disc_visual_id(){
       return $this->pers_disc_visual_id;
    }
    /** asignar a estudia */
    public function set_estudia($estudia){
       $this->estudia = $estudia;
    }
    /** return de estudia */
    public function get_estudia(){
       return $this->estudia;
    }
    /** asignar a grado_inst_id */
    public function set_grado_inst_id($grado_inst_id){
       $this->grado_inst_id = $grado_inst_id;
    }
    /** return de grado_inst_id */
    public function get_grado_inst_id(){
       return $this->grado_inst_id;
    }
    /** asignar a lugar_estudia */
    public function set_lugar_estudia($lugar_estudia){
       $this->lugar_estudia = $lugar_estudia;
    }
    /** return de lugar_estudia */
    public function get_lugar_estudia(){
       return $this->lugar_estudia;
    }
    /** asignar a estudio_realiza */
    public function set_estudio_realiza($estudio_realiza){
       $this->estudio_realiza = $estudio_realiza;
    }
    /** return de estudio_realiza */
    public function get_estudio_realiza(){
       return $this->estudio_realiza;
    }
    /** asignar a usuario_id */
    public function set_usuario_id($usuario_id){
       $this->usuario_id = $usuario_id;
    }
    /** return de usuario_id */
    public function get_usuario_id(){
       return $this->usuario_id;
    }
    /** asignar a estatus */
    public function set_estatus(){
       $this->estatus = 'true';
    }
    /** return de estatus */
    public function get_estatus(){
       return $this->estatus;
    }
    /** asignar a created_at */
    public function set_created_at(){
       $this->created_at = date('Y-m-d h:i:s');;
    }
    /** return de created_at */
    public function get_created_at(){
       return $this->created_at;
    }

}
?>