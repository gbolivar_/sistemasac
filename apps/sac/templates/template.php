<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Sistema de Gestion de Personas con Discapacidad Visual en la Sociedad Amigos de los Ciegos (SAC).</title>
<link rel="shortcut icon" href="images/icon.png" />
<link type="text/css" href="css/style.css" rel="stylesheet">
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<!--script type="text/javascript" src="js/jquery.html5form-1.5-min.js"></script-->
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/procesos.js" type="text/javascript"></script>
<script type="text/javascript" src="js/validaciones.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script type="text/javascript" src="js/exporting.js" type="text/javascript"></script>
<script type="text/javascript" src="js/highcharts.js" type="text/javascript"></script>



</head>
<body>
<?php $url=$_SERVER['HTTP_HOST'].'/'.$_SERVER['PHP_SELF'].''; ?>
<noscript> <meta http-equiv=refresh content="0; URL='http://<?php echo $url?>?logout=desconectar')?>" /> </noscript>
<!-- Contenedor general del template --->
<div id="general"
	<!-- Contenedor Cabezera --->
	<div id="cabezera">
		<div id="logo"><br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema de Gestion de Personas con Discapacidad Visual en la Sociedad Amigos de los Ciegos. </div>
		<div id="infoSession">&nbsp;&nbsp;
			<img src='images/usuario.png' title='Usuario' width='20px' alt="Usuario"/><b>: <?php echo 'Bienvenido(a): '.$_SESSION['userAut'][6].' '.$_SESSION['userAut'][2].' '.$_SESSION['userAut'][3].' C.I.: '.$_SESSION['userAut'][1].'';?></b> &nbsp;
			<img src='images/maquina.png' title='Maquina' width='20px' alt="Maquina"/><b>: <?php echo $_SESSION['realNameS']?></b>&nbsp;
			<img src='images/ip.png' title='Ip' width='20px' alt="Ip"/><b>: <?php echo $_SESSION['realIpS'];?> </b>&nbsp;
                        <img src='images/hora.png' title='Hora' width='20px' alt="Hora"/><b>:<div id="time"></div></b>
                        <div id="salir"><a href="?logout=desconectar" title="Desconectar" id="salirDesconectar"><img src="images/salir.jpeg" title="" width='23px' alt="Salir" border="0"><b>Salir</b></a></div>
                </div>
	</div>

	<!--  Contenedor Principal donde se mostrara la informacion --->
	<div id="principal">
		<div id="menu">
                   <?php include_once '_menu.php';?>
		</div>
		<!-- Contenedor mensaje ayuda --->
		<div id="mensaje"></div>
		<div id="contenido"><br/>
                    <fieldset class="subTitulos" style="width: 90%"> <legend><b>BIENVENIDO</b></legend>
                       <center> <br/><br/>Al Sistema de Gesti&oacute;n de persona con capacidad,
                        proyecto del  Grupo 1<br> de las Seccion 732 del Colegio Universitario Francisco de Miranda
                        del PNFI.</center><br/><br/>
                      </fieldset>                   
                </div>
                 <div id="overlay" style="display:none">
                        <div class="ui-overlay">
                            <div class="ui-widget-overlay"></div>
                            <center><img src="images/cargando.gif" style="width: 150px; height: 52px; position: absolute; left: 47%; top: 38%;""></center>
                        </div>     
                </div>
               
	</div>

	<!-- Contenedor pie de pagina -->
	<div id='pie'>
		<div id="conPie">Sistema desarrollado por el <a href="#" id="dialog_link" class="ui-state-default ui-corner-all">Grupo 1</a> de Ingenieria en Informatica del CUFM</div>

		<!-- Dialogo que mostrara -->
		<div id="dialog" title="Autores">
			<b>Roxenis Sanoja<br/>
			Yarelis Quiaro<br/>
			Juan Carlos Santander<br/>
			Gregorio Bol&iacute;var<br/></b>
		</div>
	</div>
        <div id="comun"></div>

</div>
</body>
</html>