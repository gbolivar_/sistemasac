<?php
  /**
    * @propiedad: PROPIETARIO DEL CODIGO
    * @Autor: Gregorio Bolivar
    * @email: elalconxvii@gmail.com
    * @Fecha de Create: 21/02/2012
    * @Fecha de Update: 26/02/2012
    * @Auditado por: Gregorio J Bolívar B
    * @Descripción: Actions encargado de procesar los methods del usuario
    * @package: actions.class
    * @version: 1.0
    */
/** Incluir los Bean necesario para procesar usuario  */
include_once '../../../../../lib/complementos/funcionesExtras.php';
include_once '../../../../../lib/base/class.BeanSubRol.php';

// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

class subRolAction extends BeanSubRol
{
    public function executarSetBeanSubRol($request){
        /** Cargar Objetos de usuario Rol*/


        $this->beanSubRol->set_id(@$request['id']);
        $this->beanSubRol->set_rol_id(@$request['rol_id']);
        $this->beanSubRol->set_detalles(@$request['detalles']);
        $this->beanSubRol->set_title(@$request['title']);
        $this->beanSubRol->set_div_id(@$request['div_id']);
        $this->beanSubRol->set_activa_menu(@$request['activa_menu']);
              
        /** Cargar Objetos generales para cada campo */
        $this->beanSubRol->set_usuario_id();
        $this->beanSubRol->set_estatus();
        $this->beanSubRol->set_created_at();
        $this->beanSubRol->set_updated_at();


    }

    public function executarShow($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }
    public function executarIndex($request){
        $datoJson='';
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $request['proceso'];
        /** Query que va a mostrar todos los permisos */
        $sql="SELECT (select CASE WHEN count(*)=0 THEN 1 ELSE 2  END AS posee from audiperm.tbl_usuario_rol WHERE a.id=sub_rol_id AND usuario_id=".$request['proceso'].") as tiene, 
            a.id, a.rol_id, b.roles, a.usuario_id, detalles, title, a.div_id, a.activa_menu  
            FROM audiperm.tbl_sub_rol a 
            INNER JOIN audiperm.tbl_rol b ON a.rol_id=b.id
            WHERE a.activa_menu='t' order by rol_id;";
  
        $this->conn->execute($sql);
        if($this->conn->num_row()>0){
            $this->listados=$this->conn->all_row();
            foreach ($this->listados as $item=>$fila):
                $datoJson['listados'][$item]=$fila;
            endforeach;

        }else{
            $datoJson['mensaje']='Error, en estraer el listado de usuario';
            $datoJson['error']='1';
        }
       $datoJson['mensaje']='LLEGO AL INDEX SUB ROL';
        echo json_encode($datoJson);
    }

    public function executarNew($request){

        $datoJson='';
        $this->beanSubRol = new BeanSubRol();
      
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);

        /** Method que se envcarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanSubRol($request);

        /** procedimiento para registro de usuario */
         $sql1="INSERT INTO audiperm.tbl_usuario(
            cedula, apellido, nombre, sexo, fech_nacimiento, tipo_usuario_id, 
            correo_elec, telefono, login, clave, estatus, nacionalidad, usuario_id, created_at)
            VALUES ('".$this->beanUsuario->get_cedula()."', '".$this->beanUsuario->get_apellido()."', '".$this->beanUsuario->get_nombre()."', '".$this->beanUsuario->get_sexo()."', '".$this->beanUsuario->get_fech_nacimiento()."', ".$this->beanUsuario->get_tipo_usuario_id().", 
            '".$this->beanUsuario->get_correo_elec()."', '".$this->beanUsuario->get_telefono()."', '".$this->beanUsuario->get_login()."', '".$this->beanUsuario->get_clave()."', '".$this->beanUsuario->get_estatus()."', '".$this->beanUsuario->get_nacionalidad()."', 
            ".$this->beanUsuario->get_usuario_id().", '".$this->beanUsuario->get_created_at()."');";
         $this->result=$this->conn->execute($sql1);
       
       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                  $datoJson['mensaje']='Registro efectuado exitosamente';
                  $datoJson['error']='0';
 
            }else{
                  $datoJson['mensaje']='Error al efectuar el registro';
                  $datoJson['error']='1';
            }

        $this->conn->close();
        echo json_encode($datoJson);
    }
    public function executarEdit($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }

    public function executarDeleted($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }

}
/** Method para validar inyeccion sql por el GET */
$valGet = new funcionesExtras();
$valGet -> validarMethodGet();
/** Validar los action del proceso que se va instanciar */
 $evento  = $_POST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_POST;
/** Instancio la class que vamos a necesitar*/
$proceso = new subRolAction();

if($evento=='new'){
    return $proceso->executarNew($request);
 }elseif($evento=='edit'){
    return $proceso->executarEdit($request);
 }elseif($evento=='index'){
    return $proceso->executarIndex($request);
 }elseif($evento=='delete'){
    return $proceso->executarDeleted($request);
 }elseif($evento=='show'){
    return $proceso->executarShow($request);
 }

?>

