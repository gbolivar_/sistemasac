<?php
/** Incluir los Bean necesario  */
include_once '../../../../../lib/base/class.BeanGrupoFamiliar.php';

// Incluir archivo de configuracion
include_once '../../../../../config/Configuration.class.php';

// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class grupoFamiliarAction extends BeanGrupoFamiliar
{

   public function executarSetBeanGrupoFamiliar($request){

        $this->beanGrupoFamiliar->set_familiar_id(@$request['familiar_id']);
        $this->beanGrupoFamiliar->set_pers_disc_visual_id(@$request['pers_disc_visual_id']);
        $this->beanGrupoFamiliar->set_apellido(@$request['apellido']);
        $this->beanGrupoFamiliar->set_nombre(@$request['nombre']);
        $this->beanGrupoFamiliar->set_fech_nacimiento(@$request['fech_nacimiento']);
        $this->beanGrupoFamiliar->set_sexo(@$request['sexo']);
        $this->beanGrupoFamiliar->set_nacionalidad(@$request['nacionalidad']);
        $this->beanGrupoFamiliar->set_parentesco_id(@$request['parentesco_id']);
        $this->beanGrupoFamiliar->set_direccion(@$request['direccion']);
        $this->beanGrupoFamiliar->set_telefono(@$request['telefono']);
        $this->beanGrupoFamiliar->set_estudia(@$request['estudia']);
        $this->beanGrupoFamiliar->set_esp_estudia(@$request['esp_estudia']);
        $this->beanGrupoFamiliar->set_trabaja(@$request['trabaja']);
        $this->beanGrupoFamiliar->set_direcc_trabajo(@$request['direcc_trabajo']);
        $this->beanGrupoFamiliar->set_ingreso(@$request['ingreso']);
        $this->beanGrupoFamiliar->set_oficio_id(@$request['oficio_id']);
        $this->beanGrupoFamiliar->set_sufre_ceguera(@$request['sufre_ceguera']);
   //     $this->beanGrupoFamiliar->set_impresacd($request['impresac']);


        /** Cargar Objetos generales para cada campo */
        $this->beanGrupoFamiliar->set_usuario_id();//$request['usuario_id']);
        $this->beanGrupoFamiliar->set_estatus();
        $this->beanGrupoFamiliar->set_created_at();
        $this->beanGrupoFamiliar->set_updated_at();

    }


    public function executarShow($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO Al SHOW';
        echo json_encode($datoJson);
    }

    public function executarIndex($request){
        $datoJson='';
         /** Instanciamos la class BeanGrupoFamiliar */
        $this->beanGrupoFamiliar = new BeanGrupoFamiliar();
        /** Instanciamos la class de conecion */
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        grupoFamiliarAction::executarSetBeanGrupoFamiliar($request);
        if($this->beanGrupoFamiliar->get_familiar_id()!=''){
           $sqlCompletar =" familiar_id = ".$this->beanGrupoFamiliar->get_familiar_id();
        }else{
           $sqlCompletar =" pers_disc_visual_id = ".$this->beanGrupoFamiliar->get_pers_disc_visual_id();
        }

        $sql="SELECT * FROM  tbl_grupo_familiar a INNER JOIN tbl_parentesco b ON a.parentesco_id=b.parentesco_id  WHERE estatus=true AND $sqlCompletar";
        $this->result=$this->conn->execute($sql);

        $this->familiares=@$this->conn->all_row();

        /** Armo el json que sera enviado la vista con los grupo familiar*/
        if($this->familiares!=''){
            foreach ($this->familiares as $p=>$pd){
                $datoJson['familiares'][$p]=$pd;
            }
        }
        /** Verifico si trae dato de base de datos para mandar el mensaje a la vista 1 = error, 0= select */
        if(@empty($datoJson)){
                  $datoJson['error']='0';

            }else{
                  $datoJson['mensaje']='Error al efectuar el registro';
                  $datoJson['error']='1';
            }
        /** Cerrar coneccion con la base de datos */
        $this->conn->close();
        echo json_encode($datoJson);
    }
/** NEW QUE EJECUTA EL INSERT EN LA TABLA GRUPO FAMILIAR */
    public function executarNew($request){
         $datoJson='';
        /** Instanciamos la class BeanGrupoFamiliar */
        $this->beanGrupoFamiliar = new BeanGrupoFamiliar();

        /** Instanciamos la class de conecion */
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $this->conn1 = new conexionDataBase(0);
        $this->conn1->conexion(0);

        /** Method que se encarga de llenar los set de los campos recibe el request y el identificador del usuario */
        grupoFamiliarAction::executarSetBeanGrupoFamiliar($request);

        /** Calcula la edad que viene por el metodo POST (formato: año/mes/dia)**/
	function edad($edad){
		list($anio,$mes,$dia) = explode("-",$edad);
		$anio_dif = date("Y") - $anio;	$mes_dif = date("m") - $mes;	$dia_dif = date("d") - $dia;
		if ($dia_dif < 0 || $mes_dif < 0)
			$anio_dif--;
		return $anio_dif;
	}
        $edad = edad($this->beanGrupoFamiliar->get_fech_nacimiento());

        /** CONDICONAL QUE EVALUA SI ES MENOR DE EDAD Y SI ES HIJO PARA INSERTAR EL CORRELATIVO SI NO SE CUMPLE SE VA AL ELSE QUE HACE EL INSERT SIN CORRELATIVO  */
	if(($this->beanGrupoFamiliar->get_parentesco_id() == 1) and ($edad < 18)){
		$sql_count = "select count(*) from tbl_grupo_familiar where pers_disc_visual_id=".$this->beanGrupoFamiliar->get_pers_disc_visual_id()." and parentesco_id=1 and num_hijo is not null";
		$this->contador=$this->conn1->execute($sql_count);
		$result_count =  $this->conn1->all_row();

		foreach ($result_count as $count){
		   $numero_hijo = $count['count'];
		}
		   $numero_hijo = $numero_hijo + 1;

		$cadena= strlen($numero_hijo);
		if($cadena==1){
			$numero_hijo= "0".$numero_hijo;
		}

	    $sql="INSERT INTO tbl_grupo_familiar(
		pers_disc_visual_id, parentesco_id, nombre, apellido,
		nacionalidad, fech_nacimiento, sexo, direccion, telefono, estudia,
		esp_estudia, trabaja, oficio_id, direcc_trabajo, ingreso, sufre_ceguera,
		usuario_id, estatus, created_at, num_hijo)
		VALUES (".$this->beanGrupoFamiliar->get_pers_disc_visual_id().", ".$this->beanGrupoFamiliar->get_parentesco_id().", '".$this->beanGrupoFamiliar->get_nombre()."', '".$this->beanGrupoFamiliar->get_apellido()."', '".$this->beanGrupoFamiliar->get_nacionalidad()."',
		'".$this->beanGrupoFamiliar->get_fech_nacimiento()."', '".$this->beanGrupoFamiliar->get_sexo()."', '".$this->beanGrupoFamiliar->get_direccion()."', ".$this->beanGrupoFamiliar->get_telefono().", '".$this->beanGrupoFamiliar->get_estudia()."',
		'".$this->beanGrupoFamiliar->get_esp_estudia()."', '".$this->beanGrupoFamiliar->get_trabaja()."', ".$this->beanGrupoFamiliar->get_oficio_id().", '".$this->beanGrupoFamiliar->get_direcc_trabajo()."', ".$this->beanGrupoFamiliar->get_ingreso().", '".$this->beanGrupoFamiliar->get_sufre_ceguera()."',
		".$this->beanGrupoFamiliar->get_usuario_id().", ".$this->beanGrupoFamiliar->get_estatus().",'".$this->beanGrupoFamiliar->get_created_at()."','".$numero_hijo."');";
		$this->result=$this->conn->execute($sql);
	}else{
	    $sql="INSERT INTO tbl_grupo_familiar(
		pers_disc_visual_id, parentesco_id, nombre, apellido,
		nacionalidad, fech_nacimiento, sexo, direccion, telefono, estudia,
		esp_estudia, trabaja, oficio_id, direcc_trabajo, ingreso, sufre_ceguera,
		usuario_id, estatus, created_at)
		VALUES (".$this->beanGrupoFamiliar->get_pers_disc_visual_id().", ".$this->beanGrupoFamiliar->get_parentesco_id().", '".$this->beanGrupoFamiliar->get_nombre()."', '".$this->beanGrupoFamiliar->get_apellido()."', '".$this->beanGrupoFamiliar->get_nacionalidad()."',
		'".$this->beanGrupoFamiliar->get_fech_nacimiento()."', '".$this->beanGrupoFamiliar->get_sexo()."', '".$this->beanGrupoFamiliar->get_direccion()."', ".$this->beanGrupoFamiliar->get_telefono().", '".$this->beanGrupoFamiliar->get_estudia()."',
		'".$this->beanGrupoFamiliar->get_esp_estudia()."', '".$this->beanGrupoFamiliar->get_trabaja()."', ".$this->beanGrupoFamiliar->get_oficio_id().", '".$this->beanGrupoFamiliar->get_direcc_trabajo()."', ".$this->beanGrupoFamiliar->get_ingreso().", '".$this->beanGrupoFamiliar->get_sufre_ceguera()."',
		".$this->beanGrupoFamiliar->get_usuario_id().", ".$this->beanGrupoFamiliar->get_estatus().",'".$this->beanGrupoFamiliar->get_created_at()."' );";
		       	   $this->result=$this->conn->execute($sql);
	}

   /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
	if($this->result){
  /** Identificador de la pers_disc_visual para poder mostrar una tabla dinamica con los familiares */
	  $datoJson['pers_disc_visual_id']=$this->beanGrupoFamiliar->get_pers_disc_visual_id();
	  $datoJson['mensaje']='Registro efectuado exitosamente';
	  $datoJson['error']='0';
	}else{
	      /** Identificador de la pers_disc_visual para poder mostrar una tabla dinamica con los familiares */
	  $datoJson['pers_disc_visual_id']=$this->beanGrupoFamiliar->get_pers_disc_visual_id();
	  $datoJson['mensaje']='Error al efectuar el registro';
	  $datoJson['error']='1';
	}
	    echo json_encode($datoJson);
    }


    public function executarEdit($request){
         $datoJson='';
        /** Instanciamos la class BeanGrupoFamiliar */
        $this->beanGrupoFamiliar = new BeanGrupoFamiliar();
        /** Instanciamos la class de conecion */
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        grupoFamiliarAction::executarSetBeanGrupoFamiliar($request);

        $sql="UPDATE tbl_grupo_familiar SET  parentesco_id=".$this->beanGrupoFamiliar->get_parentesco_id().", nombre='".$this->beanGrupoFamiliar->get_nombre()."', apellido='".$this->beanGrupoFamiliar->get_apellido()."', nacionalidad='".$this->beanGrupoFamiliar->get_nacionalidad()."',
                    fech_nacimiento='".$this->beanGrupoFamiliar->get_fech_nacimiento()."', sexo='".$this->beanGrupoFamiliar->get_sexo()."', direccion='".$this->beanGrupoFamiliar->get_direccion()."', telefono=".$this->beanGrupoFamiliar->get_telefono().", estudia='".$this->beanGrupoFamiliar->get_estudia()."',
                    esp_estudia='".$this->beanGrupoFamiliar->get_esp_estudia()."', trabaja='".$this->beanGrupoFamiliar->get_trabaja()."', oficio_id=".$this->beanGrupoFamiliar->get_oficio_id().", direcc_trabajo='".$this->beanGrupoFamiliar->get_direcc_trabajo()."', ingreso=".$this->beanGrupoFamiliar->get_ingreso().", sufre_ceguera='".$this->beanGrupoFamiliar->get_sufre_ceguera()."',
                    usuario_id=".$this->beanGrupoFamiliar->get_usuario_id()." WHERE familiar_id=".$this->beanGrupoFamiliar->get_familiar_id().";";



        $this->result=$this->conn->execute($sql);

   /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
        if($this->result){
          /** Identificador de la pers_disc_visual para poder mostrar una tabla dinamica con los familiares */
              $datoJson['pers_disc_visual_id']=$this->beanGrupoFamiliar->get_pers_disc_visual_id();
              $datoJson['mensaje']='Registro actualizado exitosamente';
              $datoJson['error']='0';

        }else{
            /** Identificador de la pers_disc_visual para poder mostrar una tabla dinamica con los familiares */
              $datoJson['pers_disc_visual_id']=$this->beanGrupoFamiliar->get_pers_disc_visual_id();
              $datoJson['mensaje']='Error cuando se intento actualizar el registro';
              $datoJson['error']='1';
        }

        echo json_encode($datoJson);
    }

    public function executarDeleted($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }

}
/** Validar los action del proceso que se va instanciar */
 $evento  = $_REQUEST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_REQUEST;
/** Instancio la class que vamos a necesitar*/
$grupoFamiliar = new grupoFamiliarAction();

if($evento=='new'){
    return $grupoFamiliar->executarNew($request);
 }elseif($evento=='edit'){
    return $grupoFamiliar->executarEdit($request);
 }elseif($evento=='index'){
    return $grupoFamiliar->executarIndex($request);
 }elseif($evento=='delete'){
    return $grupoFamiliar->executarDeleted($request);
 }elseif($evento=='show'){
    return $grupoFamiliar->executarShow($request);
 }

?>

