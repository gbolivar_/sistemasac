<?php $url=$_SERVER['HTTP_REFERER'].'/../../'; ?>
<script type="text/javascript" src="<?php echo $url?>web/js/comunes.js"></script>
<script type="text/javascript">
$(document).ready(function(){
        $("#agregar_familiar").click(function(){
                 $("#tabla_familiar").show('slow');
                 $("#agregar_familiar").attr('disabled','disabled');
           });
           
       $('#estudia').change(function(){
            if($("#estudia option:selected").val() == 'true'){
                $("#esp_estudia").removeAttr('disabled');
            }else{
                $("#esp_estudia").val('');
                $("#esp_estudia").attr('disabled', 'disabled'); 
               
            }
        });
        
        $('#trabaja').change(function(){
            if($("#trabaja option:selected").val() == 'true'){
                $("#direcc_trabajo").removeAttr('disabled');
                $("#ingreso").removeAttr('disabled');

            }else{
                $("#direcc_trabajo").val('');
                $("#direcc_trabajo").attr('disabled', 'disabled'); 
                $("#ingreso").val('');
                $("#ingreso").attr('disabled', 'disabled');
            }
        });
});
        
$("form.#editGrupoFamiliar").submit(function(e){
    $.ajax({
        type: "POST",
        url:'<?php echo $url?>apps/sac/modules/grupo_familiar/actions/actions.class.php' ,
        data: $(this).serialize(),
        beforeSend: function(Obj){
        },
        error:function(Obj,err,obj){
            alert('Error de Conexi?n');
        },
        dataType: "json",
        success: function(data){
            /** Funcion encargada de mostrar el mensaje en la vista */
            mostrarMensaje(data.error,data.mensaje);
                /** Extrear los familiares del la persona buscada */
                 $.getJSON("<?php echo $url?>apps/sac/modules/grupo_familiar/actions/actions.class.php", { action: "index", pers_disc_visual_id: data.pers_disc_visual_id }, function(dataJson){ 
                /** Procedo a armar la tabla con los registros de los familiares */
                var tablaDinamic = '';
                var dataJson = dataJson;
                tablaDinamic += '<br/><table align="center" border="0" width="80%"><tr align="center" class="colorN"><td>Cod.</td><td>Parentesco</td><td>Nombre</td><td>Apellido</td><td>Fecha Nac.</td><td>Tel&eacute;fono</td><td>Acci&oacute;n</td></tr>';
                    $.each(dataJson.familiares, function(index, value) {
                         tablaDinamic += '<tr id="del_'+ index +'"><td align="center">' + value.familiar_id + '</td>';
                         tablaDinamic += '<td >' + value.parentesco + '</td>';
                         tablaDinamic += '<td >' + value.nombre + '</td>';
                         tablaDinamic += '<td >' + value.apellido + '</td>';
                         tablaDinamic += '<td align="center">' + value.fech_nacimiento + '</td>';
                         tablaDinamic += '<td>' + value.telefono + '</td>';   
                         tablaDinamic += '<td><div class="ui-state-default ui-corner-all"><a href="#" title="Eliminar Registro"  onclick="confElim(' + value.familiar_id + ',\'tbl_grupo_familiar\', \'familiar_id\',\'del_'+ index +'\');"><span class="ui-icon ui-icon-circle-close"></span></a>';   
                         tablaDinamic += ' <a href="#" title="Actualizar Registro"  onclick="editarRegistro(' + value.familiar_id + ');"><span class=" ui-icon ui-icon-pencil" style="float:right; margin-top: -17px; "></span></a></div></td></tr>';   
                    });
                tablaDinamic += '</table>';
                    $("#datosGrupoF").html(tablaDinamic);
                    $("#datosGrupoF").show(900);
                });
                 $("#tabla_familiar").hide('slow');
                 $("form").each(function(){
                            this.reset();
                 });
        } 
    });
 return false;
});

        $("form.#addGrupoFamiliar").submit(function(e){
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/grupo_familiar/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n');
                },
                dataType: "json",
                success: function(data){
                    /** Funcion encargada de mostrar el mensaje en la vista */
                    mostrarMensaje(data.error,data.mensaje);
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario */
                    if(data.error==0){

                        $("#tabla_familiar").hide('slow');
                        //este es para activar el boton de agregar familiar
                        $("#agregar_familiar").removeAttr('disabled');
                        //es para limpiar todos los campos del formulario despues q' ha sido procesado
                        
                        $.getJSON("<?php echo $url?>apps/sac/modules/grupo_familiar/actions/actions.class.php", { action: "index", pers_disc_visual_id: data.pers_disc_visual_id }, function(dataJson){ 
                                /** Procedo a armar la tabla con los registros de los familiares */
                                var tablaDinamic = '';
                                var dataJson = dataJson;
                                tablaDinamic += '<br/><table align="center" border="0" width="80%"><tr align="center" class="colorN"><td>Cod.</td><td>Parentesco</td><td>Nombre</td><td>Apellido</td><td>Fecha Nac.</td><td>Tel&eacute;fono</td></tr>';
                                    $.each(dataJson.familiares, function(index, value) {
                                         tablaDinamic += '<tr ><td align="center">' + value.familiar_id + '</td>';
                                         tablaDinamic += '<td >' + value.parentesco + '</td>';
                                         tablaDinamic += '<td >' + value.nombre + '</td>';
                                         tablaDinamic += '<td >' + value.apellido + '</td>';
                                         tablaDinamic += '<td align="center">' + value.fech_nacimiento + '</td>';
                                         tablaDinamic += '<td>' + value.telefono + '</td></tr>';                                        
                                    });
                                tablaDinamic += '</table>';
                                $("#datosGrupoF").html(tablaDinamic);
                                $("#datosGrupoF").show(900);
                                 });
                        $("form").each(function(){
                            this.reset();
                        });
                    }
                }
            });
            return false;
        });

function numeric(e) {
    tecla = (document.all)?e.keyCode:e.which;
    if (tecla==8) return true;
    patron = /[0-9 ]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}
function editarRegistro(valor) {
         $("#tabla_familiar").slideDown(900);
         $("#agregar_familiar").attr('disabled','disabled');
         $.getJSON("<?php echo $url?>apps/sac/modules/grupo_familiar/actions/actions.class.php", { action: "index", familiar_id: valor}, function(dataJson){ 
             llenarCamposEditarGrupoF(dataJson.familiares[0]);
         });

}
function llenarCamposEditarGrupoF(data){
    /** Recorrer el arreglo json con el fin de saber el nombre y el valor para asignar dinamicamente a cada campo */
    $.each(data, function(key, value) {
        //alert(key + '->' + value);
        setTimeout(function() {
                 if(value=='t'){ value= 'true'; }else if(value=='f'){ value='false';}
                 var campo = document.getElementById(key);
                 $(campo).val(value);
          }, 500 );
    });
}

</script>