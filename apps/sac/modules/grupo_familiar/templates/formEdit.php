<!-- Incluir los js para este modulo de Grupo Familiar -->
<?php include_once '_script.php';?>
<?php include_once '_script_selectores.php';?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
            <fieldset class="subTitulos"> <legend><b>INFORMACI&Oacute;N PERSONAL</b></legend>
	        <table border="0"  align="center" class="formulario">
              <tr>
                <td align="right" class="colorN" >(*) Apellidos:</td>
                <td><input type="text" name="apellido_persona" id="apellido_persona" maxlength="30" placeholder="Apellidos" required="" disabled/></td>
                <td align="right" class="colorN" >(*) Nombres:</td>
                <td><input type="text" name="nombre_persona" id="nombre_persona" maxlength="30" placeholder="Nombres" required="" disabled/></td>
              </tr>
              <tr>
                <td align="right" class="colorN">(*) Sexo:</td>
                <td><select name="sexo_persona" id="sexo_persona" required="" disabled>
                        <option value="F">F</option>
                        <option value="M">M</option>
                    </select>
                </td>
                <td align="right" class="colorN">(*) Cedula</td>
                <td><select name="nacionalidad_persona" id="nacionalidad_persona" disabled>
                    <option value="V">V</option>
                    <option value="E">E</option>
                  </select>
                    <input name="cedula_persona" type="text" id="cedula_persona" size="14" maxlength="9" required="" disabled /></td>
              </tr>

              <tr>
              	<!--td align="center" colspan="5">
              		<input type="button" id="agregar_familiar" value="Agregar Familiar"/>
              	</td-->
              </tr>

			</table>
      </fieldset>

<div id="menInfo" style="display:none"> </div>
<div id="datosGrupoF" style="display:none"> </div>

  <table width="830px">
    <tr>
	<td colspan="6">
		<div id="tabla_familiar" style="display:none; width: 100%; ">
			<form  name="editGrupoFamiliar" id="editGrupoFamiliar" method="post" action="#">
			<input type="hidden"  name="action" id="action" value="edit" />
                        <input name="pers_disc_visual_id" type="hidden" id="pers_disc_visual_id" />
                        <input name="familiar_id" type="hidden" id="familiar_id" />
             <fieldset class="subTitulos"> <legend><b>GRUPO FAMILIAR</b></legend>
		<table border="0"  align="center" class="formulario">
			<tr>
			  <td align="right" class="colorN">(*) Nombre:</td>
                          <td><input type="text" name="nombre" id="nombre" size="14" required="" onkeypress="return soloText(event)" onkeyup="return mayuscula(this)"/></td>
			  <td align="right" class="colorN">(*) Apellido:</td>
			  <td><input type="text" name="apellido" id="apellido" size="14" required="" onkeypress="return soloText(event)" onkeyup="return mayuscula(this)"/></td>
		  </tr>
			<tr>
			 	<td width="145" align="right" class="colorN">Fecha Nacimiento:</td>
			 	<td width="92">
                                    <input name="fech_nacimiento" id="fech_nacimiento" type="text" class="datepicker" placeholder="dd-mm-yyyy" size="12" required=""/></td>

			 	<td width="80" align="right" class="colorN">(*) Sexo:</td>
	 	    <td width="227">
                                <select name="sexo" id="sexo" required="">
                                  <option value="M">M</option>
                                  <option value="F">F</option>
                                </select>
                    </td>
		  </tr>


			 <tr>
			   <td align="right" class="colorN"> (*) Nacionalidad</td>
			 	<td>
                                    <select name="nacionalidad" id="nacionalidad" required="">
                                          <option value="V">V</option>
                                          <option value="E">E</option>
                                   </select>
                                </td>

			 	<td align="right" class="colorN">(*) Parentezco</td>
			 	<td>
                                    <select name="parentesco_id" id="parentesco_id" required="">
                                      <option value="" selected="selected">Seleccion</option>
                                </select></td>
			 </tr>

			 <tr>
			   <td align="right" class="colorN">Direcci&oacute;n</td>
			 	<td><textarea name="direccion" id="direccion" cols="25" rows="1"></textarea></td>

			 	<td align="right" class="colorN">Telefono:</td>
		    <td><input type="text" name="telefono" id="telefono" size="14" maxlength="11" onkeypress="return numeric(event)"/></td>
			</tr>

			<tr>
			<td align="right" class="colorN">(*) Estudia</td>
				<td>
			 		<select name="estudia" id="estudia" required="">
                                                <option value="">Seleccion</option>
						<option value="true">SI</option>
						<option value="false">NO</option>
					</select>
				</td>

			 	<td align="right" class="colorN">Especifique</td>
			 	<td>
			 		<input type="text" name="esp_estudia" id="esp_estudia" size="14">			 	</td>
			</tr>

			 <tr>
			 	<td align="right" class="colorN">(*) Trabaja</td>
			 	<td>
			 		<select name="trabaja" id="trabaja" required="">
						<option value="">Seleccion</option>
						<option value="true">SI</option>
						<option value="false">NO</option>
					</select>
				</td>

			 	<td align="right" class="colorN">Direcci&oacute;n</td>
				<td>
					<textarea name="direcc_trabajo" id="direcc_trabajo" cols="30" rows="1"></textarea>
				</td>
			</tr>

			<tr>
			  <td align="right" class="colorN">Ingresos</td>
			 	<td>
			 		<input type="text" name="ingreso" id="ingreso"  size="14"  maxlength="5" onkeypress="return numeric(event)">
			 	</td>

			 	<td align="right" class="colorN">Ocupaci&oacute;n</td>
			 	<td>
                                    <select name="oficio_id" id="oficio_id" required="">
                                        <option value="0" selected="selected">Seleccion</option>
                                    </select> 
                                </td>
			</tr>

			<tr>
			<td align="right" class="colorN">(*) Sufre alg&uacute;n tipo ceguera</td>
			 	<td>
                                    <select name="sufre_ceguera" id="sufre_ceguera" required="">
                                            <option value="">Seleccion</option>
                                            <option value="true">SI</option>
                                            <option value="false">NO</option>
                                    </select> 
                                </td>
			 </tr>
		</table>


             </fieldset>
             <fieldset class="subTitulos" id="buttEnviar">
            	 <input type="reset" value="Limpiar Datos" name="Limpiar">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <input type="submit" value="Actualizar Datos" name="Actualizar Datos" >
             </fieldset>

	</div>
    </form>

		</div>
	</td>
</tr>
  </table>
</div>

