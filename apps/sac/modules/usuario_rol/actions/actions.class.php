<?php
  /**
    * @propiedad: PROPIETARIO DEL CODIGO
    * @Autor: Gregorio Bolivar
    * @email: elalconxvii@gmail.com
    * @Fecha de Create: 21/02/2012
    * @Fecha de Update: 26/02/2012
    * @Auditado por: Gregorio J Bolívar B
    * @Descripción: Actions encargado de procesar los methods del usuario
    * @package: actions.class
    * @version: 1.0
    */
/** Incluir los Bean necesario para procesar usuario  */
include_once '../../../../../lib/complementos/funcionesExtras.php';
include_once '../../../../../lib/base/class.BeanUsuarioRol.php';

// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

class usuarioRolAction extends BeanUsuarioRol
{
    public function executarSetBeanUsuarioRol($request){
        /** Cargar Objetos de usuario Rol*/

        $this->beanUsuarioRol->set_id(@$request['id']);
        $this->beanUsuarioRol->set_rol_id(@$request['rol_id']);
        $this->beanUsuarioRol->set_sub_rol_id(@$request['sub_rol_id']);
        $this->beanUsuarioRol->set_desasociar(@$request['desasociar']);
              
        /** Cargar Objetos generales para cada campo */
        $this->beanUsuarioRol->set_usuario_id(@$request['usuario_id']);
        $this->beanUsuarioRol->set_estatus();
        $this->beanUsuarioRol->set_created_at();
        $this->beanUsuarioRol->set_updated_at();


    }

    public function executarShow($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }
    public function executarIndex($request){
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }

    public function executarProcesarPermisos($request){

        $datoJson='';
        $this->beanUsuarioRol = new BeanUsuarioRol();
      
        $this->conn = new conexionDataBase(0);
        

        /** Method que se envcarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanUsuarioRol($request);
        
        $this->rol=        $this->beanUsuarioRol->get_rol_id();
        $this->sub_rol=    $this->beanUsuarioRol->get_sub_rol_id();
        $this->usuario_id= $this->beanUsuarioRol->get_usuario_id();
        $this->desasociar= $this->beanUsuarioRol->get_desasociar();
         foreach ($this->sub_rol as $this->item=>  $this->valor):
             $this->conn->conexion(0);
             if(@$this->desasociar[$this->item]==='on'){
                 $sql="INSERT INTO audiperm.tbl_usuario_rol(rol_id, sub_rol_id, usuario_id, estatus, created_at) VALUES (".$this->rol[$this->item].", ".$this->sub_rol[$this->item].",". $this->usuario_id[$this->item].", '".$this->beanUsuarioRol->get_estatus()."','".$this->beanUsuarioRol->get_created_at()."');";
             }else{
                 $sql="DELETE FROM audiperm.tbl_usuario_rol WHERE rol_id=".$this->rol[$this->item]." AND  sub_rol_id=".$this->sub_rol[$this->item]." AND usuario_id=". $this->usuario_id[$this->item];
             }
             $this->result=$this->conn->execute($sql);
             $this->conn->close();
         endforeach;

       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                  $datoJson['mensaje']='Registro efectuado exitosamente';
                  $datoJson['error']='0';
 
            }else{
                  $datoJson['mensaje']='Error al efectuar el registro';
                  $datoJson['error']='1';
            }
        echo json_encode($datoJson);
    }
    
    public function executarEdit($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }

    public function executarDeleted($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }

}
/** Method para validar inyeccion sql por el GET */
$valGet = new funcionesExtras();
$valGet -> validarMethodGet();
/** Validar los action del proceso que se va instanciar */
 $evento  = $_POST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_POST;
/** Instancio la class que vamos a necesitar*/
$proceso = new usuarioRolAction();

if($evento=='procesar_permisos'){
    return $proceso->executarProcesarPermisos($request);
 }elseif($evento=='edit'){
    return $proceso->executarEdit($request);
 }elseif($evento=='index'){
    return $proceso->executarIndex($request);
 }elseif($evento=='delete'){
    return $proceso->executarDeleted($request);
 }elseif($evento=='show'){
    return $proceso->executarShow($request);
 }

?>

