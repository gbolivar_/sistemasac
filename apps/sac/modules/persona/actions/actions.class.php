<?php
  /**
    * @propiedad: PROPIETARIO DEL CODIGO
    * @Autor: Gregorio Bolivar
    * @email: elalconxvii@gmail.com
    * @Fecha de Create: 21/02/2012
    * @Fecha de Update: 26/02/2012
    * @Auditado por: Gregorio J Bolívar B
    * @Descripción: Generado por el generador de set y get del autor
    * @package: actions.class
    * @version: 1.0
    */
/** Incluir los Bean necesario para procesar Personas  */
include_once '../../../../../lib/base/class.BeanPersona_old.php';

// Incluir archivo de configuracion
include_once '../../../../../config/Configuration.class.php';

// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

class personaAction extends BeanPersona
{
    public function executarSetBeanPersona($request, $pers_disc_visual_id){
        /** Cargar Objetos de Personas OJO CREAR CLASS PARA HACER ESTO*/
        $this->beanPersona->set_pers_disc_visual_id($pers_disc_visual_id);
        $this->beanPersona->set_apellido($request['apellido']);
        $this->beanPersona->set_nombre($request['nombre']);
        $this->beanPersona->set_nacionalidad($request['nacionalidad']);
        $this->beanPersona->set_cedula($request['cedula']);
        $this->beanPersona->set_sexo($request['sexo']);
        $this->beanPersona->set_co_estado($request['co_estado']);
        $this->beanPersona->set_co_municipio($request['co_municipio']);
        $this->beanPersona->set_co_parroquia($request['co_parroquia']);
        $this->beanPersona->set_lugar_nacimiento($request['lugar_nacimiento']);
        $this->beanPersona->set_fech_nacimiento($request['fech_nacimiento']);
        $this->beanPersona->set_telf_habitacion($request['telf_habitacion']);
        $this->beanPersona->set_telf_celular($request['telf_celular']);
        $this->beanPersona->set_correo($request['correo']);
        $this->beanPersona->set_direccion($request['direccion']);
        $this->beanPersona->set_tenencia_vivienda_id($request['tenencia_vivienda_id']);
        $this->beanPersona->set_tipo_vivienda_id($request['tipo_vivienda_id']);
        $this->beanPersona->set_nom_ape_pers_contacto($request['nom_ape_pers_contacto']);
        $this->beanPersona->set_telef_pers_contacto($request['telef_pers_contacto']);
        $this->beanPersona->set_direccion_pers_contacto($request['direccion_pers_contacto']);


        /** Cargar Objetos de Info Laboral */
         $this->beanPersona->set_trabaja($request['trabaja']);
         $this->beanPersona->set_empresa_trabaja(@$request['empresa_trabaja']);
         $this->beanPersona->set_direccion_trabajo(@$request['direccion_trabajo']);
         $this->beanPersona->set_telef_trabajo(@$request['telef_trabajo']);
         $this->beanPersona->set_fecha_ingreso(@$request['fecha_ingreso']);
         $this->beanPersona->set_ingresos(@$request['ingresos']);
         $this->beanPersona->set_oficio_id(@$request['oficio_id']);
         $this->beanPersona->set_profesion_id(@$request['profesion_id']);


         /** Cargar Objetos de Info Estudios */
        $this->beanPersona->set_estudia(@$request['estudia']);
        $this->beanPersona->set_grado_inst_id(@$request['grado_inst_id']);
        $this->beanPersona->set_lugar_estudia(@$request['lugar_estudia']);
        $this->beanPersona->set_estudio_realiza(@$request['estudio_realiza']);

        /** Cargar Objetos generales para cada campo */
        $this->beanPersona->set_usuario_id();//$request['usuario_id']);
        $this->beanPersona->set_estatus();
        $this->beanPersona->set_created_at();
        $this->beanPersona->set_updated_at();

    }

    public function executarShow($request){
        $datoJson='';
        $this->beanPersona = new BeanPersona();
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        
        /** Captura los valores que envie desde el formulario de busqueda del modulo de persona */
        $this->beanPersona->set_cedula($request['cedula']);
        /** Accion extra que se envia con el jquery cuando es busqueda de diagnostico medico */
        $this->act=@$request['act'];
        if(@$this->act==''){
                /** Consultar el identintificador siguiente para cargar al usuario */
                $sql ="SELECT * FROM tbl_pers_disc_visual a
                LEFT JOIN tbl_laboral b ON a.id=b.pers_disc_visual_id 
                LEFT JOIN tbl_estudio_persona c ON a.id=c.pers_disc_visual_id
                WHERE id=".$this->beanPersona->get_cedula()." OR cedula = ".$this->beanPersona->get_cedula().";";
        }else{
                $sql ="SELECT * FROM tbl_pers_disc_visual a
                LEFT JOIN tbl_diag_medico b ON a.id=b.pers_disc_visual_id 
                WHERE id=".$this->beanPersona->get_cedula()." OR cedula = ".$this->beanPersona->get_cedula().";";
        }
                 /** Execute el sql */

        $this->conn->execute($sql);
         /** Defuelve un array con todos los valores */
        $this->persona=$this->conn->all_row();

        /** Armo el json que sera enviado la vista */
        if($this->persona[0]!=''){
            foreach ($this->persona[0] as $p=>$pd){
                $datoJson[$p]=$pd;
            }
        }
        /** Verifico si trae dato de base de datos para mandar el mensaje a la vista 1 = error, 0= select */
        if(@empty ($datoJson)){
            $datoJson['error']='1';
            $datoJson['mensaje']='C&eacute;dula o N&uacute;mero de carnet no se encuentra registrado';//.print_r($this->conn);
        }else{
            $datoJson['error']='0';
        }
        $this->conn->close();
        echo json_encode($datoJson);
    }
    public function executarValidarExistencia($request){
        $datoJson='';
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $cedula=$request['cedula'];
        $sql="SELECT * FROM tbl_pers_disc_visual WHERE cedula=$cedula;";
        /** Execute el sql */
        $this->conn->execute($sql);
         /** Defuelve un array con todos los valores */
        $this->persona=$this->conn->all_row();
        
        if($this->persona>0){
            $datoJson['error']='1';
            $datoJson['mensaje']='C&eacute;dula de persona con discapacidad visual ya se encuentra registrad@';//.print_r($this->conn);
        }else{
            $datoJson['error']='0';
        }
        //$this->conn->close();
        
        echo json_encode($datoJson);
        exit();
    }
    
    public function executarIndex($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO AL INDEX';
        echo json_encode($datoJson);
    }
    
    public function executeValPolEdad($fecha){
        /** Method de calcular fecha */
        $this->fech= new funcionesExtras();
        $edad=$this->fech->calculaEdad($fecha);
        if($edad<=15){
            $datoJson['mensaje']='Error, la persona que intenta registrar debe ser mayor a 15 a&ntilde;os';
            $datoJson['error']='1';        
            echo json_encode($datoJson);
            exit();
        }
    }

    public function executarNew($request){

        $datoJson='';
        $this->beanPersona = new BeanPersona();
      
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        
        self::executeValPolEdad($request['fech_nacimiento']);
        //self::executarValidarExistencia($request);
        
        /** Consultar el identintificador siguiente para cargar al usuario */
        $sql = "select nextval('tbl_pers_disc_visual_pers_disc_visual_id_seq');";
        $this->conn->execute($sql);
        $this->resultPers = $this->conn->all_row();
        $this->resultPers[0]['nextval'];

        /** Method que se envcarga de llenar los set de los campos recibe el request y el identificador del usuario */
        personaAction::executarSetBeanPersona($request, $this->resultPers[0]['nextval']);

        
         $sql1="INSERT INTO tbl_pers_disc_visual(id,nombre, apellido, nacionalidad, cedula, sexo, co_estado, co_municipio, co_parroquia, lugar_nacimiento, fech_nacimiento, telf_habitacion, telf_celular, correo, direccion,  tenencia_vivienda_id, tipo_vivienda_id, nom_ape_pers_contacto, telef_pers_contacto, direccion_pers_contacto, usuario_id, estatus, created_at) VALUES(".$this->beanPersona->get_pers_disc_visual_id().",'".$this->beanPersona->get_nombre()."', '".$this->beanPersona->get_apellido()."', '".$this->beanPersona->get_nacionalidad()."', ".$this->beanPersona->get_cedula().", '".$this->beanPersona->get_sexo()."', ".$this->beanPersona->get_co_estado().", ".$this->beanPersona->get_co_municipio().", ".$this->beanPersona->get_co_parroquia().", '".$this->beanPersona->get_lugar_nacimiento()."', '".$this->beanPersona->get_fech_nacimiento()."', ".$this->beanPersona->get_telf_habitacion().", ".$this->beanPersona->get_telf_celular().", '".$this->beanPersona->get_correo()."', '".$this->beanPersona->get_direccion()."', ".$this->beanPersona->get_tenencia_vivienda_id().", ".$this->beanPersona->get_tipo_vivienda_id().", '".$this->beanPersona->get_nom_ape_pers_contacto()."', ".$this->beanPersona->get_telef_pers_contacto().", '".$this->beanPersona->get_direccion_pers_contacto()."', ".$this->beanPersona->get_usuario_id().", ".$this->beanPersona->get_estatus().", '".$this->beanPersona->get_created_at()."');";
       //echo $this->sql1="INSERT INTO tbl_pers_disc_visual VALUES(".$this->beanPersona->get_pers_disc_visual_id().", '".$this->beanPersona->get_nombre()."', '".$this->beanPersona->get_apellido()."', '".$this->beanPersona->get_nacionalidad()."', ".$this->beanPersona->get_cedula().", '".$this->beanPersona->get_sexo()."', ".$this->beanPersona->get_co_estado().", ".$this->beanPersona->get_co_municipio().", ".$this->beanPersona->get_co_parroquia().", '".$this->beanPersona->get_lugar_nacimiento()."', '".$this->beanPersona->get_fech_nacimiento()."', ".$this->beanPersona->get_telf_habitacion().", ".$this->beanPersona->get_telf_celular().", '".$this->beanPersona->get_correo()."', '".$this->beanPersona->get_direccion()."', ".$this->beanPersona->get_cod_postal().", ".$this->beanPersona->get_tenencia_vivienda_id().", ".$this->beanPersona->get_tipo_vivienda_id().", '".$this->beanPersona->get_nom_ape_pers_contacto()."', ".$this->beanPersona->get_telef_pers_contacto().", '".$this->beanPersona->get_direccion_pers_contacto()."', ".$this->beanPersona->get_usuario_id().", ".$this->beanPersona->get_estatus().",'".$this->beanPersona->get_created_at()."', '".$this->beanPersona->get_updated_at()."');";
       $this->result=$this->conn->execute($sql1);
       
       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                /** Registro de tbl_laboral */
                  $sql2="INSERT INTO tbl_laboral(pers_disc_visual_id, trabaja, empresa_trabaja, direccion_trabajo, telef_trabajo,  ingresos, oficio_id, profesion_id, usuario_id, estatus, created_at) VALUES (".$this->beanPersona->get_pers_disc_visual_id().", '".$this->beanPersona->get_trabaja()."', '".$this->beanPersona->get_empresa_trabaja()."', '".$this->beanPersona->get_direccion_trabajo()."', '".$this->beanPersona->get_telef_trabajo()."',  ".$this->beanPersona->get_ingresos().", ".$this->beanPersona->get_oficio_id().", ".$this->beanPersona->get_profesion_id().", ".$this->beanPersona->get_usuario_id().", '".$this->beanPersona->get_estatus()."', '".$this->beanPersona->get_created_at()."');";
                  $result1 = $this->conn->execute($sql2);

                /** Registro de tbl_estudio_persona */
                  $sql4="INSERT INTO tbl_estudio_persona(pers_disc_visual_id, estudia, grado_inst_id, lugar_estudia, estudio_realiza, usuario_id, estatus, created_at ) VALUES (".$this->beanPersona->get_pers_disc_visual_id().", '".$this->beanPersona->get_estudia()."', ".$this->beanPersona->get_grado_inst_id().", '".$this->beanPersona->get_lugar_estudia()."', '".$this->beanPersona->get_estudio_realiza()."', ".$this->beanPersona->get_usuario_id().", '".$this->beanPersona->get_estatus()."', '".$this->beanPersona->get_created_at()."');";
                  $result2 = $this->conn->execute($sql4);
                  $datoJson['mensaje']='Registro efectuado exitosamente';
                  $datoJson['error']='0';
 
            }else{
                  $datoJson['mensaje']='Error al efectuar el registro';
                  $datoJson['error']='1';
            }       


        $this->conn->close();
        echo json_encode($datoJson);
    }
    public function executarEdit($request){
        $datoJson='';
        $this->beanPersona = new BeanPersona();

        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        
        self::executeValPolEdad($request['fech_nacimiento']);

        /** Method que se envcarga de llenar los set de los campos recibe el request y el identificador del usuario */
        personaAction::executarSetBeanPersona($request, $request['pers_disc_visual_id']);

       $sql1="UPDATE tbl_pers_disc_visual SET nombre='".$this->beanPersona->get_nombre()."', apellido='".$this->beanPersona->get_apellido()."', nacionalidad='".$this->beanPersona->get_nacionalidad()."', cedula=".$this->beanPersona->get_cedula().", sexo='".$this->beanPersona->get_sexo()."', co_estado=".$this->beanPersona->get_co_estado().", co_municipio=".$this->beanPersona->get_co_municipio().", co_parroquia=".$this->beanPersona->get_co_parroquia().", lugar_nacimiento='".$this->beanPersona->get_lugar_nacimiento()."', fech_nacimiento='".$this->beanPersona->get_fech_nacimiento()."', telf_habitacion=".$this->beanPersona->get_telf_habitacion().", telf_celular=".$this->beanPersona->get_telf_celular().", correo='".$this->beanPersona->get_correo()."', direccion='".$this->beanPersona->get_direccion()."', tenencia_vivienda_id=".$this->beanPersona->get_tenencia_vivienda_id().", tipo_vivienda_id=".$this->beanPersona->get_tipo_vivienda_id().", nom_ape_pers_contacto='".$this->beanPersona->get_nom_ape_pers_contacto()."', telef_pers_contacto=".$this->beanPersona->get_telef_pers_contacto().", direccion_pers_contacto='".$this->beanPersona->get_direccion_pers_contacto()."', usuario_id=".$this->beanPersona->get_usuario_id()." WHERE id=".$this->beanPersona->get_pers_disc_visual_id().";";
       $this->result=$this->conn->execute($sql1);

       /** Validar que el UPDATE de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                /** UPDATE de tbl_laboral */
                if($this->beanPersona->get_fecha_ingreso()==''){
                       $sql2="UPDATE tbl_laboral SET trabaja='".$this->beanPersona->get_trabaja()."', empresa_trabaja='".$this->beanPersona->get_empresa_trabaja()."', direccion_trabajo='".$this->beanPersona->get_direccion_trabajo()."', telef_trabajo='".$this->beanPersona->get_telef_trabajo()."',  ingresos=".$this->beanPersona->get_ingresos().", oficio_id=".$this->beanPersona->get_oficio_id().", profesion_id=".$this->beanPersona->get_profesion_id().", usuario_id=".$this->beanPersona->get_usuario_id()." WHERE pers_disc_visual_id=".$this->beanPersona->get_pers_disc_visual_id().";";
                    }else{
                       $sql2="UPDATE tbl_laboral SET trabaja='".$this->beanPersona->get_trabaja()."', empresa_trabaja='".$this->beanPersona->get_empresa_trabaja()."', direccion_trabajo='".$this->beanPersona->get_direccion_trabajo()."', telef_trabajo='".$this->beanPersona->get_telef_trabajo()."', fecha_ingreso='".$this->beanPersona->get_fecha_ingreso()."', ingresos=".$this->beanPersona->get_ingresos().", oficio_id=".$this->beanPersona->get_oficio_id().", profesion_id=".$this->beanPersona->get_profesion_id().", usuario_id=".$this->beanPersona->get_usuario_id()." WHERE pers_disc_visual_id=".$this->beanPersona->get_pers_disc_visual_id().";";
                 }
                  $result1 = $this->conn->execute($sql2);

                /** UPDATE de tbl_estudio_persona */
                  $sql4="UPDATE tbl_estudio_persona SET  estudia='".$this->beanPersona->get_estudia()."', grado_inst_id=".$this->beanPersona->get_grado_inst_id().", lugar_estudia='".$this->beanPersona->get_lugar_estudia()."', estudio_realiza='".$this->beanPersona->get_estudio_realiza()."', usuario_id=".$this->beanPersona->get_usuario_id()." WHERE pers_disc_visual_id=".$this->beanPersona->get_pers_disc_visual_id().";";
                  $result2 = $this->conn->execute($sql4);
                  $datoJson['mensaje']='Registro actualizado exitosamente';
                  $datoJson['error']='0';

            }else{
                  $datoJson['mensaje']='Error cuando intentaba actualizar los registro';
                  $datoJson['error']='1';
            }
        $this->conn->close();
        echo json_encode($datoJson);
    }

    public function executarDeleted($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }
    
    public function executarAsociarH($request){
        $datoJson='';
        
        $this->beanPersona = new BeanPersona();
        
        $this->beanPersona->set_usuario_id();
        $this->beanPersona->set_updated_at();
        $this->beanPersona->set_horario_id($request['tipo_horario_id']);
        $this->beanPersona->set_cedula($request['cedula_persona']);
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $sql="UPDATE tbl_pers_disc_visual SET horario_id = ".$this->beanPersona->get_horario_id().", updated_at = '".$this->beanPersona->get_updated_at()."', usuario_id = ".$this->beanPersona->get_usuario_id()." where cedula = ".$this->beanPersona->get_cedula()." ; ";
        $this->result = $this->conn->execute($sql);
        
        if($this->result){
            $datoJson['mensaje']='Horario asociado exitosamente';
            $datoJson['error']='0';
        }else{
            $datoJson['mensaje']='Error cuando intentaba asociar el horario';
            $datoJson['error']='1';
        }
        echo json_encode($datoJson);
    }

}
/** Validar los action del proceso que se va instanciar */
 $evento  = $_REQUEST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_REQUEST;
/** Instancio la class que vamos a necesitar*/
$proceso = new personaAction();

if($evento=='new'){
    return $proceso->executarNew($request);
 }elseif($evento=='edit'){
    return $proceso->executarEdit($request);
 }elseif($evento=='index'){
    return $proceso->executarIndex($request);
 }elseif($evento=='delete'){
    return $proceso->executarDeleted($request);
 }elseif($evento=='show'){
    return $proceso->executarShow($request);
 }elseif($evento=='asociarH'){
    return $proceso->executarAsociarH($request);
 }elseif($evento=='validarexistencia'){
    return $proceso->executarValidarExistencia($request);
 }

?>

