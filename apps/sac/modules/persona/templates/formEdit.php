<!-- Incluir los js para este modulo de persona -->
<?php include_once '_script.php';?>
<?php include_once '_script_selectores.php';?>
<?php include_once '_script_editar.php';?>

<form id="personaEdit" method="POST" action="#">
<input type="hidden"  name="action" id="action" value="edit" />
<input type="hidden"  name="pers_disc_visual_id" id="pers_disc_visual_id"  />

<div id="tabs">
    <ul>
	<li><a href="#tabs-1">INFORMACI&Oacute;N PERSONAL</a></li>
	<li><a href="#tabs-2">INFORMACI&Oacute;N LABORAL</a></li>
	<li><a href="#tabs-3">INFORMACI&Oacute;N EDUCATIVA</a></li>
	<li><a href="#tabs-4">OTROS</a></li>
    </ul>
<div id="menInfo" style="display:none"> </div>
    <div id="tabs-1">
            <fieldset class="subTitulos"> <legend><b>INFORMACI&Oacute;N PERSONAL <div id='carnet'></div> </b></legend>
	        <table border="0"  align="center" class="formulario">
              <!--tr>
                <td align="right" class="colorN">N&uacute;mero Impresac:</td>
                <td><input type="text" size="4" name="n_impre" id="n_impre" /></td>
                <td align="right" class="colorN">&Uacute;ltima actualizaci&oacute;n:</td>
                <td><input type="text" size="12" maxlength="12" name="actualizacion"/></td>
              </tr-->
              <tr>
                <td align="right" class="colorN" >(*) Apellidos:</td>
                <td><input type="text" name="apellido" id="apellido" maxlength="30" placeholder="Apellidos" required="" onkeypress="return soloText(event)" onkeyup="return mayuscula(this)"/></td>
                <td align="right" class="colorN" >(*) Nombres:</td>
                <td><input type="text" name="nombre" id="nombre" maxlength="30" placeholder="Nombres" required="" onkeypress="return soloText(event)" onkeyup="return mayuscula(this)"/></td>
              </tr>
              <tr>
                <td align="right" class="colorN">(*) Sexo:</td>
                <td>
                    <select name="sexo" id="sexo" required="">
                        <option value="F">F</option>
                        <option value="M">M</option>
                    </select>
                </td>
                <td align="right" class="colorN">(*) C&eacute;dula</td>
                <td><select name="nacionalidad" id="nacionalidad"  required="" >
                    <option value="V">V</option>
                    <option value="E">E</option>
                  </select>
                    <input name="cedula" type="text" id="cedula" size="14" maxlength="9" required="" readonly /></td>
              </tr>
              <tr>
                <td align="right" class="colorN">Lugar de Nacimiento:</td>
                <td><input type="text" size="20" name="lugar_nacimiento"  id="lugar_nacimiento" required=""/></td>
                <td align="right" class="colorN">(*) Fecha Nacimiento:</td>
                <td><input type="text"  size="12" maxlength="12" name="fech_nacimiento" id="fech_nacimiento" class="datepicker" placeholder="dd-mm-yyyy" required=""/></td>
              </tr>
              <tr>
                <td align="right" class="colorN">(*) Estado:</td>
                <td><select name="co_estado" id="co_estado" required="" onchange="cargar_dependiente('co_estado','co_municipio', 'municipio');">
                    <option value="">SELECCIONE</option>
                  </select>
                </td>
                <td align="right" class="colorN">(*) Municipio:</td>
                 <td><input type="hidden" id="co_municipio2" name="co_municipio2" size="40" maxlength="40"/>
               <select name="co_municipio" id="co_municipio" required="" onchange="cargar_dependiente('co_municipio','co_parroquia','parroquia')">
                    <option value="" >SELECCIONE</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td align="right" class="colorN">(*) Parroquia:</td>
               <td>
                <input type="hidden" id="co_parroquia2" name="co_parroquia2" size="40" maxlength="40"/>
                <select name="co_parroquia" id="co_parroquia" required="">
                    <option value="" >SELECCIONE</option>
                </select>
               </td>

                <td  align="right" class="colorN">(*) Correo Electr&oacute;nico:</td>
                <td colspan="3"><input name="correo" id="correo"  maxlength="150"  placeholder="anything@example.com" type="email"/></td>

              </tr>
              <tr>
                <td align="right" class="colorN">Tel&eacute;fono Celular:</td>
                <td><input type="text" name="telf_celular" id="telf_celular" placeholder="04129000000" maxlength="11" onkeypress="return numeric(event)"/></td>
                <td align="right" class="colorN">Tel&eacute;fono Habitaci&oacute;n:</td>
                <td><input type="text" name="telf_habitacion" id="telf_habitacion" placeholder="02129000000" maxlength="11" onkeypress="return numeric(event)"/></td>

              </tr>


              <tr>
                <td align="right" class="colorN">(*) Tenencia Vivienda: </td>
                <td><span class="colorN">
                  <select name="tenencia_vivienda_id" id="tenencia_vivienda_id" required="">
                    <option value="" >SELECCIONE</option>
                  </select>
                </span></td>
                <td align="right" class="colorN">(*) Tipo Vivienda:</td>
                <td><select name="tipo_vivienda_id" id="tipo_vivienda_id" required="">
                    <option value="">SELECCIONE</option>
                </select></td>
              </tr>
              <tr>
                <td align="right" class="colorN">Direcci&oacute;n:</td>
                <td colspan="3"><textarea name="direccion" id="direccion" cols="50" rows="1" maxlength="300"></textarea></td>
              </tr>
            </table>
      </fieldset>
    </div>
    <div id="tabs-2">
      <fieldset class="subTitulos"><legend><b>INFORMACI&Oacute;N LABORAL</b></legend>
	<table border="0"  align="center" class="formulario">
	<tr>
            <td align="right" class="colorN">(*) Laborando:</td>
            <td>
                    <select name="trabaja" id="trabaja" required="">
                            <option value="SI">Si</option>
                            <option value="NO">No</option>
                    </select>
                </td>
	     <td align="right" class="colorN">Empresa donde trabaja:</td>
             <td><input type="text" name="empresa_trabaja" id="empresa_trabaja" class="actDescLab"/></td>
	 </tr><tr>
	     <td align="right" class="colorN" >Fecha Ingreso</td>
             <td><input type="text" name="fecha_ingreso" id="fecha_ingreso" class="datepicker actDescLab" maxlength="12" /></td>
	     <td align="right" class="colorN">Profesi&oacute;n</td>
	     <td><span class="colorN">
		<select name="profesion_id" id="profesion_id" class="actDescLab">
                    <option value="0">SELECCIONE</option>
                </select>
		</span></td>
	</tr> <tr>
            <td align="right" class="colorN">Oficio</td>
	    <td><span class="colorN">
                <select name="oficio_id" id="oficio_id" class="actDescLab">
                  <option value="0">SELECCIONE</option>
                </select></span>
            </td>
	 </tr><tr>
            <td align="right" class="colorN">Tel&eacute;fono de trabajo</td>
            <td><input type="text" name="telef_trabajo"  id="telef_trabajo" size="" class="actDescLab" maxlength="11" onkeypress="return numeric(event)"></td>
            <td align="right" class="colorN">Ingresos</td>
            <td><input name="ingresos" id="ingresos" type="text" size="12"  maxlength="12" class="actDescLab" onkeypress="return numeric(event)"/> BsF</td>
	  </tr><tr>
            <td align="right" class="colorN">Direcci&oacute;n de Trabajo:</td>
            <td colspan="3"><textarea name="direccion_trabajo" id="direccion_trabajo"  cols="50" rows="2" class="actDescLab" ></textarea></td>
	  </tr>
	</table>
    </fieldset>
	</div>
	<div id="tabs-3">
        <fieldset class="subTitulos"> <legend><b>FORMACI&Oacute;N EDUCATIVA</b></legend>
	<table width="601" border="0"  align="center" class="formulario">
			<tr>
			  <td width="145" align="right" class="colorN">(*) Estudia actualmente:</td>
                          <td width="147">
                            <select name="estudia" id="estudia" required="">
					<option value="SI">Si</option>
					<option value="NO">No</option>
                            </select>
                          </td>
			  <td width="147" align="right" class="colorN">Lugar de estudio:</td>
			  <td width="144"><input type="text" name="lugar_estudia" id="lugar_estudia" size="" class="actDescLab2"/></td>
	    </tr>
			<tr>
			  <td align="right" class="colorN">Estudio que realiza:</td>
			  <td><input type="text" name="estudio_realiza" id="estudio_realiza" size="" class="actDescLab2"/></td>
			  <td align="right" class="colorN">Grado Intrucci&oacute;n</td>
			  <td><span class="colorN">
			    <select name="grado_inst_id" id="grado_inst_id" class="actDescLab2">
                  <option value="0">SELECCIONE</option>
                </select>
			  </span></td>
	    </tr>
		</table>
      </fieldset>
	</div>
	<div id="tabs-4">
             <fieldset class="subTitulos"> <legend><b>OTROS</b></legend>
		     <table width="578" border="0"  align="center" class="formulario">
               <tr>
                 <td width="141" align="right" class="colorN">Persona Contacto:</td>
                 <td width="171"><input name="nom_ape_pers_contacto" type="text" id="nom_ape_pers_contacto"/></td>
                 <td width="65" align="right" class="colorN">Tel&eacute;fono</td>
                 <td width="183"><input name="telef_pers_contacto" type="text" maxlength="11" id="telef_pers_contacto" onkeypress="return numeric(event)"/></td>
               </tr>
               <tr>
                 <td align="right" class="colorN">Direcci&oacute;n:</td>
                 <td colspan="3"><textarea name="direccion_pers_contacto" id="direccion_pers_contacto" cols="50" rows="2"></textarea> </td>
               </tr>
             </table>
      </fieldset>
             <fieldset class="subTitulos" id="buttEnviar">
            	 	<input type="reset" value="Limpiar Datos" name="Limpiar" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" value="Actualizar Datos" name="registro" >
             </fieldset>
	</div>
</div>
</form>