<!-- Script de Personas -->

<?php $url=$_SERVER['HTTP_REFERER'].'/../../'; ?>
<script type="text/javascript" src="<?php echo $url?>web/js/comunes.js"></script>
<script type="text/javascript">
//$('#personaNew').html5form({
//        async : false,
//        messages : 'es', // Opciones 'en', 'es', 'it', 'de', 'fr', 'nl', 'be', 'br'
//        responseDiv : '#menInfo'
//    
//    })
$(document).ready(function(){
    /** Validar la existencia de la persona si ya se encuentra registrada */
    $('#personaNew#cedula').blur(function(){
        var cedula = $('#cedula').val();
       /** Validar que el campo cedula sea mayor o igual a 4 */
       if(cedula.length<=4){
             mostrarMensaje(1,'La c&eacute;dula debe ser mayor o igual a 5 digitos.');
             $('#cedula').val('');
             return false;
        }
        $.post('<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php', {action:'validarexistencia', cedula:cedula }, function(datoJson){
            if(datoJson.error==1){
                mostrarMensaje(datoJson.error,datoJson.mensaje);
                $('#cedula').val('');
            }
        },'json')
    })
    
    $("#trabaja").change(trabajo); // callback
    function trabajo(event){
         if($("#trabaja").val()=='NO'){
             $(".actDescLab").attr("disabled","disabled");
         }else{
            $(".actDescLab").removeAttr("disabled");
         }
         
    }
     $("#estudia").change(function(){
         if($("#estudia").val()=='NO'){
             $(".actDescLab2").attr("disabled","disabled");
         }else{
            $(".actDescLab2").removeAttr("disabled");
         }
         
    });
        /** Funcion encargada de procesar la informacion desde la vista al action del mismo */
        $("form.#personaNew").submit(function(e){
            C = $('#cedula').val();
            N = $('#nacionalidad').val();
            if(C >80000000 && N=='V')
            {
                mostrarMensaje(1,'N&uacute;mero de c&eacute;dula no coincide con la nacionalidad');
                return false;
            }
            if(C < 80000000 && N=='E')
            {
                mostrarMensaje(1,'N&uacute;mero de c&eacute;dula no coincide con la nacionalidad');
                return false;
            }
            
            if($("#apellido").val()=='')
            {
                mostrarMensaje(1,'Correo Electrónico no es valido');
                return false;
            }
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en procesarNew', '_script_list.php','nueva persona','68');
                        alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                    /** Funcion encargada de mostrar el mensaje en la vista */
                    mostrarMensaje(data.error,data.mensaje);
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario */
                    if(data.error==0 && data.mensaje!=''){
                        $("form").each(function(){
                            this.reset();
                        });
                    }
                }
            });
            return false;
        });
     /** Funcion encargada de procesar la busqueda de la persona */
     $("form.#buscarPersona").submit(function(e){
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en buscarPersona', '_script_list.php','Buscar Personas','95');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){   
                        /** Validar si es 0 = Return los datos de la consulta, 1= error no esta registrado  */
                        if(data.error==0){
                            /** Renombrar la action para la accion edit */
                            $('#contenido').load('<?php echo $url?>apps/sac/modules/persona/templates/formEdit.php');
                            /** Funcion encargada de llenar los campos que vienen del action luego que cargue el formulario */
                            llenarCamposEditarPersonas(data);
                            /** cargar el valor del usuario como el carnet*/
                            var car = data.id;
                            setTimeout(function() {
                                $('#carnet').html(' CARNET NRO: ' + car +'');
                            }, 1400);

                        }else{
                            /** Funcion encargada de mostrar el mensaje en la vista */
                            mostrarMensaje(data.error,data.mensaje);
                        }
                    }
            });
        return false;
        });

     $("form.#personaEdit").submit(function(e){
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), personaEdit', '_script_list.php','Editar personas','96');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                    /** Funcion encargada de mostrar el mensaje en la vista */
                    mostrarMensaje(data.error,data.mensaje);
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario */
                    if(data.error==0){
                        $("form").each(function(){
                            this.reset();
                        });
                        setTimeout(function() {
                            $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "buscarPersona"});
                        }, 500);
                    }
                    
                }
            });
            return false;
        });

    /** Agregar grupo familiar */
     $("form.#buscarPersonaAddGrupFamiliar").submit(function(e){
          $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), personaEdit', '_script_list.php','Editar personas','96');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){                   
                    if(data.error==0){
                         $('#contenido').load('<?php echo $url?>apps/sac/modules/grupo_familiar/templates/formNew.php');
                          setTimeout(function() {
                            $("#nombre_persona").val(data.nombre);
                            $("#apellido_persona").val(data.apellido);
                            $("#sexo_persona").val(data.sexo);
                            $("#nacionalidad_persona").val(data.nacionalidad);
                            $("#cedula_persona").val(data.cedula);
                            $("#pers_disc_visual_id").val(data.pers_disc_visual_id);
                            /** Extrear los familiares del la persona buscada */
                            $.getJSON("<?php echo $url?>apps/sac/modules/grupo_familiar/actions/actions.class.php", { action: "index", pers_disc_visual_id: data.pers_disc_visual_id }, function(dataJson){ 
                                /** Procedo a armar la tabla con los registros de los familiares */
                                var tablaDinamic = '';
                                var dataJson = dataJson;
                                tablaDinamic += '<br/><table align="center" border="0" width="80%"><tr align="center" class="colorN"><td>Parentesco</td><td>Nombre</td><td>Apellido</td><td>Fecha Nac.</td><td>Tel&eacute;fono</td></tr>';
                                    $.each(dataJson.familiares, function(index, value) {
                                         tablaDinamic += '<tr >';
                                         tablaDinamic += '<td >' + value.parentesco + '</td>';
                                         tablaDinamic += '<td >' + value.nombre + '</td>';
                                         tablaDinamic += '<td >' + value.apellido + '</td>';
                                         tablaDinamic += '<td align="center">' + value.fech_nacimiento + '</td>';
                                         tablaDinamic += '<td>' + value.telefono + '</td></tr>';                                        
                                    });
                                tablaDinamic += '</table>';
                                $("#datosGrupoF").html(tablaDinamic);
                                $("#datosGrupoF").show(900);
                                 });
                           }, 100 );
                     }else{
                        /** Funcion encargada de mostrar el mensaje en la vista */
                        mostrarMensaje(data.error,data.mensaje);
                     }
                }
            });
          return false;
        });
        
         $("form.#buscarPersonaEditGrupFamiliar").submit(function(e){
          $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), buscarPersonaEditGrupFamiliar', '_script_list.php','buscarPersonaEditGrupFamiliar','209');                     
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                    if(data.error==0){
                         $('#contenido').load('<?php echo $url?>apps/sac/modules/grupo_familiar/templates/formEdit.php');
                          setTimeout(function() {
                            $("#nombre_persona").val(data.nombre);
                            $("#apellido_persona").val(data.apellido);
                            $("#sexo_persona").val(data.sexo);
                            $("#nacionalidad_persona").val(data.nacionalidad);
                            $("#cedula_persona").val(data.cedula);
                            $("#pers_disc_visual_id").val(data.pers_disc_visual_id);
                            /** Extrear los familiares del la persona buscada */
                            $.getJSON("<?php echo $url?>apps/sac/modules/grupo_familiar/actions/actions.class.php", { action: "index", pers_disc_visual_id: data.pers_disc_visual_id }, function(dataJson){ 
                                /** Procedo a armar la tabla con los registros de los familiares */
                                var tablaDinamic = '';
                                var dataJson = dataJson;
                                tablaDinamic += '<br/><table align="center" border="0" width="80%"><tr align="center" class="colorN"><td>Parentesco</td><td>Nombre</td><td>Apellido</td><td>Fecha Nac.</td><td>Tel&eacute;fono</td><td>Acci&oacute;n</td></tr>';
                                    $.each(dataJson.familiares, function(index, value) {
                                         tablaDinamic += '<tr id="del_'+ index +'">';
                                         tablaDinamic += '<td >' + value.parentesco + '</td>';
                                         tablaDinamic += '<td >' + value.nombre + '</td>';
                                         tablaDinamic += '<td >' + value.apellido + '</td>';
                                         tablaDinamic += '<td align="center">' + value.fech_nacimiento + '</td>';
                                         tablaDinamic += '<td>' + value.telefono + '</td>';   
                                         tablaDinamic += '<td><div class="ui-state-default ui-corner-all"><a href="#" title="Eliminar Registro"  onclick="confElim(' + value.familiar_id + ',\'tbl_grupo_familiar\', \'familiar_id\',\'del_'+ index +'\');"><span class="ui-icon ui-icon-circle-close"></span></a>';   
                                         tablaDinamic += ' <a href="#" title="Actualizar Registro"  onclick="editarRegistro(' + value.familiar_id + ');"><span class=" ui-icon ui-icon-pencil" style="float:right; margin-top: -17px; "></span></a></div></td></tr>';   
                                    });
                                tablaDinamic += '</table>';
                                $("#datosGrupoF").html(tablaDinamic);
                                $("#datosGrupoF").show(900);
                                 });
                           }, 100 );
                     }else{
                     /** Funcion encargada de mostrar el mensaje en la vista */
                         mostrarMensaje(data.error,data.mensaje);
                     }
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario
                    if(data.error==0){
                        $("form").each(function(){
                            this.reset();
                        });
                    }*/
                }
            });
  
          return false;
        });
     		/** Agregar Diagnostico */
            $("form.#buscarPersonaAddDiagnostico").submit(function(e){
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php' ,
                data: $(this).serialize()+'&act=valDiag',
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), buscarPersonaAddDiagnostico', '_script_list.php','Agregar diagnostico','268');                     
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                    if(data.diag_medico_id!=null){
                        mostrarMensaje(1,'La persona ya tiene el diagnostico registrado');

                    }else if(data.error==0 && data.diag_medico_id==null){
                         $('#contenido').load('<?php echo $url?>apps/sac/modules/diagnostico/templates/formNew.php');
                         setTimeout(function() {
                            $("#nombre_persona").val(data.nombre);
                            $("#apellido_persona").val(data.apellido);
                            $("#sexo_persona").val(data.sexo);
                            $("#nacionalidad_persona").val(data.nacionalidad);
                            $("#cedula_persona").val(data.cedula);
                            $("#pers_disc_visual_id").val(data.id)
                           }, 100 );
                     }else{
                         /** Funcion encargada de mostrar el mensaje en la vista */
                         mostrarMensaje(data.error,data.mensaje);
                     }
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario
                    if(data.error==0){
                        $("form").each(function(){
                            this.reset();
                        });
                    }*/
                }
            });
            return false;
            });


             $("form.#expedienteDiagnostico").submit(function(e){
                /** Funcion encargada de mostrar el mensaje en la vista */
                    //mostrarMensaje('data.mensaje');
                    $.ajax({
                    type: "GET",
                    url:'<?php echo $url?>apps/sac/modules/diagnostico/actions/actions.class.php' ,
                    data: {action:'show', cedula:$("#cedula").val()},
                    beforeSend: function(Obj){
                    },
                    error:function(Obj,err,obj){
                        exceptionLog('Error(EV01), expedienteDiagnostico', '_script_list.php','expediente','312');                     
                        alert('Error(EV01)');
                    },
                    dataType: "json",
                    success: function(data){
                        /** Funcion encargada de mostrar el mensaje en la vista */
                        if(data.error==0){
                            mostrarBottonReporte(data.mensaje)
                        }else{
                            mostrarMensaje(data.error,data.mensaje);
                        }
                        
                        /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario
                        if(data.error==0){
                            $("form").each(function(){
                                this.reset();
                            });
                        }*/
                    }
                });
                return false;
             });
             
             /* REPORTE DE HORARIO */
             $("form.#expedienteHorario").submit(function(e){
                    $.ajax({
                    type: "GET",
                    url:'<?php echo $url?>apps/sac/modules/horario/actions/actions.class.php' ,
                    data: {action:'show', cedula:$("#cedula").val()},
                    beforeSend: function(Obj){
                    },
                    error:function(Obj,err,obj){
                        exceptionLog('Error(EV01), expedienteHorario', '_script_list.php','expediente horario','344');                     
                        alert('Error(EV01)');
                    },
                    dataType: "json",
                    success: function(data){
                        /** Funcion encargada de mostrar el mensaje en la vista */
                        if(data.error==0){
                            mostrarBottonReporte(data.mensaje)
                        }else{
                            mostrarMensaje(data.error,data.mensaje);
                        }
                    }
                });
                return false;
             });
             
              $('form#buscarPersonaHorario').submit(function(){
                $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), buscarPersonaHorario', '_script_list.php','buscar Persona horario','368');                     
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                    
                    if(data.error==0){
                         $('#contenido').load('<?php echo $url?>apps/sac/modules/horario/templates/asociarHorario.php');
                          setTimeout(function() {
                            $("#nombre_persona").val(data.nombre);
                            $("#apellido_persona").val(data.apellido);
                            $("#sexo_persona").val(data.sexo);
                            $("#nacionalidad_persona").val(data.nacionalidad);
                            $("#cedula_persona").val(data.cedula);
                            $("#pers_disc_visual_id").val(data.pers_disc_visual_id);
       
                               
                           }, 100 );
                     }else{
                         /** Funcion encargada de mostrar el mensaje en la vista */
                            mostrarMensaje(data.error,data.mensaje);
                     }
                }
            });
                return false;
            });
            
$("form.#registrarAyuda").submit(function(e){
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), registrarAyuda', '_script_list.php','registrar ayuda','96');                     
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){   
                        /** Validar si es 0 = Return los datos de la consulta, 1= error no esta registrado  */
                        if(data.error==0){
                            /** Renombrar la action para la accion edit */
                            $('#contenido').load('<?php echo $url?>apps/sac/modules/ayuda/templates/registrarAyuda.php');
                            /** Funcion encargada de llenar los campos que vienen del action luego que cargue el formulario */
                            llenarCamposEditarPersonas(data);
                        }else{
                            /** Funcion encargada de mostrar el mensaje en la vista */
                            mostrarMensaje(data.error,data.mensaje);
                        }
                    }
            });
        return false;
    });
    /** codigo encargado de poder saber todas las ayudas entregadas */
    $("form.#searchAyuda").submit(function(e){
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/ayuda/actions/actions.class.php' ,
                data: {'action': 'show', 'cedula': $('#cedula').val()},
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), searchAyuda', '_script_list.php','Buscar ayuda','431');                     
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(dataJson){   
                        /** Validar si es 0 = Return los datos de la consulta, 1= error no esta registrado  */
                        if(dataJson.error==0){
                            /** Renombrar la action para la accion edit */
                            $('#contenido').load('<?php echo $url?>apps/sac/modules/ayuda/templates/listado.php');
                           /** alert(dataJson.listados);
                             pers_disc_visual_id":"304","medicina":null,"beneficiario":"Prueba Prueba","":"Lentes","":"200","usuario_id":"3","estatus":"t","":"2012-06-03 09:40:26" */
                             /** Listado de tabla dinamica */
                            setTimeout(function() {
                                var tablaDinamic = '';
                                tablaDinamic += '<br/><table align="center" border="0" width="98%"><tr align="center" class="colorN"><td>Nombre.</td><td>Ayuda</td><td>Monto</td><td>Status</td><td>Fecha</td></tr>';
                                    $.each(dataJson.listados, function(index, value) {

                                        tablaDinamic += '<tr id="del_'+ index +'">';
                                        tablaDinamic += '<td align="center">' + value.beneficiario + '</td>';
                                        tablaDinamic += '<td align="center">' + value.tipo_ayuda + '</td>';
                                        tablaDinamic += '<td align="center">' + value.monto+'</td>';
                                        tablaDinamic += '<td align="center">' + value.estatus + '</td>';
                                        tablaDinamic += '<td align="center">' + value.created_at + '</td>';    

                                        /** Solo cuando es administrador 
                                        <?php //if($usuarioId==1){ ?>
                                            tablaDinamic += '<a href="#" title="Bloquear/Desbloquear"  onclick="cambiarEstatus(' + value.id + ',\''+ value.estatus +'\');">' + valBoolean(value.estatus) + '</a>';
                                            tablaDinamic += '<a href="#" title="Gestionar los permisos"  onclick="permisos(' + value.id + ');"><span class="ui-icon ui-icon-key" style="margin-left:45%;"></span></a>';   
                                        <?php //} ?>*/
                                        tablaDinamic += '</div></td></tr>';
                                });
                                tablaDinamic += '</table>';
                               /* alert(tablaDinamic);*/
                                $("#listado").html(tablaDinamic);
                                $("#listado").show(900);
                         }, 1000 );
                        }else{
                            mostrarMensaje(dataJson.error,dataJson.mensaje);
                        }
                    }
            });
        return false;
  });
  /** Fin del ready */
});

function llenarCamposEditarPersonas(data){
    /** Recorrer el arreglo json con el fin de saber el nombre y el valor para asignar dinamicamente a cada campo */
    $.each(data, function(key, value) {
       // alert(key + '->' + value);
        setTimeout(function() {
                     var campo = document.getElementById(key);
                     if(key == 'co_municipio'){
                     	$("#co_municipio2").val(value);
                     }else{
                     	if(key == 'co_parroquia'){
                     		$("#co_parroquia2").val(value);
                     	}else{
                     		$(campo).val(value);
                     	}
                     }
                }, 500 );
    });
}

function cargar_dependiente(carga, acargar, t, v){
    	var select1 = document.getElementById(carga);
    	var select2 = document.getElementById(acargar);
		var value_select = $(select1).val();
		$.ajax({
        type: "POST",
        url:'<?php echo $url?>apps/sac/modules/comunes/actions/actions.class.php' ,
        data: {action:t, valor: value_select},
        beforeSend: function(Obj){
        },
        error:function(Obj,err,obj){
            exceptionLog('Error(EV01), cargar_dependiente', '_script_list.php','cargar_dependiente','507');                     
            alert('Error(EV01)');
        },
        dataType: "json",
        success: function(data){
        	$(select2).html('<option value="">SELECCIONE</option>');
        	for(var i=0; i < data.length; i++){
        		if(v != ''){
	        		if(data[i].id == v){
	        			$(select2).append('<option value="' + data[i].id + '" selected="selected">'+data[i].data+'</option>');
	        		}else{
	        			$(select2).append('<option value="' + data[i].id + '">'+data[i].data+'</option>');
	        		}
        		}else{
        			$(select2).append('<option value="' + data[i].id + '">'+data[i].data+'</option>');
        		}
			}
        }
    });
    }
</script>