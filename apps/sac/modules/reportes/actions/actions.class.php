<?php
   /**
    * @propiedad: PROPIETARIO DEL CODIGO
    * @Autor: Roxenis Sanoja
    * @email: roxenis.sanoja50@gmail.com
    * @Fecha de Create: 28/02/2012
    * @Fecha de Update: 28/02/2012
    * @Auditado por: Gregorio J Bolívar B
    * @Descripción: Generado por el generador de set y get del autor
    * @package: actions.class
    * @version: 1.0
    */
/** Incluir los Bean necesario para procesar Personas  */
include_once '../../../../../lib/complementos/HighChart.class.php';
include_once '../../../../../lib/complementos/Complements.class.php';


// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class reportesAction
{
    public function executarShowCausaCeguera($request){
        $fecha_inicio=$request['fech_inicio'];
        $fecha_fin=$request['fech_fin'];
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        
        $sql="SELECT (((count(b.tipo_ceguera_id)*100)/ (select count(diag_medico_id) from tbl_diag_medico where created_at::date between '$fecha_inicio' AND '$fecha_fin')))AS cantidad, b.tipo_ceguera as nombre from tbl_diag_medico a
              inner join tbl_tipo_cegera b ON a.tipo_ceguera_id=b.tipo_ceguera_id
              where created_at::date between '$fecha_inicio' AND '$fecha_fin'
              group by b.tipo_ceguera";
        $this->conn->execute($sql);
        $matriz = $this->conn->all_row();
        $this->numRow = $this->conn->num_row();

        if($this->numRow > 0){

         $nameColum= 'Cantidades';
         $yElem = 'Cantidades de tipo de ceguera';

        $div = "container";		
	$this->titulo = "<center><b>Porcentaje de tipos de ceguera  <br/> <b>desde ".$fecha_inicio." hasta  ".$fecha_fin." </b></center>";

                $this->grafica = '';
                if(!empty ($matriz)){
                    $titulo = $this->titulo;
                    $xElem  = Complements::getCategories($matriz,null);
                    $leyenda = Complements::getLegend('off');
                    $adicional = Complements::getAdditional();
                    $series = Complements::getSeriesData($matriz,null);
                    $anch = Complements::getWidthGraphic(1);
                    $alto = Complements::getHeightGraphic();
                    $this->grafica = HighChart::ColumnRotated($div, $titulo, null, $leyenda, $adicional, $xElem, $yElem, $series, $nameColum, $alto, $anch);
                    $this->result = $matriz;
                }
                
            
        }else{
                    $this->grafica ="Gráfica 1, No hay nada que reportar <br/>";
             }
        echo $this->grafica;
    }


     

}
/** Validar los action del proceso que se va instanciar */
 $evento  = $_REQUEST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_REQUEST;
/** Instancio la class que vamos a necesitar*/
 $proceso= new reportesAction();
if($evento=='showCausaCeguera'){
    return $proceso->executarShowCausaCeguera($request);
 }
?>

