<?php

// Incluir los Bean necesario
include_once '../../../../../lib/base/class.BeanTipoCeguera.php';

// Incluir archivo de configuracion
include_once '../../../../../config/Configuration.class.php';
// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

class tipoCegueraAction extends BeanTipoCeguera
{


   public function executarSetBeanTipoCeguera($request){
       
        $this->beanTipoCegera->set_tipo_ceguera_id(@$request['tipo_ceguera_id']);
        $this->beanTipoCegera->set_tipo_ceguera(@$request['tipo_ceguera']);
    }

      public function executarNew($request){
      	$datoJson='';
        $this->beanTipoCegera = new BeanTipoCeguera();

        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);

        /** Method que se encarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanTipoCeguera($request);
        $sql1 = "INSERT INTO tbl_tipo_cegera (tipo_ceguera_id, tipo_ceguera) VALUES (default,'".$this->beanTipoCegera->get_tipo_ceguera()."')";

       $this->result=$this->conn->execute($sql1);
       $this->conn->close();

       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                $datoJson['mensaje']="Registro efectuado exitosamente.";
                $datoJson['error']='0';
            }else{
                $datoJson['mensaje']='Error al efectuar el registro';
                $datoJson['error']='1';
            }
        echo json_encode($datoJson);
        } 
}
/** Method para validar inyeccion sql por el GET */
$valGet = new funcionesExtras();
$valGet -> validarMethodGet();
/** Validar los action del proceso que se va instanciar */
 $evento  = $_POST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_POST;
/** Instancio la class que vamos a necesitar*/
$tipo_ceguera = new tipoCegueraAction();

if($evento=='new'){
    return $tipo_ceguera->executarNew($request);
 }
?>

