<?php

// Incluir los Bean necesario
include_once '../../../../../lib/base/class.BeanMedico.php';

// Incluir archivo de configuracion
include_once '../../../../../config/Configuration.class.php';
// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

class medicoAction extends BeanMedico
{


   public function executarSetBeanMedico($request){
       
        $this->beanMedico->set_medico_id(@$request['medico_id']);
        $this->beanMedico->set_med_nacionalidad(@$request['med_nacionalidad']);
        $this->beanMedico->set_med_cedula(@$request['med_cedula']);
        $this->beanMedico->set_med_nombre(@$request['med_nombre']);

        /** Cargar Objetos generales para cada campo */
        $this->beanMedico->set_usuario_id();//$request['usuario_id']);
        $this->beanMedico->set_estatus();
        $this->beanMedico->set_created_at();
    }

      public function executarNew($request){
      	$datoJson='';
        $this->beanMedico = new BeanMedico();

        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);

        /** Method que se encarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanMedico($request);
        $sql1 = "INSERT INTO tbl_medico(
            medico_id, med_nacionalidad, med_cedula, med_nombre, usuario_id, estatus, created_at)
    VALUES (default,'".$this->beanMedico->get_med_nacionalidad()."', ".$this->beanMedico->get_med_cedula().", '".$this->beanMedico->get_med_nombre()."', ".$this->beanMedico->get_usuario_id().", '".$this->beanMedico->get_estatus()."', '".$this->beanMedico->get_created_at()."')";

       $this->result=$this->conn->execute($sql1);
       $this->conn->close();

       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                $datoJson['mensaje']="Registro efectuado exitosamente.";
                $datoJson['error']='0';
            }else{
                $datoJson['mensaje']='Error al efectuar el registro';
                $datoJson['error']='1';
            }
        echo json_encode($datoJson);
        } 
}
/** Method para validar inyeccion sql por el GET */
$valGet = new funcionesExtras();
$valGet -> validarMethodGet();
/** Validar los action del proceso que se va instanciar */
 $evento  = $_POST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_POST;
/** Instancio la class que vamos a necesitar*/
$medico = new medicoAction();

if($evento=='new'){
    return $medico->executarNew($request);
 }
?>

