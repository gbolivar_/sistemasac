<?php $url=$_SERVER['HTTP_REFERER'].'/../../'; ?>
<script type="text/javascript" src="<?php echo $url?>web/js/comunes.js"></script>

<script type="text/javascript">
$(document).ready(function(){

$("form.#addTipoAyuda").submit(function(e){
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/ayuda/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en el registro de ayudas', '_script.php','registrarAyuda','52');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                     mostrarMensaje(data.error,data.mensaje);
                        if(data.error==0){
                            $("form").each(function(){
                                this.reset();
                            });
                        }
                     }
            });
        return false;
   });


});
/** Fin Document ready */

function llenarCamposEditarPersonas(data){
    /** Recorrer el arreglo json con el fin de saber el nombre y el valor para asignar dinamicamente a cada campo */
    $.each(data, function(key, value) {
        //alert(key + '->' + value);
        setTimeout(function() {
        	var campo = document.getElementById(key);
        	$(campo).val(value);
           }, 500 );

    });
}
function procesarMedico(){
    var nac = $('#med_nacionalidad').val();
    var nom = $('#med_nombre').val();
    var ced = $('#med_cedula').val();
    var act = $('#action').val();
    if(nom==''  || ced== ''){
       
                     mostrarMensaje2(1,'Los campos (*) son obligatorio');

        return false;
    }else{
         $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/medico/actions/actions.class.php' ,
                data: {'med_nacionalidad':nac, 'med_nombre':nom, 'med_cedula':ced, 'action':act  },
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en el registro de ayudas', '_script.php','registrarAyuda','52');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                     mostrarMensaje2(data.error,data.mensaje);
                        if(data.error==0){
                            $("form").each(function(){
                                this.reset();
                            });
                           $('#medico_id').html('<option value="">SELECCIONE</option>');
                           selectores('medico_id','<?php echo base64_encode('tbl_medico') ?>','<?php echo base64_encode('medico_id') ?>','<?php echo base64_encode('med_nombre') ?>');

                        }
                     }
            });
    }
}

</script>