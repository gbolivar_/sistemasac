<?php $url=$_SERVER['HTTP_REFERER'].'/../../'; ?>
<script type="text/javascript" src="<?php echo $url?>web/js/comunes.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	selectores('medico_id','<?php echo base64_encode('tbl_medico') ?>','<?php echo base64_encode('medico_id') ?>','<?php echo base64_encode('med_nombre') ?>');
        selectores('tipo_ceguera_id','<?php echo base64_encode('tbl_tipo_cegera') ?>','<?php echo base64_encode('tipo_ceguera_id') ?>','<?php echo base64_encode('tipo_ceguera') ?>');
        /** Agregar la imagen de agregar + medicos */
        $("#addMedico").append('<div id="medico"><a href="#" id="addMedicoDiag"><span class="ui-icon ui-icon-circle-plus">&nbsp;</span></a></div>');
        $("#addTipoCegueraD").append('<div id="ceguera"><a href="#" id="addTipoCegueraDiag"><span class="ui-icon ui-icon-circle-plus">&nbsp;</span></a></div>');

       /** Funcion encargada de registrar el medico */
        $('#addMedicoDiag').click(function(){
            $('#comun').load("<?php echo $url?>apps/sac/modules/medico/templates/formNew.php");
            $('#comun').dialog({
                        autoOpen: false,
			buttons: {"Ok":  function(){
                           // EXECUTE ACTIONES
                           $(this).dialog('close');
                      }},
                  modal: true,
                  title: 'Registrar un medico',
                  width: 380,
                  autoOpen: true,
		  height: 300}); 

     });
     
      /** Funcion encargada de registrar el medico */
        $('#addTipoCegueraDiag').click(function(){
            $('#comun').load("<?php echo $url?>apps/sac/modules/tipo_ceguera/templates/formNew.php");
            $('#comun').dialog({
                        autoOpen: false,
			buttons: {"Ok":  function(){
                           // EXECUTE ACTIONES
                           $(this).dialog('close');
                      }},
                  modal: true,
                  title: 'Registrar un medico',
                  width: 380,
                  autoOpen: true,
		  height: 250}); 

     });
	/** Funcion encargada de procesar la informacion desde la vista al action del mismo */
        $("form.#addDiagnostico").submit(function(e){
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/diagnostico/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01) en agregar el diagnostico', '_script.php','diagnostico','17');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                    /** Funcion encargada de mostrar el mensaje en la vista */
                    //mostrarMensaje(data.mensaje);
                    mostrarMensaje(data.error, data.mensaje);

                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario */
                    if(data.error==0){
                        $("form").each(function(){
                            this.reset();
                        });
                         /** Permite redireccionar a la ficha de busqueda de registro de diagnostico */
                           setTimeout(function() {
                         		$('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "buscarPersonaAddDiagnostico"});
                          }, 500 );
                    }
                }
            });
            return false;
        });
});

</script>