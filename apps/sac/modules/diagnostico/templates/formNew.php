<!-- Incluir los js para este modulo de persona -->
<?php include_once '_script.php';?>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
<fieldset class="subTitulos"> <legend><b>INFORMACI&Oacute;N PERSONAL</b></legend>
	        <table border="0"  align="center" class="formulario">
            <tr>
                <td align="right" class="colorN" >(*) Apellidos:</td>
                <td><input type="text" name="apellido_persona" id="apellido_persona" maxlength="30" placeholder="Apellidos" required="" disabled/></td>
                <td align="right" class="colorN" >(*) Nombres:</td>
                <td><input type="text" name="nombre_persona" id="nombre_persona" maxlength="30" placeholder="Nombres" required="" disabled/></td>
              </tr>
              <tr>
                <td align="right" class="colorN">(*) Sexo:</td>
                <td><select name="sexo_persona" id="sexo_persona" required="" disabled>
                        <option value="F">F</option>
                        <option value="M">M</option>
                    </select>
                </td>
                <td align="right" class="colorN">(*) Cedula</td>
                <td><select name="nacionalidad_persona" id="nacionalidad_persona" disabled>
                    <option value="V">V</option>
                    <option value="E">E</option>
                  </select>
                    <input name="cedula_persona" type="text" id="cedula_persona" size="14" maxlength="9" required="" disabled /></td>
              </tr>

	</table>
      </fieldset>
	<form  name="addDiagnostico" id="addDiagnostico" method="post" action="#">
        <div id="menInfo" style="display:none"> </div>
		<input type="hidden"  name="action" id="action" value="new" />
            <input name="pers_disc_visual_id" type="hidden" id="pers_disc_visual_id" />
             <fieldset class="subTitulos"> <legend><b>DIAGNOSTICO MEDICO</b></legend>
        <table border="0"  align="center" class="formulario"><tr><td><table border="0"  align="center" class="formulario">
          <tr>
            <td class="colorN">Diagnostico:</td>
            <td colspan="2">
            	<textarea name="diagnostico" id="diagnostico" cols="70" rows="1" required=""></textarea>
            </td>
          </tr>
          <tr>
            <td width="50%" colspan="2" class="colorN">Usa Lentes:

            <select name="usa_lentes" id="usa_lentes" required="">
                <option value="">SELECCIONE</option>
                <option value="TRUE">SI</option>
                <option value="FALSE">NO</option>
            </select>
            </td>

            <td width="50%" colspan="2"  class="colorN">Medico Tratante

               <div id="addMedico">
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select name="medico_id" id="medico_id">
                        <option value="">SELECCIONE</option>
                    </select>
               </div>
            </td>
            <td colspan="2" align="right">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4" align="right" class="colorN"><div align="center"><b> Campo Visual</b> </div></td>
          </tr>
          <tr>
              <td width="50%" colspan="2" class="colorN">Ojo Izquierdo:
            <input type="text" name="campo_visual_oi" id="campo_visual_oi" required="" /></td>
            <td width="50%" colspan="2"class="colorN">Ojo Derecho:
            <input type="text" name="campo_visual_od" id="agudez_visual_od"  required="" /></td>
          </tr>
          <tr>
            <td colspan="4" align="right" class="colorN"><div align="center"> <b>Agudeza Visual</b> </div></td>
          </tr>
          <tr>
              <td  width="50%" class="colorN" colspan="2">Ojo Izquierdo:
            <input type="text" name="agudez_visual_id" id="agudez_visual_id" required="" /></td>
              <td width="50%" class="colorN" colspan="2">Ojo Derecho:
            <input type="text" name="agudez_visual_od" id="agudez_visual_od" required="" /></td>
          </tr>
          <tr>
            <td colspan="4" align="right" class="colorN"><div align="justify">
              <p><b>NOTA:</b> De acuerdo a la clasificaci&oacute;n de la O.M.S. se considera Legalmente Ciego a toda Persona que posee una Agudeza Visual hasta:<br/>
                20/400 y Deficiente Visual hasta 20/70 con la mejor correcci&oacute;n posible.                </p>
              </div></td>
          </tr>
          <tr>
            <td colspan="4" align="left" class="colorN">
            		De acuerdo a la evaluaci&oacute;n realizada se considera esta persona ciega o deficiente visual:
                        <select name="consideracion_visual" id="consideracion_visual" required="">
				    <option value="">SELECCIONE</option>
				    <option value="CIEGO">CIEGO</option>
				    <option value="DEFICIENTE VISUAL">DEFICIENTE VISUAL</option>
				  </select>
            </td>
          </tr>
          <td colspan="4" align="left" class="colorN">Causa de la Ceguera
            <div id="addTipoCegueraD">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <select name="tipo_ceguera_id" id="tipo_ceguera_id" required="">
                  <option value="">SELECCIONE</option>
              </select>
            </div>
          </td>
          <tr>  
          </tr>
        </table>
        </td>
		</tr>
		</table>
      </fieldset>
            <fieldset class="subTitulos" id="buttEnviar">
            	 	<input type="reset" value="Limpiar Datos" name="Limpiar" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             		<input type="submit" value="Registrar Datos" name="registrar Datos"  >
             </fieldset>
    </form>
</div>


