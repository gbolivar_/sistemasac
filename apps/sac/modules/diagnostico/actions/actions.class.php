<?php
   /**
    * @propiedad: PROPIETARIO DEL CODIGO
    * @Autor: Roxenis Sanoja
    * @email: roxenis.sanoja50@gmail.com
    * @Fecha de Create: 28/02/2012
    * @Fecha de Update: 28/02/2012
    * @Auditado por: Gregorio J Bolívar B
    * @Descripción: Generado por el generador de set y get del autor
    * @package: actions.class
    * @version: 1.0
    */
/** Incluir los Bean necesario para procesar Personas  */
include_once '../../../../../lib/base/class.BeanDiagnosticoMedico.php';

// Incluir archivo de configuracion
include_once '../../../../../config/Configuration.class.php';

// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class diagnosticoMedicoAction
{
    public function executarSetBeanDiagnosticoMedico($request){

        /** Cargar Objetos de Personas OJO CREAR CLASS PARA HACER ESTO*/
        $this->beanDiagnosticoMedico->set_pers_disc_visual_id(@$request['pers_disc_visual_id']);
        $this->beanDiagnosticoMedico->set_medico_id(@$request['medico_id']);
        $this->beanDiagnosticoMedico->set_tipo_ceguera_id(@$request['tipo_ceguera_id']);
        $this->beanDiagnosticoMedico->set_diagnostico(@$request['diagnostico']);
        $this->beanDiagnosticoMedico->set_pronostico(@$request['pronostico']);
        $this->beanDiagnosticoMedico->set_campo_visual_od(@$request['campo_visual_od']);
        $this->beanDiagnosticoMedico->set_campo_visual_oi(@$request['campo_visual_oi']);
        $this->beanDiagnosticoMedico->set_agudez_visual_od(@$request['agudez_visual_od']);
        $this->beanDiagnosticoMedico->set_agudez_visual_id(@$request['agudez_visual_id']);
        $this->beanDiagnosticoMedico->set_usa_lentes(@$request['usa_lentes']);
        $this->beanDiagnosticoMedico->set_consideracion_visual(@$request['consideracion_visual']);

        /** Cargar Objetos generales para cada campo */
        $this->beanDiagnosticoMedico->set_usuario_id();//$request['usuario_id']);
        $this->beanDiagnosticoMedico->set_estatus();
        $this->beanDiagnosticoMedico->set_created_at();

    }

    public function executarShow($request){
        
        require_once('../../../../../lib/complementos/tcpdf/config/lang/eng.php');
        require_once('../../../../../lib/complementos/tcpdf/tcpdf.php');
        
        
        
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $cedula=$request['cedula'];
        $sql="SELECT * FROM tbl_pers_disc_visual a, tbl_diag_medico b, tbl_medico c, tbl_tipo_cegera tc WHERE a.id=b.pers_disc_visual_id AND b.medico_id=c.medico_id AND a.cedula=$cedula and b.tipo_ceguera_id = tc.tipo_ceguera_id";
        $this->conn->execute($sql);
        $this->result = $this->conn->all_row();
        $this->numRow = $this->conn->num_row(); 
        $this->conn->close();
        
        $this->conn2 = new conexionDataBase(0);
        $this->conn2->conexion(0);
        $sql2="SELECT * FROM tbl_pers_disc_visual a  WHERE  a.cedula=$cedula AND a.estatus='TRUE' ";
        $this->conn2->execute($sql2);
        $this->numRow2 = $this->conn2->num_row(); 
        $this->conn2->close();

        if($this->numRow2 > 0){
             if($this->numRow==0){
                $datoJson['mensaje']="No tiene diagn&oacute;stico m&eacute;dico registrado";
                $datoJson['error']=1;
                echo json_encode($datoJson);
                exit();
            }
            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Roxenis Sanoja');
            $pdf->SetTitle('Expediente');

            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            //set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            //set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            //set some language-dependent strings
            $pdf->setLanguageArray($l);

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('helvetica', '', 11);

            //set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            
            // add a page
            $pdf->AddPage();

            // create some HTML content
            $html = '
                <p align="right"><b><font size="14px">Expediente nro: '.$this->result[0]['id'].'</font></b></p>
                <b>INFORMACI&Oacute;N PERSONAL</b><br/><br/>
                   <table border="0" width="100%" >
                <tr>
                    <td> Apellidos: '.$this->result[0]['apellido'].'</td>
                    <td> Nombres: '.$this->result[0]['nombre'].'</td>
               </tr>
               <tr>
                    <td>Sexo: '; if($this->result[0]['sexo'] == 'F'){
                        $html .= 'Femenino ';
                    }else{
                        if($this->result[0]['sexo'] == 'M'){
                            $html .= 'Masculino ';
                        }
                    } $html .='</td>
                    <td>Cedula: '.$this->result[0]['nacionalidad'].'-'.$this->result[0]['cedula'].'</td>              
                 </tr>
            </table><br/>
            <b>DIAGN&Oacute;STICO MEDICO</b><br/><br/>
            <table border="0"  align="center" width="100%">
              <tr>
                <td colspan="2"><p align="justify">Diagn&oacute;stico: '.$this->result[0]['diagnostico'].'</p></td>
              </tr>
              <tr>
                <td colspan="2"><p align="justify">Pron&oacute;stico: '.$this->result[0]['pronostico'].'</p></td>
              </tr>
              <tr>
                <td colspan="2" class="colorN"><p align="left">Usa Lentes: '; if($this->result[0]['usa_lentes'] == 't'){
                    $html .= 'Si ';
                }else{
                    if($this->result[0]['usa_lentes'] == 'f'){
                       $html .= 'No '; 
                    }
                }
                $html .= '&nbsp;&nbsp;&nbsp;&nbsp;
                    Medico Tratante: '.$this->result[0]['med_nombre'].'</p></td>
              </tr>
              <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td><center>
                                <table width="50%" align="center">
                                    <tr>
                                        <td colspan="2"><div align="center"><b> Campo Visual</b> </div></td>
                                    </tr>
                                    <tr>
                                        <td>OI: </td><td>'.$this->result[0]['campo_visual_oi'].'</td>
                                    </tr>
                                    <tr>
                                        <td>OD: </td><td>'.$this->result[0]['campo_visual_od'].'</td>
                                    </tr>
                                </table>
                                </center>
                            </td>
                            <td>
                                <center>
                                <table width="50%" align="center">
                                    <tr>
                                        <td colspan="2"><div align="center"><b> Agudeza Visual</b> </div></td>
                                    </tr>
                                    <tr>
                                        <td>OI: </td><td>'.$this->result[0]['agudez_visual_id'].'</td>
                                    </tr>
                                    <tr>
                                        <td>OD: </td><td>'.$this->result[0]['agudez_visual_od'].'</td>
                                    </tr>
                                </table>
                                </center>
                            </td>
                       </tr>
                   </table>
                </td>
              </tr>
              <tr>
                <td colspan="2"><div align="justify">
                  <p><b>NOTA:</b> De acuerdo a la clasificaci&oacute;n de la O.M.S. se considera Legalmente Ciego a toda Persona que posee una Agudeza Visual hasta: 20/400 y Deficiente Visual hasta 20/70 con la mejor correcci&oacute;n posible.
                  <br/>
                  <br/>
                  Causa de la ceguera: '.$this->result[0]['tipo_ceguera'].'<br/><br/>
                  De acuerdo a la evaluaci&oacute;n realizada se considera esta persona ciega o deficiente visual: '.$this->result[0]['consideracion_visual'].'
                  </p>
                  </div></td>
              </tr>
            </table>';
            // output the HTML content
            $pdf->writeHTML($html, true, 0, true, 0);

            $this->conn = new conexionDataBase(0);
            $this->conn->conexion(0);
            $cedula=$request['cedula'];
            $sql="SELECT * FROM tbl_pers_disc_visual a, tbl_tipo_vivienda tv, tbl_tenencia_vivienda tvv, tbl_laboral tl, tbl_oficio o, tbl_estudio_persona es,
                    tbl_profesion pr, tbl_grado_instruccion gi
                    where a.cedula=$cedula and tv.tipo_vivienda_id = a.tipo_vivienda_id and tvv.tenencia_vivienda_id = a.tenencia_vivienda_id
                    and tl.pers_disc_visual_id = a.id and tl.oficio_id = o.oficio_id and es.pers_disc_visual_id = a.id and 
                    gi.grado_inst_id = es.grado_inst_id and tl.profesion_id = pr.profesion_id";
            $this->conn->execute($sql);
            $this->result = $this->conn->all_row();
            $this->numRow = $this->conn->num_row();
           
            
            
            // add a page
            $pdf->AddPage();
            
            $html2 = ' <p align="center"><b>Sociedad Amigos de los Ciegos<br/>INPRESAC</b></p>
                <p align="right">N&uacute;mero de Afiliaci&oacute;n: '.$this->result[0]['id'].'</p>
                <p align="justify">Cedula: '.$this->result[0]['nacionalidad'].'-'.$this->result[0]['cedula'].' Nombres: '.$this->result[0]['nombre'].'   
                Apellidos: '.$this->result[0]['apellido'].'<br/><br/>
                Fecha: '.$this->result[0]['fech_nacimiento'].', Lugar de nacimiento: '.$this->result[0]['lugar_nacimiento'].' <br/><br/>
                Direcci&oacute;n de habitaci&oacute;n: '.$this->result[0]['direccion'].', Tel&eacute;fono: '.$this->result[0]['telf_habitacion'].'<br/><br/>
                <b>Tipo de Vivienda: </b>'.$this->result[0]['tipo_vivienda'].' &nbsp;&nbsp;&nbsp; <b>Tenencia de Vivienda: </b>'.$this->result[0]['tenencia_vivienda'].'<br/><br/>
                Trabaja: '.$this->result[0]['trabaja'].' <br/><br/>';
                if($this->result[0]['trabaja'] == 'SI'){
                    $html2 .= 'D&oacute;nde: '.$this->result[0]['empresa_trabaja'].', Direcci&oacute;n: '.$this->result[0]['direccion_trabajo'].' <br/><br/>';
                }
                $html2 .= 'Oficio: '.$this->result[0]['oficio'].' Ingresos: '.$this->result[0]['ingresos'].' BsF <br/><br/>
                    Grado de Instrucci&oacute;n: '.$this->result[0]['grado_instruccion'].'<br/><br/>
                    Profesi&oacute;n: '.$this->result[0]['profesion'].'<br/><br/>
                    Estudia: '.$this->result[0]['estudia'].' ';
                if($this->result[0]['estudia'] == 'SI'){
                    $html2 .= 'Lugar: '.$this->result[0]['lugar_estudia'].'<br/><br/>
                    Estudio que realiza: '.$this->result[0]['estudio_realiza'].'<br/><br/>';
                }
            $html2 .='</p>
            ';
            $id_pers_disc = $this->result[0]['id'];
            $this->conn = new conexionDataBase(0);
            $this->conn->conexion(0);
            $cedula=$request['cedula'];
            $sql="SELECT * FROM tbl_grupo_familiar gf, tbl_parentesco p, tbl_oficio o 
                    where gf.pers_disc_visual_id = $id_pers_disc and p.parentesco_id = gf.parentesco_id and o.oficio_id = gf.oficio_id";
            $this->conn->execute($sql);
            $this->result = $this->conn->all_row();
            $this->numRow = $this->conn->num_row(); 
            if($this->numRow > 0){
                $html2 .= '<p><b>Grupo Familiar: </b></p>';
                $html2 .= '<table border="0" width="100%">';
                for($i = 0; $i < count($this->numRow); $i++){
                    $html2 .= '<tr>
                        <td colspan="3">Nombre y Apellido: '.$this->result[$i]['nombre'].' '.$this->result[$i]['apellido'].'</td></tr>
                        <tr>
                        <td>Fecha de Nacimiento: '.$this->result[$i]['fech_nacimiento'].'</td>
                        <td>Sexo: '.$this->result[$i]['sexo'].'</td>
                        <td>Nacionalidad: '.$this->result[$i]['nacionalidad'].'</td>
                        </tr>
                        <tr>
                        <td colspan="2">Parentesco: '.$this->result[$i]['parentesco'].'</td>
                        <td>Tel&eacute;fono: '.$this->result[$i]['telefono'].'</td>
                        </tr>
                        <tr>
                        <td colspan="3">Direcci&oacute;n: '.$this->result[$i]['direccion'].'</td>
                        </tr>
                        <tr>
                        <td colspan="3">Estudia: '; if($this->result[$i]['estudia'] == 't'){ $html2 .= 'SI, Especifique: '.$this->result[$i]['esp_estudia'].' ';}
                        if($this->result[$i]['estudia'] == 'f'){ $html2 .= 'NO';}
                        $html2 .='</td>
                        </tr>
                        <tr>
                        <td colspan="3">Trabaja: '; if($this->result[$i]['trabaja'] == 't'){ $html2 .= 'SI, Direcci&oacute;n: '.$this->result[$i]['direcc_trabajo'].' ';}
                        if($this->result[$i]['trabaja'] == 'f'){ $html2 .= 'NO';}
                        $html2 .='</td>
                        </tr>
                        <tr>
                        <td>Ingresos: '.$this->result[$i]['ingreso'].' BsF.</td><td colspan="3">Oficio: '.$this->result[$i]['oficio'].'</td>
                        </tr>
                        <tr>
                        <td colspan="3">Sufre alg&uacute;n tipo de ceguera: '; if($this->result[$i]['sufre_ceguera'] == 't'){ $html2 .= 'SI ';}
                        if($this->result[$i]['sufre_ceguera'] == 'f'){ $html2 .= 'NO';}
                        $html2 .='</td>
                        </tr>';
                }
                $html2 .='</table><br/><hr width="100%" size="15px">';
            }
            $pdf->writeHTML($html2, true, 0, true, 0);
            
            // reset pointer to the last page
            $pdf->lastPage();

            // ---------------------------------------------------------

            //Close and output PDF document
            $pdf->Output('../../../../../web/expediente.pdf', 'F');

            //============================================================+
            // END OF FILE
            //============================================================+
            $datoJson='';
            $datoJson['mensaje']="<a href='../web/expediente.pdf' class='ui-state-default ui-corner-all' target='_new'>Descargar Expediente</a>";
            $datoJson['error']=0;
        }else{
            $datoJson='';
            $datoJson['mensaje']="C&eacute;dula no se encuentra registrada";
            $datoJson['error']=1;
        }
        echo json_encode($datoJson);
    }
    public function executarIndex($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL INDEX';
        echo json_encode($datoJson);
    }

    public function executarNew($request){

        $datoJson='';
        $this->beanDiagnosticoMedico = new BeanDiagnosticoMedico();

        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);

        /** Method que se envcarga de llenar los set de los campos recibe el request y el identificador del siguiente registro */
        diagnosticoMedicoAction::executarSetBeanDiagnosticoMedico($request);


        $sql1="INSERT INTO tbl_diag_medico(pers_disc_visual_id,medico_id, diagnostico, pronostico, campo_visual_od, campo_visual_oi, agudez_visual_od, agudez_visual_id, usa_lentes, consideracion_visual, usuario_id, estatus, created_at, tipo_ceguera_id)" .
        		"VALUES(".$this->beanDiagnosticoMedico->get_pers_disc_visual_id().",".$this->beanDiagnosticoMedico->get_medico_id().", " .
        				"'".$this->beanDiagnosticoMedico->get_diagnostico()."', '".$this->beanDiagnosticoMedico->get_pronostico()."', " .
        						"'".$this->beanDiagnosticoMedico->get_campo_visual_od()."', '".$this->beanDiagnosticoMedico->get_campo_visual_oi()."'," .
        								" '".$this->beanDiagnosticoMedico->get_agudez_visual_od()."', '".$this->beanDiagnosticoMedico->get_agudez_visual_id()."', " .
        										"'".$this->beanDiagnosticoMedico->get_usa_lentes()."', '".$this->beanDiagnosticoMedico->get_consideracion_visual()."', " .
        												"".$this->beanDiagnosticoMedico->get_usuario_id().", '".$this->beanDiagnosticoMedico->get_estatus()."', " .
        														"'".$this->beanDiagnosticoMedico->get_created_at()."', ".$this->beanDiagnosticoMedico->get_tipo_ceguera_id().");";
       $this->result=@$this->conn->execute($sql1);

       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                /** registrar en la tabla tbl_diag_medico */
                  $datoJson['mensaje']='Registro efectuado exitosamente';
                  $datoJson['error']='0';

            }else{
                  $datoJson['mensaje']='Error al efectuar el registro';
                  $datoJson['error']='1';
            }


        echo json_encode($datoJson);
    }
    public function executarEdit($request){
        $datoJson='';

        $datoJson['mensaje']='LLEGO EL EDITAR';
        echo json_encode($datoJson);
    }

    public function executarDeleted($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }

}
/** Validar los action del proceso que se va instanciar */
 $evento  = $_REQUEST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_REQUEST;
/** Instancio la class que vamos a necesitar*/
 $proceso= new diagnosticoMedicoAction();
if($evento=='new'){
    return $proceso->executarNew($request);
 }elseif($evento=='edit'){
    return $proceso->executarEdit($request);
 }elseif($evento=='index'){
    return $proceso->executarIndex($request);
 }elseif($evento=='delete'){
    return $proceso->executarDeleted($request);
 }elseif($evento=='show'){
    return $proceso->executarShow($request);
 }

?>

