<?php
/** Incluir los Bean necesario  */

/** Incluir los Bean necesario para procesar Horario  */
include_once '../../../../../lib/base/class.BeanHorario.php';

/** Incluir los Bean necesario para procesar detalle del Horario  */
include_once '../../../../../lib/base/class.BeanDetalleHorario.php';

// Incluir archivo de configuracion
include_once '../../../../../config/Configuration.class.php';

// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class horarioAction extends BeanHorario
{

    public function executarSetBeanHorario($request, $id){

        $this->beanHorario->set_id($id);
        $this->beanHorario->set_observacion($request['observacion']);
        $this->beanHorario->set_tipo_horario_id($request['tipo_horario_id']);
        $this->beanHorario->set_estatus();
        $this->beanHorario->set_usuario_id();
        $this->beanHorario->set_created_up();
        $this->beanHorario->set_updated_up();
        
        /***        ***/
        $this->beanDetalleHorario = new BeanDetalleHorario();
        $array_areas[0] = $request['area_lunes'];
        $array_areas[1] = $request['area_martes'];
        $array_areas[2] = $request['area_miercoles'];
        $array_areas[3] = $request['area_jueves'];
        $array_areas[4] = $request['area_viernes'];
        
        $array_dias[0] = 1;
        $array_dias[1] = 2;
        $array_dias[2] = 3;
        $array_dias[3] = 4;
        $array_dias[4] = 5;
        
        $this->beanDetalleHorario->set_area_id($array_areas);
        $this->beanDetalleHorario->set_dia_id($array_dias);
        $this->beanDetalleHorario->set_hora_inicio($request['hora_inicio']);
        $this->beanDetalleHorario->set_hora_fin($request['hora_fin']);
        $this->beanDetalleHorario->set_horario_id($id);
    }
    /** Exportar el horario  */
    public function executarShow($request){
        require_once('../../../../../lib/complementos/tcpdf/config/lang/eng.php');
        require_once('../../../../../lib/complementos/tcpdf/tcpdf.php');
        
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $cedula=$request['cedula'];
        $sql="SELECT count(*) from tbl_detalle_horario a
        INNER JOIN tbl_area b ON a.area_id=b.area_id 
        INNER JOIN tbl_dia c ON a.dia_id=c.id
        INNER JOIN tbl_horario d ON a.horario_id=d.id
        INNER JOIN tbl_tipo_horario h ON d.tipo_horario_id=h.id
        INNER JOIN tbl_pers_disc_visual e ON d.id=e.horario_id
        WHERE h.id=1 AND cedula=".$cedula;
        $this->conn->execute($sql);
        $this->result = $this->conn->all_row();
        $this->numRow = $this->conn->num_row(); 
        $this->conn->close();
        
        if($this->numRow != 0){
            // create new PDF document
            $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Gregorio Jose Bolivar Bolivar');
            $pdf->SetTitle('Reporte de Horario del Sistema de Gestion de Personas con Discapacidad Visual en la Sociedad Amigos de los Ciegos. ');
            $pdf->SetSubject('Sistema de Gestion de Personas con Discapacidad Visual en la Sociedad Amigos de los Ciegos. ');
            $pdf->SetKeywords('Horarios');

            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            //set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            //set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            //set some language-dependent strings
            $pdf->setLanguageArray($l);

            // ---------------------------------------------------------

            
            $html = '
            <b>INFORMACI&Oacute;N PERSONAL</b><br/><br/>
                     <table  width="100%" height="40%" style="border: 1px solid black; text-align:center;">
                     <th width="130px"><center>Lunes</center></th><th width="130px">Martes</th><th width="130px">Miercoles</th><th width="130px">Jueves</th><th width="130px">Viernes</th></tr>
                     <tr><td colspan="6">';
            
                /** Crear 6 columna 0=>Hora, 1=>Lunes, 2=>Martes, 3=>Miercoles, 4=>Jueves, 5=>Viernes, */
                for($rows=1; $rows<=5; $rows++){

                             switch ($rows){
                                    case 1:
                                        $html0='<table style="border: 1px solid black;" align="center"><tr><th style="font-size:12; text-decoration: underline" align="center"><b>Horas</b></th></tr>';  /** Para horario */
                                        $html1='<table style="border: 1px solid black;" align="center"><tr><th style="font-size:12; text-decoration: underline;" align="center"><b>Lunes</b></th></tr>';  /** Para horario */
                                    break;                                
                                    case 2:
                                        $html2='<table style="border: 1px solid black;" align="center"><tr><th style="font-size:12; text-decoration: underline;" align="center"><b>Martes</b></th></tr>';  /** Para horario */
                                    break;
                                    case 3:
                                        $html3='<table style="border: 1px solid black;" align="center"><tr><th style="font-size:12; text-decoration: underline;" align="center"><b>Miercoles</b></th></tr>';  /** Para horario */
                                    break;
                                    case 4:
                                        $html4='<table style="border: 1px solid black;" align="center"><tr><th style="font-size:12; text-decoration: underline;" align="center"><b>Jueves</b></th></tr>';  /** Para horario */
                                    break;

                                    case 5:
                                        $html5='<table style="border: 1px solid black;" align="center"><tr><th style="font-size:12; text-decoration: underline;" align="center"><b>Viernes</b></th></tr>';  /** Para horario */
                                    break ;

                                }
                            $this->conn2 = new conexionDataBase(0);
                            $this->conn2->conexion(0);
                            $cedula=$request['cedula'];
                            $sql="SELECT a.hora_inicio AS hora1,  a.hora_fin AS hora2, b.area, a.dia_id, c.dia, e.nombre, e.apellido from tbl_detalle_horario a
                                INNER JOIN tbl_area b ON a.area_id=b.area_id 
                                INNER JOIN tbl_dia c ON a.dia_id=c.id
                                INNER JOIN tbl_horario d ON a.horario_id=d.id
                                INNER JOIN tbl_tipo_horario h ON d.tipo_horario_id=h.id
                                INNER JOIN tbl_pers_disc_visual e ON d.id=e.horario_id
                                WHERE a.dia_id=$rows AND cedula=$cedula ORDER BY dia_id";
                            $this->conn2->execute($sql);
                            $this->rows = $this->conn2->num_row();
                            $this->result2 = $this->conn2->all_row();
                            $this->conn2->close();
                            
                            if($this->rows!=0){
                             foreach ($this->result2 as $i=>$item):
                            /** De esta menera solo selecciono el horario */
                            if($i<=5 AND $item['dia_id']==1){
                                $html0.='<tr><td width="70px" style="border: 1px solid black">'.substr($item['hora1'],0,-3).'--'.substr($item['hora2'],0,-3).'</td></tr>';
                                $html1.='<tr><td width="120px" style="border: 1px solid black">'.$item['area'].'</td></tr>';                                
                            }else{
                                 switch ($item['dia_id']){
                                                                   
                                    case 2:
                                        $html2.='<tr><td width="120px" style="border: 1px solid black">'.$item['area'].'</td></tr>'; 
                                    break;
                                    case 3:
                                        $html3.='<tr><td width="120px" style="border: 1px solid black">'.$item['area'].'</td></tr>'; 
                                    break;
                                    case 4:
                                        $html4.='<tr><td width="120px" style="border: 1px solid black">'.$item['area'].'</td></tr>'; 
                                    break;

                                    case 5:
                                        $html5.='<tr><td width="120px" style="border: 1px solid black"><s>'.$item['area'].'</s></td></tr>'; 
                                    break ;

                                }
                            }
                        endforeach;
                        }
                             switch ($rows){
                                    case 1:
                                        $html0.='</table>';  /** Para horario */
                                        $html1.='</table>';  /** Para horario */
                                    break;                                
                                    case 2:
                                        $html2.='</table>';  /** Para horario */
                                    break;
                                    case 3:
                                        $html3.='</table>';  /** Para horario */
                                    break;
                                    case 4:
                                        $html4.='</table>';  /** Para horario */
                                    break;

                                    case 5:
                                        $html5.='</table>';  /** Para horario */
                                    break ;

                                }

                                    }
                $html.="</td></tr></table>";
//               echo $html;
//              exit();
//            // output the HTML content
//            $pdf->writeHTML($html, true, 0, true, 0);
            
            // add a page
$pdf->SetFont('times', '', 14);
$pdf->AddPage();

$pdf->Write(0, "Sociedad Amigos de los Ciegos
Dirección de Relaciones Institucionales
Departamento Trabajo Social
Integrante: ".$this->result2[0]['nombre'].' '.$this->result2[0]['apellido'].""."\n", '', 0, 'J', true, 0, false, false, 0);

$pdf->Write(0, "Horario de Rehabilitación Funcional"."\n A", '', 0, 'C', true, 0, false, false, 0);

$pdf->Ln(3);

$pdf->SetFont('times', '', 10);

// set color for background
$pdf->SetFillColor(255, 255, 255);

// set color for text
$pdf->SetTextColor(0, 0, 0);

// Columna Horario
$pdf->MultiCell(30, 0, $html0, 1, 'C', 1, 0, '', 65, true, 0, true, true, 0);
// Columna Lunes
$pdf->MultiCell(44, 0, $html1, 1, 'C', 1, 0, '', '', true, 0, true, true, 0);
// Columna Martes
$pdf->MultiCell(44, 0, $html2, 1, 'C', 1, 0, 89, 65, true, 0, true, true, 0);
// Columna Miercoles
$pdf->MultiCell(44, 0, $html3, 1, 'C', 1, 0, '', '', true, 0, true, true, 0);
// Columna Jueves
$pdf->MultiCell(44, 0, $html4, 1, 'C', 1, 0, '', '', true, 0, true, true, 0);
// Columna Viernes
$pdf->MultiCell(44, 0, $html5, 1, 'C', 1, 0, '', '', true, 0, true, true, 0);


// reset pointer to the last page

            // reset pointer to the last page
            $pdf->lastPage();

            // ---------------------------------------------------------

            //Close and output PDF document
            $pdf->Output('../../../../../web/horario.pdf', 'F');

            //============================================================+
            // END OF FILE
            //============================================================+
            $datoJson='';
            $datoJson['mensaje']="<a href='../web/horario.pdf' class='ui-state-default ui-corner-all' target='_new'>Descargar horario</a>";
            $datoJson['error']=0;
        }else{
            $datoJson='';
            $datoJson['mensaje']="No tienes expediente registrados";
            $datoJson['error']=1;
        }
        echo json_encode($datoJson);
    }
    public function executarIndex($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL INDEX';
        echo json_encode($datoJson);
    }

    public function executarNew($request){
        $datoJson='';
        $this->beanHorario = new BeanHorario();
      
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);

        /** Consultar el identintificador siguiente para cargar al usuario */
        $sql = "select nextval('tbl_horario_id_seq');";
        $this->conn->execute($sql);
        $this->resultHor = $this->conn->all_row();
        $this->resultHor[0]['nextval'];
        
        
        /** Method que se envcarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanHorario($request, $this->resultHor[0]['nextval']);

         $sql1="INSERT INTO tbl_horario(id, observacion, estatus, tipo_horario_id, usuario_id, created_up)
             VALUES(".$this->beanHorario->get_id().",'".$this->beanHorario->get_observacion()."', '".$this->beanHorario->get_estatus()."',
             ".$this->beanHorario->get_tipo_horario_id().", ".$this->beanHorario->get_usuario_id().", '".$this->beanHorario->get_created_up()."');";

       $this->result=$this->conn->execute($sql1);
       
       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                $this->areas    = $this->beanDetalleHorario->get_area_id();
                $this->hora_inicio  = $this->beanDetalleHorario->get_hora_inicio();
                $this->hora_fin     = $this->beanDetalleHorario->get_hora_fin();

                $dia = 1;
                foreach ($this->areas as $area):
                   $i = 0;
                   foreach ($area as $ar):
                       $sql_hor = "INSERT INTO tbl_detalle_horario (hora_inicio, hora_fin, dia_id, area_id, horario_id)
                           values ('".$this->hora_inicio[$i]."', '".$this->hora_fin[$i]."',".$dia.", ".$ar.", ".$this->beanDetalleHorario->get_horario_id().");";
                       $this->result=$this->conn->execute($sql_hor);
                       $i++;
                    endforeach; 
                    $dia++;
                 endforeach; 
                  $datoJson['mensaje']='Registro efectuado exitosamente';
                  $datoJson['error']='0';
            }else{
                  $datoJson['mensaje']='Error al efectuar el registro';
                  $datoJson['error']='1';
            }

        $this->conn->close();
        echo json_encode($datoJson);
    }
    public function executarEdit($request){
        $datoJson='';

        $datoJson['mensaje']='LLEGO EL EDITAR';
        echo json_encode($datoJson);
    }

    public function executarDeleted($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }
    
    public function horarios_disponibles($request){
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        /**  Consulto   **/
        if(@$request['mostrar'] == 'disponibles'){
            $sql = "select th.id, th.tipo_horario from tbl_tipo_horario th where th.id not in (select tipo_horario_id from tbl_horario) order by th.tipo_horario";
        }else{
            if(@$request['mostrar'] == 'a_asociar'){
                $sql = 'select h.id, th.tipo_horario from tbl_tipo_horario th inner join tbl_horario h on h.tipo_horario_id = th.id  order by tipo_horario';
            }
        }
         /** Execute el sql */
        $this->conn->execute($sql);
         /** Defuelve un array con todos los valores */
        $this->informacion=$this->conn->all_row();
        /** Armo el json que sera enviado a la vista **/
        if($this->informacion!=''){
            foreach ($this->informacion as $es){
                    $arr[] = array('id' => $es['id'], 'data' => $es['tipo_horario']);
            }
        }else{
                    $arr = '';
        }
        $this->conn->close();
        echo json_encode($arr);
    }

}
/** Validar los action del proceso que se va instanciar */
$evento  = $_REQUEST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_REQUEST;
/** Inst4zancio la class que vamos a necesitar*/
$proceso = new horarioAction();

if($evento=='new'){
    return $proceso->executarNew($request);
 }elseif($evento=='edit'){
    return $proceso->executarEdit($request);
 }elseif($evento=='index'){
    return $proceso->executarIndex($request);
 }elseif($evento=='delete'){
    return $proceso->executarDeleted($request);
 }elseif($evento=='show'){
    return $proceso->executarShow($request);
 }elseif($evento == 'consulta_horarios'){
     return $proceso->horarios_disponibles($request);
 }

?>

