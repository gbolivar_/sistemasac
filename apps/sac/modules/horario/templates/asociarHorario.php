<!-- Incluir los js para este modulo de Grupo Familiar -->
<?php include_once '_script_editar.php';?>
<br></br>
<form id="horario_a_asociar" method="POST" action="#">
<div id="menInfo" style="display:none"> </div>
<input type="hidden" name="action" id="action" value="asociarH" />
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
    <fieldset class="subTitulos"> <legend><b>INFORMACI&Oacute;N PERSONAL</b></legend>
        <table border="0"  align="center" class="formulario">
      <tr>
        <td align="right" class="colorN" >(*) Apellidos:</td>
        <td><input type="text" name="apellido_persona" id="apellido_persona" maxlength="30" placeholder="Apellidos" required="" readonly=""/></td>
        <td align="right" class="colorN" >(*) Nombres:</td>
        <td><input type="text" name="nombre_persona" id="nombre_persona" maxlength="30" placeholder="Nombres" required="" readonly=""/></td>
      </tr>
      <tr>
        <td align="right" class="colorN">(*) Sexo:</td>
        <td><select name="sexo_persona" id="sexo_persona" required="" disabled>
                <option value="F">F</option>
                <option value="M">M</option>
            </select>
        </td>
        <td align="right" class="colorN">(*) C&eacute;dula</td>
        <td><select name="nacionalidad_persona" id="nacionalidad_persona" disabled>
            <option value="V">V</option>
            <option value="E">E</option>
          </select>
            <input name="cedula_persona" type="text" id="cedula_persona" size="14" maxlength="9" required="" readonly="" /></td>
      </tr>
      <tr>
        <td align="right" class="colorN">(*)Horario </td>
        <td colspan="4">
            <select name="tipo_horario_id" id="tipo_horario_id" required="">
                <option value="">Seleccione</option>
            </select>
        </td>
      </tr>
      <tr>
        <td align="center" colspan="5">
                <input type="submit" id="asociar_horario" value="Asociar Horario"/>
        </td>
      </tr>
</table>
</fieldset>
</form>
