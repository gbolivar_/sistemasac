<!-- Script de horarios -->
<?php $url=$_SERVER['HTTP_REFERER'].'/../../'; ?>
<script type="text/javascript" src="<?php echo $url?>web/js/comunes.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    nombre_horario('tipo_horario_id', 'a_asociar');
    
    /** Funcion encargada de procesar la informacion desde la vista al action del mismo */
        $("form.#horario_a_asociar").submit(function(e){
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/persona/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n');
                },
                dataType: "json",
                success: function(data){
                    /** Funcion encargada de mostrar el mensaje en la vista */
                    mostrarMensaje(data.error,data.mensaje);
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario */
                    if(data.error==0){
                        $("form").each(function(){
                            this.reset();
                            setTimeout(function() {

                                $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "buscarPersonaHorario"});
                            },900);
                        });
                    }
                }
            });
            return false;
        });
});
</script>