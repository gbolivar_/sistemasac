<!-- Incluir los js para este modulo de Padrinos -->
<?php include_once '_script.php';?><br/>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="width: 90%">
<div id="menInfo" style="display:none"> </div>
<form  name="registrarAyuda" id="registrarAyuda" method="POST" action="#">
       <fieldset class="subTitulos"> <legend><b>DATOS PERSONA</b></legend>
	        <table border="0"  align="center" class="formulario">
              <tr>
                <td align="right" class="colorN">(*) Cedula</td>
                <td><select name="nacionalidad" id="nacionalidad" disabled>
                    <option value="V">V</option>
                    <option value="E">E</option>
                  </select>
                    <input name="cedula" type="text" id="cedula" size="14" maxlength="9" required="" disabled /></td>
                <td align="right" class="colorN" >(*) Apellidos:</td>
                <td><input type="text" name="apellido" id="apellido" maxlength="30" placeholder="Apellidos" required="" disabled/></td>
                <td align="right" class="colorN" >(*) Nombres:</td>
                <td><input type="text" name="nombre" id="nombre" maxlength="30" placeholder="Nombres" required="" disabled/></td>
              </tr>
              </table>
		</fieldset>


		 <fieldset class="subTitulos"> <legend><b>AYUDA</b></legend>
	<input type="hidden"  name="action" id="action" value="new" />
        <input type="hidden"  name="pers_disc_visual_id" id="id"  />
	<table border="0"  align="center" class="formulario">
		<tr>
			<td align="right">
				(*) Monto
			</td>
			<td>
                            <input type="text" name="monto" id="monto" size="10" required="" onkeypress="return numeric(event)" maxlength="6" title="Debe especificar el valor de tipo de ayuda que sera entregada. Ejemplo: 1000 cuesta el bastón seleccionado">
			</td>

			<td align="right">
				(*) Tipo de Ayuda
			</td>

			<td>
                            <select name="tipo_ayuda_id" id="tipo_ayuda" required="">   </select>
                            <select name="medicina_id" id="medicina_id">    </select>
			</td>
                        

			<td align="right">
				(*) Fecha de entrega
			</td>
			<td>
					<input type="text"  size="12" maxlength="12" name="fecha_entrega" id="fecha_entrega" class="datepicker" placeholder="dd-mm-yyyy" required=""/>
			</td>
		</tr>
                <tr> 
                    <td align="right">
				Observaci&oacute;n
			</td>
                    <td colspan="5">
                        <textarea cols="60" rows="1" name="observacion" id="observacion"></textarea>
                    </td>
		</tr>
	</table>
      </fieldset>
             <fieldset class="subTitulos" id="buttEnviar">
            	 	<input type="reset" value="Limpiar Datos" class="ui-state-default ui-corner-all" name="Limpiar">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             		<input type="submit" value="Guardar Datos" class="ui-state-default ui-corner-all"  name="Asociar Datos">
             </fieldset>

</form>