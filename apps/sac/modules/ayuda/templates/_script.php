<?php $url=$_SERVER['HTTP_REFERER'].'/../../'; ?>
<script type="text/javascript" src="<?php echo $url?>web/js/comunes.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    /*$('#monto').change(function(){
        alert('Hola');
    });*/
     $('#medicina_id').hide();

 $('#tipo_ayuda').change(function(){
     /** Valido si el tipo de ayuda es medicamento */
     if($('#tipo_ayuda').val()==1){
         $('#medicina_id').show(900);
        selectores('medicina_id','<?php echo base64_encode('tbl_medicina') ?>','<?php echo base64_encode('medicina_id') ?>','<?php echo base64_encode('medicina') ?>');
     }else{
         $('#medicina_id').hide();
     }
 });

 $("form.#registrarAyuda").submit(function(e){
        TSO = '<?php echo date('d-m-Y').';' ?>';
        TSI = $('#fecha_entrega').val();
        if(TSI>TSO){
            mostrarMensaje(1,'Error:El sistema no permite entregar una ayuda mayor a la fecha de hoy');
            return false;
        }
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/ayuda/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en el registro de ayudas', '_script.php','registrarAyuda','47');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                     mostrarMensaje(data.error,data.mensaje);
                        if(data.error==0){
                            $("form").each(function(){
                                this.reset();
                               
                            });
                            setTimeout(function() {
                             $('#contenido').load('../apps/sac/modules/persona/templates/searchPersona.php', {divId: "registrarAyuda"});
                        }, 500);
                        }
                     }
            });
        return false;
   });
$("form.#addTipoAyuda").submit(function(e){
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/ayuda/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en el registro de ayudas', '_script.php','registrarAyuda','52');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                     mostrarMensaje(data.error,data.mensaje);
                        if(data.error==0){
                            $("form").each(function(){
                                this.reset();
                            });
                        }
                     }
            });
        return false;
   });


$("form.#registrarAyudaEspecial").submit(function(e){

        TSO = '<?php echo date('d-m-Y').';' ?>';
        TSI = $('#fecha_entrega').val();
        if(TSI>TSO){
            mostrarMensaje(1,'Error:El sistema no permite entregar una ayuda mayor a la fecha de hoy');
            return false;
        }
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/ayuda/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en el registro de ayudas espaciales', '_script.php','registrarAyuda','51');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                     mostrarMensaje(data.error,data.mensaje);
                        if(data.error==0){
                            $("form").each(function(){
                                this.reset();
                            });
                        }
                     }
            });
        return false;
   });

});
/** Fin Document ready */

function llenarCamposEditarPersonas(data){
    /** Recorrer el arreglo json con el fin de saber el nombre y el valor para asignar dinamicamente a cada campo */
    $.each(data, function(key, value) {
        //alert(key + '->' + value);
        setTimeout(function() {
        	var campo = document.getElementById(key);
        	$(campo).val(value);
           }, 500 );

    });
}

selectores('tipo_ayuda','<?php echo base64_encode('tbl_tipo_ayuda') ?>','<?php echo base64_encode('tipo_ayuda_id') ?>','<?php echo base64_encode('tipo_ayuda') ?>');

</script>