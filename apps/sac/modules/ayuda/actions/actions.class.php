<?php

// Incluir los Bean necesario
include_once '../../../../../lib/base/class.BeanAyuda.php';
include_once '../../../../../lib/base/class.BeanAyudaEspecial.php';
include_once '../../../../../lib/base/class.BeanTipoAyuda.php';

// Incluir archivo de configuracion
include_once '../../../../../config/Configuration.class.php';
// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

class ayudaAction extends BeanAyuda
{


   public function executarSetBeanAyuda($request){
       
        $this->beanAyuda->set_medicina_id(@$request['medicina_id']);
        $this->beanAyuda->set_observacion(@$request['observacion']);
        $this->beanAyuda->set_pers_disc_visual_id($request['pers_disc_visual_id']);
        $this->beanAyuda->set_tipo_ayuda_id(@$request['tipo_ayuda_id']);
        $this->beanAyuda->set_monto(@$request['monto']);
        $this->beanAyuda->set_fecha_entrega(@$request['fecha_entrega']);

        /** Cargar Objetos generales para cada campo */
        $this->beanAyuda->set_usuario_id();//$request['usuario_id']);
        $this->beanAyuda->set_estatus();
        $this->beanAyuda->set_created_at();
    }
    public function executarSetBeanAyudaEspacial($request){
        $this->beanAyudaEspeciales->set_nacionalidad(@$request['nacionalidad']);
        $this->beanAyudaEspeciales->set_cedula(@$request['cedula']);
        $this->beanAyudaEspeciales->set_nombres(@$request['nombre']);
        $this->beanAyudaEspeciales->set_apellidos(@$request['apellido']);
        $this->beanAyudaEspeciales->set_medicina_id(@$request['medicina_id']);
        $this->beanAyudaEspeciales->set_observacion(@$request['observacion']);
        $this->beanAyudaEspeciales->set_tipo_ayuda_id(@$request['tipo_ayuda_id']);
        $this->beanAyudaEspeciales->set_monto(@$request['monto']);
        $this->beanAyudaEspeciales->set_fecha_entrega(@$request['fecha_entrega']);

        /** Cargar Objetos generales para cada campo */
        $this->beanAyudaEspeciales->set_usuario_id();//$request['usuario_id']);
        $this->beanAyudaEspeciales->set_estatus();
        $this->beanAyudaEspeciales->set_created_at();
    }
     public function executarSetBeanTipoAyuda($request){
        $this->beanTipoAyuda->set_tipo_ayuda(@$request['tipo_ayuda']);
        $this->beanTipoAyuda->set_total_monto(@$request['total_monto']);
        $this->beanTipoAyuda->set_anio_autorizado();

        /** Cargar Objetos generales para cada campo */
        $this->beanTipoAyuda->set_usuario_id();//$request['usuario_id']);
        $this->beanTipoAyuda->set_estatus();
        $this->beanTipoAyuda->set_created_at();
    }
   public function executarShow($request){
        $datoJson='';
        
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $sql ="SELECT a.pers_disc_visual_id, c.medicina, d.nombre || ' '  || d.apellido as beneficiario, b.tipo_ayuda,monto,a.usuario_id, CASE WHEN a.estatus='TRUE' THEN 'ENTREGADA' ELSE 'DENEGADA' END AS estatus , to_char(a.created_at,'dd/mm/YYYY MI:SS') AS created_at FROM tbl_ayuda a 
            INNER JOIN tbl_tipo_ayuda b ON a.tipo_ayuda_id=b.tipo_ayuda_id
            LEFT JOIN tbl_medicina c ON c.medicina_id=a.medicina_id
            INNER JOIN tbl_pers_disc_visual d ON d.id=a.pers_disc_visual_id WHERE d.cedula=".$request['cedula']." OR d.id=".$request['cedula'].";";
        $this->conn->execute($sql);
                $this->rows=$this->conn->num_row();


        if($this->rows!=0){
            $this->usuario=$this->conn->all_row();
            foreach ($this->usuario as $p=>$pdv){
                $datoJson['listados'][$p]=$pdv;
            }
            $datoJson['error']='0';
        }else{
            $datoJson['error']='1';
            $datoJson['mensaje']='Error Persona no se encuentra';
        }

        /** Verifico si trae dato de base de datos para mandar el mensaje a la vista 1 = error, 0= select */

        echo json_encode($datoJson);
    }

   public function executarIndex($request){
        $datoJson='';
        $this->beanPersona = new BeanPersona();
        $this->beanAyuda = new BeanAyuda();
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        self::executarSetBeanAyuda($request);

        /** Consultar el identintificador siguiente para cargar al usuario */
        $sqlPersona ="select pers_disc_visual_id, nombre as per_nom, apellido as per_ape, nacionalidad as per_nac, cedula as per_ced from tbl_pers_disc_visual where cedula= ".$this->beanPersona->get_cedula().";";

         /** Execute el sql */
        $this->conn->execute($sqlPersona);
        $this->pers_disc_visual=$this->conn->all_row();

        if($this->pers_disc_visual!=''){
            foreach ($this->pers_disc_visual[0] as $p=>$pdv){
                $datoJson[$p]=$pdv;
            }
            $datoJson['error']='0';
        }else{
			$datoJson['error']='1';
            $datoJson['mensaje']='Error Persona no se encuentra';
        }

        /** Verifico si trae dato de base de datos para mandar el mensaje a la vista 1 = error, 0= select */

        echo json_encode($datoJson);
    }

    public function executarNew($request){
      	$datoJson='';
        $this->beanAyuda = new BeanAyuda();

        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);

        /** Method que se encarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanAyuda($request);
   

        $sql1 = "INSERT INTO tbl_ayuda(medicina_id, pers_disc_visual_id, tipo_ayuda_id, monto, observacion, 
            usuario_id, estatus, created_at)  VALUES (".$this->beanAyuda->get_medicina_id().", ".$this->beanAyuda->get_pers_disc_visual_id().", ".$this->beanAyuda->get_tipo_ayuda_id().", ".$this->beanAyuda->get_monto().", '".$this->beanAyuda->get_observacion()."', ".$this->beanAyuda->get_usuario_id().", '".$this->beanAyuda->get_estatus()."', '".$this->beanAyuda->get_created_at()."');";
       $this->result=$this->conn->execute($sql1);
       $this->conn->close();

       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                if($this->beanAyuda->get_tipo_ayuda_id() == 2 || $this->beanAyuda->get_tipo_ayuda_id() == 1){
                    self::executarReporte($this->beanAyuda->get_tipo_ayuda_id());
                    $datoJson['mensaje']="Registro efectuado exitosamente. <a href='../web/ayuda.pdf' class='ui-state-default ui-corner-all' target='_new'>Descargar Comprobante</a>";
                    $datoJson['error']='0';
                }else{
                    $datoJson['mensaje']="Registro efectuado exitosamente.";
                    $datoJson['error']='0';
                }
            }else{
              $datoJson['mensaje']='Error al efectuar el registro';
              $datoJson['error']='1';
            }
        echo json_encode($datoJson);
        }

    public function executarTipoAyuda($request){
      	$datoJson='';
        $this->beanTipoAyuda = new BeanTipoAyuda();
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);

        /** Method que se encarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanTipoAyuda($request);
       $sql1 = "INSERT INTO tbl_tipo_ayuda(tipo_ayuda, total_monto, anio_autorizado, usuario_id, 
            estatus, created_at) VALUES ('".$this->beanTipoAyuda->get_tipo_ayuda()."', ".$this->beanTipoAyuda->get_total_monto().", ".$this->beanTipoAyuda->get_anio_autorizado().", ".$this->beanTipoAyuda->get_usuario_id().", 
            '".$this->beanTipoAyuda->get_estatus()."', '".$this->beanTipoAyuda->get_created_at()."');";
 
       $this->result=$this->conn->execute($sql1);

       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){

                  $datoJson['mensaje']='Registro efectuado exitosamente';
                  $datoJson['error']='0';

            }else{
                  $datoJson['mensaje']='Error al efectuar el registro';
                  $datoJson['error']='1';
            }


        echo json_encode($datoJson);
    }
    
    public function executarNewEspecial($request){
      	$datoJson='';
        $this->beanAyudaEspeciales = new BeanAyudaEspecial();


        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);

        /** Method que se encarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanAyudaEspacial($request);
       $sql1 = "INSERT INTO tbl_ayuda_especial(
            nacionalidad, nombres, apellidos, monto, tipo_ayuda_id, fecha_entrega, 
            usuario_id, estatus, created_at, medicina_id, cedula, observacion)
    VALUES ('".$this->beanAyudaEspeciales->get_nacionalidad()."', '".$this->beanAyudaEspeciales->get_nombres()."', '".$this->beanAyudaEspeciales->get_apellidos()."', ".$this->beanAyudaEspeciales->get_monto().", ".$this->beanAyudaEspeciales->get_tipo_ayuda_id().", '".$this->beanAyudaEspeciales->get_fecha_entrega()."', 
            ".$this->beanAyudaEspeciales->get_usuario_id().", '".$this->beanAyudaEspeciales->get_estatus()."', '".$this->beanAyudaEspeciales->get_created_at()."', ".$this->beanAyudaEspeciales->get_medicina_id().", ".$this->beanAyudaEspeciales->get_cedula().", '".$this->beanAyudaEspeciales->get_observacion()."');";
       
       $this->result=$this->conn->execute($sql1);

       /** Validar que el INSERT de tbl_ayuda_especial sea efectivo */
            if($this->result){
                  self::executarReporteAyudaEspecial();
                  $datoJson['mensaje']="Registro efectuado exitosamente. <a href='../web/ayuda.pdf' class='ui-state-default ui-corner-all' target='_new'>Descargar Comprobante</a>";
                  $datoJson['error']='0';

            }else{
                  $datoJson['mensaje']='Error al efectuar el registro';
                  $datoJson['error']='1';
            }


        echo json_encode($datoJson);
    }


    public function executarDeleted($request){
        $datoJson='';
        $datoJson['mensaje']='LLEGO EL ELIMINAR';
        echo json_encode($datoJson);
    }
    public function executarReporte($tipo_report){
        
        require_once('../../../../../lib/complementos/tcpdf/config/lang/eng.php');
        require_once('../../../../../lib/complementos/tcpdf/tcpdf.php');
        
        $this->conn2 = new conexionDataBase(0);
        $this->conn2->conexion(0);

        $sql2="SELECT *, to_char(age (current_date, pd.fech_nacimiento),'YY') as edad from tbl_pers_disc_visual pd where pd.id = ". $this->beanAyuda->get_pers_disc_visual_id();

        $this->conn2->execute($sql2);
        $this->result = $this->conn2->all_row();
        $this->numRow = $this->conn2->num_row();
        $this->conn2->close();
        
        if($tipo_report == 2){
        
        if($this->numRow > 0){
            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Roxenis Sanoja');
            $pdf->SetTitle('Comprobante de Ayudas');

            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            //set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            //set some language-dependent strings
            $pdf->setLanguageArray($l);

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('helvetica', '', 11);

            // add a page
            $pdf->AddPage();

            // create some HTML content
            $html = ' <table border="0" align="center">
                    <tr>
                        <td width="20%"><img src="../../../../../web/images/icon.png"/></td>
                        <td width="60%">
                            <font size="12px"><b><i>Sociedad Amigos de los Ciegos<br/>
                            Direcci&oacute;n de Relaciones Institucionales<br/>
                            Departamento de Trabajo Social
                            </i></b></font>
                        </td>
                        <td width="20%"></td>
                    </tr>
                </table>
                <p align="right">Caracas, '.date('d F').' de '.date('Y').'</p>
                <p align="center"><b>Solicitud de Lentes</b></p>
                <p align="left">Nombres y Apellidos: '.$this->result[0]['nombre'].' '.$this->result[0]['apellido'].'</p>
                <p align="left">CI.: '.$this->result[0]['nacionalidad'].'-'.$this->result[0]['cedula'].'</p>
                <p  align="left">Direcci&oacute;n: '.$this->result[0]['direccion'].'</p>
                <p  align="left">Tel&eacute;fono: '.$this->result[0]['telf_habitacion'].'</p>
                <p  align="left">Edad: '.$this->result[0]['edad'].' a&ntilde;os</p>
                <p  align="left">Motivo de solicitud: Ayuda para Donaci&oacute;n de Lentes</p>
                <p align="justify">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;En presupuesto de la &Oacute;ptica Caron&iacute; los Lentes tienen un costo de BsF. '.$this->beanAyuda->get_monto().'.<br/><br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Este departamento solicita de sus buenos oficios y de su calidad humana para donar los lentes requeridos, ya que los mismos permitir&aacute;n optimizar la visi&oacute;n del paciente con lo que estar&iacute;amos contribuyendo a mejorar su visi&oacute;n, y por ende elevar su calidad de vida.<br/><br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agradeciendo de antemano su valiosa colaboraci&oacute;n, se despide,
                </p>
                <br/><br/>
                <p align="center">Atentamente,</p>
                <br/><br/>
                <table width="100%" border="0" align="center">
                    <tr>
                        <td align="center">____________________</td>
                        <td align="center">____________________</td>
                    </tr>
                    <tr>
                        <td align="center">TRABAJADOR(A) SOCIAL</td>
                        <td align="center">DIRECTOR</td>
                    </tr>
                </table>
                ';
            // output the HTML content
            $pdf->writeHTML($html, true, 0, true, 0);

            // reset pointer to the last page
            $pdf->lastPage();

            // ---------------------------------------------------------

            //Close and output PDF document
            $pdf->Output('../../../../../web/ayuda.pdf', 'F');
            }
        }else{  // cierre del condicional que verifica que sea reporte para lentes
         
        $this->conn3 = new conexionDataBase(0);
        $this->conn3->conexion(0);

        $sql3="SELECT * from tbl_medicina m where m.medicina_id = ".$this->beanAyuda->get_medicina_id()." ";

        $this->conn3->execute($sql3);
        $this->result3 = $this->conn3->all_row();
        $this->numRow3 = $this->conn3->num_row();
        $this->conn3->close();
        if($this->numRow3 > 0){
            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Roxenis Sanoja');
            $pdf->SetTitle('Comprobante de Ayudas');

            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
            
            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            //set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            //set some language-dependent strings
            $pdf->setLanguageArray($l);

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('helvetica', '', 11);

            // add a page
            $pdf->AddPage();

            // create some HTML content
            $html = '
                <p align="center"><b>DONACI&Oacute;N DE MEDICINAS</b></p>
                <p align="left"><b>Para: DPTO DE ADMINISTRACI&Oacute;N</b></p>
                <p align="left">Fecha: '.date('d/m/Y').'</p>
                <p align="left">Nombres y Apellidos: '.$this->result[0]['nombre'].' '.$this->result[0]['apellido'].'</p>
                <p align="left">CI.: '.$this->result[0]['nacionalidad'].'-'.$this->result[0]['cedula'].'</p>
                <p  align="left">Medicinas: '.$this->result3[0]['medicina'].'</p>
                <p  align="left">Observaci&oacute;n: '.$this->beanAyuda->get_observacion().'</p>
                <p  align="left">Solicitado por: Trabajo Social</p>
                <p  align="left">Autorizado por: _______________________________________________</p>
                <p  align="left">Anexo: Recipe M&eacute;dico</p>
                <p  align="left">C&oacute;digo de Inpresac: '.$this->result[0]['id'].'</p>
                <p align="right"><font size="8px">Firma y sello</font></p>
                <hr>
                ';
            // output the HTML content
            $pdf->writeHTML($html, true, 0, true, 0);

            // reset pointer to the last page
            $pdf->lastPage();

            // ---------------------------------------------------------

            //Close and output PDF document
            $pdf->Output('../../../../../web/ayuda.pdf', 'F');
            }
            
        }   
        return $pdf;
    }
    
    public function executarReporteAyudaEspecial(){
        
        require_once('../../../../../lib/complementos/tcpdf/config/lang/eng.php');
        require_once('../../../../../lib/complementos/tcpdf/tcpdf.php');
        
        $this->conn3 = new conexionDataBase(0);
        $this->conn3->conexion(0);

        $sql3="SELECT * from tbl_ayuda_especial ae, tbl_tipo_ayuda ta 
            where ta.tipo_ayuda_id = ae.tipo_ayuda_id and ae.cedula = ".$this->beanAyudaEspeciales->get_cedula()."
                order by ae.id desc limit 1";

        $this->conn3->execute($sql3);
        $this->result3 = $this->conn3->all_row();
        $this->numRow3 = $this->conn3->num_row();
        $this->conn3->close();
        if($this->numRow3 > 0){
            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Roxenis Sanoja');
            $pdf->SetTitle('Comprobante de Ayudas');

            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
            
            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            //set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            //set some language-dependent strings
            $pdf->setLanguageArray($l);

            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('helvetica', '', 11);

            // add a page
            $pdf->AddPage();

            // create some HTML content
            $html = '
                <p align="center"><b>COMPROBANTE DE DONACI&Oacute;N</b></p>
                <p align="left">Fecha: '.$this->beanAyudaEspeciales->get_fecha_entrega().'</p>
                <p align="left">Nombre y Apellido: '.$this->beanAyudaEspeciales->get_nombres().' '.$this->beanAyudaEspeciales->get_apellidos().'</p>
                <p align="left">C&eacute;dula: '.$this->beanAyudaEspeciales->get_nacionalidad().'-'.$this->beanAyudaEspeciales->get_cedula().'</p>
                <p align="justify">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;He recibido del INSTITUTO DE PREVENCIÓN SOCIAL AMIGOS DE LOS CIEGOS (INPRESAC) la siguiente donaci&oacute;n: <b>'.$this->result3[0]['tipo_ayuda'].'</b><br/><br/><br/></p>
                <table width="100%" border="0" align="center">
                    <tr>
                        <td align="center">____________________</td>
                        <td align="center">____________________</td>
                    </tr>
                    <tr>
                        <td align="center">SOLICITANTE</td>
                        <td align="center">TRABAJADOR(A) SOCIAL</td>
                    </tr>
                </table>
                ';
            // output the HTML content
            $pdf->writeHTML($html, true, 0, true, 0);

            // reset pointer to the last page
            $pdf->lastPage();

            // ---------------------------------------------------------

            //Close and output PDF document
            $pdf->Output('../../../../../web/ayuda.pdf', 'F');
            }
    }

}
/** Method para validar inyeccion sql por el GET */
$valGet = new funcionesExtras();
$valGet -> validarMethodGet();
/** Validar los action del proceso que se va instanciar */
 $evento  = $_POST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_POST;
/** Instancio la class que vamos a necesitar*/
$ayuda = new ayudaAction();

if($evento=='new'){
    return $ayuda->executarNew($request);
 }elseif($evento=='edit'){
    return $ayuda->executarEdit($request);
 }elseif($evento=='index'){
    return $ayuda->executarIndex($request);
 }elseif($evento=='newEspecial'){
    return $ayuda->executarNewEspecial($request);
 }elseif($evento=='show'){
    return $ayuda->executarShow($request);
 }elseif($evento=='tipoAyuda'){
    return $ayuda->executarTipoAyuda($request);
 }
?>

