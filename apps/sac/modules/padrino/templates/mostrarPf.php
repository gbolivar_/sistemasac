<!-- Incluir los js para este modulo de Padrinos -->
<?php include_once '_script.php';?><br/>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="width: 90%">
<div id="menInfo" style="display:none"> </div>

<form  name="asociarPf" id="asociarPf" method="POST" action="#">
	<input type="hidden"  name="action" id="action" value="asociarpf" />
        <input type="hidden"  name="padrino_id" id="id"  />
        <input type="hidden"  name="pers_disc_visual_id" id="pers_disc_visual_id"  />

		<div id="infoPadrino" style="display:none">
            <fieldset class="subTitulos"> <legend><b>DATOS PADRINO</b></legend>
	        <table border="0"  align="center" class="formulario">
              <tr>
              <td align="right" class="colorN">Cedula</td>
                <td><select name="nacionalidad" id="nacionalidad" required="" disabled>
                    <option value="V">V</option>
                    <option value="E">E</option>
                  </select>
                    <input name="cedula" type="text" id="cedula" size="14" maxlength="8" required=""  disabled/>
                </td>
                <td align="right" class="colorN" >Apellidos:</td>
                <td><input type="text" name="apellido" id="apellido" size="20" maxlength="30" placeholder="Apellidos" required="" disabled/></td>
                <td align="right" class="colorN" >Nombres:</td>
                <td><input type="text" name="nombre" id="nombre" size="20" maxlength="30" placeholder="Nombres" required="" disabled/></td>
              </tr>
            </table>
            </fieldset>
		</div>

	<div id="infoPersona" style="display:none">
       <fieldset class="subTitulos"> <legend><b>DATOS DEL BENEFICIARIO</b></legend>
	        <table border="0"  align="center" class="formulario">
              <tr>
                 <td align="right" class="colorN">Carnet</td>
                <td>
                    <input name="per_ced" type="text" id="per_ced" size="4" maxlength="9" required="" disabled />
                    <input name="num_hijo" type="text" id="num_hijo" size="3" maxlength="9" required="" disabled />
                </td>
                <td align="right" class="colorN" >Apellidos:</td>
                <td><input type="text" name="per_ape" id="per_ape" size="20" maxlength="30" placeholder="Apellidos" required="" disabled/></td>
                <td align="right" class="colorN" >Nombres:</td>
                <td><input type="text" name="per_nom" id="per_nom" size="20" maxlength="30" placeholder="Nombres" required="" disabled/></td>
              </tr>
              </table>
		</fieldset>
    </div>

		 <fieldset class="subTitulos"> <legend><b>INFORMACI&Oacute;N </b></legend>
	<table border="0"  align="center" class="formulario">
        <tr>
            <td colspan="2" align="right" class="colorN"><div align="center">Padrino:</div></td><td colspan="2" align="right" class="colorN"><div align="center">Beneficiario:</div></td>
        </tr><tr>
            <td width="90" align="right" class="colorN">&gt;&nbsp;&nbsp;</td>
            <td width="206"><a href="#" id="llamar_padrino"><div id="nombre_pad"></div></a></td>
            <td width="90" align="right" class="colorN">&gt;&nbsp;&nbsp; </td>
            <td width="173"><a href="#" id="llamar_persona"><div id="nombre_per"></div></a></td>
        </tr><tr>
            <td width="90" align="right" class="colorN">(*) Fecha de entrega</td>
            <td width="206" class="colorN"><input type="text"  size="12" maxlength="12" name="fecha_inicio" id="fecha_inicio" class="datepicker" placeholder="dd-mm-yyyy" required=""/></td>
            <td width="90"  align="right" class="colorN">Ingrese la Cantidad:</td>
            <td width="173" align="right" class="colorN"><div align="left"><input type="text" name="cant_donacion_bs" id="cant_donacion_bs" size="5" />Bsf.</div></td>
        </tr>
        </table>
      </fieldset>
             <fieldset class="subTitulos" id="buttEnviar">
            	 	<input type="reset" value="Limpiar Datos" class="ui-state-default ui-corner-all" name="Limpiar">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             		<input type="submit" value="Asociar Datos" class="ui-state-default ui-corner-all"  name="Asociar Datos">
             </fieldset>

</form>