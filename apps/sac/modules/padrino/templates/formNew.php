<!-- Incluir los js para este modulo de Padrinos -->
<?php include_once '_script.php';?>
<br/>
<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="width: 90%">
<div id="menInfo" style="display:none"> </div>
<form  name="addPadrino" id="addPadrino" method="POST" action="#">
			<input type="hidden"  name="action" id="action" value="new" />
            <fieldset class="subTitulos"> <legend><b>REGISTRO PADRINO</b></legend>
	        <table border="0"  align="center" class="formulario">
              <tr>
                <td align="right" class="colorN" >(*) Apellidos:</td>
                <td><input type="text" name="apellido" id="apellido" maxlength="30" placeholder="Apellidos" required="" onkeypress="return soloText(event)" onkeyup="return mayuscula(this)"/></td>
                <td align="right" class="colorN" >(*) Nombres:</td>
                <td><input type="text" name="nombre" id="nombre" maxlength="30" placeholder="Nombres" required="" onkeypress="return soloText(event)" onkeyup="return mayuscula(this)"/></td>
              </tr>
              <tr>
                <td align="right" class="colorN">(*) C&eacute;dula</td>
                <td><select name="nacionalidad" id="nacionalidad" required="">
                    <option value="V">V</option>
                    <option value="E">E</option>
                  </select>
                    <input name="cedula" type="text" id="cedula" size="14" maxlength="8" required="" onkeypress="return numeric(event)" />
                </td>

                <td align="right" class="colorN">Tel&eacute;fono:</td>
                <td><input type="text" name="telefono" id="telefono" placeholder="04129000000" maxlength="11" onkeypress="return numeric(event)" /></td>
              </tr>

              <tr>
                <td  align="right" class="colorN">(*) Correo Electr&oacute;nico:</td>
                <td colspan="3"><input name="correo" id="correo" size="45" maxlength="150"  placeholder="anything@example.com" required="" type="email"/></td>
              </tr>
            </table>
      </fieldset>

      <fieldset class="subTitulos" id="buttEnviar">

    	 	<input type="reset" value="Limpiar Datos" name="Limpiar" class="ui-state-default ui-corner-all">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" value="Registrar Datos" name="registro" class="ui-state-default ui-corner-all" >

     </fieldset>
    </div>
</form>
 </div>