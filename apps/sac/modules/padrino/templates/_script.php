<?php $url=$_SERVER['HTTP_REFERER'].'/../../'; ?>
<script type="text/javascript" src="<?php echo $url?>web/js/comunes.js"></script>

<script type="text/javascript">
$(document).ready(function(){
/* AGREGAR PADRINO */
        $("form.#addPadrino").submit(function(e){
            /** Validar que el correo sea correcto */
            if(!validar_email($("#correo").val()))
            {
                mostrarMensaje(1,'Correo Electrónico no es valido');
                return false;
            }
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/padrino/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n padrino');
                },
                dataType: "json",
                success: function(data){
                    /** Funcion encargada de mostrar el mensaje en la vista */
                    mostrarMensaje(data.error,data.mensaje);
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario */
                    if(data.error==0){
                            $("form").each(function(){
                            this.reset();
                        });
                    }
                }
            });
            return false;
        });


/* BUSCAR PADRINO */
        $("form.#buscarPadrino").submit(function(e){
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/padrino/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n');
                },
                dataType: "json",
                success: function(data){
                	//alert(data.padrino_id);
                	$('#padrino_id').val(data.padrino_id);
                        /** Validar si es 0 = Return los datos de la consulta, 1= error no esta registrado  */
                        if(data.error==0){
                            /** Renombrar la action para la accion edit */
                            $('#contenido').load('<?php echo $url?>apps/sac/modules/padrino/templates/formEdit.php');
                            /** Funcion encargada de llenar los campos que vienen del action luego que cargue el formulario */
                            llenarCamposEditarPadrino(data);
                        }else{
                            /** Funcion encargada de mostrar el mensaje en la vista */
                            mostrarMensaje(data.error,data.mensaje);
                        }
                    }
            });
        return false;
        });

/* EDITAR PADRINO */
$("form.#padrinoEdit").submit(function(e){
     /** Validar que el correo sea correcto */
            if(!validar_email($("#correo").val()))
            {
                mostrarMensaje(1,'Correo Electrónico no es valido');
                return false;
            }
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/padrino/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n');
                },
                dataType: "json",
                success: function(data){
                    /** Funcion encargada de mostrar el mensaje en la vista */
                    mostrarMensaje(data.error,data.mensaje);
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario */
                    if(data.error==0){
                        $("form").each(function(){
                            this.reset();
                        });
                        setTimeout(function() {
                            $('#contenido').load('../apps/sac/modules/padrino/templates/searchPadrino.php');
                        }, 800 );
                    }
                }
            });
            return false;
        });

/* BUSCAR PADRINO Y PERSONA */
   $("form.#searchPadPac").submit(function(e){
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/padrino/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n');
                },
                dataType: "json",
                success: function(data){

                	$('#padrino_id').val(data.padrino_id);
                	$('#pers_disc_visual_id').val(data.pers_disc_visual_id);

                        /** Validar si es 0 = Return los datos de la consulta, 1= error no esta registrado  */

                        if((data.error1==1) && (data.error2==1)){
                                    mostrarMensaje(1,'No existe ninguno de los registros');
                        }else{
	                        if(data.error1==1){
	                        	mostrarMensaje(data.error1,data.mensaje1);
	                        }else if(data.error2==1){
                                        mostrarMensaje(data.error2,data.mensaje2);

                                }
                        }
	                    if((data.error1==0) && (data.error2==0)){
                                    $('#contenido').load('<?php echo $url?>apps/sac/modules/padrino/templates/mostrarPf.php');
	                            /** Funcion encargada de llenar los campos que vienen del action luego que cargue el formulario */
	                            llenarCamposEditarPadrino(data);
	                            llenarCamposEditarPersonas(data);
	                            setTimeout(function() {
					        		$('#nombre_per').html(data.per_nom + ' ' + data.per_ape);
					        		$('#nombre_pad').html(data.nombre + ' ' + data.apellido);
					           }, 500 );
	                     }

	if((data.error1==0) && (data.error3==0)){
		$('#contenido').load('<?php echo $url?>apps/sac/modules/padrino/templates/mostrarPp.php');
	    /** Funcion encargada de llenar los campos que vienen del action luego que cargue el formulario */
	    llenarCamposEditarPadrino(data);
	    llenarCamposEditarPersonas(data);
	    setTimeout(function() {
			$('#nombre_per').html(data.per_nom + ' ' + data.per_ape);
			$('#nombre_pad').html(data.nombre + ' ' + data.apellido);
	       }, 500 );
    	            }
         }
            });
        return false;
   });
});

/* BUSCAR PADRINO Y PERSONA PARA DESASOCIAR */
$("form.#searchPadPacDes").submit(function(e){
        $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/padrino/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n');
                },
                dataType: "json",
                success: function(data){

                	$('#padrino_id').val(data.padrino_id);
                	$('#pers_disc_visual_id').val(data.pers_disc_visual_id);

                        /** Validar si es 0 = Return los datos de la consulta, 1= error no esta registrado  */

	if(data.error1==0){
    	$('#contenido').load('<?php echo $url?>apps/sac/modules/padrino/templates/mostrarPfDes.php');
		llenarCamposEditarPadrino(data);
	    llenarCamposEditarPersonas(data);
	    setTimeout(function() {
			$('#nombre_per').html(data.per_nom + ' ' + data.per_ape);
			$('#nombre_pad').html(data.nombre + ' ' + data.apellido);
	   }, 500 );
	}else{
	    mostrarMensaje(data.error1,data.mensaje1);
	}

	if(data.error2==0){
	                        	$('#contenido').load('<?php echo $url?>apps/sac/modules/padrino/templates/mostrarPpDes.php');
                                        llenarCamposEditarPadrino(data);
                                        llenarCamposEditarPersonas(data);
	                            setTimeout(function() {
                                                        $('#nombre_per').html(data.per_nom + ' ' + data.per_ape);
                                                        $('#nombre_pad').html(data.nombre + ' ' + data.apellido);
					           }, 500 );
	                        }else{
                                    if(data.mensaje1!=null){
                                        mostrarMensaje(data.error1,data.mensaje1);
                                    }
	                        }

	            }
            });
        return false;
   });

/* ASOCIAR PADRINO PERSONA */
$("form.#asociarPp").submit(function(e){
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/padrino/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n');
                },
                dataType: "json",
                success: function(data){

                    if(data.error==0){
                    	mostrarMensaje(data.error,data.mensaje);
                    	setTimeout(function() {
                            $('#contenido').html('');
                            $('#contenido').load('<?php echo $url?>/apps/sac/modules/padrino/templates/searchPadrinoPersona.php');
                        }, 2000 );
                    }else{
                            mostrarMensaje(data.error,data.mensaje);
                        }
                    }
    });
    return false;
});

/* ASOCIAR PADRINO Y FAMILIAR */
$("form.#asociarPf").submit(function(e){
    $.ajax({
        type: "POST",
        url:'<?php echo $url?>apps/sac/modules/padrino/actions/actions.class.php' ,
        data: $(this).serialize(),
        beforeSend: function(Obj){
        },
        error:function(Obj,err,obj){
            alert('Error de Conexi?n');
        },
        dataType: "json",
        success: function(data){

        if(data.error==0){
        	mostrarMensaje(data.error,data.mensaje);
                    	setTimeout(function() {
                            $('#contenido').html('');
                            $('#contenido').load('<?php echo $url?>/apps/sac/modules/padrino/templates/searchPadrinoPersona.php');
                        }, 2000 );
        }else{
            mostrarMensaje(data.error,data.mensaje);
                }

        }
            });
            return false;
        });


/* DESASOCIAR PADRINO PERSONA */
        $("form.#desasociarPp").submit(function(e){
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/padrino/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    alert('Error de Conexi?n');
                },
                dataType: "json",
                success: function(data){

                    if(data.error==0){
                    	mostrarMensaje(data.error,data.mensaje);
                        setTimeout(function() {
                            $('#contenido').html('');
                            $('#contenido').load('<?php echo $url?>/apps/sac/modules/padrino/templates/searchPadrinoPersonaDes.php');
                        }, 2000 );
                    }else{
                        mostrarMensaje(data.error,data.mensaje);
                    }

                }
            });
            return false;
        });

/* DESASOCIAR PADRINO Y FAMILIAR */
$("form.#desasociarPf").submit(function(e){
    $.ajax({
        type: "POST",
        url:'<?php echo $url?>apps/sac/modules/padrino/actions/actions.class.php' ,
        data: $(this).serialize(),
        beforeSend: function(Obj){
        },
        error:function(Obj,err,obj){
            alert('Error de Conexi?n');
        },
        dataType: "json",
        success: function(data){

            if(data.error==0){
            	mostrarMensaje(data.error,data.mensaje);
            	$("form").each(function(){
                    this.reset();
                });
            }else{
                mostrarMensaje(data.error,data.mensaje);
            }

        }
    });
    return false;
});

 function llenarCamposEditarPadrino(data){
    /** Recorrer el arreglo json con el fin de saber el nombre y el valor para asignar dinamicamente a cada campo */
    $.each(data, function(key, value) {
        //alert(key + '->' + value);
        setTimeout(function() {
        	var campo = document.getElementById(key);
        	$(campo).val(value);

           }, 500 );
    });
}

function llenarCamposEditarPersonas(data){
    /** Recorrer el arreglo json con el fin de saber el nombre y el valor para asignar dinamicamente a cada campo */
    $.each(data, function(key, value) {
        //alert(key + '->' + value);
        setTimeout(function() {
        	var campo = document.getElementById(key);
        	$(campo).val(value);
           }, 500 );

    });
}

$(document).ready(function(){
	$("#llamar_padrino").click(function(){
		$("#infoPadrino").show(900);
		$("#infoPersona").hide(900);
		setTimeout(function() {
			$( "#infoPadrino:visible" ).removeAttr( "style" ).fadeOut();
		}, 6000 );
	});

	$("#llamar_persona").click(function(){
		$("#infoPersona").show(900);
		$("#infoPadrino").hide(900);
		setTimeout(function() {
			$( "#infoPersona:visible" ).removeAttr( "style" ).fadeOut();
		}, 6000 );
	});
});
</script>