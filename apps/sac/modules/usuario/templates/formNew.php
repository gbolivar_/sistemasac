<!-- Incluir los js para este modulo de Usuario -->
<?php include_once '_script.php';?>
<br/>
<script type="text/javascript" src="js/contrasenia.js" type="text/javascript"></script>

<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="width: 90%">
<div id="menInfo" style="display:none"> </div>
<form  name="usuarioNew" id="usuarioNew" method="POST" action="#">
            <input type="hidden"  name="action" id="action" value="new" />
            <fieldset class="subTitulos"> <legend><b>REGISTRO DE USUARIO</b></legend>
	        <table border="0"  align="center" class="formulario">
              <tr>
                <td align="right" class="colorN" >(*) Apellidos:</td>
                <td><input type="text" name="apellido" id="apellido" maxlength="30" placeholder="Apellidos" required=""/></td>
                <td align="right" class="colorN" >(*) Nombres:</td>
                <td><input type="text" name="nombre" id="nombre" maxlength="30" placeholder="Nombres" required=""/></td>
              </tr>
              <tr>
                <td align="right" class="colorN">(*) C&eacute;dula</td>
                <td>
                    <select name="nacionalidad" id="nacionalidad" required="">
                        <option value="V">V</option>
                        <option value="E">E</option>
                    </select>
                    <input name="cedula" type="text" id="cedula_usuario" size="14" maxlength="8" required="" onkeypress="return numeric(event)" />
                </td>
                <td align="right" class="colorN">(*)Sexo:</td>
                <td>
                    <select name="sexo" id="sexo" required="">
                        <option value="F">F</option>
                        <option value="M">M</option>
                    </select>
                </td>
              </tr> <tr>
                <td align="right" class="colorN">(*) Fecha Nacimiento:</td>
                <td><input type="text"  size="12" maxlength="12" name="fech_nacimiento" id="fech_nacimiento" class="datepicker" placeholder="dd-mm-yyyy" required=""/></td>
             
                </td>
                <td align="right" class="colorN">(*) Tel&eacute;fono:</td>
                <td><input type="text" name="telefono" id="telefono" placeholder="04129000000" maxlength="11"/></td>
              </tr>

              <tr>
                <td  align="right" class="colorN">(*) Correo Electr&oacute;nico:</td>
                <td ><input name=" correo_elec" id=" correo_elec" size="40" maxlength="150"  placeholder="anything@example.com" required="" type="email"/></td>
                <td align="right" class="colorN">(*)Tipo de usuario:</td>
                <td>
                    <span class="colorN">
	                  <select name="tipo_usuario_id" id="tipo_usuario_id" required="">
	                    <option value="" selected="selected">Selecci&oacute;n</option>
	                  </select>
                    </span>
                </td>
              </tr>
              <tr>
                <td  align="right" class="colorN">(*) Login:</td>
                <td ><input name="login" id="login" size="30" maxlength="50"  placeholder="Ingrese nombre de usuario" required="" type="text"/></td>
                <td align="right" class="colorN" for="pswd">(*) Clave:</td>
                <td>
                    <input name="clave" id="clave" size="30" maxlength="50"  placeholder="Ingrese clave" required="" type="password"/>
                </td>
              </tr>
            </table>
      </fieldset>

      <fieldset class="subTitulos" id="buttEnviar">

    	 	<input type="reset" value="Limpiar Datos" name="Limpiar" class="ui-state-default ui-corner-all">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" value="Registrar Datos" name="registro" class="ui-state-default ui-corner-all" >

     </fieldset>
    </div>
</form>
    <div id="pswd_info">
        <h4>Las contrase&ntilde;as deben cumplir los siguientes requisitos::</h4>
        <ul>
                <li id="letter" class="invalid"> Posee al <strong>menos una letra</strong></li>
                <li id="capital" class="invalid"> Posee al <strong>menos una letra may&uacute;scula</strong></li>
                <li id="number" class="invalid"> Posee al <strong>menos un n&uacute;mero</strong></li>
                <li id="length" class="invalid">Tiene al <strong>menos ocho caracteres de longitud</strong></li>
        </ul>
    </div>
 </div>