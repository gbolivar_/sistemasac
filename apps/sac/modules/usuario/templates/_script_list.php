<?php
session_start();
 $usuarioId=@$_SESSION['userAut'][5];
?>
<?php include_once '../../../../../lib/complementos/funcionesExtras.php'; ?>
<?php $url=$_SERVER['HTTP_REFERER'].'/../../'; ?>
<script type="text/javascript" src="<?php echo $url?>web/js/comunes.js"></script>
<script type="text/javascript">

$(document).ready(function(){
//    $('#contenido').append('#overlay');
//    $('#contenido#overlay').show();
      tablaDinamica();
});
function tablaDinamica(){
      /** Funcion encargada de procesar la informacion para mostrar el listado del los usuarios */
 
        $.post("<?php echo $url?>apps/sac/modules/usuario/actions/actions.class.php", {action: 'index'}, function(dataJson){
            $("#listado").html(dataJson.mensaje);
            /** Listado de tabla dinamica */
                var tablaDinamic = '';
                var dataJson = dataJson;
                tablaDinamic += '<br/><table align="center" border="0" width="98%"><tr align="center" class="colorN"><td>Nombre.</td><td>Apellido</td><td>C&eacute;dula</td><td>Tip. Usu.</td><td>Telf.</td></td><td>Correo</td><td width="100px">Acci&oacute;n</td></tr>';
                    $.each(dataJson.listados, function(index, value) {
                         tablaDinamic += '<tr id="del_'+ index +'">';
                         tablaDinamic += '<td align="center">' + value.nombre + '</td>';
                         tablaDinamic += '<td>' + value.apellido + '</td>';
                         tablaDinamic += '<td>' + value.nacionalidad+'-'+value.cedula +'</td>';
                         tablaDinamic += '<td>' + value.descripcion + '</td>';
                         tablaDinamic += '<td>' + value.telefono + '</td>';   
                         tablaDinamic += '<td>' + value.correo_elec + '</td>';   
                         tablaDinamic += '<td><div class="ui-state-default ui-corner-all">';
                         /** Solo cuando es administrador */
                         <?php if($usuarioId==1){ ?>
                            tablaDinamic += '<a href="#" title="Bloquear/Desbloquear"  onclick="cambiarEstatus(' + value.id + ',\''+ value.estatus +'\');">' + valBoolean(value.estatus) + '</a>';
                            tablaDinamic += '<a href="#" title="Gestionar los permisos"  onclick="permisos(' + value.id + ');"><span class="ui-icon ui-icon-key" style="margin-left:45%;"></span></a>';   
                         <?php } ?>
                         tablaDinamic += '<a href="#" title="Actualizar Registro"  onclick="editarRegistro(' + value.id + ');"><span class=" ui-icon ui-icon-pencil" style="float:right; margin-top: -17px; "></span></a>';   
                         tablaDinamic += '</div></td></tr>';
                  });
                tablaDinamic += '</table>';
                    $("#listado").html(tablaDinamic);
                    $("#listado").show(900);
        },'json');
        //$('#overlay').hide(900);
}

 
function valBoolean(valor){
    var ico='';
    if(valor=='t'){
        ico='<span class="ui-icon ui-icon-unlocked" style="width:20px;float:left"></span>';
    }else{
        ico='<span class="ui-icon ui-icon-locked" style="width:18px; float:left"></span>';
    }
    return ico;
}

function permisos(valor){
    $.post("<?php echo $url?>apps/sac/modules/sub_rol/actions/actions.class.php", {action: 'index', proceso:valor}, function(dataJson){
        //$('#vistaPermisos').show(900);
            //$('#vistaPermisos').hide(900);vistaPermisosDeralles
             var tablaDinamic = '';
                var dataJson = dataJson;
                tablaDinamic += '<br/><form id="procesarPermisos" name="procesarPermisos" method="POST">';
                tablaDinamic += '';
                tablaDinamic += '<input type="hidden"  name="action" id="action" value="procesar_permisos"/>';
                tablaDinamic += '<table align="center" border="0" width="98%"><tr align="center" class="colorN"><td>Modulo.</td><td>Permiso</td><td width="70px">Acci&oacute;n &nbsp;<input id="checkboxAll" type="checkbox" onclick="checkAll();" title="Todos"/></td></tr>';
                    $.each(dataJson.listados, function(index, value) {
                         tablaDinamic += '<tr id="del_'+ index +'" >';
                         tablaDinamic += '<td> Modulo: ' + value.roles + '</td>';
                         tablaDinamic += '<td> Permiso a: ' + value.detalles + '</td>';
        
                         tablaDinamic += '<td><div class="ui-state-default ui-corner-all">';
                         tablaDinamic += ''+ valCheckbox(value.tiene,index) +'';
                         tablaDinamic += '<input type="hidden"  name="rol_id['+index+']" id="rol_id" value="'+value.rol_id+'"/>';
                         tablaDinamic += '<input type="hidden"  name="usuario_id['+index+']" id="usuario_id" value="'+valor+'"/>'; 
                         tablaDinamic += '<input type="hidden"  name="sub_rol_id['+index+']" id="sub_rol_id" value="'+value.id+'"/>'; 
                         tablaDinamic += '</div></td></tr>';
                  });
                tablaDinamic += '</table>';
                tablaDinamic += '<center><input type="reset" value="Limpiar Datos" class="ui-state-default ui-corner-all" name="Limpiar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                tablaDinamic += '<input type="submit" value="Procesar Permisos" class="ui-state-default ui-corner-all"  name="procesar_datos"></center></form>';
                
                $("#vistaPermisos").html(tablaDinamic);
                
                //$('form.#procesarPermisos')[0].reset();
                 $('#vistaPermisos').dialog({
                        autoOpen: false,
			buttons: {"Ok":  function(){
                           $('#vistaPermisos').remove();
                            /** Volvemos a instanciar la class que llama al listado */
                            $('#contenido').html('');
                            $('#contenido').load('../apps/sac/modules/usuario/templates/listado.php');
                           $(this).dialog('close');
                      }},
                  modal: true,
                  title: 'Listado de permisos',
                  width: 550,
                  autoOpen: true,
		  height: 500
                });
                
             $('form.#procesarPermisos').submit(function(){
                $.ajax({
                    type: "POST",
                    url:'<?php echo $url?>apps/sac/modules/usuario_rol/actions/actions.class.php' ,
                    data: $(this).serialize(),
                    beforeSend: function(Obj){
                    },
                    error:function(Obj,err,obj){
                        exceptionLog('Error(EV01), Error en procesarPermisos', '_script_list.php','permisos','96');
                        alert('Error(EV01)');
                    },
                    dataType: "json",
                    success: function(data){
                        /** Agregamos el div para que luego que tengamos el resultamo mande el mensaje a la vista flotante */
                        $('#vistaPermisos').html('<div id="menInfo" style="display:none; font-text:16px;"> </div>');
                        /** Controlamos los mensajes */
                        mostrarMensaje(data.error,data.mensaje);
                        /** Agregamos un estilo para mejorar la apariciencia del dialog */
                        $('#vistaPermisos').attr({style:'font-size: 11px; width: 542px; min-height: 0px; height: 51.1667px;" scrolltop="0" scrollleft="0"'})
                    }
                });    
                return false; 
              });
		               
    },'json');
}

function valCheckbox(valor,index){
    /** Validar que debe permitir el poder tener acceso a [listar los usuarios y registro de personas] modulos comunes */
    if(index==17 || index==3){ 
        button='<center><input  type="checkbox" name="desasociar['+index+']"  readonly="readonly" checked onclick="javascript: return false;" /></center>';
        return button;
    }
    
    if(valor==1){
        button='<center><input class="desasociar" type="checkbox" name="desasociar['+index+']" /></center>';
    }else{
        button='<center><input class="desasociar" type="checkbox" name="desasociar['+index+']" checked/></center>';
    }
    return button;
}
/** Funcion que permite cambiarle es estatus al usuario */
function cambiarEstatus(valor, estatus){
            var mens='';
            if(estatus=='t'){
                mens = "<div id='confMens'><span class='ui-icon ui-icon-alert' style='float: left; margin: 1pt 7px 20px 0pt;'></span>¿Esta seguro de bloquear al usuario seleccionado?</div>";
            }else{
                mens = "<div id='confMens'><span class='ui-icon ui-icon-alert' style='float: left; margin: 1pt 7px 20px 0pt;'></span>¿Esta seguro de desbloquear al usuario seleccionado?</div>";
            }
            $(mens).dialog({
                height: 150,
                buttons: {
                            "Cancelar": function(){
                                $(this).dialog('destroy');
                        },
                            "Aceptar": function(){
                                cambiarEstatusProceso(valor, estatus);                              
                                $(this).dialog('destroy');
                        }},
            modal: true,
            title: '!Confirmacion'
            });
}

function cambiarEstatusProceso(valor, estatus){
    $.post("<?php echo $url?>apps/sac/modules/usuario/actions/actions.class.php", {action: 'delete', proceso:valor, estatus:estatus },function(dataJson){
        tablaDinamica();
        $('#listado').before('<div id="menInfo"></div>');
        mostrarMensaje(dataJson.error,dataJson.mensaje);
    }, 'json');

}
function editarRegistro(valor){
 $.post("<?php echo $url?>apps/sac/modules/usuario/actions/actions.class.php", {action:'show',proceso:valor}, function(dataJson){
      $('#contenido').load('<?php echo $url?>apps/sac/modules/usuario/templates/formEdit.php');
      $.each(dataJson, function(key, value) {
        setTimeout(function() {
                   
                     var campo = $('#'+key);
                     		$(campo).val(value);   
                     if(key=='tipo_usuario_id' ){ /** && (value==2 || value==3)*/
                                setTimeout(function() {
                                $('#tipo_usuario_id').hide();
                                var nac = $("#tipo_usuario_id option:selected").text();
                                $('#result').append(nac);
                        }, 500 );
                    }
                }, 1600 );
    });
 },'json');

}
/** Funcion encargada de seleccionar todos los checkbox de los perfiles de usuarios */
function checkAll()
{
   $("#checkboxAll").click(function(event){
     if($(this).is(":checked")) {
	 	$(".desasociar:checkbox:not(:checked)").attr("checked", "checked");
	 }else{
		 $(".desasociar:checkbox:checked").removeAttr("checked");
	 }
   });
}

</script>