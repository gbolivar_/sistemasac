
<?php $url=$_SERVER['HTTP_REFERER'].'/../../'; ?>
<script type="text/javascript" src="<?php echo $url?>web/js/comunes.js"></script>
<script type="text/javascript">
$(document).ready(function(){
 /** Validar la existencia de la persona si ya se encuentra registrada */
    $('#cedula_usuario').blur(function(){
        var cedula = $('#cedula_usuario').val();
       /** Validar que el campo cedula sea mayor o igual a 4 */
       if(cedula.length<=4){
             mostrarMensaje(1,'La c&eacute;dula debe ser mayor o igual a 5 digitos.');
             $('#cedula_usuario').val('');
             return false;
        }
        $.post('<?php echo $url?>apps/sac/modules/usuario/actions/actions.class.php', {action:'validarexistencia', cedula:cedula }, function(datoJson){
            if(datoJson.error==1){
                mostrarMensaje(datoJson.error,datoJson.mensaje);
                $('#cedula_usuario').val('');
            }
        },'json')
    });
    
//   tr4
//    $('# correo_elec').blur(function(){
//        var correo = $('# correo_elec').val();
//       /** Validar que el campo cedula sea mayor o igual a 4 */
//       if(cedula.length<=4){
//             mostrarMensaje(1,'Error en el correo.');
//             $('#correo_elec').val('');
//             return false;
//        }
//        $.post('<?php echo $url?>apps/sac/modules/usuario/actions/actions.class.php', {action:'validarCorreo', correo:correo }, function(datoJson){
//            if(datoJson.error==1){
//                mostrarMensaje(datoJson.error,datoJson.mensaje);
//                $('#cedula_usuario').val('');
//            }
//        },'json')
//    });
/** Funcion encargada de procesar la informacion desde la vista al action del mismo */
        $("form.#usuarioNew").submit(function(e){
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/usuario/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en registrar usuario', '_script.php','form.#usuarioNew','41');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                    /** Funcion encargada de mostrar el mensaje en la vista */
                    mostrarMensaje(data.error,data.mensaje);
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario */
                    if(data.error==0){
                        $("form").each(function(){
                            this.reset();
                        });
                    }
                }
            });
            return false;
        });
        
        /** Funcion encargada de procesar el editar usuario del sistema */
        $("form.#usuarioEdit").submit(function(e){
            $.ajax({
                type: "POST",
                url:'<?php echo $url?>apps/sac/modules/usuario/actions/actions.class.php' ,
                data: $(this).serialize(),
                beforeSend: function(Obj){
                },
                error:function(Obj,err,obj){
                    exceptionLog('Error(EV01), Error en editar usuario', '_script.php','form.#usuarioEdit','41');
                    alert('Error(EV01)');
                },
                dataType: "json",
                success: function(data){
                    /** Funcion encargada de mostrar el mensaje en la vista */
                    mostrarMensaje(data.error,data.mensaje);
                    /** Permitir controlar que si el action.class revuelve un error no reinicia el formulario */
                    if(data.error==0){
                        $("form").each(function(){
                            this.reset();
                        });
                    }
                }
            });
            return false;
        });
 });
 /** Cargar selector de tipo de usuario */
 selectores('tipo_usuario_id','<?php echo base64_encode('audiperm.tbl_tipo_usuario'); ?>','<?php echo base64_encode('id');?>','<?php echo base64_encode('descripcion'); ?>');


  </script>