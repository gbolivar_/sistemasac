<?php
  /**
    * @propiedad: PROPIETARIO DEL CODIGO
    * @Autor: Gregorio Bolivar
    * @email: elalconxvii@gmail.com
    * @Fecha de Create: 21/02/2012
    * @Fecha de Update: 26/02/2012
    * @Auditado por: Gregorio J Bolívar B
    * @Descripción: Actions encargado de procesar los methods del usuario
    * @package: actions.class
    * @version: 1.0
    */
/** Incluir los Bean necesario para procesar usuario  */
include_once '../../../../../lib/complementos/funcionesExtras.php';
include_once '../../../../../lib/base/class.BeanUsuario.php';

// Incluir las conexion de base de datos
include_once '../../../../../config/ConexionDataBase.class.php';

class usuarioAction extends BeanUsuario
{
    public function executarSetBeanUsuario($request){
        /** Cargar Objetos de usuario */
        $this->beanUsuario->set_id(@$request['id']);
        $this->beanUsuario->set_apellido($request['apellido']);
        $this->beanUsuario->set_nombre($request['nombre']);
        $this->beanUsuario->set_nacionalidad($request['nacionalidad']);
        $this->beanUsuario->set_cedula($request['cedula']);
        $this->beanUsuario->set_sexo($request['sexo']);
        $this->beanUsuario->set_fech_nacimiento($request['fech_nacimiento']);
        $this->beanUsuario->set_telefono($request['telefono']);
        $this->beanUsuario->set_correo_elec($request['correo_elec']);
        $this->beanUsuario->set_tipo_usuario_id($request['tipo_usuario_id']);        
        $this->beanUsuario->set_login($request['login']);
        $this->beanUsuario->set_clave($request['clave']);
        
        /** Cargar Objetos generales para cada campo */
        $this->beanUsuario->set_usuario_id();
        $this->beanUsuario->set_estatus();
        $this->beanUsuario->set_created_at();
        $this->beanUsuario->set_updated_at();


    }

    public function executarShow($request){
        $datoJson='';
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $this->methods_= new funcionesExtras();
        $sql="SELECT * from audiperm.tbl_usuario WHERE id=".$request['proceso'].";";
        $this->conn->execute($sql);
        $this->usuario=$this->conn->all_row();

        /** Armo el json que sera enviado la vista */
        if($this->usuario[0]!='')
        {
            foreach ($this->usuario[0] as $p=>$pd):
                if($p=='login' or $p=='clave'){
                    $datoJson[$p]=  $this->methods_->decrypt($pd);
                }else{
                    $datoJson[$p]=$pd;
                }
                
            endforeach;
        }
        echo json_encode($datoJson);
    }
    
    public function executarIndex($request){
        $datoJson='';
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        session_start();
        /** Tipo de usuario */
        $tipo_usuario_id=$_SESSION['userAut'][5];
        /** Identificador del usuario autenticado */
        $usuario_id=$_SESSION['userAut'][0];
        /** Cuando es Administrador */
        if($tipo_usuario_id==1){
             /** query encargado de mostrar todos los usuarios registrados en la tabla */
            $sql="SELECT a.id, a.nacionalidad, a.cedula, a.apellido, a.nombre, a.sexo, a.fech_nacimiento, a.tipo_usuario_id, b.descripcion, 
            a.correo_elec, a.telefono, a.login, a.estatus from audiperm.tbl_usuario a INNER JOIN audiperm.tbl_tipo_usuario b ON a.tipo_usuario_id=b.id ORDER BY a.id DESC";
        }else{
             /** query encargado de mostrar todos los usuarios registrados en la tabla */
            $sql="SELECT a.id, a.nacionalidad, a.cedula, a.apellido, a.nombre, a.sexo, a.fech_nacimiento, a.tipo_usuario_id, b.descripcion, 
            a.correo_elec, a.telefono, a.login, a.estatus from audiperm.tbl_usuario a INNER JOIN audiperm.tbl_tipo_usuario b ON a.tipo_usuario_id=b.id WHERE a.id=$usuario_id ORDER BY a.id DESC";
        }
        $this->conn->execute($sql);
        if($this->conn->num_row()>0){
            $this->listados=$this->conn->all_row();
            foreach ($this->listados as $item=>$fila):
                $datoJson['listados'][$item]=$fila;
            endforeach;

        }else{
            $datoJson['mensaje']='Error, en estraer el listado de usuario';
            $datoJson['error']='1';
        }
       $datoJson['mensaje']='LLEGO AL INDEX';
        echo json_encode($datoJson);
    }
    
    

    public function executarNew($request){
        $datoJson='';
        $this->beanUsuario = new BeanUsuario();
      
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        
        /** Method que se envcarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanUsuario($request);
//        self::executarValidarExistencia($request);
        //self::executarValidarExistenciaCorreo($request);
        self::executeValPolEdad($this->beanUsuario->get_fech_nacimiento());
        
        
        
        /** procedimiento para registro de usuario */
         $sql1="INSERT INTO audiperm.tbl_usuario(
            cedula, apellido, nombre, sexo, fech_nacimiento, tipo_usuario_id, 
            correo_elec, telefono, login, clave, estatus, nacionalidad, usuario_id, created_at)
            VALUES ('".$this->beanUsuario->get_cedula()."', '".$this->beanUsuario->get_apellido()."', '".$this->beanUsuario->get_nombre()."', '".$this->beanUsuario->get_sexo()."', '".$this->beanUsuario->get_fech_nacimiento()."', ".$this->beanUsuario->get_tipo_usuario_id().", 
            '".$this->beanUsuario->get_correo_elec()."', '".$this->beanUsuario->get_telefono()."', '".$this->beanUsuario->get_login()."', '".$this->beanUsuario->get_clave()."', '".$this->beanUsuario->get_estatus()."', '".$this->beanUsuario->get_nacionalidad()."', 
            ".$this->beanUsuario->get_usuario_id().", '".$this->beanUsuario->get_created_at()."');";
         $this->result=$this->conn->execute($sql1);
       
       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                  $datoJson['mensaje']='Registro efectuado exitosamente';
                  $datoJson['error']='0';
 
            }else{
                  $datoJson['mensaje']='Error al efectuar el registro';
                  $datoJson['error']='1';
            }

        $this->conn->close();
        echo json_encode($datoJson);
    }
    
    public function executarEdit($request){
        $datoJson='';
        $this->beanUsuario = new BeanUsuario();
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);

        /** Method que se envcarga de llenar los set de los campos recibe el request y el identificador del usuario */
        self::executarSetBeanUsuario($request);
        self::executeValPolEdad($this->beanUsuario->get_fech_nacimiento());

        /** procedimiento para registro de usuario */
         $sql="UPDATE audiperm.tbl_usuario SET cedula='".$this->beanUsuario->get_cedula()."', apellido='".$this->beanUsuario->get_apellido()."', nombre='".$this->beanUsuario->get_nombre()."', sexo='".$this->beanUsuario->get_sexo()."', fech_nacimiento='".$this->beanUsuario->get_fech_nacimiento()."', tipo_usuario_id=".$this->beanUsuario->get_tipo_usuario_id().", 
            correo_elec='".$this->beanUsuario->get_correo_elec()."', telefono='".$this->beanUsuario->get_telefono()."', clave='".$this->beanUsuario->get_clave()."', nacionalidad='".$this->beanUsuario->get_nacionalidad()."', usuario_id=".$this->beanUsuario->get_usuario_id().", updated_at='".$this->beanUsuario->get_created_at()."' WHERE id=".$this->beanUsuario->get_id().";";
         $this->result=$this->conn->execute($sql);
       
       /** Validar que el INSERT de tbl_pers_disc_visual sea efectivo */
            if($this->result){
                  $datoJson['mensaje']='Registro actualizado exitosamente';
                  $datoJson['error']='0';
 
            }else{
                  $datoJson['mensaje']='Error al actualizadar el registro';
                  $datoJson['error']='1';
            }

        $this->conn->close();
        echo json_encode($datoJson);
    }
     public function executarValidarExistencia($request){
        $datoJson='';
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $cedula=$request['cedula'];
        $sql="SELECT * FROM audiperm.tbl_usuario WHERE cedula='$cedula';";
        /** Execute el sql */
        $this->conn->execute($sql);
         /** Defuelve un array con todos los valores */
        $this->persona=$this->conn->all_row();
        
        if($this->persona>0){
            $datoJson['error']='1';
            $datoJson['mensaje']='C&eacute;dula de usuario ya se encuentra registrad@';//.print_r($this->conn);
        }else{
            $datoJson['error']='0';
        }
        echo json_encode($datoJson);        
    }
    public function executarValidarExistenciaCorreo($request){
        $datoJson='';
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $correo=$request['correo_elec'];
        $sql="SELECT * FROM audiperm.tbl_usuario WHERE correo_elec='$correo';";
        /** Execute el sql */
        $this->conn->execute($sql);
         /** Defuelve un array con todos los valores */
        $this->persona=$this->conn->all_row();
        
        if($this->persona>0){
            $datoJson['error']='1';
            $datoJson['mensaje']='Ese correo ya esta registrado y asociado a otro usuario';//.print_r($this->conn);
        }else{
            $datoJson['error']='0';
        }
        //$this->conn->close();
       echo json_encode($datoJson);
       die();
    }
    public function executeValPolEdad($fecha){
        /** Method de calcular fecha */
        $this->fech= new funcionesExtras();
        $edad=$this->fech->calculaEdad($fecha);
        if($edad<=15){
            $datoJson['mensaje']='Error, El usuario que intenta registrar debe tener m&iacute;nimo 15 a&ntilde;os';
            $datoJson['error']='1';        
            echo json_encode($datoJson);
            exit();
        }
    }

    public function executarDeleted($request){
        $datoJson='';
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $statu = $request['estatus'];
        $value = $request['proceso'];
        
        /** Cargar Objetos generales para cada campo */
        $this->beanUsuario= new BeanUsuario();
        $this->beanUsuario->set_usuario_id();
        $usuario_id = $this->beanUsuario->get_usuario_id();
        
        if($usuario_id==$value)
        {
            $datoJson['mensaje']='Error, Operación no permitida no puede hacer nada con este usuario.';
            $datoJson['error']=1;
             
        }else{   
            if($statu=='t')
            {
                $sql="UPDATE audiperm.tbl_usuario SET estatus='FALSE' WHERE id=$value";
                $this->conn->execute($sql);
                $datoJson['mensaje']='Usuario Bloqueado exitosamente';
                $datoJson['error']=0;
            }else{
                $sql="UPDATE audiperm.tbl_usuario SET estatus='TRUE' WHERE id=$value";
                $this->conn->execute($sql);
                $datoJson['mensaje']='Usuario Debloqueado exitosamente';
                $datoJson['error']=0;
            }          
        }
     $this->conn->close();
     echo json_encode($datoJson); 
    }

}
/** Method para validar inyeccion sql por el GET */
$valGet = new funcionesExtras();
$valGet -> validarMethodGet();
/** Validar los action del proceso que se va instanciar */
 $evento  = $_POST['action'];
/** Capturo los datos que vienen por cualquier method */
$request = $_POST;
/** Instancio la class que vamos a necesitar*/
$proceso = new usuarioAction();

if($evento=='new'){
    return $proceso->executarNew($request);
 }elseif($evento=='edit'){
    return $proceso->executarEdit($request);
 }elseif($evento=='index'){
    return $proceso->executarIndex($request);
 }elseif($evento=='delete'){
    return $proceso->executarDeleted($request);
 }elseif($evento=='show'){
    return $proceso->executarShow($request);
 }elseif($evento=='validarexistencia'){
    return $proceso->executarValidarExistencia($request);
 }

?>

