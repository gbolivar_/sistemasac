<html>
<head>
<title>Sistema de Gestion de Personas con Discapacidad Visual en la Sociedad Amigos de los Ciegos (SAC). </title>
<link rel="shortcut icon" href="images/icon.png" />
<link type="text/css" href="css/style2.css" rel="stylesheet">
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/procesos.js" type="text/javascript"></script>
<?php if(@$_SESSION['mensaje']){ ?>
<script type="text/javascript">

 $(function() {
    $( "#contenidoAutenticar" ).effect( 'shake', 300, callback );

    // callback function to bring a hidden box back
    function callback() {
            setTimeout(function() {
                    $( "#contenidoAutenticar" ).removeAttr( "style" ).hide().fadeIn();
            }, 10 );
    };
    
 });
</script>
<?php } ?>
</head>
<body>
    <noscript>
            <div class="ui-widget-overlay">
                <div class="redondo" style="color:  #00000; width: 28%; height: 30%; margin: 17% auto auto 35.5%; border: 1px solid #000000; text-align: justify:">
                   <h2>
                    <p >La p&aacute;gina que est&aacute;s viendo requiere para su funcionamiento el uso de JavaScript. 
                    Si lo has deshabilitado intencionadamente, por favor vuelve a activarlo.</p></h2>
                </div>
            </div>  
        </noscript>   
<!-- Contenedor general del template --->
<div id="general">
	<!-- Contenedor Cabezera --->
	<div id="cabezera">
		<div id="logo"><br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema de Gestion de Personas con Discapacidad Visual en la Sociedad Amigos de los Ciegos. </div>
		<div id="infoSession">&nbsp;&nbsp;</div>
	</div>

	<!--  Contenedor Principal donde se mostrara la informacion --->
	<div id="principal">
	<!-- Contenedor mensaje ayuda --->
        <div id="mensaje" <?php if($_SESSION['mensaje']){ echo "style='display: block'";}?>><img src='images/error.png' title='Error' width='15px' alt="Error"/>&nbsp;&nbsp;<?php echo $_SESSION['mensaje'];?></div>
		<div id="contenido">
                    <div id="contenidoAutenticar">


                    </div>
		</div>
	</div>

	<!-- Contenedor pie de pagina -->
	<div id='pie'>
		<div id="conPie">Sistema desarrollado por el <a href="#" id="dialog_link" class="ui-state-default ui-corner-all">Grupo 1</a> de Ingenieria en Informatica del CUFM</div>

		<!-- Dialogo que mostrara -->
		<div id="dialog" title="Autores">
			<b>Roxenis Sanoja<br/>
			Yarelis Quiaro<br/>
			Juan Carlos Santander<br/>
			Gregorio Bol&iacute;var<br/></b>
		</div>
	</div>
        <div id="renovacion" style="display: none">
            <fieldset class="redondo">
			<form id="renovacionForm" name="renovacionForm" action="" method="post">
                            <input id="action" type="hidden" name="action" value="renovar"/>
				<table align="center" border="0" cellspacing="1" cellpadding="1" style="font-size: 12px; height: 80%">
					<tr>
                                                <th width="50%" style="text-align: left;">C&eacute;dula:</th>
                                                <td width="50%"><input id="cedula" type="text" name="cedula" size="10" maxlength="10" required="true"/></td>
					</tr>
					<tr>
						<th style="text-align: left;">Fecha Nacimiento:</th>
						<td><input type="text" id="fecha_nac" name="fecha_nac" size='12' class='datepicker' required="true"/></td>
					</tr>
                                        <tr>
						<th style="text-align: left;">Correo:</th>
						<td><input id="correo" type="email" name="correo"  size="15" maxlength="100" required="true"/></td>
					</tr>
                                        <tr>
                                            <td colspan="2">
                                                <center><input type="submit" class="botomInicio" style="font-size: 13px;" id="renoverUser" value="Renovar" /></center>
                                                
                                            </td>
					</tr>
				</table>
                            </fieldset>
			</form>
                </fieldset>
                <div id="resulRen"></div>
            </div>
</div>
</body>
</html>