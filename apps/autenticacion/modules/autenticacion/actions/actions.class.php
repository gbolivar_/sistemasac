<?php
/**
* @propiedad: PROPIETARIO DEL CODIGO
* @Autor: Gregorio Bolivar
* @email: elalconxvii@gmail.com
* @Fecha de Creacion: 5/03/2012
* @Fecha de Actualizacion: 03/06/2012
* @Auditado por: Gregorio J Bolívar B
* @Descripción: Class para el inicio de session y desconectar
* @package: actions.class.php
* @version: 2.0
*/

class autenticarAction 
{
    public $conn;
    public $exeptionLog;

    /** COnstructur de la class */
    public function __construct() {
            $this->conn;
            $this->exeptionLog;
    }
    /** Procedimiento para autenticacion de usuario */
    public function executarAutenticar($user, $pass, $ip, $nameHost){
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        /** instanciar el method de exeption */
        $this->exeptionLog= new funcionesExtras();
        $sql="select a.id, a.cedula, a.apellido, a.nombre, a.sexo, a.fech_nacimiento, a.tipo_usuario_id, b.descripcion, 
        a.correo_elec, a.telefono, a.login, a.estatus from audiperm.tbl_usuario a INNER JOIN audiperm.tbl_tipo_usuario b ON a.tipo_usuario_id=b.id where login='$user' AND clave='$pass';";
        $this->conn->execute($sql);
        /** Verificamos que al memos alla una dupla correcta*/
        if($this->conn->num_row()>0){
            /** Extraigo los datos relacionado con el usuario */
            $usuarios=$this->conn->all_row();
            /** Cerrar Conexion */
            $this->conn->close();
            /** Valido si el usuario esta activo */
            
            if($usuarios[0]['estatus']=='f'){
                 $this->exeptionLog->executeLog('Usuario '.$this->exeptionLog->decrypt($user).' esta Desactivado, intento acceder desde el host '.$ip.'('.$nameHost.') al sistema, estatus falso',__CLASS__, __FUNCTION__, __LINE__);
                return $mensaje='Usuario se encuentra Desactivado por favor comunicarse con el administrador del sistema.';
            }else{
                
                $this->conn->conexion(0);
                /** Procedimiento para extraer los sub-menus de los roles */
                $sql="SELECT b.id, b.rol_id, b.usuario_id, b.detalles, b.title, b.div_id, b.activa_menu AS sub_menu_activo
                    FROM audiperm.tbl_usuario_rol a
                    INNER JOIN audiperm.tbl_sub_rol b on a.sub_rol_id=b.id
                    WHERE a.estatus='true' AND activa_menu='true' AND a.usuario_id=".$usuarios[0]['id']." group by b.id, b.rol_id, b.usuario_id, b.detalles, b.title, b.div_id, b.activa_menu  order by b.div_id";
                $this->conn->execute($sql);
                /** Valido que existan duplas en los registros */
                if($this->conn->num_row()>0){
                     /** Extraemos todos los registros asociados con los roles de usuario */
                     $roles = $this->conn->all_row();
                     /** Cerrar la conexion con la base de datos */
                     $this->conn->close();
                     
                     $this->conn->conexion(0);
                     /** Para extraer todos los menus principales asociados al usuario con permisologia*/
                    $sql="SELECT b.id, b.roles, b.modulo, b.activa_menu, count(*)
                        FROM audiperm.tbl_usuario_rol a
                        INNER JOIN audiperm.tbl_rol b on a.rol_id=b.id
                        WHERE a.estatus='true' AND a.usuario_id=".$usuarios[0]['id']."  
                        GROUP BY b.id, b.roles, b.modulo, b.activa_menu having count(*) >=1 order by b.id";
                    $this->conn->execute($sql);
                    $menu_princip=$this->conn->all_row();
                    /** Cerrar la conexion con la base de datos */
                     $this->conn->close();
                     /** Recorremos todos los roles de usuarios con el fin de poder cargar todos los permisos en session */
                     foreach ($roles as $item => $rol):                       
                      /** Cargamos las credenciales compuestas por las actiones que puede hacer el usuario
                         NombreRol | MenuActivo | Modulo | Credential | Funciones (Aplicamos una funcion para evaluar el arreglo y luego que elimine los que estan vacios) | Eventos */
                        $credencial[$item]=array('rol_id' => $rol['rol_id'], 'act'=> $rol['sub_menu_activo'] , 'title' => $rol['title'], 'div_id' => $rol['div_id'], 'detalles' => $rol['detalles'] );
                        endforeach;
                      /** Indicamos las sesiones relacionadas con el fin de que pueda iniciar session */
                     
                     $_SESSION['userAut']=array($usuarios[0]['id'],$usuarios[0]['cedula'],$usuarios[0]['apellido'],$usuarios[0]['nombre'],$usuarios[0]['fech_nacimiento'],$usuarios[0]['tipo_usuario_id'],$usuarios[0]['descripcion'],$usuarios[0]['correo_elec'],$usuarios[0]['telefono'],$usuarios[0]['login']);
                     $_SESSION['credenciales']=$credencial;
                     $_SESSION['menu_principal']=$menu_princip;
                     $_SESSION['inicio']=1;
                     $_SESSION['autenticado']=1;
                     $mensaje='';
//		     session_id();
                     
                }else{
                    $this->exeptionLog->executeLog('Acceso Fallido, El usuario '.$this->exeptionLog->decrypt($user).' no tienes permisos relacionados, intento iniciar session desde '.$ip.'('.$nameHost.')',__CLASS__, __FUNCTION__, __LINE__);
                    return $mensaje='Usuario se encuentra registrado pero no tienes permisos relacionados por favor comunicarse con el administrador del sistema.';
                }
               
            }
           
            /** Validar los datos de usuarios */
        }else{
            /** Verifico la cantidad de intentos del usuario */
            $valorIntentos=self::executeValidar3Intentos($user, $pass); 
             if($valorIntentos>=3){
                        self::executeBloqueoUser($user);
                        $this->exeptionLog->executeLog('Acceso bloqueado, El usuario fue bloquedo exedio la cantidad de intentos, contacte al administrador del sistema '.$ip.'('.$nameHost.') con el siguiente usuario '.$this->exeptionLog->decrypt($user).'',__CLASS__, __FUNCTION__, __LINE__);
                        $mensaje='El usuario fue bloquedo exedio la cantidad de intentos, contacte al administrador del sistema.';
                    }else{  
                        $this->exeptionLog->executeLog('Acceso Fallido, El nombre de usuario o la contrase&ntilde;a introducidos no son correctos, intento iniciar session desde '.$ip.'('.$nameHost.') con el siguiente usuario '.$this->exeptionLog->decrypt($user).'',__CLASS__, __FUNCTION__, __LINE__);
                        $mensaje='El nombre de usuario o la contrase&ntilde;a introducidos no son correctos.';
                    }
            
        }
        
        return $mensaje;

    }
    /** Procedimiento para desconectar los usuarios */
    public function executarDesconectar(){
        session_destroy();
        $_SESSION['inicio']=null;
        $_SESSION['autenticado']=null;
        unset ($_SESSION);

    }
    
    public function executarRenovacion($request)
    {
        include_once '../../../../../config/ConexionDataBase.class.php';
        include_once '../../../../../lib/complementos/funcionesExtras.php';
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        /** Datos que vienen por  */
        $cedula = $request['cedula'];
        $fecha =  $request['fecha'];
        $correo = $request['correo'];
        
        /** instanciar el method de exeption */
        $this->methods_= new funcionesExtras();
        $sql="select login, clave from audiperm.tbl_usuario  WHERE estatus='true' AND fech_nacimiento='$fecha' AND correo_elec='$correo' AND  cedula='$cedula'";
        $this->conn->execute($sql); 
        $num=$this->conn->num_row();
        $datoJson='';
        if($num>0){ 
            $this->usuario=$this->conn->all_row();
             /** Armo el json que sera enviado la vista */
            if($this->usuario[0]!='')
            {
                foreach ($this->usuario[0] as $p=>$pd):
                    if($p=='login' or $p=='clave'){
                        $datoJson[$p]=$this->methods_->decrypt($pd);
                    }else{
                        $datoJson[$p]=$pd;
                    }              
                endforeach;
                 $datoJson['mensaje']='Los datos de acceso al sistema son los siguiente: ';
                 $datoJson['error']=0;
            }
       
        }else{
            $datoJson['mensaje']='Error, no hay registros asociados, contacte al administrador del sistema';
            $datoJson['error']=1;
        }
        echo json_encode($datoJson);
    }
    /** Method encargado de controlar el intento de ingreso 3 veces */
    public function executeValidar3Intentos($user, $pass) {
        $this->conn = new conexionDataBase(0);
        $this->conn->conexion(0);
        $sql="SELECT a.id, a.cedula, a.apellido, a.nombre, a.sexo, a.fech_nacimiento, a.tipo_usuario_id, b.descripcion, 
        a.correo_elec, a.telefono, a.login, a.estatus FROM audiperm.tbl_usuario a 
        INNER JOIN audiperm.tbl_tipo_usuario b ON a.tipo_usuario_id=b.id WHERE a.estatus='true' AND login='$user';";
        $this->conn->execute($sql);

        /** Verificamos que al memos alla una dupla correcta*/
        if($this->conn->num_row()>0){

            /** Procedimiento cuando es el segundo intento */
            if(@$_SESSION['login']==$user){
                    if($_SESSION['intentos']==''){
                        $v=1;
                        $_SESSION['intentos']= $v;                     
                    }else{
                        $v=$_SESSION['intentos']+1;

                        $_SESSION['intentos']= $v;
                    }
            }else{
                    /** destruyo las variable de session debido que son nuevos intentos de usuarios diferente al anterior */
                    unset($_SESSION['login']);
                    unset($_SESSION['intentos']);
                    $_SESSION['login']=$user;
                    if(@$_SESSION['intentos']==''){
                        $v=1;
                        $_SESSION['intentos']= $v;                     
                    }else{
                        $v=$_SESSION['intentos']+1;

                        $_SESSION['intentos']= $v;
                    }  
            }
            return $_SESSION['intentos'];
        }
    }
    
      /** Method para verificar si efectuo tres intentos diferentes clave */
  public function executeBloqueoUser($user) {
      
       $this->conn = new conexionDataBase(0);
       $this->conn->conexion(0);
       $sql="UPDATE audiperm.tbl_usuario SET estatus='FALSE' WHERE estatus='true' AND login='$user';";           
       $this->conn->execute($sql);
  }

    /** Destructor de la class */
    public function __destruct() {
        $this->conn;
    }
}
if(count($_POST)>3){
    $renovar=new autenticarAction();
    $renovar->executarRenovacion($request=$_POST);
}
?>
